# The ouput of all these installation steps is noisy. With this utility
# the progress report is nice and concise.
function install {
    echo installing $1
    shift
    apt-get -y install "$@" >/dev/null 2>&1
}

#echo updating package information
#apt-get -y update >/dev/null 2>&1

install Git git
install PostgreSQL postgresql postgresql-contrib libpq-dev

echo creating gustavo_brigido database
sudo -u postgres createuser --superuser gustavo_brigido
sudo -u postgres psql -c "ALTER USER gustavo_brigido WITH PASSWORD 'gustavo_brigido';"
sudo -u postgres createdb -O gustavo_brigido gustavo_brigido_development
sudo -u postgres createdb -O gustavo_brigido gustavo_brigido_test

echo creating plantao_enem database
sudo -u postgres createuser --superuser plantao_enem
sudo -u postgres psql -c "ALTER USER plantao_enem WITH PASSWORD 'plantao_enem';"
sudo -u postgres createdb -O plantao_enem plantao_enem_development
sudo -u postgres createdb -O plantao_enem plantao_enem_test

echo creating marta_garcia database
sudo -u postgres createuser --superuser marta_garcia
sudo -u postgres psql -c "ALTER USER marta_garcia WITH PASSWORD 'marta_garcia';"
sudo -u postgres createdb -O marta_garcia marta_garcia_development
sudo -u postgres createdb -O marta_garcia marta_garcia_test

install 'Nokogiri dependencies' libxml2 libxml2-dev libxslt1-dev


if [[ ! -s "/usr/local/rvm/bin/rvm" ]] ; then
	echo installing RVM

	gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3 >/dev/null 2>&1
	\curl -sSL https://get.rvm.io | bash -s stable
	source /etc/profile.d/rvm.sh

	echo installing required dependencies
	rvm requirements

	echo installing ruby 1.8.7
	rvm install ruby-1.8.7-head
	rvm gemset create gustavo-brigido
	rvm --default use 1.8.7@gustavo-brigido

	echo downgrading to rubygems 1.6.2 to support ruby 1.8.7
	gem update --system 1.6.2

	echo gem: --no-rdoc --no-ri >> /home/vagrant/.gemrc	
fi
	
#gem install bundler -v=1.3.5
cd /vagrant

echo installing project gems
bundle install

echo running migrations
SITE_THEME=gustavo_brigido bundle exec rake db:migrate
SITE_THEME=plantao_enem bundle exec rake db:migrate
SITE_THEME=marta_garcia bundle exec rake db:migrate

# Needed for docs generation.
update-locale LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8 LC_ALL=en_US.UTF-8
#sudo chown vagrant:vagrant /usr/local/rvm/ -R
echo 'all set, rock on!'