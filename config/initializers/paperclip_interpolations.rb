Paperclip.interpolates :normalized_filename do |attachment, style|
  FilenameNormalizer.normalize(attachment.original_filename)
end
