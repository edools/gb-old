# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_gustavo_brigido_session',
  :secret      => 'f6d69145ef5b5fa03816c80aa3a56061435783ee64671d544f9515335dac43c50ac2635ffe4c849473fbfa8cec445c45539796f69ab1b0a292a3f665c4f19fe6'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
ActionController::Base.session_store = :active_record_store
ActiveRecord::SessionStore::Session.recent_activity_limit = 4.hours

ActionController::Dispatcher.middleware.insert_before(ActionController::Base.session_store, FlashSessionCookieMiddleware, ActionController::Base.session_options[:key])
