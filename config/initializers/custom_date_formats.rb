#############################################
##########   Custom date formats   ##########
#############################################

Time::DATE_FORMATS[:formato_mensal]   = "%m/%Y"
Time::DATE_FORMATS[:mes_abreviado]    = "%b"
Time::DATE_FORMATS[:dia]              = "%d"
Time::DATE_FORMATS[:padrao_numerico]  = "%d/%m/%Y"
Time::DATE_FORMATS[:padrao_data]      = "%d/%B/%Y"
Time::DATE_FORMATS[:padrao_hora]      = "%H:%M"
Time::DATE_FORMATS[:padrao_data_hora] = "%d/%B/%Y às %H:%M"
Time::DATE_FORMATS[:padrao_data_hora_datetimepicker] = "%d/%m/%Y %H:%M"
Time::DATE_FORMATS[:week_day]         = "%A"
Date::DATE_FORMATS[:padrao_data]      = "%d/%B/%Y"
Date::DATE_FORMATS[:padrao_input_data]      = "%d/%m/%Y"
Date::DATE_FORMATS[:data_liturgia]    = "%A, %d de %B de %Y"