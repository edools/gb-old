# ############################################################ ##########
# Content types using "file" command   ##########
# ############################################################
def custom_content_type
  unless path.blank?
    mime_type = IO.popen("file --brief --mime #{path}").gets
    mime_type.split("\;").first
  end
end

class Tempfile
  def content_type
    puts "[DEBUG] tempfile content_type\n"
    custom_content_type
  end
end

module Paperclip
  module Upfile
    def content_type
      puts "[DEBUG] upfile content_type\n"
      custom_content_type
    end
  end

  class Attachment
    def content_type
      puts "[DEBUG] attachment content_type\n"
      custom_content_type
    end
  end
end
# ############################################################
# ####################  END CONTENT TYPE  ####################
# ############################################################
