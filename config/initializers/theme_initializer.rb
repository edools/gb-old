current_theme = ENV['SITE_THEME']
theme_path = File.join(Rails.root, "app", "themes", current_theme) if current_theme

if current_theme && File.exists?(theme_path)
  views_path = File.join(theme_path, "views")
  if File.exists? views_path
    ActionController::Base.prepend_view_path views_path
    ActionMailer::Base.prepend_view_path views_path
  end

  cells_path = File.join(theme_path, "cells")
  if File.exists? cells_path
    Cell::Base.view_paths << cells_path
    dep = ::ActiveSupport::Dependencies
    if dep.respond_to?(:autoload_paths)
      dep.autoload_paths << cells_path
    else
      dep.load_paths << cells_path
    end
  end

  presenters_path = File.join(Rails.root, "app", "themes", current_theme, "presenters")
  if File.exists?(presenters_path)
    Rails.configuration.autoload_paths += [presenters_path]
  end
end