key =  File.join(Rails.root,'config','cloudfront.pem')
if File.exists?(key)
  AWS::CF::Signer.configure do |config|
    config.key_path = key
    config.key_pair_id = Settings.cloudfront.key_pair_id
  end
end