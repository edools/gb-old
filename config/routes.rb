ActionController::Routing::Routes.draw do |map|

  #map.connect 'admin/videos/download/:id', :controller => 'admin/video_uploads', :action => 'download', :conditions => { :method => :get }
  map.active '/active', :controller => 'sessions', :action => 'active'
  map.timeout '/timeout', :controller => 'sessions', :action => 'timeout'

  map.with_options :controller => 'carrinho' do |carrinho|
    carrinho.index 'carrinho', :action => 'index', :conditions => {:method => :get}
    carrinho.delete 'carrinho/delete', :action => 'delete'
    carrinho.aplicar_cupom 'carrinho/atualizar_cupom', :action => 'atualizar_cupom', :conditions => { :method => :post }
    carrinho.update_quantity_cart 'carrinho/alterar_quantidade/:id', :action => 'update_quantity', :conditions => { :method => :post }
  end

  map.with_options :controller => 'pedido' do |pedido|
    pedido.show 'pedido/show', :action => 'show'
    pedido.checkout 'pedido/checkout', :action => 'checkout'
    pedido.confirmacao 'pedido/confirmacao', :action => 'confirmacao'
  end

  map.resources :passwords, :only => [:new, :create]
  map.resources :contacts, :as => 'contact'
  map.resources :users, :as => 'usuarios', :collection => { :update_cidade => :post }
  map.resources :parceiros, :only => [:index]
  map.resources :noticias, :collection => { :rss => :get }
  map.resources :artigos, :collection => { :rss => :get }
  map.resources :professors, :as => "professores"
  map.resources :newsletter_subscribes

  map.resources :video_aulas, :member => { :add_to_cart => :get, :assistir => :get, :download_material_apoio => :get }, :collection => { :rss => :get, :sort => :get, :sort_plantao_enem => :get, :paginate => :get, :paginate_plantao_enem => :get }, :as => 'cursos-on-line'
  map.resources :pacotes, :controller => "video_aulas", :only => [:show]
  map.resources :simulated_tests, :as => 'simulados', :only => [:show, :index], :member => { :add_to_cart => :get, :acessar => :get }
  map.answer_simulated_test '/simulados/:id/responder', :controller => 'simulated_tests', :action => 'responder', :method => :post
  map.restart_simulated_test '/simulados/:id/refazer', :controller => 'simulated_tests', :action => 'refazer', :method => :post
  map.resources :cursos, :collection => { :rss => :get }, :as => 'cursos-presenciais'
  map.resources :video_uploads, :member => { :download => :get, :download_file => :get }, :as => 'videos'
  map.resources :assinaturas, :member => { :add_to_cart => :get }
  map.resources :arquivos, :member => { :add_to_cart => :get, :acessar => :get, :download => :get, :download_demonstracao => :get}, :collection => {:verify_watermark_job => :get}, :as => 'ebooks'
  map.resources :classroom_events, :member => { :add_to_cart => :get }, :as => 'eventos'
  map.resources :tickets, :member => { :add_to_cart => :get, :assistir => :get, :download_material_apoio => :get }, :as => 'aulas-ao-vivo'
  map.resources :live_channel, :member => {:download_supporting_material => :get}, :as => 'canal-gb'
  map.redirect    '/banners/redirect', :controller => 'banners',   :action => 'redirect'
  map.meus_cursos '/meus-cursos', :controller => "video_aulas", :action => "meus_cursos"
  map.meus_arquivos '/meus-ebooks', :controller => "arquivos", :action => "meus_arquivos"
  map.meus_tickets '/minhas-aulas-ao-vivo', :controller => 'tickets', :action => 'meus_tickets'
  map.my_classroom_tickets '/meus-ingressos', :controller => 'classroom_events', :action => 'my_tickets'
  map.my_simulated_tests '/meus-simulados', :controller => 'simulated_tests', :action => 'my_simulated_tests'
  map.sobre      '/sobre',      :controller => "home", :action => "sobre"
  map.faq      '/faq',      :controller => "home", :action => "faq"
  map.termos      '/termos',      :controller => "home", :action => "termos"
  map.historico_produtos '/historico-de-compras', :controller => 'users', :action => 'historico'

  map.browsers   '/browsers',  :controller => 'home',      :action => 'browsers'
  map.home       '/',          :controller => 'home',      :action => 'home'

  # rotas para cadastro e acesso de usuarios
  map.login           '/login',           :controller => 'sessions',  :action => 'new'
  # mudar senha backend
  map.change_password '/change-password', :controller => 'admin/passwords', :action => 'update'
  # mudar senha frontend
  map.alterar_senha   '/alterar-senha',   :controller => 'passwords', :action => 'update'
  map.resource        :session

  # rotas comuns ao login do frontend e backend
  map.reset_password  '/reset-password/:reset_password_token', :controller => 'passwords', :action => 'create'
  map.forgot_password '/forgot-password', :controller => 'passwords', :action => 'new'
  map.logout          '/logout',          :controller => 'sessions',  :action => 'destroy'
  map.resources       :passwords, :only => [:new, :create, :update]

  map.resources       :support, :controller => 'contacts',  :action => 'support'
  map.admin           '/admin',     :controller => 'admin/dashboards',  :action => 'show'

  map.namespace(:admin) do |admin|
    admin.resources :cupoms, :as => 'cupons', :collection => { :generate_code => :get }
    admin.resources :video_uploads, :member => { :download => :get }, :collection => { :ajax_search => :get, :ajax_search_simulados => :get }, :as => 'videos'
    admin.resources :faturas, :collection => {:resumo => :get, :summary_change_product_type => :get, :exportar_emails => :get}
    admin.resources :products, :collection => { :ajax_search_watchable => :get, :ajax_search_without_signatures => :get, :download_attachment => :get, :update_attachment => :put}, :as => 'produtos'
    admin.resources :video_aulas, :collection => { :ajax_search => :get }, :as => 'cursos-on-line'
    admin.resources :video_demos, :new => {:upload => :post}, :collection => {:authorize => :get}
    admin.resources :professors, :collection => { :ajax_search => :get }, :as => "professores"
    admin.resources :parceiros
    admin.resources :artigos, :collection => {:download_attachment => :get, :update_attachment => :put}
    admin.resources :noticias
    admin.resources :tickers, :as => "destaques"
    admin.resources :depoimentos
    admin.resources :banners
    admin.resources :cursos, :collection => {:download_attachment => :get, :update_attachment => :put}, :as => 'cursos-presenciais'
    admin.resources :pacotes
    admin.resources :categoria_cursos, :as => "categorias", :collection => { :index_ajax => :get, :destaques => [:get,:post] }
    admin.resources :users, :collection => { :exportar_emails => :get, :update_cidade => :post }
    admin.resources :passwords, :only => [:update]
    admin.resource  :dashboard, :only => [:show]
    admin.resources :contacts,  :only => [:index, :create]
    admin.resources :assinaturas
    admin.resources :descontos
    admin.resources :arquivos, :as => 'ebooks'
    admin.resources :tickets, :as => 'aulas-ao-vivo'
    admin.resources :classroom_events, :as => 'eventos', :member => {:tickets => :get, :write_off => :post}
    admin.resource :termos, :only => [:edit, :update]
    admin.resources :reports, :collection => { :financial => :get }
    admin.resource :sobre, :controller => 'sobre', :only => [:edit, :update]
		admin.resources :simulated_tests, :as => 'simulados', :member => [:activity_report]
    admin.resource :live_channel, :controller => 'live_channel', :only => [:edit, :update], :collection => {:download_attachment => :get, :update_attachment => :put}, :as => "canal-ao-vivo"
    admin.resources :manage_views, :as => 'gerenciar-visualizacoes', :only => [:index, :edit], :collection => {:view_counts => :put}
    admin.resources :tinymce_assets, :only => [:create]
  end


  map.root :home

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller

  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
