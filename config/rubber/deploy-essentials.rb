
namespace :rubber do
  namespace :essentials do
    rubber.allow_optional_tasks(self)

    namespace :postgresql do
      before "deploy:migrate", "rubber:essentials:postgresql:add_pg_superuser"
      after  "deploy:migrate", "rubber:essentials:postgresql:remove_pg_superuser"

      # We might want to set the db user as superuser to allow the migrations
      # to execute some code that requires certain superuser privileges, like the migration:
      # see 20131007232926_add_unaccent_extension
      task :add_pg_superuser, :roles => [:postgresql_master, :postgresql_slave] do
        alter_user_cmd = "ALTER USER #{rubber_env.db_user} SUPERUSER"
        rubber.sudo_script "add_superuser", <<-ENDSCRIPT
          sudo -i -u postgres psql -c "#{alter_user_cmd}"
        ENDSCRIPT
      end

      task :remove_pg_superuser, :roles => [:postgresql_master, :postgresql_slave] do
        alter_user_cmd = "ALTER USER #{rubber_env.db_user} NOSUPERUSER"
        rubber.sudo_script "add_superuser", <<-ENDSCRIPT
          sudo -i -u postgres psql -c "#{alter_user_cmd}"
        ENDSCRIPT
      end
    end

    before "rubber:config", "rubber:essentials:create_secret_folder"
    after "deploy:update_code", "rubber:essentials:create_secret_folder"

    task :create_secret_folder do
      run <<-EOF
        cd #{release_path} && mkdir -p secrets/#{Rubber.env}/rubber
      EOF
    end

    after "deploy:update_code", "rubber:essentials:copy_essentials"
    task :copy_essentials do
      ["newrelic", "pagseguro", "settings"].each do |config|
        file = File.read(File.join(Rubber.root, "secrets", Rubber.env, "config", "#{config}.yml"))
        put file, "#{release_path}/config/#{config}.yml"
      end

      if rubber_env.ssl.enabled
        ["site.crt", "site.key"].each do |ssl|
          file = File.read(File.join(Rubber.root, "secrets", Rubber.env, "ssl", "certs", ssl))
          put file, "#{release_path}/config/#{ssl}"
        end
      end

      cloudfront_key = File.join(Rubber.root, "secrets", Rubber.env, "aws", "cloudfront", "cloudfront.pem")
      if File.exists?(cloudfront_key)
        file = File.read(cloudfront_key)
        put file, "#{release_path}/config/cloudfront.pem"
      end

      public_dir = File.join(Rubber.root, "secrets", Rubber.env, "public")

      Dir.entries(public_dir).each do |public_file|
        public_file_path = File.join(public_dir, public_file)
        if File.file? public_file_path
          file = File.read(public_file_path)
          put file, "#{release_path}/public/#{public_file}"
        end
      end
    end

    def read_file(path)
      File.read(File.join(File.dirname(__FILE__), "..", file))
    end
  end
end
