# Capistrano Recipes for managing delayed_job
#
# These recipes are stolen directly from delayed_job's recipes - https://github.com/collectiveidea/delayed_job/blob/master/lib/delayed/recipes.rb
#
# If you want to use command line options, for example to start multiple workers,
# define a Capistrano variable delayed_job_args:
#
#   set :delayed_job_args, "-n 2"
#
# If you've got delayed_job workers running on separate servers, you can also specify
# which servers have delayed_job running and should be restarted after deploy.
# Defaults to :delayed_job
#
#   set :delayed_job_server_role, :worker
#

on :load do
  set :delayed_job_server_role, :background
  set :delayed_job_args, "-n #{rubber_env.delayed_job_workers}"
end

set :delayed_job_command, "bundle exec ruby ./script/delayed_job" # "script/delayed_job"

namespace :rubber do

  namespace :background do

    namespace :delayed_job do

      rubber.allow_optional_tasks(self)

      after "deploy:stop",    "rubber:background:delayed_job:stop"
      after "deploy:start",   "rubber:background:delayed_job:start"
      after "deploy:restart", "rubber:background:delayed_job:restart"

      after 'rubber:setup_app_permissions', 'rubber:background:delayed_job:set_run_permissions'

      def rails_env
        fetch(:rails_env, false) ? "RAILS_ENV=#{fetch(:rails_env)}" : ''
      end

      def args
        fetch(:delayed_job_args, "")
      end

      def roles
        fetch(:delayed_job_server_role, :delayed_job)
      end

      def delayed_job_command
        fetch(:delayed_job_command, "script/delayed_job")
      end

      desc "Stop the delayed_job process"
      task :stop, :roles => lambda { roles } do
        rsudo "cd #{current_path} && #{rails_env} #{delayed_job_command} stop #{delayed_job_args}", :as => rubber_env.app_user
      end

      desc "Start the delayed_job process"
      task :start, :roles => lambda { roles } do
        rsudo "cd #{current_path} && #{rails_env} #{delayed_job_command} start #{delayed_job_args}", :as => rubber_env.app_user
      end

      desc "Restart the delayed_job process"
      task :restart, :roles => lambda { roles } do
        rsudo "cd #{current_path} && #{rails_env} #{delayed_job_command} stop #{delayed_job_args}", :as => rubber_env.app_user
        rsudo "cd #{current_path} && #{rails_env} #{delayed_job_command} start #{delayed_job_args}", :as => rubber_env.app_user
      end

      # By default, script/delayed_job doesn't have run permissions so we must add them
      task :set_run_permissions, :roles => lambda { roles } do
        run "cd #{current_path}; sudo chmod +x script/delayed_job"
      end

      desc <<-DESC
        Live tail of delayed_job log files for all machines
      DESC
      task :tail_logs, :roles => lambda { roles } do
        log_file_glob = rubber.get_env("FILE", "Log files to tail", true, "#{current_path}/log/delayed*.log")
        run "tail -qf #{log_file_glob}" do |channel, stream, data|
          puts  # for an extra line break before the host name
          puts data
          break if stream == :err
        end
      end
    end
  end
end