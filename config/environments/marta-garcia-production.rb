require 'rack/ssl-enforcer'
::Rack::SslEnforcer
# Settings specified here will take precedence over those in config/environment.rb
ENV['SITE_THEME'] = 'marta_garcia'

# The production environment is meant for finished, "live" apps.
# Code is not reloaded between requests
config.cache_classes = true

# Full error reports are disabled and caching is turned on
config.action_controller.consider_all_requests_local = false
config.action_controller.perform_caching             = true
config.action_view.cache_template_loading            = true

# See everything in the log (default is :info)
# config.log_level = :debug

# Use a different logger for distributed setups
# config.logger = SyslogLogger.new

# Use a different cache store in production
# config.cache_store = :mem_cache_store

# Enable serving of images, stylesheets, and javascripts from an asset server
# config.action_controller.asset_host = "http://assets.example.com"

# Disable delivery errors, bad email addresses will be ignored
# config.action_mailer.raise_delivery_errors = false
ActionMailer::Base.smtp_settings.merge!(:enable_starttls_auto => false)

Synthesis::AssetPackage.merge_environments << "marta-garcia-production"
# Enable threaded mode
# config.threadsafe!
