# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150924134547) do

  create_table "artigo_anexos", :force => true do |t|
    t.string   "arquivo_file_name"
    t.string   "arquivo_content_type"
    t.string   "arquivo_file_size"
    t.datetime "arquivo_updated_at"
    t.string   "legenda"
    t.integer  "artigo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "artigos", :force => true do |t|
    t.string   "nome",          :limit => 100,                :null => false
    t.string   "descricao"
    t.integer  "professor_id"
    t.text     "corpo"
    t.integer  "visualizacoes",                :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assinatura_items", :force => true do |t|
    t.integer "produto_id"
    t.integer "assinatura_id"
  end

  create_table "assinatura_items_ignorados", :force => true do |t|
    t.integer "produto_id"
    t.integer "assinatura_id"
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "changes"
    t.integer  "version",        :default => 0
    t.datetime "created_at"
  end

  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "banner_histories", :force => true do |t|
    t.integer  "banner_id",                                  :null => false
    t.string   "ip",         :limit => 50
    t.boolean  "tipo",                     :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banners", :force => true do |t|
    t.string   "descricao"
    t.integer  "tipo",                 :default => 1,     :null => false
    t.integer  "posicao",              :default => 1,     :null => false
    t.boolean  "visivel",              :default => false
    t.integer  "limite_cliques"
    t.integer  "limite_visualizacoes"
    t.datetime "limite_data"
    t.string   "url"
    t.string   "string"
    t.boolean  "target",               :default => true
    t.integer  "total_cliques",        :default => 0
    t.integer  "total_visualizacoes",  :default => 0
    t.string   "arquivo_file_name"
    t.string   "arquivo_content_type"
    t.integer  "arquivo_file_size"
    t.datetime "arquivo_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categoria_cursos", :force => true do |t|
    t.string   "nome",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ancestry"
  end

  add_index "categoria_cursos", ["ancestry"], :name => "index_categoria_cursos_on_ancestry"
  add_index "categoria_cursos", ["nome"], :name => "index_categoria_cursos_on_nome", :unique => true

  create_table "categoria_newsletter_subscribes", :force => true do |t|
    t.string   "nome",                          :null => false
    t.boolean  "exibivel",   :default => false
    t.boolean  "removivel",  :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cidades", :force => true do |t|
    t.string  "nome"
    t.integer "estado_id"
  end

  create_table "classroom_tickets", :force => true do |t|
    t.integer  "user_id",            :null => false
    t.integer  "classroom_event_id", :null => false
    t.string   "code",               :null => false
    t.string   "status",             :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "fatura_id"
  end

  add_index "classroom_tickets", ["classroom_event_id"], :name => "index_classroom_tickets_on_classroom_event_id"
  add_index "classroom_tickets", ["code"], :name => "index_classroom_tickets_on_code"
  add_index "classroom_tickets", ["status"], :name => "index_classroom_tickets_on_status"
  add_index "classroom_tickets", ["user_id"], :name => "index_classroom_tickets_on_user_id"

  create_table "cupoms", :force => true do |t|
    t.date     "validade",                                       :null => false
    t.text     "codigo",                                         :null => false
    t.integer  "produto_id"
    t.integer  "desconto",                                       :null => false
    t.integer  "quantidade_disponivel"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "apply_for_all_cursos_online"
    t.boolean  "apply_for_all_cursos_em_pdf"
    t.boolean  "apply_for_all_pacotes"
    t.boolean  "apply_for_all_assinaturas"
    t.boolean  "apply_for_all_tickets",       :default => false, :null => false
  end

  add_index "cupoms", ["codigo"], :name => "cupoms_codigo_unico_index", :unique => true

  create_table "curso_anexos", :force => true do |t|
    t.string   "arquivo_file_name"
    t.string   "arquivo_content_type"
    t.integer  "arquivo_file_size"
    t.datetime "arquivo_updated_at"
    t.string   "legenda"
    t.integer  "curso_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cursos", :force => true do |t|
    t.string   "nome",                                                            :null => false
    t.text     "descricao"
    t.decimal  "investimento"
    t.integer  "categoria_curso_id"
    t.boolean  "status",                                       :default => false
    t.datetime "data_inicio"
    t.datetime "data_expiracao"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "inicio",                        :limit => 100
    t.string   "horario",                       :limit => 100
    t.boolean  "ativo",                                        :default => true,  :null => false
    t.string   "carga_horaria"
    t.string   "numero_encontros"
    t.string   "publico_alvo"
    t.string   "vacancies"
    t.string   "weekdays"
    t.string   "price_condition"
    t.integer  "demonstration_video_upload_id"
  end

  create_table "cursos_professors", :force => true do |t|
    t.integer "curso_id"
    t.integer "professor_id"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",       :default => 0
    t.integer  "attempts",       :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "custom_id"
    t.string   "polling_job_id"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"
  add_index "delayed_jobs", ["custom_id"], :name => "index_delayed_jobs_on_custom_id"

  create_table "depoimentos", :force => true do |t|
    t.string   "nome",                      :limit => 80,                     :null => false
    t.text     "descricao",                                                   :null => false
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "testimonial_type",                        :default => "text", :null => false
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.string   "video_updated_at"
    t.string   "video_splash_file_name"
    t.string   "video_splash_content_type"
    t.integer  "video_splash_file_size"
    t.string   "video_splash_updated_at"
  end

  create_table "desconto_assinatura_items", :force => true do |t|
    t.integer "desconto_id", :null => false
    t.integer "produto_id",  :null => false
  end

  create_table "desconto_items", :force => true do |t|
    t.integer "desconto_id", :null => false
    t.integer "produto_id",  :null => false
  end

  create_table "descontos", :force => true do |t|
    t.integer  "desconto",                                     :null => false
    t.date     "inicia_at",                                    :null => false
    t.date     "termina_at",                                   :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "aplicar_todos_items",       :default => false, :null => false
    t.boolean  "aplicar_todas_assinaturas", :default => false, :null => false
  end

  create_table "destaque_produtos", :force => true do |t|
    t.integer "produto_id", :null => false
    t.integer "position"
  end

  add_index "destaque_produtos", ["produto_id"], :name => "index_destaque_produtos_on_produto_id", :unique => true

  create_table "estados", :force => true do |t|
    t.string "nome"
    t.string "sigla"
  end

  create_table "fatura_items", :force => true do |t|
    t.decimal  "price"
    t.integer  "quantity",       :default => 1
    t.integer  "fatura_id"
    t.integer  "produto_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "grupo_id"
    t.text     "cupom_codigo"
    t.integer  "cupom_desconto"
    t.date     "expira_at"
  end

  create_table "faturas", :force => true do |t|
    t.string   "status",         :null => false
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "processado_at"
    t.text     "pagseguro_info"
  end

  create_table "horarios", :force => true do |t|
    t.integer  "curso_id"
    t.datetime "inicio"
    t.datetime "fim"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "imagems", :force => true do |t|
    t.string   "legenda"
    t.integer  "user_id",             :null => false
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "live_channel_supporting_materials", :force => true do |t|
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "label"
    t.integer  "live_channel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "live_channels", :force => true do |t|
    t.text     "embedded_content"
    t.boolean  "show_supporting_materials"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "facebook_embedded_content"
  end

  create_table "newsletter_subscribes", :force => true do |t|
    t.string   "nome",                              :null => false
    t.string   "email",                             :null => false
    t.integer  "categoria_newsletter_subscribe_id", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "noticias", :force => true do |t|
    t.string   "nome",                :limit => 1000, :null => false
    t.text     "descricao"
    t.text     "corpo",                               :null => false
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pacote_items", :force => true do |t|
    t.integer  "pacote_id",                    :null => false
    t.integer  "video_aula_id",                :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",      :default => 0, :null => false
  end

  add_index "pacote_items", ["position"], :name => "index_pacote_items_on_position"

  create_table "parceiros", :force => true do |t|
    t.string   "nome",                :limit => 100, :null => false
    t.string   "url",                 :limit => 100
    t.text     "detalhes"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products_video_uploads", :force => true do |t|
    t.integer "product_id"
    t.integer "video_upload_id"
    t.integer "position",        :default => 0, :null => false
    t.integer "view_limit"
  end

  add_index "products_video_uploads", ["position"], :name => "index_products_video_uploads_on_position"

  create_table "produto_anexos", :force => true do |t|
    t.string   "arquivo_file_name"
    t.string   "arquivo_content_type"
    t.integer  "arquivo_file_size"
    t.datetime "arquivo_updated_at"
    t.string   "legenda"
    t.integer  "produto_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "demonstracao",         :default => false
  end

  create_table "produtos", :force => true do |t|
    t.string   "nome",                         :limit => 100,                   :null => false
    t.text     "descricao"
    t.decimal  "investimento",                                                  :null => false
    t.integer  "visualizacoes",                               :default => 0
    t.integer  "categoria_curso_id"
    t.string   "publico_alvo",                 :limit => 100
    t.integer  "video_upload_demonstracao_id"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "desconto",                                    :default => 0.0
    t.string   "type"
    t.boolean  "ativo",                                       :default => true, :null => false
    t.string   "carga_horaria"
    t.string   "numero_videos"
    t.string   "periodicidade"
    t.text     "codigo_embed"
    t.boolean  "disponivel_venda",                            :default => true, :null => false
    t.integer  "tempo_expiracao"
    t.text     "price_condition"
    t.boolean  "enable_download"
    t.date     "available_from"
    t.date     "available_to"
    t.datetime "start_datetime"
    t.text     "instrucoes"
  end

  add_index "produtos", ["ativo"], :name => "index_produtos_on_ativo"
  add_index "produtos", ["categoria_curso_id"], :name => "index_produtos_on_categoria_curso_id"
  add_index "produtos", ["created_at"], :name => "index_produtos_on_created_at"
  add_index "produtos", ["disponivel_venda"], :name => "index_produtos_on_disponivel_venda"
  add_index "produtos", ["nome"], :name => "index_produtos_on_nome"
  add_index "produtos", ["start_datetime"], :name => "index_produtos_on_start_datetime"

  create_table "produtos_professors", :force => true do |t|
    t.integer "produto_id"
    t.integer "professor_id"
  end

  create_table "professors", :force => true do |t|
    t.string   "nome",                :null => false
    t.string   "email"
    t.string   "site"
    t.string   "area_atuacao"
    t.text     "curriculo"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "professors", ["nome"], :name => "index_professors_on_nome"

  create_table "roles", :force => true do |t|
    t.string "name"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  add_index "roles_users", ["role_id"], :name => "index_roles_users_on_role_id"
  add_index "roles_users", ["user_id"], :name => "index_roles_users_on_user_id"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "ip"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "simulated_test_products", :force => true do |t|
    t.integer  "simulated_test_id", :null => false
    t.integer  "product_id",        :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "simulated_test_questions", :force => true do |t|
    t.integer  "simulated_test_id",                   :null => false
    t.text     "question"
    t.text     "answer"
    t.string   "answer_option"
    t.integer  "position",                            :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "comments"
    t.integer  "video_upload_id"
    t.boolean  "mandatory",         :default => true
    t.string   "question_type"
  end

  add_index "simulated_test_questions", ["position"], :name => "index_simulated_test_questions_on_position"

  create_table "site_page_contents", :force => true do |t|
    t.text     "content"
    t.string   "name",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "content_2"
    t.text     "content_3"
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type"], :name => "index_taggings_on_taggable_id_and_taggable_type"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "tickers", :force => true do |t|
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.string   "link"
    t.string   "descricao",           :limit => 80
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "category",                          :default => "online", :null => false
  end

  create_table "user_histories", :force => true do |t|
    t.datetime "login"
    t.integer  "user_id", :null => false
  end

  add_index "user_histories", ["login"], :name => "index_user_histories_on_login"

  create_table "user_simulated_test_answers", :force => true do |t|
    t.integer  "simulated_test_question_id", :null => false
    t.integer  "user_simulated_test_id",     :null => false
    t.text     "answer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_simulated_test_answers", ["user_simulated_test_id", "simulated_test_question_id"], :name => "user_simulated_test_answers_ust_id_stq_id_unique_idx", :unique => true

  create_table "user_simulated_tests", :force => true do |t|
    t.integer  "simulated_test_id", :null => false
    t.integer  "user_id",           :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_simulated_tests", ["user_id", "simulated_test_id"], :name => "user_simulated_tests_user_id_simulated_test_id_unique_index", :unique => true

  create_table "user_video_view_counters", :force => true do |t|
    t.integer  "user_id",                         :null => false
    t.integer  "video_id",                        :null => false
    t.integer  "video_class_id",                  :null => false
    t.integer  "counter",          :default => 0, :null => false
    t.integer  "additional_views", :default => 0, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "package_id"
  end

  add_index "user_video_view_counters", ["counter"], :name => "index_user_video_view_counters_on_counter"

  create_table "user_watermark_attachments", :force => true do |t|
    t.integer  "user_id",                             :null => false
    t.string   "job_id",                              :null => false
    t.integer  "base_product_attachment_id",          :null => false
    t.string   "watermarked_attachment_file_name",    :null => false
    t.string   "watermarked_attachment_content_type"
    t.integer  "watermarked_attachment_file_size"
    t.datetime "watermarked_attachment_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "login",                           :limit => 40
    t.string   "first_name",                      :limit => 100, :default => ""
    t.string   "email",                           :limit => 100
    t.string   "crypted_password",                :limit => 40
    t.string   "salt",                            :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token",                  :limit => 40
    t.datetime "remember_token_expires_at"
    t.string   "reset_password_token",            :limit => 40
    t.string   "reset_password_token_expires_at", :limit => 40
    t.boolean  "admin",                                          :default => false
    t.datetime "last_login"
    t.string   "cpf",                             :limit => 14
    t.boolean  "ativo",                                          :default => true
    t.string   "endereco"
    t.string   "nascimento",                      :limit => 10
    t.string   "complemento",                     :limit => 100
    t.string   "cep",                             :limit => 10
    t.string   "bairro",                          :limit => 100
    t.string   "telefone_fixo",                   :limit => 30
    t.string   "telefone_celular",                :limit => 30
    t.integer  "estado_id"
    t.integer  "cidade_id"
    t.string   "profissao"
    t.string   "last_name"
  end

  add_index "users", ["login"], :name => "index_users_on_login", :unique => true

  create_table "video_upload_tipos", :force => true do |t|
    t.string   "nome"
    t.string   "identificador"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "video_uploads", :force => true do |t|
    t.integer  "creator_id"
    t.string   "nome"
    t.string   "caption",                :limit => 1000
    t.text     "description"
    t.integer  "duracao",                                :default => 0
    t.integer  "video_upload_tipo_id"
    t.integer  "categoria_curso_id"
    t.string   "streaming_file_name"
    t.string   "streaming_content_type"
    t.integer  "streaming_file_size"
    t.datetime "streaming_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "video_uploads", ["nome"], :name => "index_video_uploads_on_nome"

  add_foreign_key "artigo_anexos", ["artigo_id"], "artigos", ["id"], :name => "artigo_anexos_artigo_id_fkey"

  add_foreign_key "artigos", ["professor_id"], "professors", ["id"], :name => "artigos_professor_id_fkey"

  add_foreign_key "assinatura_items", ["produto_id"], "produtos", ["id"], :name => "assinatura_items_produto_id_fkey"
  add_foreign_key "assinatura_items", ["assinatura_id"], "produtos", ["id"], :name => "assinatura_items_assinatura_id_fkey"

  add_foreign_key "assinatura_items_ignorados", ["produto_id"], "produtos", ["id"], :name => "assinatura_items_ignorados_produto_id_fkey"
  add_foreign_key "assinatura_items_ignorados", ["assinatura_id"], "produtos", ["id"], :name => "assinatura_items_ignorados_assinatura_id_fkey"

  add_foreign_key "banner_histories", ["banner_id"], "banners", ["id"], :name => "banner_histories_banner_id_fkey"

  add_foreign_key "cidades", ["estado_id"], "estados", ["id"], :name => "cidades_estado_id_fkey"

  add_foreign_key "classroom_tickets", ["user_id"], "users", ["id"], :name => "classroom_tickets_user_id_fkey"
  add_foreign_key "classroom_tickets", ["classroom_event_id"], "produtos", ["id"], :name => "classroom_tickets_classroom_event_id_fkey"
  add_foreign_key "classroom_tickets", ["fatura_id"], "faturas", ["id"], :on_delete => :cascade, :name => "classroom_tickets_fatura_id_fkey"

  add_foreign_key "cupoms", ["produto_id"], "produtos", ["id"], :name => "cupoms_produto_id_fkey"

  add_foreign_key "curso_anexos", ["curso_id"], "cursos", ["id"], :name => "curso_anexos_curso_id_fkey"

  add_foreign_key "cursos", ["categoria_curso_id"], "categoria_cursos", ["id"], :name => "cursos_categoria_curso_id_fkey"
  add_foreign_key "cursos", ["demonstration_video_upload_id"], "video_uploads", ["id"], :name => "cursos_demonstration_video_upload_id_fkey"

  add_foreign_key "desconto_assinatura_items", ["desconto_id"], "descontos", ["id"], :name => "desconto_assinatura_items_desconto_id_fkey"
  add_foreign_key "desconto_assinatura_items", ["produto_id"], "produtos", ["id"], :name => "desconto_assinatura_items_produto_id_fkey"

  add_foreign_key "desconto_items", ["desconto_id"], "descontos", ["id"], :name => "desconto_items_desconto_id_fkey"
  add_foreign_key "desconto_items", ["produto_id"], "produtos", ["id"], :name => "desconto_items_produto_id_fkey"

  add_foreign_key "destaque_produtos", ["produto_id"], "produtos", ["id"], :name => "destaque_produtos_produto_id_fkey"

  add_foreign_key "fatura_items", ["fatura_id"], "faturas", ["id"], :name => "fatura_items_fatura_id_fkey"
  add_foreign_key "fatura_items", ["produto_id"], "produtos", ["id"], :name => "fatura_items_produto_id_fkey"
  add_foreign_key "fatura_items", ["grupo_id"], "fatura_items", ["id"], :name => "fatura_items_grupo_id_fkey"

  add_foreign_key "faturas", ["user_id"], "users", ["id"], :name => "faturas_user_id_fkey"

  add_foreign_key "horarios", ["curso_id"], "cursos", ["id"], :name => "horarios_curso_id_fkey"

  add_foreign_key "imagems", ["user_id"], "users", ["id"], :name => "imagems_user_id_fkey"

  add_foreign_key "live_channel_supporting_materials", ["live_channel_id"], "live_channels", ["id"], :name => "live_channel_supporting_materials_live_channel_id_fkey"

  add_foreign_key "newsletter_subscribes", ["categoria_newsletter_subscribe_id"], "categoria_newsletter_subscribes", ["id"], :name => "newsletter_subscribes_categoria_newsletter_subscribe_id_fkey"

  add_foreign_key "pacote_items", ["pacote_id"], "produtos", ["id"], :name => "pacote_items_pacote_id_fkey"
  add_foreign_key "pacote_items", ["video_aula_id"], "produtos", ["id"], :name => "pacote_items_video_aula_id_fkey"

  add_foreign_key "products_video_uploads", ["video_upload_id"], "video_uploads", ["id"], :name => "products_video_uploads_video_upload_id_fkey"
  add_foreign_key "products_video_uploads", ["product_id"], "produtos", ["id"], :name => "products_video_uploads_product_id_fkey"

  add_foreign_key "produto_anexos", ["produto_id"], "produtos", ["id"], :name => "produto_anexos_produto_id_fkey"

  add_foreign_key "produtos", ["video_upload_demonstracao_id"], "video_uploads", ["id"], :name => "produtos_video_upload_demonstracao_id_fkey"
  add_foreign_key "produtos", ["categoria_curso_id"], "categoria_cursos", ["id"], :name => "produtos_categoria_curso_id_fkey"

  add_foreign_key "produtos_professors", ["produto_id"], "produtos", ["id"], :name => "produtos_professors_produto_id_fkey"
  add_foreign_key "produtos_professors", ["professor_id"], "professors", ["id"], :name => "produtos_professors_professor_id_fkey"

  add_foreign_key "simulated_test_products", ["simulated_test_id"], "produtos", ["id"], :on_delete => :cascade, :name => "simulated_test_products_simulated_test_id_fkey"
  add_foreign_key "simulated_test_products", ["product_id"], "produtos", ["id"], :on_delete => :cascade, :name => "simulated_test_products_product_id_fkey"

  add_foreign_key "simulated_test_questions", ["simulated_test_id"], "produtos", ["id"], :on_delete => :cascade, :name => "simulated_test_questions_simulated_test_id_fkey"
  add_foreign_key "simulated_test_questions", ["video_upload_id"], "video_uploads", ["id"], :name => "simulated_test_questions_video_upload_id_fkey"

  add_foreign_key "user_simulated_test_answers", ["simulated_test_question_id"], "simulated_test_questions", ["id"], :name => "user_simulated_test_answers_simulated_test_question_id_fkey"
  add_foreign_key "user_simulated_test_answers", ["user_simulated_test_id"], "user_simulated_tests", ["id"], :name => "user_simulated_test_answers_user_simulated_test_id_fkey"

  add_foreign_key "user_simulated_tests", ["simulated_test_id"], "produtos", ["id"], :name => "user_simulated_tests_simulated_test_id_fkey"
  add_foreign_key "user_simulated_tests", ["user_id"], "users", ["id"], :name => "user_simulated_tests_user_id_fkey"

  add_foreign_key "user_video_view_counters", ["user_id"], "users", ["id"], :on_delete => :cascade, :name => "user_video_view_counters_user_id_fkey"
  add_foreign_key "user_video_view_counters", ["video_id"], "video_uploads", ["id"], :on_delete => :cascade, :name => "user_video_view_counters_video_id_fkey"
  add_foreign_key "user_video_view_counters", ["video_class_id"], "produtos", ["id"], :on_delete => :cascade, :name => "user_video_view_counters_video_class_id_fkey"
  add_foreign_key "user_video_view_counters", ["package_id"], "produtos", ["id"], :on_delete => :cascade, :name => "user_video_view_counters_package_id_fkey"

  add_foreign_key "user_watermark_attachments", ["base_product_attachment_id"], "produto_anexos", ["id"], :on_delete => :cascade, :name => "user_watermark_attachments_base_product_attachment_id_fkey"
  add_foreign_key "user_watermark_attachments", ["user_id"], "users", ["id"], :on_delete => :cascade, :name => "user_watermark_attachments_user_id_fkey"

  add_foreign_key "users", ["estado_id"], "estados", ["id"], :name => "users_estado_id_fkey"
  add_foreign_key "users", ["cidade_id"], "cidades", ["id"], :name => "users_cidade_id_fkey"

  add_foreign_key "video_uploads", ["video_upload_tipo_id"], "video_upload_tipos", ["id"], :name => "video_uploads_video_upload_tipo_id_fkey"
  add_foreign_key "video_uploads", ["categoria_curso_id"], "categoria_cursos", ["id"], :name => "video_uploads_categoria_curso_id_fkey"

end
