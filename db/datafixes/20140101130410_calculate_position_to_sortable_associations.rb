#!/usr/bin/env ruby

unless ENV['RAILS_ENV']
  puts "Select an environment passing RAILS_ENV when running this script"
  exit
end

require File.expand_path('../../../config/boot',  __FILE__)
require RAILS_ROOT + '/config/environment'
#ActiveRecord::Base.logger = Logger.new(STDOUT)

VideoAula.all.each do |video_aula|
  position = 0
  VideoAulasVideoUpload.find(:all, :include => :video_upload, :conditions => ["video_aula_id=?", video_aula.id], :order => "video_uploads.nome ASC").each do |association|
    association.update_attribute :position, position
    position += 1
  end
end

Pacote.all.each do |pacote|
  position = 0
  PacoteItem.find(:all, :include => :video_aula, :conditions => ["pacote_id=?", pacote.id], :order => "produtos.nome ASC").each do |association|
    association.update_attribute :position, position
    position += 1
  end
end

