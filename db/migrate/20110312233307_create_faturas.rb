class CreateFaturas < ActiveRecord::Migration
  def self.up
    create_table :faturas do |t|
      t.string :status, :null => false
      t.references :user
      t.timestamps
    end
    
    add_foreign_key(:faturas, :user_id, :users, :id)
  end

  def self.down
    remove_foreign_key(:faturas, :faturas_user_id_fkey)
    drop_table :faturas
  end
end
