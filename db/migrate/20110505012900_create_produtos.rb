class CreateProdutos < ActiveRecord::Migration
  def self.up    
       
    remove_foreign_key(:fatura_items, :fatura_items_video_aula_id_fkey)
    remove_foreign_key(:video_aulas, :video_aulas_categoria_curso_id_fkey)
    remove_foreign_key(:video_aulas, :video_aulas_video_upload_demonstracao_id_fkey)
    remove_foreign_key(:video_aulas, :video_aulas_video_upload_id_fkey)
    rename_table :video_aulas, :produtos
    add_foreign_key(:produtos, :video_upload_id, :video_uploads, :id)
    add_foreign_key(:produtos, :video_upload_demonstracao_id, :video_uploads, :id)
    add_foreign_key(:produtos, :categoria_curso_id, :categoria_cursos, :id)
    rename_column :produtos, :preco, :investimento
    add_column :produtos, :desconto, :decimal, :default => 0
    add_column :produtos, :type, :string
    
    rename_column :fatura_items, :video_aula_id, :produto_id
    add_foreign_key(:fatura_items, :produto_id, :produtos, :id)
    
    add_column :fatura_items, :grupo_id, :integer
    add_foreign_key(:fatura_items, :grupo_id, :fatura_items, :id)
     
  end

  def self.down

    remove_foreign_key(:fatura_items, :fatura_items_grupo_id_fkey)
    remove_foreign_key(:fatura_items, :fatura_items_produto_id_fkey)
    remove_column :fatura_items, :grupo_id
    remove_column :produtos, :type
    remove_column :produtos, :desconto
    rename_column :produtos, :investimento, :preco
    rename_column :fatura_items, :produto_id, :video_aula_id
    remove_foreign_key(:produtos, :produtos_categoria_curso_id_fkey)
    remove_foreign_key(:produtos, :produtos_video_upload_demonstracao_id_fkey)
    remove_foreign_key(:produtos, :produtos_video_upload_id_fkey)
    rename_table :produtos, :video_aulas
    add_foreign_key(:video_aulas, :video_upload_id, :video_uploads, :id)
    add_foreign_key(:video_aulas, :video_upload_demonstracao_id, :video_uploads, :id)
    add_foreign_key(:video_aulas, :categoria_curso_id, :categoria_cursos, :id)
    add_foreign_key(:fatura_items, :video_aula_id, :video_aulas, :id)
    
  end
end
