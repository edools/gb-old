class CreateVideoAulasProfessors < ActiveRecord::Migration
  def self.up
    create_table :video_aulas_professors do |t|
      t.references :video_aula
      t.references :professor
    end
  end

  def self.down
    drop_table :video_aulas_professors
  end
end
