class RemoveAnswerNotNullConstraintForUserSimulatedTestAnswers < ActiveRecord::Migration
  def self.up
    change_column :user_simulated_test_answers, :answer, :text, :null => true
  end

  def self.down
    change_column :user_simulated_test_answers, :answer, :text, :null => false
  end
end
