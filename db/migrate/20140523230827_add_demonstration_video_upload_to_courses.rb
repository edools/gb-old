class AddDemonstrationVideoUploadToCourses < ActiveRecord::Migration
  def self.up
    add_column :cursos, :demonstration_video_upload_id, :integer
    add_foreign_key(:cursos, :demonstration_video_upload_id, :video_uploads, :id)
  end

  def self.down
    remove_foreign_key(:cursos, :cursos_demonstration_video_upload_id_fkey)
    remove_column :cursos, :demonstration_video_upload_id
  end
end
