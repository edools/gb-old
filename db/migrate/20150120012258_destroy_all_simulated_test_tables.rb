class DestroyAllSimulatedTestTables < ActiveRecord::Migration
  def self.up
    remove_foreign_key(:user_simulated_tests, :user_simulated_tests_user_id_fkey)
    drop_table :user_simulated_tests
    drop_table :simulated_tests
  end

  def self.down
    create_table :simulated_tests do |t|
      t.boolean :enabled, :null => false, :default => false
      t.text :description

      1.upto(80) do |i|
        t.text :"question_#{i}"
        t.text :"answer_#{i}"
        t.string :"answer_option_#{i}"
      end

      t.timestamps
    end

    create_table :user_simulated_tests do |t|
      t.integer :user_id, :unique => false, :null => false

      1.upto(80) do |i|
        t.string :"answer_#{i}", :null => false
      end

      t.timestamps
    end
    add_foreign_key(:user_simulated_tests, :user_id, :users, :id)
  end
end
