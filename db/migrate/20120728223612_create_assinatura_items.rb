class CreateAssinaturaItems < ActiveRecord::Migration
  def self.up
    create_table :assinatura_items do |t|
      t.references :produto
      t.references :assinatura
    end

    add_foreign_key :assinatura_items, :produto_id, :produtos, :id
    add_foreign_key :assinatura_items, :assinatura_id, :produtos, :id
  end

  def self.down
    drop_table :assinatura_items
  end
end
