class CreateSimulatedTests < ActiveRecord::Migration
  def self.up
		create_table :simulated_tests do |t|
			t.boolean :enabled, :null => false, :default => false
			t.text :description

			1.upto(80) do |i|
				t.text :"question_#{i}"
				t.text :"answer_#{i}"
			end

			t.timestamps
		end
  end

  def self.down
		drop_table :simulated_tests
  end
end
