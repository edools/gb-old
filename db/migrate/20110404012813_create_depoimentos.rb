class CreateDepoimentos < ActiveRecord::Migration
  def self.up
    create_table :depoimentos do |t|
      t.string :nome, :null => false, :limit => 80
      t.text :descricao, :null => false
      t.string :imagem_file_name
      t.string :imagem_content_type
      t.integer :imagem_file_size
      t.datetime :imagem_updated_at

      t.timestamps
    end
  end

  def self.down
    drop_table :depoimentos
  end
end
