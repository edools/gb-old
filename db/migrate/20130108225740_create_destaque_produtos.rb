class CreateDestaqueProdutos < ActiveRecord::Migration
  def self.up
    create_table :destaque_produtos do |t|
      t.integer :produto_id, :null => false
      t.integer :position
    end

    add_foreign_key(:destaque_produtos, :produto_id, :produtos, :id)
    add_index(:destaque_produtos, :produto_id, :unique => true)
  end

  def self.down
    remove_index :destaque_produtos, :produto_id
    remove_foreign_key(:destaque_produtos, :destaque_produtos_produto_id_fkey)
    drop_table :destaque_produtos
  end
end
