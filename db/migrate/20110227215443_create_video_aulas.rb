class CreateVideoAulas < ActiveRecord::Migration
  def self.up
    create_table :video_aulas do |t|
      t.string :nome, :null => false, :limit => 100 
      t.text :descricao
      t.decimal :preco, :scale => 2, :null => false
      t.integer :visualizacoes, :default => 0
      t.references :video_upload
      t.references :categoria_curso
      t.string :publico_alvo, :limit => 100
      t.belongs_to :video_upload_demonstracao

      t.string :imagem_file_name
      t.string :imagem_content_type
      t.integer :imagem_file_size
      t.datetime :imagem_updated_at

      t.timestamps
    end

    add_foreign_key(:video_aulas, :video_upload_id, :video_uploads, :id)
    add_foreign_key(:video_aulas, :categoria_curso_id, :categoria_cursos, :id)
    add_foreign_key(:video_aulas, :video_upload_demonstracao_id, :video_uploads, :id)
  end

  def self.down
    remove_foreign_key(:video_aulas, :video_aulas_video_upload_demonstracao_id_fkey)
    remove_foreign_key(:video_aulas, :video_aulas_categoria_curso_id_fkey)
    remove_foreign_key(:video_aulas, :video_aulas_video_upload_id_fkey)
    drop_table :video_aulas
  end
end