class AddSearchIndexToVideoUploads < ActiveRecord::Migration
  def self.up
    add_index :video_uploads, :nome
  end

  def self.down
    remove_index :video_uploads, :nome
  end
end
