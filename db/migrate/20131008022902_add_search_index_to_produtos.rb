class AddSearchIndexToProdutos < ActiveRecord::Migration
  def self.up
    add_index :produtos, :nome
  end

  def self.down
    remove_index :produtos, :nome
  end
end
