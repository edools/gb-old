class RenameDescricaoToUrlToParceiros < ActiveRecord::Migration
  def self.up
    rename_column :parceiros, :descricao, :url
  end

  def self.down
    rename_column :parceiros, :url, :descricao
  end
end
