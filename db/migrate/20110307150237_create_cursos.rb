class CreateCursos < ActiveRecord::Migration
  def self.up
    create_table :cursos do |t|
      t.string :nome, :null => false
      t.text :descricao
      t.string :carga_horaria
      t.decimal :investimento
      t.references :categoria_curso
      t.string :publico_alvo, :limit => 100
      t.integer :numero_aulas, :default => 0
      t.boolean :status, :default => false
      t.datetime :data_inicio
      t.datetime :data_expiracao

      t.string :imagem_file_name
      t.string :imagem_content_type
      t.integer :imagem_file_size
      t.datetime :imagem_updated_at

      t.timestamps
    end

    add_foreign_key(:cursos, :categoria_curso_id, :categoria_cursos, :id)
  end

  def self.down
    remove_foreign_key(:cursos, :categoria_cursos_curso_id_fkey)
    drop_table :cursos
  end
end
