class AddCupomFieldsToFaturaItems < ActiveRecord::Migration
  def self.up
    add_column :fatura_items, :cupom_codigo, :text
    add_column :fatura_items, :cupom_desconto, :integer
  end

  def self.down
    remove_column :fatura_items, :cupom_desconto
    remove_column :fatura_items, :cupom_codigo
  end
end
