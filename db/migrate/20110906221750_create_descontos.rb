class CreateDescontos < ActiveRecord::Migration
  def self.up
    create_table :descontos do |t|
      t.integer :desconto, :null => false # 0% a 100%
      t.date :inicia_at, :null => false
      t.date :termina_at, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :descontos
  end
end
