class RenameVideoAulasVideoUploadsToProductVideoUploads < ActiveRecord::Migration
  def self.up

    remove_foreign_key :video_aulas_video_uploads, 'video_aulas_video_uploads_video_aula_id_fkey'
    remove_foreign_key :video_aulas_video_uploads, 'video_aulas_video_uploads_video_upload_id_fkey'
    rename_index :video_aulas_video_uploads, 'index_video_aulas_video_uploads_on_position', 'index_products_video_uploads_on_position'
    rename_column :video_aulas_video_uploads, :video_aula_id, :product_id
    rename_table :video_aulas_video_uploads, :products_video_uploads
    add_foreign_key :products_video_uploads, :video_upload_id, :video_uploads, :id
    add_foreign_key :products_video_uploads, :product_id, :produtos, :id
    execute 'ALTER INDEX video_aulas_video_uploads_pkey RENAME to products_video_uploads_pkey'
  end

  def self.down
    remove_foreign_key :products_video_uploads, 'products_video_uploads_product_id_fkey'
    remove_foreign_key :products_video_uploads, 'products_video_uploads_video_upload_id_fkey'
    rename_index :products_video_uploads, 'index_products_video_uploads_on_position', 'index_video_aulas_video_uploads_on_position'
    rename_column :products_video_uploads, :product_id, :video_aula_id
    rename_table :products_video_uploads, :video_aulas_video_uploads
    add_foreign_key :video_aulas_video_uploads, :video_upload_id, :video_uploads, :id
    add_foreign_key :video_aulas_video_uploads, :video_aula_id, :produtos, :id
    execute 'ALTER INDEX products_video_uploads_pkey RENAME to video_aulas_video_uploads_pkey'
  end
end
