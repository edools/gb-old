class CreateVideoUploadTipos < ActiveRecord::Migration
  def self.up
    create_table :video_upload_tipos do |t|
      t.string :nome
      t.string :identificador

      t.timestamps
    end
  end

  def self.down
    drop_table :video_upload_tipos
  end
end
