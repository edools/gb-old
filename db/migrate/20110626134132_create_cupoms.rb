class CreateCupoms < ActiveRecord::Migration
  def self.up
    create_table :cupoms do |t|
      t.date :validade, :null => false # data que o cupom vai expirar
      t.text :codigo, :null => false # codigo usado pra identificar o cupom
      t.references :produto, :null => false # produto que vai ser aplicado o desconto
      t.integer :desconto, :null => false # 0% a 100%
      t.integer :quantidade_disponivel # NULL significa cupom ilimitado dentro do periodo
      t.timestamps
    end
    add_index(:cupoms, :codigo, :unique => true, :name => 'cupoms_codigo_unico_index')
    add_foreign_key(:cupoms, :produto_id, :produtos, :id)
  end

  def self.down
    remove_index(:cupoms,'cupoms_codigo_unico_index')
    remove_foreign_key(:cupoms, :cupoms_produto_id_fkey)
    drop_table :cupoms
  end
end
