class CreateCategoriaNewsletterSubscribes < ActiveRecord::Migration
  def self.up
    create_table :categoria_newsletter_subscribes do |t|
      t.string :nome, :null => false
      t.boolean :exibivel, :default => false
      t.boolean :removivel, :default => true

      t.timestamps
    end

    CategoriaNewsletterSubscribe.create(:nome => "Default", :exibivel => true, :removivel => false)
  end

  def self.down
    drop_table :categoria_newsletter_subscribes
  end
end
