class AddAplicarTodosCursosAndAplicarTodasAssinaturasToDescontos < ActiveRecord::Migration
  def self.up
    add_column :descontos, :aplicar_todos_cursos, :boolean, :null => false, :default => false
    add_column :descontos, :aplicar_todas_assinaturas, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :descontos, :aplicar_todas_assinaturas
    remove_column :descontos, :aplicar_todos_cursos
  end
end
