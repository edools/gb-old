class AtualizarCamposNoticias < ActiveRecord::Migration
  def self.up
    change_table :noticias do |t|
      t.change :descricao, :text
      t.change :nome, :string, :null => false, :limit => 1000
    end
  end

  def self.down
    change_table :noticias do |t|
      t.change :descricao, :string, :limit => 255
      t.change :nome, :string, :null => false, :limit => 255
    end
  end
end
