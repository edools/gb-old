class AddCreatedAtIndexToProdutos < ActiveRecord::Migration
  def self.up
    add_index :produtos, :created_at, :order => {:created_at => :desc}
  end

  def self.down
    remove_index :produtos, :created_at
  end
end
