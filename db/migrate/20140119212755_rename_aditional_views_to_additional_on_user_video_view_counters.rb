class RenameAditionalViewsToAdditionalOnUserVideoViewCounters < ActiveRecord::Migration
  def self.up
    rename_column :user_video_view_counters, :aditional_views, :additional_views
  end

  def self.down
    rename_column :user_video_view_counters, :additional_views, :aditional_views
  end
end
