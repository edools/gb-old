class AddApplyForAllTicketsToCupoms < ActiveRecord::Migration
  def self.up
    add_column :cupoms, :apply_for_all_tickets, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :cupoms, :apply_for_all_tickets
  end
end
