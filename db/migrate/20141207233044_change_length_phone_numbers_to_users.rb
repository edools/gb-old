class ChangeLengthPhoneNumbersToUsers < ActiveRecord::Migration
  def self.up
		change_column :users, :telefone_fixo, :string, :limit => 30
		change_column :users, :telefone_celular, :string, :limit => 30
  end

  def self.down
		change_column :users, :telefone_fixo, :string, :limit => 13
		change_column :users, :telefone_celular, :string, :limit => 13
  end
end
