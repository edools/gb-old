class CreateDescontoItems < ActiveRecord::Migration
  def self.up
    create_table :desconto_items do |t|
      t.references :desconto, :null => false
      # item que sera aplicado o desconto, pode ser uma categoria, um pacote ou video aula
      t.references :produto, :null => false
    end

    add_foreign_key :desconto_items, :desconto_id, :descontos, :id
    add_foreign_key :desconto_items, :produto_id, :produtos, :id
  end

  def self.down
    drop_table :desconto_items
  end
end
