class AddProcessadoAtToFaturas < ActiveRecord::Migration
  def self.up
    add_column :faturas, :processado_at, :datetime
  end

  def self.down
    remove_column :faturas, :processado_at
  end
end
