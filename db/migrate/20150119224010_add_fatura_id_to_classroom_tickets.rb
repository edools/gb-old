class AddFaturaIdToClassroomTickets < ActiveRecord::Migration
  def self.up
    add_column :classroom_tickets, :fatura_id, :integer, :null => true
    add_foreign_key :classroom_tickets, :fatura_id, :faturas, :id, :on_delete => :cascade
  end

  def self.down
    remove_foreign_key :classroom_tickets, :classroom_tickets_fatura_id_fkey
    remove_column :classroom_tickets, :fatura_id
  end
end
