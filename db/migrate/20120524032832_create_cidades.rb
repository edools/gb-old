class CreateCidades < ActiveRecord::Migration
  def self.up
    create_table :cidades do |t|
      t.string :nome
      t.references :estado
    end
    add_foreign_key :cidades, :estado_id, :estados, :id
  end

  def self.down
    drop_table :cidades
  end
end
