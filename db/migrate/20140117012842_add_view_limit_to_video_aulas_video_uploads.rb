class AddViewLimitToVideoAulasVideoUploads < ActiveRecord::Migration
  def self.up
    add_column :video_aulas_video_uploads, :view_limit, :integer
  end

  def self.down
    remove_column :video_aulas_video_uploads, :view_limit
  end
end
