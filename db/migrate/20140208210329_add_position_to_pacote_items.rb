class AddPositionToPacoteItems < ActiveRecord::Migration
  def self.up
    add_column :pacote_items, :position, :integer, :null => false, :default => 0
    add_index :pacote_items, :position
  end

  def self.down
    remove_index :pacote_items, :position
    remove_column :pacote_items, :position
  end
end
