class CreateRolesFixtures < ActiveRecord::Migration
  def self.up
    say "Role administrador criada com sucesso!" if Role.create(:name => "administrador")
  end

  def self.down
    say "Role administrador removida com sucesso!" if Role.destroy(Role.find_by_name("administrador"))
  end
end
