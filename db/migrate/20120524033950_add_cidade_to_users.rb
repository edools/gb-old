class AddCidadeToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.references :estado
      t.references :cidade
    end
    add_foreign_key :users, :estado_id, :estados, :id
    add_foreign_key :users, :cidade_id, :cidades, :id
  end

  def self.down
    remove_column :users, :estado_id
    remove_column :users, :cidade_id
  end
end
