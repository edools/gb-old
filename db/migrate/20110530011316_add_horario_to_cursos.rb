class AddHorarioToCursos < ActiveRecord::Migration
  def self.up
    add_column :cursos, :inicio, :string, :limit => 100
    add_column :cursos, :horario, :string, :limit => 100
    remove_column :cursos, :publico_alvo
    remove_column :cursos, :carga_horaria
    remove_column :cursos, :numero_aulas
  end

  def self.down
    add_column :cursos, :carga_horaria, :string
    add_column :cursos, :publico_alvo, :string
    add_column :cursos, :numero_aulas, :integer, :default => 0
    remove_column :cursos, :horario
    remove_column :cursos, :inicio
  end
end
