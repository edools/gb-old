class CreateUserHistories < ActiveRecord::Migration
  def self.up
    create_table :user_histories do |t|
      t.datetime :login
      t.references "user", :null => false
    end

    add_index :user_histories, :login
  end

  def self.down
    drop_table :user_histories
  end
end
