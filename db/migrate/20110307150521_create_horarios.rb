class CreateHorarios < ActiveRecord::Migration
  def self.up
    create_table :horarios do |t|
      t.references :curso
      t.datetime :inicio
      t.datetime :fim

      t.timestamps
    end

    add_foreign_key(:horarios, :curso_id, :cursos, :id)
  end

  def self.down
    remove_foreign_key(:horarios, :horarios_curso_id_fkey)
    drop_table :horarios
  end
end
