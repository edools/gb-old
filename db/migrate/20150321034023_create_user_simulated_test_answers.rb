class CreateUserSimulatedTestAnswers < ActiveRecord::Migration
  def self.up
    create_table :user_simulated_test_answers do |t|
      t.integer :simulated_test_question_id, :null => false
      t.integer :user_simulated_test_id, :null => false
      t.text :answer, :null => false

      t.timestamps
    end
    add_foreign_key(:user_simulated_test_answers, :simulated_test_question_id, :simulated_test_questions, :id)
    add_foreign_key(:user_simulated_test_answers, :user_simulated_test_id, :user_simulated_tests, :id)
  end

  def self.down
    remove_foreign_key(:user_simulated_test_answers, :user_simulated_test_answers_user_simulated_test_id_fkey)
    remove_foreign_key(:user_simulated_test_answers, :user_simulated_test_answers_simulated_test_question_id_fkey)

    drop_table :user_simulated_test_answers
  end
end
