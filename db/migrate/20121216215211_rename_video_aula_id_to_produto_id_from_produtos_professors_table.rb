class RenameVideoAulaIdToProdutoIdFromProdutosProfessorsTable < ActiveRecord::Migration
  def self.up
    rename_column :produtos_professors, :video_aula_id, :produto_id
    add_foreign_key(:produtos_professors, :produto_id, :produtos, :id)
    add_foreign_key(:produtos_professors, :professor_id, :professors, :id)
  end

  def self.down
    remove_foreign_key(:produtos_professors, :produtos_professors_professor_id_fkey)
    remove_foreign_key(:produtos_professors, :produtos_professors_produto_id_fkey)
    rename_column :produtos_professors, :produto_id, :video_aula_id
  end
end
