class AddTempoExpiracaoToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :tempo_expiracao, :integer
  end

  def self.down
    remove_column :produtos, :tempo_expiracao
  end
end
