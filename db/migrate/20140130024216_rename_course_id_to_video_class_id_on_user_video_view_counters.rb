class RenameCourseIdToVideoClassIdOnUserVideoViewCounters < ActiveRecord::Migration
  def self.up
    remove_foreign_key :user_video_view_counters, :user_video_view_counters_course_id_fkey
    rename_column :user_video_view_counters, :course_id, :video_class_id
    add_foreign_key :user_video_view_counters, :video_class_id, :produtos, :id, :on_delete => :cascade
  end

  def self.down
    remove_foreign_key :user_video_view_counters, :user_video_view_counters_video_class_id_fkey
    rename_column :user_video_view_counters, :video_class_id, :course_id
    add_foreign_key :user_video_view_counters, :course_id, :produtos, :id, :on_delete => :cascade
  end
end
