class CreateUserSimulatedTests < ActiveRecord::Migration
  def self.up
		create_table :user_simulated_tests do |t|
			t.integer :user_id, :unique => false, :null => false

			1.upto(80) do |i|
				t.string :"answer_#{i}", :null => false
			end

			t.timestamps
		end
		add_foreign_key(:user_simulated_tests, :user_id, :users, :id)
  end

  def self.down
		remove_foreign_key(:user_simulated_tests, :user_simulated_tests_user_id_fkey)
		drop_table :user_simulated_tests
  end
end
