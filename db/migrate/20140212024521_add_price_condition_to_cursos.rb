class AddPriceConditionToCursos < ActiveRecord::Migration
  def self.up
    add_column :cursos, :price_condition, :string
  end

  def self.down
    remove_column :cursos, :price_condition
  end
end
