class AddRecordedVideoUploadIdToTickets < ActiveRecord::Migration
  def self.up
    add_column :produtos, :recorded_video_upload_id, :integer
    add_foreign_key(:produtos, :recorded_video_upload_id, :video_uploads, :id)
  end

  def self.down
    remove_foreign_key(:produtos, :produtos_recorded_video_upload_id_fkey)
    remove_column :produtos, :recorded_video_upload_id
  end
end
