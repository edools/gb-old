class ChangePriceConditionTypeOnProdutos < ActiveRecord::Migration
  def self.up
    change_column :produtos, :price_condition, :text
  end

  def self.down
    change_column :produtos, :price_condition, :string
  end
end
