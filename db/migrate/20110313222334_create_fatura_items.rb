class CreateFaturaItems < ActiveRecord::Migration
  def self.up
    create_table :fatura_items do |t|
      t.decimal :price
      t.integer :quantity, :default => 1
      t.references :fatura
      t.references :video_aula      
      t.timestamps
    end
    
    add_foreign_key(:fatura_items, :fatura_id, :faturas, :id)
    add_foreign_key(:fatura_items, :video_aula_id, :video_aulas, :id)
  end

  def self.down
    remove_foreign_key(:fatura_items, :fatura_items_fatura_id_fkey)
    remove_foreign_key(:fatura_items, :fatura_items_video_aula_id_fkey)
    
    drop_table :fatura_items
  end
end
