class AddPagseguroInfoToFaturas < ActiveRecord::Migration
  def self.up
    add_column :faturas, :pagseguro_info, :text
  end

  def self.down
    remove_column :faturas, :pagseguro_info
  end
end
