class AddRoleCliente < ActiveRecord::Migration
  def self.up
    say "Role cliente criada com sucesso!" if Role.create(:name => "cliente")
  end

  def self.down
    say "Role cliente removida com sucesso!" if Role.destroy(Role.find_by_name("cliente"))
  end
end
