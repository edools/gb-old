class CreateCursosProfessors < ActiveRecord::Migration
  def self.up
    create_table :cursos_professors do |t|
      t.references :curso
      t.references :professor
    end
  end

  def self.down
    drop_table :cursos_professors
  end
end
