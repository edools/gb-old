class AddVideoSplashImagemToDepoimentos < ActiveRecord::Migration
  def self.up
    add_column :depoimentos, :video_splash_file_name, :string
    add_column :depoimentos, :video_splash_content_type, :string
    add_column :depoimentos, :video_splash_file_size, :integer
    add_column :depoimentos, :video_splash_updated_at, :string
  end

  def self.down
    remove_column :depoimentos, :video_splash_file_name
    remove_column :depoimentos, :video_splash_content_type
    remove_column :depoimentos, :video_splash_file_size
    remove_column :depoimentos, :video_splash_updated_at
  end
end
