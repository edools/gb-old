class AddEnableDownloadToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :enable_download, :boolean
  end

  def self.down
    remove_column :produtos, :enable_download
  end
end
