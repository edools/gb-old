class AddSimuladoNewTypeToVideoUploadTipos < ActiveRecord::Migration
  def self.up
    VideoUploadTipo.create(:nome => "Simulado", :identificador => "simulado")
  end

  def self.down
    VideoUploadTipo.destroy(VideoUploadTipo.find_by_identificador("simulado"))
  end
end
