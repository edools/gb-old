class CreateArtigoAnexos < ActiveRecord::Migration
  def self.up
    create_table :artigo_anexos do |t|
      t.string :arquivo_file_name
      t.string :arquivo_content_type
      t.string :arquivo_file_size
      t.datetime :arquivo_updated_at
      t.string :legenda
      t.references :artigo

      t.timestamps
    end

    add_foreign_key(:artigo_anexos, :artigo_id, :artigos, :id)
  end

  def self.down
    remove_foreign_key(:artigo_anexos, :artigo_anexos_artigo_id_fkey)
    drop_table :artigo_anexos
  end
end
