class RenameVideoAulasProfessorsTable < ActiveRecord::Migration
  def self.up
    rename_table :video_aulas_professors, :produtos_professors
  end

  def self.down
    rename_table :produtos_professors, :video_aulas_professors
  end
end
