class CreateVideoUploadTemps < ActiveRecord::Migration
  def self.up
    create_table :video_upload_temps do |t|
      t.string   :local_file_name
      t.string   :local_content_type
      t.integer  :local_file_size
      t.datetime :local_updated_at
      t.timestamps
    end
  end

  def self.down
    drop_table :video_upload_temps
  end
end
