class AddVideoUploadSimulatedTestQuestions < ActiveRecord::Migration
  def self.up
    change_table(:simulated_test_questions) do |t|
      t.belongs_to :video_upload
    end
    add_foreign_key(:simulated_test_questions, :video_upload_id, :video_uploads, :id)
  end

  def self.down
    remove_foreign_key(:video_aulas, :simulated_test_questions_video_upload_id_fkey)
    remove_column(:simulated_test_questions, :video_upload_id)
  end
end
