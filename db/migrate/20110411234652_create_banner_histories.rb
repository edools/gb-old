class CreateBannerHistories < ActiveRecord::Migration
  def self.up
    create_table :banner_histories do |t|
      t.references :banner, :null => false
      t.string :ip, :limit => 50
      t.boolean :tipo, :default => true

      t.timestamps
    end

    add_foreign_key(:banner_histories, :banner_id, :banners, :id)
  end

  def self.down
    remove_foreign_key(:banner_histories, :banner_histories_banner_id_fkey)
    drop_table :banner_histories
  end
end
