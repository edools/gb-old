class RemoveColumnsFromVideoUploads < ActiveRecord::Migration
  def self.up
    remove_column :video_uploads, :is_public
    remove_column :video_uploads, :local_file_name
    remove_column :video_uploads, :local_content_type
    remove_column :video_uploads, :local_file_size
    remove_column :video_uploads, :local_updated_at
    remove_column :video_uploads, :status
    remove_column :video_uploads, :status_progress
    remove_column :video_uploads, :width
    remove_column :video_uploads, :height
  end

  def self.down
    add_column :video_uploads, :is_public, :boolean
    add_column :video_uploads, :local_file_name, :string
    add_column :video_uploads, :local_content_type, :string
    add_column :video_uploads, :local_file_size, :integer
    add_column :video_uploads, :local_updated_at, :datetime
    add_column :video_uploads, :status, :string, :null => false
    add_column :video_uploads, :status_progress, :float, :default => 0, :null => false
    add_column :video_uploads, :width, :string
    add_column :video_uploads, :height, :string
  end
end
