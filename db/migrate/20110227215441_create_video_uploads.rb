class CreateVideoUploads < ActiveRecord::Migration
  def self.up
    create_table :video_uploads, :force => true do |t|
      t.integer  :creator_id
      t.string   :nome
      t.string   :caption,        :limit => 1000
      t.text     :description
      t.boolean  :is_public,      :default => true
      t.string   :width   
      t.string   :height
      t.integer  :duracao, :default => 0
      t.references :video_upload_tipo
      t.references :categoria_curso
      
      t.string   :local_file_name
      t.string   :local_content_type
      t.integer  :local_file_size
      t.datetime :local_updated_at
      
      t.string   :streaming_file_name
      t.string   :streaming_content_type
      t.integer  :streaming_file_size
      t.datetime :streaming_updated_at
      
      t.string :status, :null => false
      t.float :status_progress, :default => 0, :null => false
            
      t.timestamps
    end

    add_foreign_key(:video_uploads, :video_upload_tipo_id, :video_upload_tipos, :id)
    add_foreign_key(:video_uploads, :categoria_curso_id, :categoria_cursos, :id)
  end

  def self.down
    remove_foreign_key(:video_uploads, :video_uploads_categoria_curso_id_fkey)
    remove_foreign_key(:video_uploads, :video_uploads_video_upload_tipo_id_fkey)
    drop_table :video_uploads
  end
end