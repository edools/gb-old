class AddExpiraAtToFaturaItems < ActiveRecord::Migration
  def self.up
    # quando uma fatura for aprovada, esse campo guarda a data que o item da fatura expira
    add_column :fatura_items, :expira_at, :date
  end

  def self.down
    remove_column :fatura_items, :expira_at
  end
end