class CreateSimulatedTestProducts < ActiveRecord::Migration
  def self.up
    create_table :simulated_test_products do |t|
      t.integer :simulated_test_id, :null => false
      t.integer :product_id, :null => false
      t.timestamps
    end

    add_foreign_key(:simulated_test_products, :simulated_test_id, :produtos, :id, :on_delete => :cascade)
    add_foreign_key(:simulated_test_products, :product_id, :produtos, :id, :on_delete => :cascade)
  end

  def self.down
    remove_foreign_key(:simulated_test_products, :simulated_test_products_simulated_test_id_fkey)
    remove_foreign_key(:simulated_test_products, :simulated_test_products_product_id_fkey)

    drop_table :simulated_test_products
  end
end
