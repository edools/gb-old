class CreateImagems < ActiveRecord::Migration
  def self.up
    create_table :imagems do |t|
      t.string :legenda
      t.references :user, :null => false
      t.string :imagem_file_name
      t.string :imagem_content_type
      t.integer :imagem_file_size
      t.datetime :imagem_updated_at

      t.timestamps
    end
    add_foreign_key(:imagems, :user_id, :users, :id)
  end

  def self.down
    drop_table :imagems
  end
end