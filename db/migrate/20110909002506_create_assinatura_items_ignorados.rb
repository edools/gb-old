class CreateAssinaturaItemsIgnorados < ActiveRecord::Migration
  def self.up
    create_table :assinatura_items_ignorados do |t|
      t.references :produto
      t.references :assinatura
    end
    add_foreign_key :assinatura_items_ignorados, :produto_id, :produtos, :id
    add_foreign_key :assinatura_items_ignorados, :assinatura_id, :produtos, :id
  end

  def self.down
    drop_table :assinatura_items_ignorados
  end
end
