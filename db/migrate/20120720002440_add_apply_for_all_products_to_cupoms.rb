class AddApplyForAllProductsToCupoms < ActiveRecord::Migration
  def self.up
    add_column :cupoms, :apply_for_all_cursos_online, :boolean
    add_column :cupoms, :apply_for_all_cursos_em_pdf, :boolean
    add_column :cupoms, :apply_for_all_pacotes, :boolean
    add_column :cupoms, :apply_for_all_assinaturas, :boolean
    change_column :cupoms, :produto_id, :integer, :null => true
  end

  def self.down
    remove_column :cupoms, :apply_for_all_cursos_online
    remove_column :cupoms, :apply_for_all_cursos_em_pdf
    remove_column :cupoms, :apply_for_all_pacotes
    remove_column :cupoms, :apply_for_all_assinaturas
    change_column :cupoms, :produto_id, :integer, :null => false
  end
end
