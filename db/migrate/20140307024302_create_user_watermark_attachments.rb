class CreateUserWatermarkAttachments < ActiveRecord::Migration
  def self.up
    create_table :user_watermark_attachments do |t|
      t.integer :user_id, :null => false
      t.string :job_id, :null => false
      t.integer :base_product_attachment_id, :null => false
      t.string :watermarked_attachment_file_name, :null => false
      t.string :watermarked_attachment_content_type
      t.integer :watermarked_attachment_file_size
      t.datetime :watermarked_attachment_updated_at

      t.timestamps
    end

    add_foreign_key(:user_watermark_attachments, :base_product_attachment_id, :produto_anexos, :id, :on_delete => :cascade)
    add_foreign_key(:user_watermark_attachments, :user_id, :users, :id, :on_delete => :cascade)
  end

  def self.down
    remove_foreign_key(:user_watermark_attachments, :user_watermark_attachments_base_product_attachment_id_fkey)
    remove_foreign_key(:user_watermark_attachments, :user_watermark_attachments_user_id_fkey)
    drop_table :user_watermark_attachments
  end
end
