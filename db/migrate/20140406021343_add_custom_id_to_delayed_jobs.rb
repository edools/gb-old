class AddCustomIdToDelayedJobs < ActiveRecord::Migration
  def self.up
    add_column :delayed_jobs, :custom_id, :string
    add_index :delayed_jobs, :custom_id
  end

  def self.down
    remove_index :delayed_jobs, :custom_id
    remove_column :delayed_jobs, :custom_id
  end
end
