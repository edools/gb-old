class AddInstrucoesToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :instrucoes, :text
  end

  def self.down
    remove_column :produtos, :instrucoes
  end
end
