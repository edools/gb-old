class CreateSimulatedTestQuestions < ActiveRecord::Migration
  def self.up
    create_table :simulated_test_questions do |t|
      t.integer :simulated_test_id, :null => false
      t.text :question
      t.text :answer
      t.string :answer_option
      t.integer :position, :null => false
      t.timestamps
    end
    add_index :simulated_test_questions, :position
    add_foreign_key(:simulated_test_questions, :simulated_test_id, :produtos, :id, :on_delete => :cascade)
  end

  def self.down
    remove_index :simulated_test_questions, :position
    remove_foreign_key(:simulated_test_questions, :simulated_test_questions_simulated_test_id_fkey)

    drop_table :simulated_test_questions
  end
end
