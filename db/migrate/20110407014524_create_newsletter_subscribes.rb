class CreateNewsletterSubscribes < ActiveRecord::Migration
  def self.up
    create_table :newsletter_subscribes do |t|
      t.string :nome, :null => false
      t.string :email, :null => false
      t.references :categoria_newsletter_subscribe, :null => false

      t.timestamps
    end

    add_foreign_key(:newsletter_subscribes, :categoria_newsletter_subscribe_id, :categoria_newsletter_subscribes, :id)
  end

  def self.down
    remove_foreign_key(:newsletter_subscribes, :newsletter_subscribes_categoria_newsletter_subscribe_id_fkey)
    drop_table :newsletter_subscribes
  end
end
