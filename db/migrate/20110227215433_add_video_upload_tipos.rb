class AddVideoUploadTipos < ActiveRecord::Migration
  def self.up
    VideoUploadTipo.create(:nome => "Completo", :identificador => "completo")
    VideoUploadTipo.create(:nome => "Demonstração", :identificador => "demonstracao")
  end

  def self.down
    VideoUploadTipo.destroy(VideoUploadTipo.find_by_identificador("completo"))
    VideoUploadTipo.destroy(VideoUploadTipo.find_by_identificador("demonstracao"))
  end
end
