class AddMandatoryToSimulatedTestQuestions < ActiveRecord::Migration
  def self.up
    add_column :simulated_test_questions, :mandatory, :boolean, :default => true
  end

  def self.down
    remove_column :simulated_test_questions, :mandatory
  end
end
