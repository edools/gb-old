class AddIndexesToProdutos < ActiveRecord::Migration
  def self.up
    add_index :produtos, :ativo
    add_index :produtos, :disponivel_venda
    add_index :produtos, :categoria_curso_id
  end

  def self.down
    remove_index :produtos, :ativo
    remove_index :produtos, :disponivel_venda
    remove_index :produtos, :categoria_curso_id
  end
end
