class AddStartDatetimeIndexToProdutos < ActiveRecord::Migration
  def self.up
    add_index :produtos, :start_datetime
  end

  def self.down
    remove_index :produtos, :start_datetime
  end
end
