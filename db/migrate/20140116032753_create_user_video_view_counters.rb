class CreateUserVideoViewCounters < ActiveRecord::Migration
  def self.up
    create_table :user_video_view_counters do |t|
      t.integer :user_id, :null => false
      t.integer :video_id, :null => false
      t.integer :course_id, :null => false
      t.integer :counter, :null => false, :default => 0
      t.integer :aditional_views, :null => false, :default => 0

      t.timestamps
    end

    add_foreign_key :user_video_view_counters, :user_id, :users, :id, :on_delete => :cascade
    add_foreign_key :user_video_view_counters, :video_id, :video_uploads, :id, :on_delete => :cascade
    add_foreign_key :user_video_view_counters, :course_id, :produtos, :id, :on_delete => :cascade
    add_index :user_video_view_counters, :counter
  end

  def self.down
    remove_foreign_key :user_video_view_counters, :user_video_view_counters_user_id_fkey
    remove_foreign_key :user_video_view_counters, :user_video_view_counters_video_id_fkey
    remove_foreign_key :user_video_view_counters, :user_video_view_counters_course_id_fkey
    drop_table :user_video_view_counters
  end
end
