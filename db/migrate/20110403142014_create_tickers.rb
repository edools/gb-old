class CreateTickers < ActiveRecord::Migration
  def self.up
    create_table :tickers do |t|
      t.string :imagem_file_name
      t.string :imagem_content_type
      t.integer :imagem_file_size
      t.datetime :imagem_updated_at
      t.string :link
      t.string :descricao, :limit => 80

      t.timestamps
    end
  end

  def self.down
    drop_table :tickers
  end
end
