class AddMaterialApoioToVideoAulas < ActiveRecord::Migration
  def self.up
    create_table :produto_anexos do |t|
      t.string   :arquivo_file_name
      t.string   :arquivo_content_type
      t.integer  :arquivo_file_size
      t.datetime :arquivo_updated_at

      t.string   :legenda

      t.references :produto

      t.timestamps
    end

    add_foreign_key(:produto_anexos, :produto_id, :produtos, :id)
  end

  def self.down
    remove_foreign_key(:produto_anexos, :produto_anexos_produto_id_fkey)
    drop_table :curso_anexos
  end
end
