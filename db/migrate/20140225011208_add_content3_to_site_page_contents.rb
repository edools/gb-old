class AddContent3ToSitePageContents < ActiveRecord::Migration
  def self.up
    add_column :site_page_contents, :content_3, :text
  end

  def self.down
    add_column :site_page_contents, :content_3, :text
  end
end
