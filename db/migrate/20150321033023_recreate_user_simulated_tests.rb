class RecreateUserSimulatedTests < ActiveRecord::Migration
  def self.up
    create_table :user_simulated_tests do |t|
      t.integer :simulated_test_id, :null => false
      t.integer :user_id, :null => false
      t.timestamps
    end
    add_foreign_key(:user_simulated_tests, :simulated_test_id, :produtos, :id)
    add_foreign_key(:user_simulated_tests, :user_id, :users, :id)
  end

  def self.down
    remove_foreign_key(:user_simulated_tests, :user_simulated_tests_user_id_fkey)
    remove_foreign_key(:user_simulated_tests, :user_simulated_tests_simulated_tests_id_fkey)

    drop_table :user_simulated_tests
  end
end
