class CreateBanners < ActiveRecord::Migration
  def self.up
    create_table :banners do |t|
      t.string :descricao
      t.integer :tipo, :null => false, :default => 1
      t.integer :posicao, :null => false, :default => 1
      t.boolean :visivel, :default => false
      t.integer :limite_cliques
      t.integer :limite_visualizacoes
      t.datetime :limite_data
      t.string :url, :string
      t.boolean :target, :default => true
      t.integer :total_cliques, :default => 0
      t.integer :total_visualizacoes, :default => 0
      t.string :arquivo_file_name
      t.string :arquivo_content_type
      t.integer :arquivo_file_size
      t.datetime :arquivo_updated_at      

      t.timestamps
    end
  end

  def self.down
    drop_table :banners
  end
end
