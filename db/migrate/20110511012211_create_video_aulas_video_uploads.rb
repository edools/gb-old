class CreateVideoAulasVideoUploads < ActiveRecord::Migration
  def self.up
    create_table :video_aulas_video_uploads do |t|
      t.integer :video_aula_id
      t.references :video_upload
    end

    add_foreign_key(:video_aulas_video_uploads, :video_aula_id, :produtos, :id)
    add_foreign_key(:video_aulas_video_uploads, :video_upload_id, :video_uploads, :id)
  end

  def self.down
    remove_foreign_key(:video_aulas_video_uploads, :video_aulas_video_uploads_video_aula_id_fkey)
    remove_foreign_key(:video_aulas_video_uploads, :video_aulas_video_uploads_video_upload_id_fkey)

    drop_table :video_aulas_video_uploads
  end
end
