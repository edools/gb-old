class AddTelefonesToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :telefone_fixo, :string, :limit => 13
    add_column :users, :telefone_celular, :string, :limit => 13
  end

  def self.down
    remove_column :users, :telefone_celular
    remove_column :users, :telefone_fixo
  end
end
