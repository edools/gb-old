class AddAnswerOptionToSimulatedTests < ActiveRecord::Migration
  def self.up
		1.upto(80) do |i|
			add_column :simulated_tests, :"answer_option_#{i}", :string
		end
  end

  def self.down
		1.upto(80) do |i|
			remove_column :simulated_tests, :"answer_option_#{i}"
		end
  end
end
