class RenameAplicarTodosCursosColumnFromDescontos < ActiveRecord::Migration
  def self.up
    rename_column :descontos, :aplicar_todos_cursos, :aplicar_todos_items
  end

  def self.down
    rename_column :descontos, :aplicar_todos_items, :aplicar_todos_cursos
  end
end
