class AddStartDatetimeToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :start_datetime, :datetime
  end

  def self.down
    remove_column :produtos, :start_datetime
  end
end
