class RemoveVideoUploadFromVideoAulas < ActiveRecord::Migration
  def self.up
    remove_column :produtos, :video_upload_id
  end

  def self.down
    add_column :produtos, :video_upload, :integer
  end
end
