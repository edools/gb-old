class AddMaisCamposToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :ativo, :boolean, :default => true, :null => false
    add_column :produtos, :carga_horaria, :string
    add_column :produtos, :numero_videos, :string
  end

  def self.down
    remove_column :produtos, :numero_videos
    remove_column :produtos, :carga_horaria
    remove_column :produtos, :ativo
  end
end
