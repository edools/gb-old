class DropVideoUploadTemps < ActiveRecord::Migration
  def self.up
    drop_table :video_upload_temps
  end

  def self.down
    create_table "video_upload_temps", :force => true do |t|
      t.string   "local_file_name"
      t.string   "local_content_type"
      t.integer  "local_file_size"
      t.datetime "local_updated_at"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end

end
