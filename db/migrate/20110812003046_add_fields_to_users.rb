class AddFieldsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :nascimento, :string, :null => true, :limit => 10
    add_column :users, :complemento, :string, :null => true, :limit => 100
    add_column :users, :cep, :string, :null => true, :limit => 10
    add_column :users, :bairro, :string, :null => true, :limit => 100
    add_column :users, :estado, :string, :null => true, :limit => 2
    add_column :users, :cidade, :string, :null => true, :limit => 200
    
    
  end

  def self.down
    remove_column :users, :nascimento
    remove_column :users, :complemento
    remove_column :users, :cep
    remove_column :users, :bairro
    remove_column :users, :estado
    remove_column :users, :cidade
  end
end
