class AddFieldsToCursos < ActiveRecord::Migration
  def self.up
    add_column :cursos, :vacancies, :string
    add_column :cursos, :weekdays, :string
  end

  def self.down
    remove_column :cursos, :vacancies
    remove_column :cursos, :weekdays
  end
end
