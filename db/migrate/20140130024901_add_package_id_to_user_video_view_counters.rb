class AddPackageIdToUserVideoViewCounters < ActiveRecord::Migration
  def self.up
    add_column :user_video_view_counters, :package_id, :integer
    add_foreign_key :user_video_view_counters, :package_id, :produtos, :id, :on_delete => :cascade
  end

  def self.down
    remove_foreign_key :user_video_view_counters, :user_video_view_counters_package_id_fkey
    remove_column :user_video_view_counters, :package_id
  end
end
