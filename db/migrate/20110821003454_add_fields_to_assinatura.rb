class AddFieldsToAssinatura < ActiveRecord::Migration
  def self.up
    add_column :produtos, :periodicidade, :string
  end

  def self.down
    remove_column :produtos, :periodicidade
  end
end
