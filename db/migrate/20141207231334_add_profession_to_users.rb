class AddProfessionToUsers < ActiveRecord::Migration
  def self.up
		add_column :users, :profissao, :string, :null => true
  end

  def self.down
		remove_column :users, :profissao
  end
end
