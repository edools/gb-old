class CreateRolesAdmins < ActiveRecord::Migration
  def self.up
    user = execute("SELECT id FROM users WHERE login='admin' LIMIT 1;").first
    execute("SELECT id FROM roles;").each do |role|
      execute "INSERT INTO roles_users(role_id, user_id) VALUES(#{role['id']}, #{user['id']});"
    end
    say "Roles atribuidas a admin com sucesso!"
  end

  def self.down
    user = execute("SELECT id FROM users WHERE login='admin' LIMIT 1;").first
    execute "DELETE FROM roles_users WHERE user_id=#{user['id']};"
    say "Roles removidas de admin com sucesso!"
  end
end
