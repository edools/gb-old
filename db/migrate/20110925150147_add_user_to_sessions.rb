class AddUserToSessions < ActiveRecord::Migration
  def self.up
    add_column :sessions, :user_id, :integer, :null => true
    add_column :sessions, :ip, :string, :null => true
    #add_foreign_key :sessions, :user_id, :users, :id
  end

  def self.down
    #remove_foreign_key :sessions, :sessions_user_id_fkey
    remove_column :sessions, :user_id
    remove_column :sessions, :ip
  end
end
