class CreateSitePageContents < ActiveRecord::Migration
  def self.up
    create_table :site_page_contents do |t|
      t.text :content
      t.string :name, :null => false
      t.index :name, :unique => true
      t.timestamps
    end
  end

  def self.down
    drop_table :site_page_contents
  end
end
