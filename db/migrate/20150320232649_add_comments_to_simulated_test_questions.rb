class AddCommentsToSimulatedTestQuestions < ActiveRecord::Migration
  def self.up
    add_column :simulated_test_questions, :comments, :text
  end

  def self.down
    remove_column :simulated_test_questions, :comments
  end
end
