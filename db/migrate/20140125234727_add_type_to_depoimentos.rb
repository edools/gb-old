class AddTypeToDepoimentos < ActiveRecord::Migration
  def self.up
    add_column :depoimentos, :testimonial_type, :string, :default => "text", :null => false
  end

  def self.down
    remove_column :depoimentos, :testimonial_type
  end
end
