class AddCodigoEmbedToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :codigo_embed, :text
  end

  def self.down
    remove_column :produtos, :codigo_embed
  end
end
