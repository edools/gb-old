class AddPositionToVideoAulasVideoUploads < ActiveRecord::Migration
  def self.up
    add_column :video_aulas_video_uploads, :position, :integer, :null => false, :default => 0
  end

  def self.down
    remove_column :video_aulas_video_uploads, :position
  end
end
