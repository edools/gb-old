class AddDisponivelVendaToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :disponivel_venda, :boolean, :default => true, :null => false
  end

  def self.down
    remove_column :produtos, :disponivel_venda
  end
end
