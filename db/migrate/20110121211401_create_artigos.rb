class CreateArtigos < ActiveRecord::Migration
  def self.up
    create_table :artigos do |t|
      t.string :nome, :null => false, :limit => 100
      t.string :descricao, :limit => 255
      t.references :professor
      t.text :corpo
      t.integer :visualizacoes, :default => 0

      t.timestamps
    end

    add_foreign_key(:artigos, :professor_id, :professors, :id)
  end

  def self.down
    remove_foreign_key(:artigos, :artigos_professor_id_fkey)
    drop_table :artigos
  end
end
