class AddAvailablePeriodToTickets < ActiveRecord::Migration
  def self.up
    add_column :produtos, :available_from, :date
    add_column :produtos, :available_to, :date
  end

  def self.down
    remove_column :produtos, :available_from
    remove_column :produtos, :available_to
  end
end
