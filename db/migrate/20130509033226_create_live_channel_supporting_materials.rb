class CreateLiveChannelSupportingMaterials < ActiveRecord::Migration
  def self.up
    create_table :live_channel_supporting_materials do |t|
	    t.string   :attachment_file_name
      t.string   :attachment_content_type
      t.integer  :attachment_file_size
      t.datetime :attachment_updated_at

      t.string   :label

      t.references :live_channel

      t.timestamps
    end

    add_foreign_key(:live_channel_supporting_materials, :live_channel_id, :live_channels, :id)
  end

  def self.down
  	remove_foreign_key(:live_channel_supporting_materials, :live_channel_supporting_materials_live_channel_id_fkey)
    drop_table :live_channel_supporting_materials
  end
end
