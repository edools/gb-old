class AddAncestryToCategoriaCursos < ActiveRecord::Migration
  def self.up
    add_column :categoria_cursos, :ancestry, :string
    add_index :categoria_cursos, :ancestry

  end

  def self.down
    remove_index :categoria_cursos, :ancestry
    remove_column :categoria_cursos, :ancestry
  end
end
