class AddQuestionTypeToSimulatedTestQuestions < ActiveRecord::Migration
  def self.up
    add_column :simulated_test_questions, :question_type, :string
  end

  def self.down
    remove_column :simulated_test_questions, :question_type
  end
end
