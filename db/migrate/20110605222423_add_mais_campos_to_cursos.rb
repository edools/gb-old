class AddMaisCamposToCursos < ActiveRecord::Migration
  def self.up
    add_column :cursos, :ativo, :boolean, :default => true, :null => false
    add_column :cursos, :carga_horaria, :string
    add_column :cursos, :numero_encontros, :string
    add_column :cursos, :publico_alvo, :string
  end

  def self.down
    remove_column :cursos, :publico_alvo
    remove_column :cursos, :numero_encontros
    remove_column :cursos, :carga_horaria
    remove_column :cursos, :ativo
  end
end
