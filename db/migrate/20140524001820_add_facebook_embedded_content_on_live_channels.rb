class AddFacebookEmbeddedContentOnLiveChannels < ActiveRecord::Migration
  def self.up
    add_column :live_channels, :facebook_embedded_content, :text
  end

  def self.down
    remove_column :live_channels, :facebook_embedded_content
  end
end
