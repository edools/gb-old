class CreateLiveChannels < ActiveRecord::Migration
  def self.up
    create_table :live_channels do |t|
      t.text :embedded_content
      t.boolean :show_supporting_materials

      t.timestamps
    end
  end

  def self.down
    drop_table :live_channels
  end
end
