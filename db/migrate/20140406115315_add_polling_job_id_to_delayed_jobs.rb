class AddPollingJobIdToDelayedJobs < ActiveRecord::Migration
  def self.up
    add_column :delayed_jobs, :polling_job_id, :string
  end

  def self.down
    remove_column :delayed_jobs, :polling_job_id
  end
end
