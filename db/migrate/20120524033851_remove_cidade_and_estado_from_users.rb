class RemoveCidadeAndEstadoFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :cidade
    remove_column :users, :estado
  end

  def self.down
    add_column :users, :estado, :string
    add_column :users, :cidade, :string
  end
end
