class AddUserSimulatedTestsUniqueIndexes < ActiveRecord::Migration
  def self.up
    add_index(:user_simulated_tests, [:user_id, :simulated_test_id], :unique => true, :name => 'user_simulated_tests_user_id_simulated_test_id_unique_index')
    add_index(:user_simulated_test_answers, [:user_simulated_test_id, :simulated_test_question_id], :unique => true, :name => 'user_simulated_test_answers_ust_id_stq_id_unique_idx')
  end

  def self.down
    remove_index(:user_simulated_tests,'user_simulated_tests_user_id_simulated_test_id_unique_index')
    remove_index(:user_simulated_test_answers,'user_simulated_test_answers_ust_id_stq_id_unique_idx')
  end
end
