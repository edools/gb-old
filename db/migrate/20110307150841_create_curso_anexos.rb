class CreateCursoAnexos < ActiveRecord::Migration
  def self.up
    create_table :curso_anexos do |t|
      t.string   :arquivo_file_name
      t.string   :arquivo_content_type
      t.integer  :arquivo_file_size
      t.datetime :arquivo_updated_at

      t.string   :legenda

      t.references :curso

      t.timestamps
    end

    add_foreign_key(:curso_anexos, :curso_id, :cursos, :id)
  end

  def self.down
    remove_foreign_key(:curso_anexos, :curso_anexos_curso_id_fkey)
    drop_table :curso_anexos
  end
end