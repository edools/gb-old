class AddContent2ToSitePageContents < ActiveRecord::Migration
  def self.up
    add_column :site_page_contents, :content_2, :text
  end

  def self.down
    remove_column :site_page_contents, :content_2
  end
end
