class AddDemonstracaoToProdutoAnexo < ActiveRecord::Migration
  def self.up
    add_column :produto_anexos, :demonstracao, :boolean, :default => false
    ProdutoAnexo.update_all ["demonstracao = ?", false]
  end

  def self.down
    remove_column :produto_anexos, :demonstracao
  end
end
