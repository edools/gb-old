class AddPriceConditionToProdutos < ActiveRecord::Migration
  def self.up
    add_column :produtos, :price_condition, :string
  end

  def self.down
    remove_column :produtos, :price_condition
  end
end
