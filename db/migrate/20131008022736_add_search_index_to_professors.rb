class AddSearchIndexToProfessors < ActiveRecord::Migration
  def self.up
    add_index :professors, :nome
  end

  def self.down
    remove_index :professors, :nome
  end
end
