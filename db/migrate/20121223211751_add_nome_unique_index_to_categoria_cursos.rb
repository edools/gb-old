class AddNomeUniqueIndexToCategoriaCursos < ActiveRecord::Migration
  def self.up
    add_index :categoria_cursos, :nome, :unique => true
  end

  def self.down
    remove_index :categoria_cursos, :nome
  end
end
