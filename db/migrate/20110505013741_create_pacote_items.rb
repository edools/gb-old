class CreatePacoteItems < ActiveRecord::Migration
  def self.up
    create_table :pacote_items do |t|
      t.integer :pacote_id, :null => false
      t.integer :video_aula_id, :null => false

      t.timestamps
    end

    add_foreign_key(:pacote_items, :pacote_id, :produtos, :id)
    add_foreign_key(:pacote_items, :video_aula_id, :produtos, :id)
  end

  def self.down
    remove_foreign_key(:pacote_items, :pacote_items_pacote_id_fkey)
    remove_foreign_key(:pacote_items, :pacote_items_video_aula_id_fkey)

    drop_table :pacote_items
  end
end
