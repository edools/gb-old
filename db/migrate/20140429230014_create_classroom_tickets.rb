class CreateClassroomTickets < ActiveRecord::Migration
  def self.up
    create_table :classroom_tickets do |t|
      t.integer :user_id, :null => false
      t.integer :classroom_event_id, :null => false
      t.string :code, :null => false
      t.string :status, :null => false
      t.timestamps
    end

    add_index :classroom_tickets, :code
    add_index :classroom_tickets, :user_id
    add_index :classroom_tickets, :classroom_event_id
    add_index :classroom_tickets, :status

    add_foreign_key :classroom_tickets, :user_id, :users, :id
    add_foreign_key :classroom_tickets, :classroom_event_id, :produtos, :id
  end

  def self.down
    remove_foreign_key(:classroom_tickets, :classroom_tickets_user_id_fkey)
    remove_foreign_key(:classroom_tickets, :classroom_tickets_classroom_event_id_fkey)
    remove_index :classroom_tickets, :code
    remove_index :classroom_tickets, :user_id
    remove_index :classroom_tickets, :classroom_event_id
    remove_index :classroom_tickets, :status
    drop_table :classroom_tickets
  end
end