class AddAdminUser < ActiveRecord::Migration
  def self.up
    say "Adicionando usuário 'admin'"
    u = User.new( :login => "admin", :name => 'Promine', :email => "admin@promine.com.br", :password => "asdfasdf", :admin => true )
    u.save(false)
  end

  def self.down
    say "Removendo usuário 'admin'"
    u = User.find_by_login "admin"
    u.destroy
  end
end