class AddCategoryToTickers < ActiveRecord::Migration
  def self.up
    add_column :tickers, :category, :string, :null => false, :default => 'online'
  end

  def self.down
    remove_column :tickers, :category
  end
end
