class CreateDescontoAssinaturaItems < ActiveRecord::Migration
  def self.up
    create_table :desconto_assinatura_items do |t|
      t.references :desconto, :null => false
      t.references :produto, :null => false
    end

    add_foreign_key :desconto_assinatura_items, :desconto_id, :descontos, :id
    add_foreign_key :desconto_assinatura_items, :produto_id, :produtos, :id
  end

  def self.down
    drop_table :desconto_assinatura_items
  end
end
