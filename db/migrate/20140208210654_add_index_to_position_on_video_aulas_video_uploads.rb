class AddIndexToPositionOnVideoAulasVideoUploads < ActiveRecord::Migration
  def self.up
    add_index :video_aulas_video_uploads, :position
  end

  def self.down
    remove_index :video_aulas_video_uploads, :position
  end
end
