FactoryGirl.define do
  factory :customer_user, :class => User do
    sequence(:login) {|n| "foo#{n}" }
    name "foo bar"
    sequence(:email) {|n| "foo#{n}@mail.com" }
    cpf { Faker::CPF.cpf }
    password "asdfasdf"
    password_confirmation "asdfasdf"
    nascimento "06/06/1986"
    endereco "foo"
    bairro "foo"
    cep "64.009-444"
    estado { Estado.find_by_sigla("CE") }
    cidade { Cidade.find_by_nome("Fortaleza") }
    aceite_termos_de_servico "1"
    after_build { |user| user.roles << Role.find_by_name(Role::CUSTOMER) }
  end

  factory :admin_user, :class => User do
    sequence(:login) {|n| "foo#{n}" }
    name "foo bar"
    sequence(:email) {|n| "foo#{n}@mail.com" }
    cpf { Faker::CPF.cpf }
    password "asdfasdf"
    password_confirmation "asdfasdf"
    nascimento "06/06/1986"
    endereco "foo"
    bairro "foo"
    cep "64.009-444"
    estado { Estado.find_by_sigla("CE") }
    cidade { Cidade.find_by_nome("Fortaleza") }
    aceite_termos_de_servico "1"
    after_build { |user| user.roles << Role.find_by_name(Role::ADMINISTRATOR)}
  end
end