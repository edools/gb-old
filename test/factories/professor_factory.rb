FactoryGirl.define do
  factory :professor do
    nome "Joao de Paula"
    email "joaodopaula@email.com"
    site "http://joao.depaula.com"
    curriculo "Curriculo do Joao"
  end
end