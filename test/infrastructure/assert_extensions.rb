module AssertExtensions
  def assert_true(actual, message = nil)
      assert_equal true, actual, message
  end

  def assert_false(actual, message = nil)
      assert_equal false, actual, message
  end

  def assert_error_on(model, field, options = {})
    assert_false model.valid?, "expected the model object to not be valid"
    if options.is_a?(Hash) && options.has_key?(:i18n)
      expected_error_message = I18n.t(*options[:i18n])
    elsif options.is_a?(String)
      expected_error_message = options
    end
    if expected_error_message
      actual_error_messages = Array(model.errors.on(field))

      assert actual_error_messages.include?(expected_error_message), "expected validation error message to include '#{expected_error_message}', contains '#{actual_error_messages.inspect}'"
    else
      assert_false model.errors.on(field).blank?, "expected error on #{field} attribute, but none was found"
    end
  end

  def assert_no_error_on(model, field, options = {})
    model.valid?

    actual_error_messages = Array(model.errors.on(field))

    if options[:i18n]
      expected_error_message = I18n.t(*options[:i18n])
      assert_false actual_error_messages.include?(expected_error_message), "not expecting validation error message to include #{expected_error_message}, contains '#{actual_error_messages.inspect}'"
    else
      assert_blank actual_error_messages
    end
  end
end