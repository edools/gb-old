require File.dirname(__FILE__) + "/../test_helper"

class FunctionalTestCase < ActiveSupport::TestCase
  include AssertExtensions
  include FactoryGirl::Syntax::Methods

  setup :load_seeds

  private
  def load_seeds
    load File.dirname(__FILE__) + "/../seeds.rb"
  end
end