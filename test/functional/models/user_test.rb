require File.dirname(__FILE__) + "/../test_functional_helper"

class UserTest < FunctionalTestCase
  should "be also customer when is admin" do
    user = create(:admin_user)
    assert_equal 1, user.roles.count
    assert_equal Role::ADMINISTRATOR, user.roles.first.name
    assert user.is_cliente?
  end

  context "validations" do
    context "login" do
      should "not be valid when is not unique" do
        user = create(:customer_user)
        assert_error_on(User.new(:login => user.login), :login, :i18n => "activerecord.errors.messages.exclusion")
      end
    end

    context "cpf" do
      should "not be valid when is not unique" do
        user = create(:customer_user)
        assert_error_on(User.new(:cpf => user.cpf), :cpf, :i18n => "activerecord.errors.messages.exclusion")
      end
    end
  end

  context "registration_age" do
    should "return the number of days since the user registered" do
      user = create(:customer_user)

      Timecop.travel(5.days) do
        assert_equal 5, user.registration_age
      end
    end
  end

  context "trial_period?" do
    setup do
      Settings.stubs(:user => stub(:enable_trial_period => true))
    end

    should "return true when user is in trial period" do
      user = create(:customer_user)
      assert_true user.trial_period?
    end

    should "return true when user is in last day of trial period" do
      user = create(:customer_user)
      Timecop.travel(User::TRIAL_PERIOD_DAYS.days) do
        assert_true user.trial_period?
      end
    end

    should "return false when user passed the trial period" do
      user = create(:customer_user)
      Timecop.travel((User::TRIAL_PERIOD_DAYS + 1).days) do
        assert_false user.trial_period?
      end
    end
  end
end