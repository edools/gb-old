require File.dirname(__FILE__) + "/../../test_unit_helper"

class SearchableTest < UnitTestCase
  class SearchableClass < ActiveRecord::Base
    include Searchable
    refine_search :name, :operator => :ilike, :match_style => :contains
    refine_search :last_name, :operator => :ilike, :match_style => :contains
  end

  should "generate conditions when a refine_search field is given" do
    options = SearchableClass.search("name" => "marie").proxy_options

    assert_equal ["searchable_classes.name ilike ?", "%marie%"], options[:conditions]
  end

  should "have no conditions when no filter is given" do
    assert_equal Hash.new, SearchableClass.search({}).proxy_options
  end

  should "have formulas connected by and" do
    options = SearchableClass.search("name" => "marie", "last_name" => "susie").proxy_options

    assert_match /.* and .* \?/, options[:conditions].first
  end

  should "not generate conditions for fields that are not in refined_search" do
    options = SearchableClass.search("invalid_filter" => "marie", "invalid_field2" => "susie").proxy_options
    assert_equal Hash.new, SearchableClass.search({}).proxy_options
  end
end