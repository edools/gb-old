require File.dirname(__FILE__) + "/../../test_unit_helper"

class SearchCriteriaTest < UnitTestCase
  context "create criteria" do
    context "with ilike" do
      should "match styles is contains" do
        criteria = SearchCriteria.new :field, :operator => :ilike, :match_style => :contains
        assert_equal ["field ilike ?", "%value%"], criteria.to_condition("value")
      end
    end
  end
end
