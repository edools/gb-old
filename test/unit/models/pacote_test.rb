require File.dirname(__FILE__) + "/../test_unit_helper"

class PacoteTest < UnitTestCase
  context "downloadable?" do
    context "trial period?" do
      setup do
        @user = User.new
        @pacote = Pacote.new
      end

      should "be downloable when user is in trial period" do
        @user.expects(:trial_period?).once.returns(true)
        assert_true @pacote.downloadable?(@user)
      end

      should "not be downloable when user is in not trial period" do
        @user.expects(:trial_period?).once.returns(false)
        assert_false @pacote.downloadable?(@user)
      end
    end
  end

  context "has_video_to_user?" do
    context "trial period?" do
      setup do
        @user = User.new
        @pacote = Pacote.new
        @video_upload = VideoUpload.new
      end

      should "be downloable when user is in trial period" do
        @user.expects(:trial_period?).once.returns(true)
        assert_true @pacote.has_video_to_user?(@user, @video_upload)
      end

      should "not be downloable when user is in not trial period" do
        @user.expects(:trial_period?).once.returns(false)
        assert_false @pacote.has_video_to_user?(@user, @video_upload)
      end
    end
  end
end