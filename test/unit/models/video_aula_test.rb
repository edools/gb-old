require File.dirname(__FILE__) + "/../test_unit_helper"

class VideoAulaTest < UnitTestCase
  context "downloadable?" do
    context "trial period?" do
      setup do
        @user = User.new
        @video_aula = VideoAula.new
      end

      should "be downloable when user is in trial period" do
        @user.expects(:trial_period?).once.returns(true)
        assert_true @video_aula.downloadable?(@user)
      end

      should "not be downloable when user is in not trial period" do
        @user.expects(:trial_period?).once.returns(false)
        assert_false @video_aula.downloadable?(@user)
      end
    end
  end

  context "has_video_to_user?" do
    context "trial period?" do
      setup do
        @user = User.new
        @video_aula = VideoAula.new
        @video_upload = VideoUpload.new
      end

      should "be downloable when user is in trial period" do
        @user.expects(:trial_period?).once.returns(true)
        assert_true @video_aula.has_video_to_user?(@user, @video_upload)
      end

      should "not be downloable when user is in not trial period" do
        @user.expects(:trial_period?).once.returns(false)
        assert_false @video_aula.has_video_to_user?(@user, @video_upload)
      end
    end
  end
end