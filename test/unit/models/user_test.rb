require File.dirname(__FILE__) + "/../test_unit_helper"

class UserTest < UnitTestCase
  context "validations" do
    context "login" do
      should "not be blank" do
        assert_error_on(User.new, :login, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:login => "a"), :login, :i18n => "activerecord.errors.messages.blank")
      end

      should "not have length shorter than 3" do
        assert_error_on(User.new(:login => "a"), :login, :i18n => ["activerecord.errors.messages.too_short", {:count => 3}])
      end

      should "be valid when length is longer than 3" do
        assert_no_error_on(User.new(:login => "a"*3), :login, :i18n => ["activerecord.errors.messages.too_short", {:count => 3}])
      end

      should "not have length longer than 40" do
        assert_error_on(User.new(:login => "a"*41), :login, :i18n => ["activerecord.errors.messages.too_long", {:count => 40}])
      end

      should "be valid when length is shorter than 40" do
        assert_no_error_on(User.new(:login => "a"*40), :login, :i18n => ["activerecord.errors.messages.too_long", {:count => 40}])
      end

      should "not have an invalid format" do
        assert_error_on(User.new(:login => "aa#!as"), :login, :i18n => "activerecord.errors.messages.invalid_alpha_numeric")
      end

      should "be valid when using a valid format" do
        assert_no_error_on(User.new(:login => "aaaaa"), :login, :i18n => "activerecord.errors.messages.invalid_alpha_numeric")
      end
    end

    context "name" do
      should "not be blank" do
        assert_error_on(User.new, :name, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:name => "a"), :name, :i18n => "activerecord.errors.messages.blank")
      end

      should "not have login length longer than 100" do
        assert_error_on(User.new(:name => "a"*101), :name, :i18n => ["activerecord.errors.messages.too_long", {:count => 100}])
      end

      should "be valid when login length is shorter than 40" do
        assert_no_error_on(User.new(:name => "a"*100), :name, :i18n => ["activerecord.errors.messages.too_long", {:count => 100}])
      end

      should "not have an invalid format" do
        assert_error_on(User.new(:name => '&&&&'), :name, :i18n => "activerecord.errors.messages.invalid_alpha_numeric")
      end

      should "be valid when using a valid format" do
        assert_no_error_on(User.new(:name => "aaaaa"), :name, :i18n => "activerecord.errors.messages.invalid_alpha_numeric")
      end
    end

    context "email" do
      should "not be blank" do
        assert_error_on(User.new, :email, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:email => "a"), :email, :i18n => "activerecord.errors.messages.blank")
      end

      should "not have length shorter than 6" do
        assert_error_on(User.new(:email => "a"), :email, :i18n => ["activerecord.errors.messages.too_short", {:count => 6}])
      end

      should "be valid when length is longer than 6" do
        assert_no_error_on(User.new(:email => "a"*6), :email, :i18n => ["activerecord.errors.messages.too_short", {:count => 6}])
      end

      should "not have length longer than 100" do
        assert_error_on(User.new(:email => "a"*101), :email, :i18n => ["activerecord.errors.messages.too_long", {:count => 100}])
      end

      should "be valid when length is shorter than 100" do
        assert_no_error_on(User.new(:email => "a"*100), :email, :i18n => ["activerecord.errors.messages.too_long", {:count => 100}])
      end

      should "not have an invalid format" do
        assert_error_on(User.new(:email => 'invalidemail'), :email, :i18n => "activerecord.errors.messages.invalid")
      end

      should "be valid when using a valid format" do
        assert_no_error_on(User.new(:email => "email@mail.com"), :email, :i18n => "activerecord.errors.messages.invalid")
      end
    end

    context "cpf" do
      should "not be blank" do
        assert_error_on(User.new, :cpf, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:cpf => "a"), :cpf, :i18n => "activerecord.errors.messages.blank")
      end

      #TODO update brazilian rails after upgrade to Rails 3
      should "not have an invalid format" do
        assert_error_on(User.new(:cpf => '11111111111'), :cpf, "numero invalido")
      end

      should "be valid when using a valid format" do
        assert_no_error_on(User.new(:cpf => Faker::CPF.numeric), :cpf, :i18n => "activerecord.errors.messages.invalid")
      end
    end

    context "birth date" do
      should "not be blank" do
        assert_error_on(User.new, :nascimento, :i18n => "activerecord.errors.messages.blank")
      end

      should "accept blank when using simple validation is enabled" do
        user = User.new
        user.validacao_simples = true
        assert_no_error_on(user, :nascimento, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:nascimento => "a"), :nascimento, :i18n => "activerecord.errors.messages.blank")
      end

      should "not be valid when length is different from 10" do
        assert_error_on(User.new(:nascimento => "11"), :nascimento, :i18n => ["activerecord.errors.messages.wrong_length", {:count => 10}])
      end

      should "be valid when length is 10" do
        assert_no_error_on(User.new(:nascimento => "1"*10), :nascimento, :i18n => ["activerecord.errors.messages.wrong_length", {:count => 10}])
      end

      should "not accept invalid birth date dd/mm/yyyy" do
        assert_error_on(User.new(:nascimento => "40/13/1900"), :nascimento, :i18n => "activerecord.errors.messages.invalid")
      end

      should "be valid when using valid birth date" do
        assert_no_error_on(User.new(:nascimento => "06/06/1988"), :nascimento, :i18n => "activerecord.errors.messages.invalid")
      end
    end

    context "endereco" do
      should "not be blank" do
        assert_error_on(User.new, :endereco, :i18n => "activerecord.errors.messages.blank")
      end

      should "accept blank when using simple validation is enabled" do
        user = User.new
        user.validacao_simples = true
        assert_no_error_on(user, :endereco, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:endereco => "a"), :endereco, :i18n => "activerecord.errors.messages.blank")
      end
    end

    context "bairro" do
      should "not be blank" do
        assert_error_on(User.new, :bairro, :i18n => "activerecord.errors.messages.blank")
      end

      should "accept blank when simple validation is enabled" do
        user = User.new
        user.validacao_simples = true
        assert_no_error_on(user, :bairro, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:bairro => "a"), :bairro, :i18n => "activerecord.errors.messages.blank")
      end
    end

    context "cep" do
      should "not be blank" do
        assert_error_on(User.new, :cep, :i18n => "activerecord.errors.messages.blank")
      end

      should "accept blank when using simple validation is enabled" do
        user = User.new
        user.validacao_simples = true
        assert_no_error_on(user, :cep, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(User.new(:cep => "a"), :cep, :i18n => "activerecord.errors.messages.blank")
      end

      should "not be valid when length is different from 10" do
        assert_error_on(User.new(:cep => "11"), :cep, :i18n => ["activerecord.errors.messages.wrong_length", {:count => 10}])
      end

      should "be valid when length is 10" do
        assert_no_error_on(User.new(:cep => "1"*10), :cep, :i18n => ["activerecord.errors.messages.wrong_length", {:count => 10}])
      end

      should "not have an invalid format" do
        assert_error_on(User.new(:cep => 'a'*10), :cep, :i18n => "activerecord.errors.messages.invalid")
      end

      should "be valid when using a valid format" do
        assert_no_error_on(User.new(:cep => "60.000-600"), :cep, :i18n => "activerecord.errors.messages.invalid")
      end
    end

    context "terms" do
      should "not be valid when terms were not accepted" do
        assert_error_on(User.new, :aceite_termos_de_servico, :i18n => "activerecord.errors.messages.accepted")
      end

      should "not be required to accept terms when simple validation is enabled" do
        user = User.new
        user.validacao_simples = true
        assert_no_error_on(user, :aceite_termos_de_servico, :i18n => "activerecord.errors.messages.accepted")
      end

      should "be valid when terms wer accepted" do
        assert_no_error_on(User.new(:aceite_termos_de_servico => "1"), :aceite_termos_de_servico, :i18n => "activerecord.errors.messages.accepted")
      end
    end

    context "roles" do
      should "not have no roles" do
        assert_error_on(User.new, :roles, :i18n => "activerecord.errors.messages.blank")
      end
    end

    context "trial_period?" do
      should "return false when configuration is not enable for trial period" do
        Settings.stubs(:user => stub(:enable_trial_period => false))
        assert_false User.new.trial_period?
      end
    end
  end
end
