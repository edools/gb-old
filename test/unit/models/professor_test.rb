require File.dirname(__FILE__) + "/../test_unit_helper"

class ProfessorTest < UnitTestCase
  context "paginator" do
    should "have 10 items per page" do
      assert_equal 10, Professor.per_page
    end
  end

  context "validations" do
    context "name" do
      should "not be blank" do
        assert_error_on(Professor.new, :nome, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(Professor.new(:nome => "sample name"), :nome)
      end
    end

    context "curriculum" do
      should "not be blank" do
        assert_error_on(Professor.new, :curriculo, :i18n => "activerecord.errors.messages.blank")
      end

      should "be valid when is not blank" do
        assert_no_error_on(Professor.new(:curriculo => "sample curriculum"), :curriculo)
      end
    end

    context "site" do
      should "be valid when blank" do
        assert_no_error_on(Professor.new, :site)
      end

      should "be valid when using http url" do
        assert_no_error_on(Professor.new(:site => "http://site.com"), :site)
      end

      should "be valid when using https url" do
        assert_no_error_on(Professor.new(:site => "http://site.com"), :site)
      end

      should "not be valid with a invalid url" do
        assert_error_on(Professor.new(:site => "invahttp://site.comlidurl"), :site, :i18n => "activerecord.errors.messages.invalid")
      end
    end

    context "email" do
      should "be valid when blank" do
        assert_no_error_on(Professor.new, :email)
      end

      should "be valid when using a valid email" do
        assert_no_error_on(Professor.new(:email => "email@email.com"), :email)
      end

      should "not be valid when using an invalid email" do
        assert_error_on(Professor.new(:email => "invalid@.email"), :email, :i18n => "activerecord.errors.messages.invalid")
      end
    end

    context "profile picture" do
      should "be valid when blank" do
        assert_no_error_on(Professor.new, :imagem)
      end
    end
  end
end
