module RakeUtils

  RAKE_COMMAND = "/usr/local/bin/rake"

  def call_rake(task, options = {})
    options[:rails_env] ||= Rails.env
    args = options.map { |key, value| "#{key.to_s.upcase}='#{value}'" }
    logtime = DateTime.now
    system "echo === #{logtime} === >> #{Rails.root}/log/rake.log"
    system "echo === #{logtime} === >> #{Rails.root}/log/rake-err.log"
    system "#{RAKE_COMMAND} #{task} #{args.join(' ')} --trace 1>> #{Rails.root}/log/rake.log 2>> #{Rails.root}/log/rake-err.log &"
  end

  def rake_running?(task)
    rake_running = IO.popen("ps aux | grep -v 'grep' | grep '#{task.gsub(" ", ".*")}'").gets
    !rake_running.blank?
  end

end