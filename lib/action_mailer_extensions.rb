module ActionMailer
  class Base
    class_inheritable_accessor :view_paths

    def self.prepend_view_path(path)
      view_paths.unshift(*path)
    end

    def self.append_view_path(path)
      view_paths.push(*path)
    end

    def view_paths
      self.class.view_paths
    end

    private

    def self.view_paths
      @@view_paths ||= ActionController::Base.view_paths
    end

    def initialize_template_class_without_helpers(assigns)
      ActionView::Base.new(view_paths, assigns, self)
    end
  end
end

