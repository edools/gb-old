class Array
  def uniq_with_block
    h = {}
    each do |elem|
      h[yield(elem)] ||= elem
    end
    h.values
  end
end
