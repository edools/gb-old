module EnvExtensions
  def production?
    self.end_with? "production"
  end
end

Rails.env.extend(EnvExtensions)