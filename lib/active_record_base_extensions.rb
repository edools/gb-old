module PaperclipExtensions
  class << self
    def included(base)
      base.extend ClassMethods
    end
  end

  module ClassMethods
    def has_attachment(name, options = {})
      options = {
        :access_type => :public
      }.merge(options)

      if Rails.env.production?
        if options.key? :base_path
          options[:path] = options.delete(:base_path)
        else
          options[:path] = "/:attachment/:id/:style/:filename"
        end
        options[:storage] = :s3
        options[:s3_credentials] = Settings.s3.credentials

        if options[:access_type].eql?(:public)
          options[:bucket] = Settings.s3.bucket + "_public"
          options[:s3_host_alias] = Settings.cloudfront.public_distribution
        elsif options[:access_type].eql?(:private)
          options[:bucket] = Settings.s3.bucket
          options[:s3_permissions] = :private
        else
          options[:bucket] = Settings.s3.bucket
          options[:s3_permissions] = :private
          options[:s3_host_alias] = Settings.cloudfront.streaming_distribution
        end
        options[:url] = ":s3_alias_url"
      else
        if options.key? :base_path
          base_path = options.delete(:base_path)
          options[:path] = ":rails_root/public/system/#{base_path}"
          options[:url] = "/system/#{base_path}"
        end
      end
      has_attached_file name, options
    end
  end
end

ActiveRecord::Base.send(:include, PaperclipExtensions)
