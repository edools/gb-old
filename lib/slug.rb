class Slug

  def self.create(fields = [])
    fields.map { |field|
      field.to_s.downcase.\
        gsub(/\s+/, '-').\
        gsub(/[äàáâã]/, "a").\
        gsub(/[ëèéê]/,  "e").\
        gsub(/[ïìíîĩ]/, "i").\
        gsub(/[öòóôõ]/, "o").\
        gsub(/[üùúûũ]/, "u").\
        gsub(/ñ/,       "n").\
        gsub(/ç/,       "c").\
        gsub(/[^-_a-z0-9]/, "")
    }.join('-')
  end

end