module FileFormatter
  # converte segundos para minutos e segundos
  def self.convert_bytes_to_megabytes(bytes)
     if bytes >= 1.megabytes
       total_megabytes = (bytes.to_f / 1.megabytes.to_f).truncate
       "#{total_megabytes.truncate}Mbytes"
     elsif bytes >= 1.kilobytes
       total_kilobytes = (bytes.to_f / 1.kilobytes.to_f).truncate
        "#{total_kilobytes.truncate}Kbytes"
     else
       "#{bytes.round}bytes"
     end
  end
end
