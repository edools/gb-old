# LimitedSessions
# (c) 2007-2010 t.e.morgan
# made available under the MIT license
#
# this is the rails 2.3 version; it is /not/ compatible with earlier versions. 

#Configuration:
#  There are several options that can be configured.  They should be placed at
#  the end of config/environment.rb (or the individual <environment>.rb files
#  if that's preferred).
#  
#  ActionController::Request.recent_activity_limit = 2.hours
#  ActiveRecord::SessionStore::Session.recent_activity_limit = 2.hours
#    This will expire sessions after the given period of time.  This is managed
#    on the server side and if the user closes their browser, the session will
#    also be gone.  For non-ActiveRecord sessions (Cookie, Memcache, Redis, 
#    etc.), use the first variable.  For ActiveRecord, use the second.  Default
#    for non-AR is nil (disabled); default for AR is 2 hours.  For AR, requires
#    an `updated_at` column in the session table.  Non-AR stores add a session
#    variable, :last_visit.  Some datastores, such as Memcache and Redis, manage
#    their own expiry so this is unneeded.  May not be disabled (nil) when using
#    AR.
#  
#  ActionController::Request.hard_session_limit = 24.hours
#  ActiveRecord::SessionStore::Session.hard_session_limit = 24.hours
#   Sessions can also be forcefully expired without regard to the last
#    activity.  So if this is set to 24 hours and the above is two hours, the 
#    session will be terminated if a) the user has been inactive for more than
#    two hours OR b) it has been more than 24 hours since the session began.
#    Like the previous setting, use the first for non-AR and the second for AR.
#    Default in both cases is disabled (nil).  For AR, requires a `created_at` 
#    column in the session table. Non-AR stores add a session variable, 
#    :first_visit.
  
#  ActiveRecord::SessionStore::Session.auto_clean_sessions = 1000
#    Does a random test to see if the app should delete all expired sessions
#    now.  The odds are 1 in whatever value is provided here.  0 will disable
#    this option.  Default is 1000.  A busy site may want 10000 or higher.  Only
#    applicable to ActiveRecord-based session stores.
    
#  ActionController::Request.ip_restriction = :subnet
#    If set to :exact, will compare the full IP address.  If set to :subnet, 
#    will match a little more broadly based on the configured subnet size (see 
#    next options).  If no match, the session will be reset.  Defaults to :none,
#    which disables IP restriction checking.  Stores the IP match data in the 
#    session store as session[:ip].  Works with all session stores.
#
#  ActionController::Request.ipv4_mask = 24
#  ActionController::Request.ipv6_mask = 64
#    When .ip_restriction == :subnet, this configures the size of the subnet
#    that's still considered a match.  Defaults (IPv4: /24; IPv6: /64) should
#    be suitable for most applications. 

require 'ipaddr'

module ActiveRecord
  class SessionStore < ActionController::Session::AbstractStore
    class Session < ActiveRecord::Base
      self.partial_updates = false if respond_to? :partial_updates=
      
      cattr_accessor :recent_activity_limit, :hard_session_limit, :auto_clean_sessions
      self.recent_activity_limit = 2.hours  # required
      self.hard_session_limit = nil         # eg: 24.hours
      self.auto_clean_sessions = 1000       # 0 disables
      before_save :before_save_update_user_id
      
      def before_save_update_user_id        
#        if !@data.blank? and !@data[:ip].blank?
#          self.ip = @data[:ip]
#        end
        if !@data.blank? and !@data[:user_id].blank? and @data[:user_id].to_i > 0
          self.user_id = @data[:user_id].to_i
        else
          self.user_id = nil
        end        
      end
      class << self
        
        alias :find_by_session_id_old_version :find_by_session_id
        def find_by_session_id(session_id)
          consider_auto_clean
          
          now = if self.default_timezone == :utc
                  Time.now.utc
                else
                  Time.now
                end
          
          if @@hard_session_limit
            my_session = find(:first, :conditions => ['session_id = ? AND updated_at > ? AND created_at > ?', session_id, now - @@recent_activity_limit, now - @@hard_session_limit])
            
            if !my_session.blank? and !my_session.user_id.blank?
              delete_all ['session_id != ? AND user_id = ?', session_id, my_session.user_id]
            end
            
            return my_session
          else
            my_session = find(:first, :conditions => ['session_id = ? AND updated_at > ?', session_id, now - @@recent_activity_limit])
            clean_non_admin_user_old_sessions(my_session)

            if !my_session.blank? and !my_session.user_id.blank?
              delete_all ['session_id != ? AND user_id = ?', session_id, my_session.user_id]
            end
            
            return my_session
          end
        end
        
        private

        def clean_non_admin_user_old_sessions(session)
          return session.blank? || session.user_id.blank?
          sql = "DELETE FROM sessions USING users WHERE users.id=sessions.user_id AND session_id != '#{session.session_id}' AND user_id = #{session.user_id} AND admin = false"
          connection.delete(sql)
        end
        
        def consider_auto_clean
          return if @@auto_clean_sessions == 0
          if rand(@@auto_clean_sessions) == 0
            
            now = if self.default_timezone == :utc
                    Time.now.utc
                  else
                    Time.now
                  end
            
            if @@hard_session_limit
              delete_all ['updated_at < ? OR created_at < ?', now - @@recent_activity_limit, now - @@hard_session_limit]
            else
              delete_all ['updated_at < ?', now - @@recent_activity_limit]
            end
          end
        end
        
      end
    end

  end
end


#module ActionController
#  class Request < Rack::Request
#    cattr_accessor :ip_restriction
#    self.ip_restriction = :none           # options-  :none, :exact, :subnet
#    cattr_accessor :ipv4_mask, :ipv6_mask
#    self.ipv4_mask = 24
#    self.ipv6_mask = 64
#    cattr_accessor :recent_activity_limit, :hard_session_limit
#    self.recent_activity_limit = nil      # eg: 2.hours
#    self.hard_session_limit = nil         # eg: 24.hours
#    
#    alias :session_old_version :session
#    def session
#      session_old_version[:ip] = remote_ip
#      return session_old_version
#      
#    end
#    def session
#      # using  @env['rack.session'] = {}  instead of  reset_session  as the latter
#      # also resets the session id and that seems to cause some really strange
#      # behavior
#      if session_old_version[:ip]
#        case @@ip_restriction
#        when :exact
#          unless session_old_version[:ip] == remote_ip
#            ActionController::Base.logger.error "Session violation: IP #{session_old_version[:ip]} expected; #{remote_ip} received"
#            @env['rack.session'] = {}
#          end
#        when :subnet
#          session_ip = IPAddr.new(session_old_version[:ip])
#          the_mask = session_ip.ipv4? ? @@ipv4_mask : @@ipv6_mask
#          session_ip.send(:mask!, the_mask)
#          current_ip = IPAddr.new(remote_ip)
#          unless session_ip.include?(current_ip)
#            ActionController::Base.logger.error "Session violation: IP #{session_ip}/#{the_mask} expected; #{remote_ip} received"
#            @env['rack.session'] = {}
#          end
#        end
#      end
#      
#      if @@recent_activity_limit
#        if session_old_version[:last_visit]
#          if (session_old_version[:last_visit] + @@recent_activity_limit) < Time.now.to_i
#            ActionController::Base.logger.info "Session expired: no recent activity"
#            @env['rack.session'] = {} 
#          end
#        end
#        # Rounds to the nearest 5 minutes to minimize writes when a DB is in use
#        session_old_version[:last_visit] = (Time.now.to_f/300).ceil*300
#      end
#      if @@hard_session_limit
#        session_old_version[:first_visit] ||= Time.now.to_i
#        if (session_old_version[:first_visit] + @@hard_session_limit) < Time.now.to_i
#          ActionController::Base.logger.info "Session expired: hard limit reached"
#          @env['rack.session'] = {}
#        end
#      end
#      session_old_version[:ip] ||= remote_ip
#      session_old_version
#    end
#
#    
#    alias :reset_session_old_version :reset_session
#    def reset_session
#      old_id = session_options[:id]
#      
#      klass = ActionController::Base.session_store
#      if old_id && !klass.is_a?(ActionController::Session::CookieStore)
#        # DB-backed sessions need to be wiped to protected against session_id replay attacks
#        ss = klass.new(:no_app, ActionController::Base.session_options)
#        ss.send :set_session, @env, old_id, {}
#      end
#      reset_session_old_version
#    end
#    
#  end
#end