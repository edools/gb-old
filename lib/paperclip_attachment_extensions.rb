module Paperclip
  module Storage::S3
    def expiring_url_with_s3_host_alias(time = 3600, style_name = default_style)
      result = expiring_url_without_s3_host_alias(time, style_name)
      s3_host_alias.present? ?
        result.gsub(/(s3.amazonaws.com\/#{bucket_name})/, s3_host_alias) : result
    end
    alias_method_chain :expiring_url, :s3_host_alias
  end
  class Attachment
    alias_method :old_url, :url

    def url(style_name = default_style, options = {})
      if Rails.env.production?
        if self.options[:access_type].eql?(:private)
          url = expiring_url(Settings.s3.url_expiring_time, style_name)
          # if Settings.ssl.enabled
          #   url = url.gsub("http","https")
          # end
          url
        elsif self.options[:access_type].eql?(:public)
          url = old_url(style_name, options).gsub('+', '%2B')
          if Settings.ssl.enabled
            url = url.gsub("http","https")
          end
          url
        else
          if Settings.cloudfront.enable_rtmp
            "flv:" + AWS::CF::Signer.sign_path(path, :expires => (Time.now.utc + Settings.cloudfront.url_expiring_time)).gsub(".flv","")
          else
            AWS::CF::Signer.sign_url(old_url, :expires => (Time.now.utc + Settings.cloudfront.url_expiring_time))
          end
        end
      else
        old_url(style_name, options)
      end
    end
  end
end
