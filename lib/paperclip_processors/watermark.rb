module Paperclip
  class Watermark < Processor

    class InstanceNotGiven < ArgumentError; end

    def initialize(file, options = {},attachment = nil)
      super
      @file = file
      @current_format   = File.extname(@file.path)
      @basename         = File.basename(@file.path, @current_format)
      @watermark        = RAILS_ROOT + "/public/images/marca_dagua.png"
      @position         = "SouthEast"
      @options          = options
    end

    def watermark_dimensions
      return @watermark_dimensions if @watermark_dimensions
      @watermark_dimensions = Geometry.from_file @watermark
    end

    def make
      # TODO: redimensionar watermark de acordo com a imagem
        dst = Tempfile.new([@basename, @format].compact.join("-temp."))
        command = "-gravity #{@position} #{@watermark} #{File.expand_path(@file.path)} #{File.expand_path(dst.path)}"
        begin
          if @options[:geometry].include? "640x480"
            success = Paperclip.run("composite", command)
          else
            FileUtils.copy File.expand_path(@file.path), File.expand_path(dst.path)
          end
        rescue PaperclipCommandLineError
          raise PaperclipError, "There was an error processing the watermark for #{@basename}" if @whiny_thumbnails
        end
        dst
    end

  end
end
