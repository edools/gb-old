namespace :unzip do

  desc "Converte arquivos .zip de objetos do tipo MODEL. "
  task :zip_to_images => ["environment"] do
    require 'fileutils'
    require 'zip/zip'

    model = ENV["MODEL"]
    raise UndefinedModelException if model.blank?
    
    # Enquanto houver arquivos .zip, descompacta as imagens dentro deles
    # mesmo que cheguem outros arquivos .zip durante o processo
    while (zip_files = eval("#{model}Image").zip_files) and (zip_files.size > 0)
      zip_files.each do |zip_file|
        object = eval("zip_file.#{model.downcase}")
        puts "Descompactando imagens do(a) #{model.downcase}: #{object.id} - #{object.try :nome}"
        quantidade_imagens = 0
        Zip::ZipFile.open( zip_file.arquivo.path ).sort.each do |single_file|
          quantidade_imagens += 1
          begin
            if single_file.name.downcase =~ /.(jpg|jpeg|bmp|png|gif|tif|tiff)/ # TODO: get from allowed_file_types
              temp_file = Tempfile.new( ActiveSupport::SecureRandom.hex(16) + File.extname( single_file.name ).downcase )
              single_file.extract( temp_file.path, &proc {true} )
              object_image = eval("object.#{model.downcase}_images.build")
              object_image.imagem = temp_file
              if object.save
                print "\t[ OK ] "
              else
                print "\t[ ERROR ] "
              end
              puts single_file.name + " " + object.errors.map { |e,m| "#{e}: #{m}" }.join(" | ")
            else
              puts "\t[NOT ALLOWED] #{single_file.name}"
            end
          rescue Exception => e
            puts "\t[ ERROR ] #{e.message}"
          end # begin
          sleep 1 if (quantidade_imagens % 5 == 0) # para nao sobrecarregar o servidor
        end
        zip_file.destroy if object.errors.blank?
      end # zip_files.each
      sleep 5
    end # while
  end

end