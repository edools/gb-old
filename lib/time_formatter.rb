module TimeFormatter
  # converte segundos para minutos e segundos
  def self.convert_seconds_to_time(seconds)
     if seconds >= 1.minutes
       total_minutes = (seconds.to_f / 1.minutes.to_f).truncate
       seconds_in_last_minute = seconds - total_minutes.minutes
       "#{total_minutes.truncate}m #{seconds_in_last_minute.round}s"
     else
       "#{seconds.round}s"
     end
  end
end