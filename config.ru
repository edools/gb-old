# we need to protect against multiple includes of the Rails environment (trust me)
require 'config/environment' if !defined?(Rails) || !Rails.initialized?
# Unicorn self-process killer
require 'unicorn/worker_killer'
# require 'sprockets'
#
# unless Rails.env.production?
#   map '/assets' do
#     sprockets = Sprockets.env
#     run sprockets
#   end
# end

# Max requests per worker
use Unicorn::WorkerKiller::MaxRequests, 3072, 4096

# Max memory size (RSS) per worker
use Unicorn::WorkerKiller::Oom, (256*(1024**2)), (512*(1024**2))

require "config/environment"

use Rails::Rack::LogTailer
use Rails::Rack::Static
run ActionController::Dispatcher.new