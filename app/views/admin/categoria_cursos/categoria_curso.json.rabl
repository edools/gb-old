attribute :nome => :title
node :url do |categoria_curso|
 edit_admin_categoria_curso_path(categoria_curso)
end

node :isFolder do |categoria_curso|
  categoria_curso.has_children?
end

node :children do |categoria_curso|
  partial "admin/categoria_cursos/categoria_curso", :object => categoria_curso.children.sort { |a,b| a.nome <=> b.nome }
end
