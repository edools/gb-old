class Artigo < ActiveRecord::Base
  belongs_to :professor
  has_many :artigo_anexos, :dependent => :destroy

  ordered_by 'created_at DESC'

  accepts_nested_attributes_for :artigo_anexos, :allow_destroy => true

  validates_presence_of :nome
  validates_presence_of :descricao

  named_scope :search,  lambda { | filtros |
    conditions = {:query => [], :parameters => []}

    if filtros
      filtros.each do |filtro, valor|
        unless valor.blank?
          case filtro
          when "nome"
            valor = "%" + valor + "%"
            conditions[:query].push("artigos.nome ILIKE ? OR artigos.descricao ILIKE ?")
            conditions[:parameters].push(valor)
            conditions[:parameters].push(valor)
          when "professor"
            conditions[:query].push("artigos.professor_id = ?")
            conditions[:parameters].push(valor)
          end
        end
      end

      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) } if conditions[:query].length > 0
    end
  }

  def to_param
    Slug.create([id, nome])
  end
  
end
