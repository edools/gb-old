class Fatura < ActiveRecord::Base

  before_save :update_expira_at
  before_save :after_approval

  STATI = [:pending, :approved, :verifying, :canceled, :refunded]

  acts_as_audited

  belongs_to :user

  # :conditions => 'grupo_id IS NULL' e' para nao retornar os filhos do grupo
  has_many :fatura_items, :dependent => :destroy, :conditions => 'grupo_id IS NULL'
  has_many :produtos, :through => :fatura_items

  ordered_by 'created_at DESC'

  # status de compra do pagseguro
  values_for :status, :has => Fatura::STATI, :add => [:named_scopes, :predicate_methods, :constants]

  # indica se a fatura foi paga
  def pending?
    status != Fatura::STATUS_APPROVED
  end

  def approved?
    status == Fatura::STATUS_APPROVED
  end

  # total faturado
  def total_price
    return 0.para_dinheiro if fatura_items.blank?
    total = 0
    fatura_items.each do |fatura_item|
      total += fatura_item.price_com_desconto
    end
    total
  end

  def free?
    total_price == 0.0
  end

  def status_legible
    Fatura.status_label(status)
  end

  def self.status_label(value)
    case value
      when Fatura::STATUS_APPROVED
        'Aprovado'
      when Fatura::STATUS_VERIFYING
        'Verificado'
      when Fatura::STATUS_PENDING
        'Pendente Pagamento'
      when Fatura::STATUS_CANCELED
        'Cancelado'
      when Fatura::STATUS_REFUNDED
        'Reembolsado'
    end
  end

  named_scope :search, lambda { |select, filtros|
    conditions = {:query => [], :parameters => []}

    if (select.blank?)
      select = "DISTINCT(faturas.*)"
    end

    if filtros
      filtros.each do |filtro, valor|
        unless valor.blank?
          case filtro
            when "aluno"
              valor = "%" + valor + "%"
              conditions[:query].push("users.first_name ILIKE ? OR users.last_name ILIKE ? OR users.email ILIKE ?")
              conditions[:parameters].push(valor)
              conditions[:parameters].push(valor)
              conditions[:parameters].push(valor)
            when "status"
              conditions[:query].push("faturas.status = ?")
              conditions[:parameters].push(valor)
            when "curso"
              conditions[:query].push("fatura_items.produto_id = ?")
              conditions[:parameters].push(valor)
            when "expira"
              meses = valor.to_i
              now = Time.zone.now
              data_expiracao = now + meses.month
              conditions[:query].push("fatura_items.expira_at > ? AND fatura_items.expira_at <= ?")
              conditions[:parameters].push(now)
              conditions[:parameters].push(data_expiracao)
          end
        end
      end

      return {:select => select, :joins => [:user, :fatura_items], :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters])} if conditions[:query].length > 0
    end
    { :select => select, :joins => [:user, :fatura_items] }
  }

  protected

  # calcula a data data de expiracao dos itens caso o status da fatura tenha
  # mudado para aprovado
  def update_expira_at
    if (self.new_record? or self.status_changed?) and self.status == Fatura::STATUS_APPROVED
      today = Date.today
      self.fatura_items.each do |fatura_item|
        fatura_item.update_expira_at(today)
        fatura_item.touch
        fatura_item.enviar_mensagem_expiracao_atualizou()
      end
    end
  end

  def after_approval
    return unless (self.new_record? || self.status_changed?) && self.status == Fatura::STATUS_APPROVED && self.user.present?
    self.produtos.each do |product|
      product.after_invoice_approved(self)
    end
  end
end