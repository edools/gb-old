class Imagem < ActiveRecord::Base

  belongs_to :user

  ordered_by 'created_at DESC'

  has_attachment :imagem,
                    :styles => { :real => { :geometry => "640x480>", :convert_options => '-auto-orient -strip' },
                                 :thumbnail => { :geometry => "63x63#", :convert_options => '-auto-orient -strip -unsharp 0x.5' } },
                    :processors => [ :thumbnail ]

  # validations
  @file_types = YAML.load_file("#{RAILS_ROOT}/config/allowed_file_types.yml")
  validates_attachment_presence :imagem
  validates_attachment_content_type :imagem,  :content_type => @file_types['images']

end
