class VideoUpload < ActiveRecord::Base
  acts_as_audited

  belongs_to :categoria, :class_name => "CategoriaCurso", :foreign_key => "categoria_curso_id"
  belongs_to :tipo, :class_name => "VideoUploadTipo", :foreign_key => "video_upload_tipo_id"
  belongs_to :creator, :class_name => 'User', :foreign_key => 'creator_id'

  # prevents a user from submitting a crafted form that bypasses activation
  attr_protected :creator_id

  ordered_by 'nome ASC'

  has_attachment  :streaming, :access_type => :private_streaming, :base_path  => ":class/:id/:id.:extension", :streaming_content => true

  validates_attachment_presence :streaming
  validates_attachment_content_type :streaming, :content_type => ["video/x-flv",
                                                                  "flv-application/octet-stream", "application/octet-stream"]
  validates_presence_of :nome
  validates_uniqueness_of :nome
  validates_presence_of :categoria_curso_id
  validates_presence_of :video_upload_tipo_id

  named_scope :simulados, lambda {
                            # infelizmente o rails quebra as classes derivadas da class Produto caso essa checagem nao for feita
                            {:joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.simulado }}}
                          }

  named_scope :completos, lambda {
    { :joins => [:tipo],
      :conditions => { :video_upload_tipos => {
                          :identificador => VideoUploadCategoria.completo
                                              }
                    }
    }
  }

  named_scope :search_backend,  lambda { | filtros |
    conditions = {:query => [], :parameters => []}
    filtros ||= {}
    filtros.each do |filtro, valor|
      next if valor.blank?
      case filtro
      when "nome"
        valor = "%" + valor + "%"
        conditions[:query].push("video_uploads.nome ILIKE ?")
        conditions[:parameters].push(valor)
      when "categoria_curso_id"
        conditions[:query].push("video_uploads.categoria_curso_id = ?")
        conditions[:parameters].push(valor)
      when "video_upload_tipo_id"
        conditions[:query].push("video_uploads.video_upload_tipo_id = ?")
        conditions[:parameters].push(valor)
      end
    end

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  def self.per_page
    10
  end

  def streaming_exists?
    if Rails.env.production?
      true
    else
      File.exist? streaming.path
    end
  end

  def attach_file(filedata)
    filedata.content_type = MIME::Types.type_for(filedata.original_filename)[0].to_s
    self.streaming = filedata

    movie = FFMPEG::Movie.new(filedata.path)
    if movie.valid?
      self.duracao = movie.duration
    end
  end

  def duracao_label
    TimeFormatter.convert_seconds_to_time duracao
  end

  def downloadable?(user, curso_id)
    return true if self.tipo.is_demonstracao? || self.tipo.is_simulado?
    return false if user.nil?
    return true if user.is_admin?
    return false if curso_id.blank?
    curso = Produto.find_by_id curso_id
    return false if curso.nil?
    curso.has_video_to_user?(user,self)
  end

  def streaming_file_size_megabytes
    FileFormatter.convert_bytes_to_megabytes(self.streaming_file_size.blank? ? 0 : self.streaming_file_size)
  end
end
