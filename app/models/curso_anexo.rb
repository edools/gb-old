class CursoAnexo < ActiveRecord::Base
  include RakeUtils

  belongs_to :curso

  has_attachment :arquivo,
                 :base_path => ":class/:id/:style/:normalized_filename"

  ordered_by 'created_at DESC'

  before_save :set_default_legenda

  @file_types = YAML.load_file("#{RAILS_ROOT}/config/allowed_file_types.yml")

  named_scope :images,    { :conditions => { :imagem_content_type   => @file_types['images']    } }
  named_scope :zip_files, { :conditions => { :arquivo_content_type  => @file_types['archives'] } }

  def set_default_legenda
    self.legenda = self.arquivo_file_name if legenda.blank?
  end

  def is_image?
    !/^image.*$/.match(imagem.content_type).blank?
  end

  def self.allowed_images
    @file_types['images']
  end

  def self.allowed_files
    @file_types['archives']
  end

  def self.per_page
    24
  end
end
