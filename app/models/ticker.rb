class Ticker < ActiveRecord::Base
  ordered_by 'created_at DESC'

  ONLINE = "online"
  PRESENCIAL = "presencial"

  has_attachment :imagem,
                 :styles => {:thumbnail => {:geometry => "125x60#", :convert_options => '-auto-orient -strip -unsharp 0x.5'}},
                 :processors => [:thumbnail]

  # validations
  @file_types = YAML.load_file("#{RAILS_ROOT}/config/allowed_file_types.yml")
  validates_attachment_presence :imagem
  validates_format_of :link, :with => /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix, :unless => Proc.new { |p| p.link.blank? }
  validates_attachment_content_type :imagem, :content_type => @file_types['images']
  validates_inclusion_of :category, :in => [ONLINE, PRESENCIAL]
  before_validation :format_link

  named_scope :search_backend,  lambda { |filters|
    conditions = {:query => [], :parameters => []}
    if filters.present?
      filters.each do |filter, value|
        next if value.blank?
        case filter
          when "category"
            value = "#{value}"
            conditions[:query].push("category = ?")
            conditions[:parameters].push(value)
        end
      end
    end

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  def format_link
    if !link.blank? && !link.start_with?('http://') && !link.start_with?('https://')
      self.link = "http://" + self.link
    end
  end

  def self.categorias
    [["Online", ONLINE], ["Presencial", PRESENCIAL]]
  end
end
