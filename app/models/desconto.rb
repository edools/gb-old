class Desconto < ActiveRecord::Base
  include Sanitizable

  attr_accessor :inicia_at_string, :termina_at_string

  has_many :desconto_items, :dependent => :destroy, :class_name => "DescontoItem", :include => :produto, :order => "produtos.nome ASC"
  has_many :desconto_assinatura_items, :dependent => :destroy, :class_name => "DescontoAssinaturaItem", :include => :assinatura, :order => "produtos.nome ASC"
  has_many :assinaturas, :through => :desconto_assinatura_items

  sanitize_attributes_for_association :desconto_items, :association_key => :produto_id
  sanitize_attributes_for_association :desconto_assinatura_items, :association_key => :produto_id

  validates_presence_of :desconto, :inicia_at, :termina_at
  validates_numericality_of :desconto, :only_integer => true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100
  validate :validates_datas, :validates_periodo

  def inicia_at_string
    inicia_at.to_s(:padrao_input_data) unless inicia_at.blank?
  end

  def termina_at_string
    termina_at.to_s(:padrao_input_data) unless termina_at.blank?
  end

  def inicia_at_string=(inicia_at_str)
    self.inicia_at = Date.strptime(inicia_at_str, Date::DATE_FORMATS[:padrao_input_data])
  rescue ArgumentError
    @inicia_at_invalid = true
  end

  def termina_at_string=(termina_at_str)
    self.termina_at = Date.strptime(termina_at_str, Date::DATE_FORMATS[:padrao_input_data])
  rescue ArgumentError
    @termina_at_invalid = true
  end

  # retorna o preco do produto aplicado o desconto ativo
  def self.investimento_com_desconto(product)
    discount_rule_join = nil
    discount_rule_query = ["inicia_at <= ?", "? <= termina_at"]
    discount_rule_query_arguments = [DateTime.now.to_date, DateTime.now.to_date]

    if product.is_a?(Assinatura)
      discount_rule_join = "LEFT JOIN desconto_assinatura_items ON desconto_assinatura_items.desconto_id = descontos.id"
      discount_rule_query <<  "(desconto_assinatura_items.produto_id = ? OR (desconto_assinatura_items.id IS NULL AND aplicar_todas_assinaturas = ?))"
    else
      discount_rule_join = "LEFT JOIN desconto_items ON desconto_items.desconto_id = descontos.id"
      discount_rule_query << "(desconto_items.produto_id = ? OR (desconto_items.id IS NULL AND aplicar_todos_items = ?))"
    end
    discount_rule_query_arguments.concat([product.id,true])
    discount_rule_query = [discount_rule_query.join(" AND ")] + discount_rule_query_arguments

    discount_rule = Desconto.find :first,
                :order => "desconto DESC, created_at DESC",
                :joins => discount_rule_join,
                :conditions => discount_rule_query

    discount_rule ? product.investimento * (1.0 - (discount_rule.desconto/100.0)) : product.investimento
  end

  private

  def validates_datas
    errors.add(:inicia_at_string, "é inválida") if @inicia_at_invalid
    errors.add(:termina_at_string, "é inválida") if @termina_at_invalid
  end

  def validates_periodo
    errors.add(:inicia_at_string, "maior que data de término") if !self.inicia_at.blank? and !self.termina_at.blank? and self.inicia_at > self.termina_at
    errors.add(:inicia_at, "maior que data de término") if !self.inicia_at.blank? and !self.termina_at.blank? and self.inicia_at > self.termina_at
  end
end
