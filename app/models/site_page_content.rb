class SitePageContent < ActiveRecord::Base
  values_for :name, :has => [:termos,:sobre], :add => [:named_scopes, :predicate_methods, :constants]
end
