class ArtigoAnexo < ActiveRecord::Base
  belongs_to :artigo

  has_attachment :arquivo

  ordered_by 'created_at DESC'

  before_save :set_default_legenda

  @file_types = YAML.load_file("#{RAILS_ROOT}/config/allowed_file_types.yml")

  def self.allowed_attaches
    @file_types['documents'] + @file_types['archives']
  end

  def self.per_page
    10
  end

  def set_default_legenda
    self.legenda = self.arquivo_file_name if legenda.blank?
  end
end
