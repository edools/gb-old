class VideoUploadTipo < ActiveRecord::Base

  ordered_by 'nome ASC'

  # define a constante completo
  def self.completo
    "completo"
  end

  # define a constante demonstracao
  def self.demonstracao
    "demonstracao"
  end

  # define a constante simulado
  def self.simulado
    "demonstracao"
  end

  def is_completo?
    identificador == VideoUploadCategoria.completo
  end

  def is_demonstracao?
    identificador == VideoUploadCategoria.demonstracao
  end

  def is_simulado?
    identificador == VideoUploadCategoria.simulado
  end
end
