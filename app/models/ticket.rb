class Ticket < Produto
  include Sanitizable
  include Validatable

  attr_accessor :available_from_string, :available_to_string

  acts_as_audited

  has_many :videos, :class_name => "ProductsVideoUpload", :dependent => :destroy, :foreign_key => 'product_id', :include => :video_upload, :order => Settings.products_videos_order
  has_many :professors, :class_name => "ProdutosProfessor", :dependent => :destroy, :foreign_key => 'produto_id'

  sanitize_attributes_for_association :professors, :association_key => :professor_id
  sanitize_attributes_for_association :videos, :association_key => :video_upload_id
  validates_associated :videos

  validates_presence_of :available_from, :available_to
  validates_presence_of :categoria_curso_id
  validates_presence_of :codigo_embed
  validate :validates_available_period_dates, :validates_period_interval
  validates_attachment_presence :imagem

  def available_from_string
    available_from.to_s(:padrao_input_data) unless available_from.blank?
  end

  def available_to_string
    available_to.to_s(:padrao_input_data) unless available_to.blank?
  end

  def available_from_string=(available_from_str)
    self.available_from = Date.strptime(available_from_str, Date::DATE_FORMATS[:padrao_input_data])
  rescue ArgumentError
    @available_from_invalid = true
  end

  def available_to_string=(available_to_str)
    self.available_to = Date.strptime(available_to_str, Date::DATE_FORMATS[:padrao_input_data])
  rescue ArgumentError
    @available_to_invalid = true
  end

  def downloadable?(user)
    return false if user.nil?
    return true if user.is_admin?
    return Ticket.count(:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
                        :conditions => ['faturas.status=? AND faturas.user_id=? AND produtos.id=? AND produtos.ativo=?', Fatura::STATUS_APPROVED.to_s, user, self, true]) > 0
  end

  # veja na classe Produto
  def make_fatura_items(quantity, fatura, cupom)
    if !cupom.nil? && Cupom.valido?(cupom.codigo, [self.id])
      return FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
    end
    FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
  end

  def has_video_to_user?(user, video_upload)
    return false if user.nil?
    return false if video_upload.nil?
    return true if user.is_admin?
    return false unless ProductsVideoUpload.exists? :product_id => self.id, :video_upload_id => video_upload.id
    return FaturaItem.count(:joins => 'INNER JOIN faturas ON fatura_items.fatura_id=faturas.id INNER JOIN produtos ON produtos.id=fatura_items.produto_id',
                            :conditions => ['faturas.status=? AND faturas.user_id=? AND fatura_items.produto_id=? AND available_from <= ? AND ? <= available_to', Fatura::STATUS_APPROVED.to_s, user, self, DateTime.now.to_date, DateTime.now.to_date]) > 0
  end

  def to_param
    Slug.create([id, nome])
  end

  def self.total_ativas(user)
    Ticket.ativos_do_user(user).count(:distinct => true, :select => 'produtos.id')
  end

  named_scope :available, lambda {
    {:conditions => ["available_from <= ? AND ? <= available_to", DateTime.now.to_date, DateTime.now.to_date]}
  }

  named_scope :search, lambda { |filtros|
    conditions = Produto.generate_conditions(filtros)

    #conditions[:query].push("type IN (?)")
    #conditions[:parameters].push([Arquivo.name])
    conditions[:query].push("available_from <= ?")
    conditions[:parameters].push(DateTime.now.to_date)
    conditions[:query].push("? <= available_to")
    conditions[:parameters].push(DateTime.now.to_date)
    conditions[:query].push("ativo=?")
    conditions[:parameters].push(true)
    conditions[:query].push("disponivel_venda=?")
    conditions[:parameters].push(true)

    if conditions[:query].length > 0
      {:conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters])}
    end
  }

  # retorna todas os tickets de um user
  named_scope :ativos_do_user, lambda { |current_user|
    statements = {}
    conditions_query = []
    conditions_params = []
    if !current_user.is_admin?
      statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
                    :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list}
      conditions_query = ['faturas.status=?', 'faturas.user_id=?', '(fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at)']
      conditions_params = [Fatura::STATUS_APPROVED.to_s, current_user.id, Date.today.to_s]
    end


    conditions_filtros = generate_conditions(nil)
    conditions_filtros[:query].push("produtos.ativo=?")
    conditions_filtros[:parameters].push(true)
    conditions_filtros[:query].push("available_from <= ?")
    conditions_filtros[:parameters].push(DateTime.now.to_date)
    conditions_filtros[:query].push("? <= available_to")
    conditions_filtros[:parameters].push(DateTime.now.to_date)
    if conditions_filtros[:query].length > 0
      conditions_query.concat(conditions_filtros[:query])
      conditions_params.concat(conditions_filtros[:parameters])
    end

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  # busca todos os arquivos ativos, que nao expiraram
  named_scope :search_meus_tickets, lambda { |filtros, current_user|
    statements = {}
    conditions_query = []
    conditions_params = []
    if !current_user.is_admin?
      statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
                    :select => 'DISTINCT(produtos.*)', :group => Produto.col_list}
      conditions_query = ['faturas.status=?', 'faturas.user_id=?']
      conditions_params = [Fatura::STATUS_APPROVED.to_s, current_user.id]
    end


    conditions_filtros = generate_conditions(filtros)
    #conditions_filtros[:query].push("type IN (?)")
    #conditions_filtros[:parameters].push([Ticket.name])
    conditions_filtros[:query].push("available_from <= ?")
    conditions_filtros[:parameters].push(DateTime.now.to_date)
    conditions_filtros[:query].push("? <= available_to")
    conditions_filtros[:parameters].push(DateTime.now.to_date)
    conditions_filtros[:query].push("ativo=?")
    conditions_filtros[:parameters].push(true)
    if conditions_filtros[:query].length > 0
      conditions_query.concat(conditions_filtros[:query])
      conditions_params.concat(conditions_filtros[:parameters])
    end

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  private

  def validates_available_period_dates
    errors.add(:available_from_string, "é inválida") if @available_from_invalid
    errors.add(:available_to_string, "é inválida") if @available_to_invalid
  end

  def validates_period_interval
    errors.add(:available_from_string, "maior que data de término") if !self.available_from.blank? && !self.available_to.blank? && self.available_from > self.available_to
    errors.add(:available_from, "maior que data de término") if !self.available_from.blank? && !self.available_to.blank? && self.available_from > self.available_to
  end
end
