class ClassroomTicket < ActiveRecord::Base
  attr_accessible :user_id, :classroom_event_id, :code, :status, :fatura_id

  values_for :status, :has => [:active, :inactive], :add => [:named_scopes, :predicate_methods, :constants]

  belongs_to :user
  belongs_to :classroom_event

  before_validation_on_create :ensure_default_values

  validates_presence_of :user_id
  validates_presence_of :classroom_event_id
  validates_presence_of :code
  validates_presence_of :status
  validates_presence_of :fatura_id

  named_scope :active, :conditions => {:status => ClassroomTicket::STATUS_ACTIVE.to_s}
  named_scope :code, lambda { |code|
    options = {:include => :user}
    options[:conditions] = ["code ILIKE ?", "#{code}%"] if code.present?
    options
  }
  named_scope :user_tickets, lambda { |user|
    conditions = ['faturas.status=?', 'faturas.user_id=?', 'classroom_tickets.user_id=?']
    params = [Fatura::STATUS_APPROVED.to_s, user.id, user.id]

    conditions << "produtos.start_datetime>=?"
    params << DateTime.now.utc

    conditions << "classroom_tickets.status=?"
    params << ClassroomTicket::STATUS_ACTIVE.to_s

    conditions << "classroom_tickets.fatura_id = faturas.id"
    conditions = [conditions.join(" AND ")].concat(params)

    return {:joins => 'INNER JOIN produtos ON classroom_tickets.classroom_event_id = produtos.id ' +
               'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id ' +
               'INNER JOIN faturas ON fatura_items.fatura_id = faturas.id',
     :conditions => conditions,
     :order => "classroom_tickets.classroom_event_id ASC",
     :include => :classroom_event,
     :select => 'DISTINCT(classroom_tickets.*)',
     :group => ClassroomTicket.sql_column_names}
  }
  named_scope :for_classroom_event, lambda { |classroom_event|
    {:conditions => {:classroom_event_id => classroom_event.id}}
  }


  def write_off!
    self.update_attribute(:status, ClassroomTicket::STATUS_INACTIVE)
  end

  protected

  def self.sql_column_names
    ClassroomTicket.column_names.collect { |column_name|
      "classroom_tickets.#{column_name}"
    }.join(",")
  end

  def ensure_default_values
    if self.code.blank? && self.user_id.present? && self.classroom_event_id.present?
      self.code = CodeGenerator.generate_code while !ClassroomTicket.code_valid?(self.code, self.user_id, self.classroom_event_id)
    end
    self.status = ClassroomTicket::STATUS_ACTIVE if self.status.blank?
  end

  def self.code_valid?(code, user_id, classroom_event_id)
    return false if code.nil?
    !ClassroomTicket.exists? :code => code, :user_id => user_id, :classroom_event_id => classroom_event_id
  end
end
