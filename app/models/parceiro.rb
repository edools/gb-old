class Parceiro < ActiveRecord::Base

  ordered_by 'nome ASC'

  validates_presence_of :nome
  validates_presence_of :url
  validates_format_of :url, :with => /^(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?$/, :unless => Proc.new { |p| p.url.blank? }

  before_validation :format_url

  has_attachment :imagem,
                    :styles => { :thumbnail => { :geometry => "280x150#", :convert_options => '-strip -unsharp 0x.5' } },
                    :processors => [:thumbnail]

  # validations
  @file_types = YAML.load_file("#{RAILS_ROOT}/config/allowed_file_types.yml")
  validates_attachment_presence :imagem
  validates_attachment_content_type :imagem,  :content_type => @file_types['images']

  private

  def format_url
    if !url.blank? && !url.start_with?('http://') && !url.start_with?('https://')
      self.url = "http://" + self.url
    end
  end
end
