class DescontoItem < ActiveRecord::Base
  belongs_to :produto
  belongs_to :desconto

  validates_presence_of :produto_id

  ordered_by 'produtos.nome ASC'
end
