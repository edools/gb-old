class ProdutosProfessor < ActiveRecord::Base
  belongs_to :produto
  belongs_to :professor

  validates_presence_of :professor_id
  validates_uniqueness_of :professor_id, :scope => :produto_id
end
