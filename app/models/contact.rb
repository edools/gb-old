class Contact < Tableless

  column :name,         :string
  column :email,        :string
  column :subject,      :string
  column :message,      :string
  column :created_at

  validates_presence_of     :name, :email, :message
  validates_presence_of     :subject
  validates_format_of       :email,    :with => Authentication.email_regex, :message => Authentication.bad_email_message, :unless => Proc.new {|p| p.email.blank? }

end
