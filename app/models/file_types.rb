class FileTypes < Settingslogic
  source "#{Rails.root}/config/allowed_file_types.yml"
end