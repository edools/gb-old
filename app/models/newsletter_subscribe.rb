class NewsletterSubscribe < ActiveRecord::Base

  belongs_to :categoria, :class_name => "CategoriaNewsletterSubscribe", :foreign_key => "categoria_newsletter_subscribe_id"

  ordered_by 'name, email'

  validates_presence_of     :email
  validates_uniqueness_of   :email
  validates_format_of       :email,    :with => Authentication.email_regex, :message => Authentication.bad_email_message

end
