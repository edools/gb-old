class Depoimento < ActiveRecord::Base
  attr_accessible :testimonial_type, :nome, :descricao, :imagem, :video_splash, :video

  values_for :testimonial_type, :has => [:text, :video], :add => [:named_scopes, :predicate_methods, :constants]

  ordered_by 'created_at DESC'

  named_scope :videos, lambda {
    { :conditions => { :testimonial_type => TESTIMONIAL_TYPE_VIDEO.to_s } }
  }

  named_scope :texts, lambda {
    { :conditions => { :testimonial_type => TESTIMONIAL_TYPE_TEXT.to_s } }
  }

  has_attachment :imagem, :styles => { :real => { :geometry => Settings.testimonial_size, :convert_options => '-auto-orient -strip -unsharp 0x.5' } }
  has_attachment :video, :base_path => ":class/:id/:style/:normalized_filename"
  has_attachment :video_splash, :base_path => ":class/:id/video_splash/:style/:normalized_filename", :styles => { :real => { :geometry => "528x297#", :convert_options => '-auto-orient -strip -unsharp 0x.5' } }

  # validations
  @file_types = YAML.load_file("#{RAILS_ROOT}/config/allowed_file_types.yml")
  validates_attachment_content_type :imagem,  :content_type => @file_types['images']
  validates_presence_of :nome
  validates_presence_of :descricao
  validates_presence_of :testimonial_type
  validates_attachment_presence :video, :if => Proc.new { |d| TESTIMONIAL_TYPE_VIDEO.to_s.eql?(d.testimonial_type.to_s) }
  validates_attachment_content_type :video_splash,  :content_type => @file_types['images']
  #validates_attachment_content_type :video,  :content_type => @file_types['videos'].grep(/flv/)

  validate :validate_content_type

  private

  def validate_content_type
    #if self.errors.invalid? :video_content_type
    #  self.errors.add :video, "não é um arquivo flv"
    #end

    if self.errors.invalid? :imagem_content_type
      self.errors.add :imagem, "não é uma imagem"
    end

    if self.errors.invalid? :video_splash_content_type
      self.errors.add :video_splash, "não é uma imagem"
    end
  end
end