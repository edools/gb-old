class Role < ActiveRecord::Base
  ADMINISTRATOR = "administrador"
  CUSTOMER = "cliente"

  ordered_by 'name ASC'

  def self.administrador
    ADMINISTRATOR
  end

  def self.cliente
    CUSTOMER
  end

  def self.values
    {administrador => administrador.humanize, cliente => cliente.humanize}
  end

end