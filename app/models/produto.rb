class Produto < ActiveRecord::Base
  acts_as_audited

  has_many :produto_anexos, :dependent => :destroy, :conditions => 'demonstracao = false', :order => "arquivo_file_name ASC"
  has_many :fatura_items

  ordered_by "nome ASC"

  belongs_to :categoria, :class_name => "CategoriaCurso", :foreign_key => "categoria_curso_id"

  accepts_nested_attributes_for :produto_anexos, :allow_destroy => true

  validates_presence_of :nome
  validates_uniqueness_of :nome
  validates_presence_of :investimento

  usar_como_dinheiro :investimento

  has_attachment :imagem,
    :styles => { :thumbnail => { :geometry => Settings.product.thumbnail_size, :convert_options => '-auto-orient -strip -unsharp 0x.5' },
    :player => { :geometry => Settings.product.player_size, :convert_options => '-auto-orient -strip -unsharp 0x.5' }
  },
    :processors => [ :thumbnail ]

  def self.per_page
    10
  end

  def self.col_list
    Produto.column_names.collect { |column_name|
      "produtos.#{column_name}"
    }.join(",")
  end

  named_scope :destaques, :joins => "INNER JOIN destaque_produtos ON destaque_produtos.produto_id=produtos.id"

  # retorna os produtos bem como a quantidade de ativos do produto
  # ou seja, quantos produtos foram comprados e que estao ativos
  named_scope :total_por_produto, :joins => {:fatura_items => :fatura},
  :conditions => ["faturas.status=?", Fatura::STATUS_APPROVED.to_s],
  :group => Produto.col_list,
  :select => "produtos.*, COUNT(*) AS total_comprado",
  :order => "total_comprado DESC"

  def self.count_sold(product)
    self.count(
      :conditions => ["faturas.status=? AND produtos.id=?", Fatura::STATUS_APPROVED.to_s, product.id],
      :joins => {:fatura_items => :fatura}
    )
  end
  # retorna os produtos bem como a quantidade de ativos do produto
  # ou seja, quantos produtos foram comprados e que estao ativos
  named_scope :com_quantidade_nao_expirada, :joins => {:fatura_items => :fatura},
  :conditions => ["faturas.status=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at", Fatura::STATUS_APPROVED.to_s, Date.today],
  :group => Produto.col_list,
  :select => "produtos.*, COUNT(*) AS quantidade_nao_expirada",
  :order => "quantidade_nao_expirada DESC"

  named_scope :assistiveis, lambda {
    # infelizmente o rails quebra as classes derivadas da class Produto caso essa checagem nao for feita
    {:conditions => {:type => [VideoAula.name,Pacote.name] }} if self == Produto
  }

  named_scope :nao_incluir_assinaturas, lambda {
    # infelizmente o rails quebra as classes derivadas da class Produto caso essa checagem nao for feita
    {:conditions => {:type => [VideoAula.name, Pacote.name, Arquivo.name, Ticket.name, ClassroomEvent.name, SimulatedTest.name] }} if self == Produto
  }

  named_scope :ativos, :conditions => {:ativo => true}
  named_scope :disponivel_venda, :conditions => {:disponivel_venda => true}

  # busca todos os produtos assistiveis e ativos
  named_scope :search,  lambda { | filtros |
    conditions = Produto.generate_conditions(filtros)

    conditions[:query].push("type IN (?)")
    conditions[:parameters].push([VideoAula.name,Pacote.name])
    conditions[:query].push("ativo=?")
    conditions[:parameters].push(true)
    conditions[:query].push("disponivel_venda=?")
    conditions[:parameters].push(true)

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  # busca todos os produtos assistiveis e ativos da assinatura
  named_scope :search_meus_cursos_da_assinatura,  lambda { | filtros, user, assinatura_id |
    assinatura = Assinatura.ativa_do_user(user, assinatura_id).first
    add_select_if_user_is_admin = !user.is_admin? ? ",'#{assinatura.expira_at(nil,false)}' AS fatura_items_expira_at" : ""
    conditions = Produto.generate_conditions(filtros)

    #if assinatura.produtos_ignorados.length > 0
    #  conditions[:query].push("produtos.id NOT IN (?)")
    #  conditions[:parameters].push(assinatura.produtos_ignorados)
    #end
    if assinatura.produtos.length > 0
      conditions[:query].push("produtos.id IN (?)")
      conditions[:parameters].push(assinatura.produtos)
    else # caso nao foi adiciona os produtos da assinatura, forca a consulta nao retornar nenhum produto
      conditions[:query].push("produtos.id=?")
      conditions[:parameters].push(0)
    end
    conditions[:query].push("produtos.type IN (?)")
    conditions[:parameters].push([VideoAula.name,Pacote.name])
    conditions[:query].push("produtos.ativo=?")
    conditions[:parameters].push(true)
    if conditions[:query].length > 0
      {:select => "produtos.*#{add_select_if_user_is_admin}",  :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  # busca todos os produtos
  named_scope :search_backend,  lambda { | filtros |
    conditions = Produto.generate_conditions(filtros, true)

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  named_scope :search_not_expired_product, lambda { |product_id, current_user|
    statements = {}
    conditions_query = ["produtos.id=?"]
    conditions_params = [product_id]
    if !current_user.is_admin?
      statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
        :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list}
      conditions_query.concat(['faturas.status=?', 'faturas.user_id=?', '? <= fatura_items.expira_at'])
      conditions_params.concat([Fatura::STATUS_APPROVED.to_s,current_user.id, Date.today.to_s])
    end

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  # busca todos os produtos ativos, que nao expiraram e assistiveis
  named_scope :search_meus_cursos, lambda { |filtros, current_user|
    statements = {}
    conditions_query = []
    conditions_params = []
    if !current_user.is_admin? && !current_user.trial_period?
      statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
        :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list}
      conditions_query = ['faturas.status=?', 'faturas.user_id=?', '? <= fatura_items.expira_at']
      conditions_params = [Fatura::STATUS_APPROVED.to_s,current_user.id, Date.today.to_s]
    end

    conditions_filtros = generate_conditions(filtros)
    conditions_filtros[:query].push("type IN (?)")
    conditions_filtros[:parameters].push([VideoAula.name,Pacote.name])

    conditions_filtros[:query].push("ativo=?")
    conditions_filtros[:parameters].push(true)
    if conditions_filtros[:query].length > 0
      conditions_query.concat(conditions_filtros[:query])
      conditions_params.concat(conditions_filtros[:parameters])
    end

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  named_scope :not_started, lambda { {:conditions => ["start_datetime IS NULL OR start_datetime >= ?", DateTime.now.utc]} }

  def after_invoice_approved(invoice)
  end

  # converte o produto em um ou mais items de fatura
  # sobrescreva para a classe derivida de produto
  # Ex.: VideoAula, Pacote
  def make_fatura_items(quantity, fatura, cupom)

  end

  # indica se o produto pode ser baixado pelo 'user'
  # sobrescreva esse metodo na classe derivada de Produto
  def downloadable?(user)
    false
  end

  # define quando o produto expira a partir da data passada
  # e do item da fatura correspondente
  # sobrescreva esse metodo na classe derivada de Produto
  def get_expira_at(date,fatura_item)
  end

  # envia um email ao user informando que a data de expiracao foi atualizada
  # sobrescreva esse metodo na classe derivada de Produto
  def enviar_mensagem_expiracao_atualizou(user, expira_at)

  end

  # indica se o usuario comprou um video
  # sobrescreva esse metodo na classe derivada de Produto
  def has_video_to_user?(user,video_upload)
    false
  end

  def total(quantidade_produto)
    self.investimento_com_desconto_ativo * quantidade_produto
  end

  # calcula o valor total de um produto aplicando o desconto do cupom
  def total_com_desconto(quantidade,desconto_cupom)
    total(quantidade) - desconto_cupom
  end

  # retorna o preco do produto aplicado o preco do desconto da loja ativo
  def investimento_com_desconto_ativo
    Desconto.investimento_com_desconto(self)
  end

  # data de expiracao do curso
  def expira_at(user = nil, format = true)
    return @expira_at unless defined?(@expira_at).nil?

    # se o atributo existe significa que o produto foi carregado
    # atraves do named scope :search_meus_cursos
    if self.respond_to?("fatura_items_expira_at") and !self.fatura_items_expira_at.blank?
      @expira_at = format ? Date.parse(self.fatura_items_expira_at).to_s(:padrao_data) : self.fatura_items_expira_at
    elsif user.is_admin?
      @expira_at = "nunca expira"
    elsif user.trial_period?
      @expira_at = "perído grátis"
    else
      p = Produto.search_not_expired_product(self.id, user).first
      data_como_curso_avulso = p.blank? ? nil : p.fatura_items_expira_at
      data_como_assinatura = Assinatura.data_expiracao_do_produto(self, user)

      if(!data_como_curso_avulso.blank? && data_como_assinatura.blank?)
        @expira_at = format ? Date.parse(data_como_curso_avulso).to_s(:padrao_data) : data_como_curso_avulso
      elsif (data_como_curso_avulso.blank? && !data_como_assinatura.blank?)
        @expira_at = format ? Date.parse(data_como_assinatura).to_s(:padrao_data) : data_como_assinatura
      elsif Date.parse(data_como_curso_avulso) >= Date.parse(data_como_assinatura)
        @expira_at = format ? Date.parse(data_como_curso_avulso).to_s(:padrao_data) : data_como_curso_avulso
      else
        @expira_at = Date.parse(data_como_assinatura).to_s(:padrao_data)
      end
    end

    return @expira_at
  end

  def can_buy_multiple?
    false
  end

  protected

  def self.generate_conditions(filtros, filtros_backend = false)
    conditions = {:query => [], :parameters => []}
    return conditions if filtros.blank?
    filtros.each do |filtro, valor|
      next if valor.blank?
      case filtro
      when "nome"
        valor = "%" + valor + "%"
        conditions[:query].push("(produtos.nome ILIKE ? OR produtos.descricao ILIKE ?)")
        conditions[:parameters].push(valor)
        conditions[:parameters].push(valor)
      when "categoria"
        conditions[:query].push("produtos.categoria_curso_id = ?")
        conditions[:parameters].push(valor)
      when "tipo"
        if filtros_backend or [Pacote.name, VideoAula.name].include?(valor)
          conditions[:query].push("produtos.type = ?")
          conditions[:parameters].push(valor)
        end
      when "periodicidade"
        if filtros_backend
          conditions[:query].push("produtos.periodicidade = ?")
          conditions[:parameters].push(valor)
        end
      when "ativo"
        if filtros_backend
          conditions[:query].push("produtos.ativo = ?")
          conditions[:parameters].push(valor)
        end
      when "disponivel_venda"
        if filtros_backend
          conditions[:query].push("produtos.disponivel_venda = ?")
          conditions[:parameters].push(valor)
        end
      end
    end
    return conditions
  end
end