class Estado < ActiveRecord::Base
  has_many :cidades
  ordered_by 'nome ASC'
end
