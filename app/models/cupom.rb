class Cupom < ActiveRecord::Base
  belongs_to :produto
  validates_presence_of :codigo, :validade, :desconto
  validates_presence_of :produto, :unless => :apply_for_some_set_of_products?
  validates_uniqueness_of :codigo
  validates_format_of :codigo, :with => /^[a-zA-Z0-9]+$/
  validates_numericality_of :quantidade_disponivel, :only_integer => true, :greater_than_or_equal_to => 0, :allow_nil => true
  validates_numericality_of :desconto, :only_integer => true, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 100
  validate :validates_validade_string

  named_scope :search_backend,  lambda { |filters|
    conditions = {:query => [], :parameters => []}
    if filters.present?
      filters.each do |filter, value|
        next if value.blank?
        case filter
          when "code"
            value = "%#{value}%"
            conditions[:query].push("cupoms.codigo ILIKE ?")
            conditions[:parameters].push(value)
        end
      end
    end

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  def validade_string
    validade.to_s(:padrao_input_data) unless validade.blank?
  end

  def validade_string=(validade_str)
    self.validade = Date.strptime(validade_str, Date::DATE_FORMATS[:padrao_input_data])
  rescue ArgumentError
    @validade_string_invalid = true
  end

  def self.valido?(codigo, lista_produtos_ids = nil)
    cupom = Cupom.first :conditions => ['? <= validade AND codigo=?',Date.today,codigo]

    return false if cupom.nil?

    if !lista_produtos_ids.blank?
      valido_na_lista = false
      Produto.find_all_by_id(lista_produtos_ids).each do |produto|
        valido_na_lista = true and break if cupom.produto_id == produto.id
        valido_na_lista = true and break if cupom.apply_for_all_assinaturas && produto.is_a?(Assinatura)
        valido_na_lista = true and break if cupom.apply_for_all_cursos_online && produto.is_a?(VideoAula)
        valido_na_lista = true and break if cupom.apply_for_all_cursos_em_pdf && produto.is_a?(Arquivo)
        valido_na_lista = true and break if cupom.apply_for_all_pacotes && produto.is_a?(Pacote)
        valido_na_lista = true and break if cupom.apply_for_all_tickets && produto.is_a?(Ticket)
      end
      return false unless valido_na_lista
    end


    return true if cupom.quantidade_disponivel.blank?
    total_usado = FaturaItem.count :conditions => ['cupom_codigo=?',cupom.codigo]

    return total_usado < cupom.quantidade_disponivel
  end

  def self.desconto_vale_na_lista?(codigo, lista_produtos_ids)
    return false if lista_produtos_ids.blank?
    return Cupom.valido?(codigo,lista_produtos_ids)
  end

  def desconto_total(produto,quantidade_produto)
    return 0 unless Cupom.valido?(self.codigo,[produto.id])
    total = produto.total(quantidade_produto)
    total * self.desconto / 100
  end

  def total_usado
    FaturaItem.count :conditions => ['cupom_codigo=?',self.codigo]
  end

  def apply_for_all_products
    apply_for_all_assinaturas && apply_for_all_cursos_online && apply_for_all_cursos_em_pdf && apply_for_all_pacotes && apply_for_all_tickets
  end

  def apply_for_all_products=(value)

  end

  def before_save
    self.produto_id = nil if apply_for_some_set_of_products?
  end

  private

  def validates_validade_string
    errors.add(:validade_string, "é inválida") if @validade_string_invalid
  end

  def apply_for_some_set_of_products?
    apply_for_all_assinaturas || apply_for_all_cursos_online || apply_for_all_cursos_em_pdf || apply_for_all_pacotes || apply_for_all_tickets
  end
end
