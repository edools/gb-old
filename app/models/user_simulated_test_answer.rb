class UserSimulatedTestAnswer < ActiveRecord::Base
  acts_as_audited
  # attr_accessible :user_simulated_test_id, :simulated_test_question_id, :answer

  belongs_to :simulated_test_question, :class_name => "SimulatedTestQuestion", :foreign_key => "simulated_test_question_id"
  belongs_to :user_simulated_test, :class_name => "UserSimulatedTest", :foreign_key => "user_simulated_test_id"

  values_for :answer, :has => [:a, :b, :c, :d, :e, :certo, :errado], :add => [:named_scopes, :predicate_methods, :constants], :allow_nil => true
  validates_presence_of :answer, :if => "simulated_test_question.mandatory?"
  validates_presence_of :simulated_test_question_id

  def is_answer_correct?
    simulated_test_question.answer_option.to_s == answer.to_s
  end

  def is_answer?(option)
    answer.to_s == option.to_s
  end
end
