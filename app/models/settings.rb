require 'site_theme'

class Settings < Settingslogic
  source "#{Rails.root}/config/settings.yml"
  namespace ::SiteTheme.name

  def professor_image_styles
    {}.tap do |styles|
      Settings.professor.thumbnails.each do |style, size|
        styles[style.to_sym] = {:geometry => size, :convert_options => '-strip -unsharp 0x.5'}
      end
    end
  end

  def products_videos_order
    Settings.product.allow_nested_items_sort ? "products_video_uploads.position ASC" : "video_uploads.nome ASC"
  end

  def pacote_pacote_items_order
    Settings.product.allow_nested_items_sort ? "pacote_items.position ASC" : "pacote_items.created_at DESC"
  end

  def simulated_test_questions_order
    Settings.product.allow_nested_items_sort ? "simulated_test_questions.position ASC" : "simulated_test_questions.created_at DESC"
  end
end
