class LiveChannelSupportingMaterial < ActiveRecord::Base
  include RakeUtils

  belongs_to :live_channel

  has_attachment :attachment, :base_path => ":class/:id/:style/:normalized_filename"

  ordered_by 'created_at DESC'

  before_save :set_default_label

  def set_default_label
    self.label = self.attachment_file_name if label.blank?
  end

  def self.per_page
    24
  end
end
