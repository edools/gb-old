class SimulatedTestProduct < ActiveRecord::Base
  acts_as_audited

  belongs_to :simulated_test, :class_name => "SimulatedTest", :foreign_key => "simulated_test_id"
  belongs_to :product, :class_name => "Produto", :foreign_key => "product_id"

  validates_presence_of :product_id
  validates_uniqueness_of :product_id, :scope => :simulated_test_id
end
