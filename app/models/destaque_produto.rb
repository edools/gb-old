class DestaqueProduto < ActiveRecord::Base
  belongs_to :produto

  ordered_by "position ASC"
  acts_as_list

  def self.process_list(destaques)
    produtos_ids = if not destaques.blank?
                     destaques.uniq
                   else
                     []
                   end
    produtos_ids.each_with_index do |produto_id, index|
      if DestaqueProduto.exists? :produto_id => produto_id
        DestaqueProduto.update_all("position=#{index}","produto_id=#{produto_id}")
      else
        DestaqueProduto.create :produto_id => produto_id, :position => index
      end
    end

    DestaqueProduto.delete_all ["produto_id NOT IN (?)", produtos_ids + [0]]
  end
end
