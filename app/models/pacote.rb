class Pacote < Produto
  include Sanitizable
  include Validatable

  acts_as_audited

  has_many :pacote_items, :dependent => :destroy, :include => :video_aula, :order => Settings.pacote_pacote_items_order
  has_many :video_aulas, :through => :pacote_items, :order => Settings.pacote_pacote_items_order
  belongs_to :video_upload_demonstracao, :class_name => "VideoUpload", :foreign_key => "video_upload_demonstracao_id"

  sanitize_attributes_for_association :pacote_items, :association_key => :video_aula_id

  validates_presence_of_many :pacote_items
  validates_presence_of :video_upload_demonstracao_id
  validates_presence_of :categoria_curso_id
  validates_presence_of :tempo_expiracao
  validates_numericality_of :tempo_expiracao, :only_integer => true, :greater_than => 0, :less_than_or_equal_to => 999
  validates_attachment_presence :imagem

  def to_param
    Slug.create([id, nome])
  end

  def make_fatura_items(quantity, fatura, cupom)
    if !cupom.nil? && Cupom.valido?(cupom.codigo,[self.id])
      return FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
    end
    FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
  end

  def downloadable?(user)
    return false if user.nil?
    return true if user.is_admin?
    return true if user.trial_period?
    return true if Assinatura.alguma_ativa_autoriza_produto?(self,user)
    return Pacote.count(:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
      :conditions => ['faturas.status=? AND faturas.user_id=? AND produtos.id=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at',Fatura::STATUS_APPROVED.to_s,user,self,Date.today.to_s]) > 0
  end

  def has_video_to_user?(user,video_upload)
    return false if user.nil?
    return true if user.is_admin?
    return true if user.trial_period?
    return false unless PacoteItem.count(:joins => 'INNER JOIN produtos ON produtos.id=pacote_items.video_aula_id INNER JOIN products_video_uploads ON products_video_uploads.product_id=produtos.id', :conditions => ['pacote_items.pacote_id=? AND products_video_uploads.video_upload_id=? ',self,video_upload]) > 0
    return true if Assinatura.alguma_ativa_autoriza_produto?(self,user)
    return FaturaItem.count(:joins => 'INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
                            :conditions => ['faturas.status=? AND faturas.user_id=? AND fatura_items.produto_id=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at',Fatura::STATUS_APPROVED.to_s,user,self, Date.today.to_s]) > 0
  end

  # veja em Produto::get_expira_at
  def get_expira_at(date,fatura_item)
    date + tempo_expiracao.days
  end
end