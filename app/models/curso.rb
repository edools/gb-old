class Curso < ActiveRecord::Base
  include Sanitizable

  acts_as_audited

  has_many :curso_anexos, :dependent => :destroy
  has_many :professors, :class_name => "CursosProfessor", :dependent => :destroy

  belongs_to :categoria, :class_name => "CategoriaCurso", :foreign_key => "categoria_curso_id"
  belongs_to :demonstration_video_upload, :class_name => "VideoUpload", :foreign_key => "demonstration_video_upload_id"

  usar_como_dinheiro :investimento

  ordered_by 'created_at DESC'

  sanitize_attributes_for_association :professors, :association_key => :professor_id
  accepts_nested_attributes_for :curso_anexos, :allow_destroy => true

  validates_presence_of :nome
  validates_presence_of :categoria_curso_id
  validates_numericality_of :investimento, :greater_than_or_equal_to => 0

  has_attachment :imagem,
    :styles => { :player => { :geometry => Settings.product.player_size, :convert_options => '-auto-orient -strip -unsharp 0x.5' },
    :thumbnail => { :geometry => Settings.product.thumbnail_size, :convert_options => '-auto-orient -strip -unsharp 0x.5' } },
    :processors => [ :thumbnail ]

  def self.per_page
    10
  end

  named_scope :search,  lambda { | filtros |
    conditions = {:query => ["ativo=?"], :parameters => [true]}
    if filtros
      filtros.each do |filtro, valor|
        unless valor.blank?
          case filtro
          when "nome"
            valor = "%" + valor + "%"
            conditions[:query].push("cursos.nome ILIKE ? OR cursos.descricao ILIKE ?")
            conditions[:parameters].push(valor)
            conditions[:parameters].push(valor)
          when "categoria"
            conditions[:query].push("cursos.categoria_curso_id = ?")
            conditions[:parameters].push(valor)
          end
        end
      end
    end
    { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) } if conditions[:query].length > 0
  }

  def to_param
    Slug.create([id, nome])
  end
end
