class VideoAula < Produto
  include Sanitizable
  include Validatable

  acts_as_audited

  has_many :professors, :class_name => "ProdutosProfessor", :dependent => :destroy, :foreign_key => 'produto_id'
  has_many :videos, :class_name => "ProductsVideoUpload", :dependent => :destroy, :foreign_key => 'product_id', :include => :video_upload, :order => Settings.products_videos_order
  belongs_to :video_upload_demonstracao, :class_name => "VideoUpload", :foreign_key => "video_upload_demonstracao_id"

  has_many :package_items, :foreign_key => 'video_aula_id', :class_name => "PacoteItem"
  has_many :packages, :through => :package_items, :source => :pacote

  sanitize_attributes_for_association :professors, :association_key => :professor_id
  sanitize_attributes_for_association :videos, :association_key => :video_upload_id
  validates_associated :videos

  validates_presence_of_many :videos
  validates_presence_of :video_upload_demonstracao_id
  validates_presence_of :categoria_curso_id
  validates_presence_of :tempo_expiracao
  validates_numericality_of :tempo_expiracao, :only_integer => true, :greater_than => 0, :less_than_or_equal_to => 999
  validates_attachment_presence :imagem

  def downloadable?(user)
    return false if user.nil?
    return true if user.is_admin?
    return true if user.trial_period?
    return true if Assinatura.alguma_ativa_autoriza_produto?(self, user)
    return VideoAula.count(:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
                           :conditions => ['faturas.status=? AND faturas.user_id=? AND produtos.id=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at', Fatura::STATUS_APPROVED.to_s, user, self, Date.today.to_s]) > 0
  end

  # veja na classe Produto
  def make_fatura_items(quantity, fatura, cupom)
    if !cupom.nil? && Cupom.valido?(cupom.codigo, [self.id])
      return FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
    end
    FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
  end

  # veja na classe Produto
  def has_video_to_user?(user, video_upload)
    return false if user.nil?
    return true if user.is_admin?
    return true if user.trial_period?
    return false unless ProductsVideoUpload.exists? :product_id => self.id, :video_upload_id => video_upload.id
    return true if Assinatura.alguma_ativa_autoriza_produto?(self, user)
    return FaturaItem.count(:joins => 'INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
                            :conditions => ['faturas.status=? AND faturas.user_id=? AND fatura_items.produto_id=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at', Fatura::STATUS_APPROVED.to_s, user, self, Date.today.to_s]) > 0
  end

  # veja em Produto::get_expira_at
  def get_expira_at(date, fatura_item)
    date + tempo_expiracao.days
  end

  def to_param
    Slug.create([id, nome])
  end

  def contains_video?(video)
    self.videos.exists?(:video_upload_id => video)
  end
end
