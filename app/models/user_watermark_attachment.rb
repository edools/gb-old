class UserWatermarkAttachment < ActiveRecord::Base
  attr_accessible :user_id, :base_product_attachment_id, :watermarked_attachment, :job_id

  belongs_to :user
  belongs_to :base_product_attachment, :class_name => "ProdutoAnexo"

  has_attachment :watermarked_attachment,
                 :access_type => :private,
                 :base_path => ":class/:attachment/:id/:style/:normalized_filename"
end
