class ProductsVideoUpload < ActiveRecord::Base
  belongs_to :product, :class_name => "Produto"
  belongs_to :video_upload

  validates_presence_of   :video_upload_id
  validates_uniqueness_of :video_upload_id,
                          :scope => :product_id

  validates_presence_of :view_limit,
                        :if => Proc.new { |v| Settings.user.manage_views }
  validates_numericality_of :view_limit,
                            :allow_blank => false,
                            :allow_nil => false,
                            :only_integer => true,
                            :greater_than => 0,
                            :less_than => 9999,
                            :if => Proc.new { |v| Settings.user.manage_views }
end
