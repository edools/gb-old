class Arquivo < Produto
  include Sanitizable

	acts_as_audited

	has_one :material_demonstracao, :class_name => "ProdutoAnexo", :foreign_key => "produto_id", :conditions => 'demonstracao = true', :dependent => :destroy
	has_many :professors, :class_name => "ProdutosProfessor", :dependent => :destroy, :foreign_key => 'produto_id'

  sanitize_attributes_for_association :professors, :association_key => :professor_id
	accepts_nested_attributes_for :material_demonstracao

	validates_presence_of :categoria_curso_id
	validates_presence_of :tempo_expiracao
  validates_numericality_of :tempo_expiracao, :only_integer => true, :greater_than => 0, :less_than_or_equal_to => 999

	before_save :update_material_demonstracao

	def update_material_demonstracao
	  self.material_demonstracao.demonstracao = true unless self.material_demonstracao.blank?
  end

	def downloadable?(user)
    return false if user.nil?
    return true if user.is_admin?
    #return true if Assinatura.alguma_ativa_autoriza_produto?(self,user)
    return Arquivo.count(:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
      :conditions => ['faturas.status=? AND faturas.user_id=? AND produtos.id=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at',Fatura::STATUS_APPROVED.to_s,user,self,Date.today.to_s]) > 0
  end

  # veja na classe Produto
  def make_fatura_items(quantity, fatura, cupom)
    if !cupom.nil? && Cupom.valido?(cupom.codigo,[self.id])
      return FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
    end
    FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
  end

  # veja em Produto::get_expira_at
  def get_expira_at(date,fatura_item)
    date + tempo_expiracao.days
  end

  def to_param
    Slug.create([id, nome])
  end

  # retorna o total de arquivos do user
  def self.total_ativas(user)
    Arquivo.ativos_do_user(user).count(:distinct => true, :select => 'produtos.id')
  end

  named_scope :search,  lambda { | filtros |
    conditions = Produto.generate_conditions(filtros)

    #conditions[:query].push("type IN (?)")
    #conditions[:parameters].push([Arquivo.name])
    conditions[:query].push("ativo=?")
    conditions[:parameters].push(true)
    conditions[:query].push("disponivel_venda=?")
    conditions[:parameters].push(true)

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  # retorna todas os arquivos de um user
   named_scope :ativos_do_user,  lambda { | current_user |
     statements = {}
     conditions_query = []
     conditions_params = []
     if !current_user.is_admin?
       statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
         :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list }
       conditions_query = ['faturas.status=?', 'faturas.user_id=?', '(fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at)']
       conditions_params = [Fatura::STATUS_APPROVED.to_s,current_user.id, Date.today.to_s]
     end

     conditions_filtros = generate_conditions(nil)
     conditions_filtros[:query].push("produtos.ativo=?")
     conditions_filtros[:parameters].push(true)
     if conditions_filtros[:query].length > 0
       conditions_query.concat(conditions_filtros[:query])
       conditions_params.concat(conditions_filtros[:parameters])
     end

     statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
     return statements
   }

   # busca todos os arquivos ativos, que nao expiraram
   named_scope :search_meus_arquivos,  lambda { |filtros, current_user|
     statements = {}
     conditions_query = []
     conditions_params = []
     if !current_user.is_admin?
       statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
         :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list}
       conditions_query = ['faturas.status=?', 'faturas.user_id=?', '? <= fatura_items.expira_at']
       conditions_params = [Fatura::STATUS_APPROVED.to_s,current_user.id, Date.today.to_s]
     end

     conditions_filtros = generate_conditions(filtros)
     #conditions_filtros[:query].push("type IN (?)")
     #conditions_filtros[:parameters].push([Arquivo.name])

     conditions_filtros[:query].push("ativo=?")
     conditions_filtros[:parameters].push(true)
     if conditions_filtros[:query].length > 0
       conditions_query.concat(conditions_filtros[:query])
       conditions_params.concat(conditions_filtros[:parameters])
     end

     statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
     return statements
   }
end
