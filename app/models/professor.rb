class Professor < ActiveRecord::Base
  include Searchable

  validates_presence_of :nome
  validates_presence_of :curriculo
  validates_format_of   :site, :with => /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix, :unless => Proc.new { |p| p.site.blank? }
  validates_format_of   :email, :with => Authentication.email_regex, :unless => Proc.new { |p| p.email.blank? }

  refine_search :nome, :operator => :ilike, :match_style => :contains

  has_many :artigos
  ordered_by "nome ASC"
  has_attachment :imagem,
                 :styles => Settings.professor_image_styles,
                 :processors => [:thumbnail]

  def self.per_page
    10
  end

  def to_param
    Slug.create([id, nome])
  end
end