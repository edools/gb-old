class CodeGenerator
  class << self
    def generate_code(size = 10)
      alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
      out = ''
      size.times { out << alphabet[rand(alphabet.size)] }
      out.upcase
    end
  end
end