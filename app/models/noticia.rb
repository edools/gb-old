class Noticia < ActiveRecord::Base
  acts_as_taggable
  ordered_by 'created_at DESC'

  has_attachment :imagem, :styles => {:thumbnail => "90x70#"}, :processors => [:thumbnail]


  ordered_by 'created_at DESC'

  # validations
  validates_presence_of :nome
  validates_presence_of :corpo

  named_scope :search,  lambda { | filtros |
    conditions = {:query => [], :parameters => []}

    if filtros
      filtros.each do |filtro, valor|
        unless valor.blank?
          case filtro
          when "nome"
            valor = "%" + valor + "%"
            conditions[:query].push("(noticias.nome ILIKE ? OR noticias.corpo ILIKE ?)")
            conditions[:parameters].push(valor)
            conditions[:parameters].push(valor)
          end
        end
      end

      { :order => "noticias.created_at ASC", :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) } if conditions[:query].length > 0
    end
  }

  def to_param
    Slug.create([id, nome])
  end
end
