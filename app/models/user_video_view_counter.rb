class UserVideoViewCounter < ActiveRecord::Base
  attr_accessible :video_class_id, :package_id, :video_id, :user_id, :additional_views

  validates_numericality_of :additional_views,
                            :allow_blank => false,
                            :allow_nil => false,
                            :only_integer => true,
                            :greater_than_or_equal_to => 0,
                            :less_than => 9999

  validates_presence_of :video_class_id
  validate :video_class_should_belongs_to_package
  # validate :video_should_belongs_to_video_class

  belongs_to :video_class, :class_name => "VideoAula"
  belongs_to :package, :class_name => "Pacote"
  belongs_to :video, :class_name => "VideoUpload"

  named_scope :find_all_not_expired, :joins => "INNER JOIN fatura_items ON fatura_items.produto_id = video_class_id OR fatura_items.produto_id = package_id INNER JOIN faturas ON fatura_items.fatura_id = faturas.id",
              :include => [:video_class, :package, :video],
              :conditions => ["faturas.status=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at ", Fatura::STATUS_APPROVED.to_s, Date.today],
              :select => "DISTINCT user_video_view_counters.*, video_uploads.*",
              :order => "video_uploads.nome ASC"

  class << self
    def find_or_create_counter(video_class_id, package_id, video_id, user_id)
      find_or_create_by_video_class_id_and_package_id_and_video_id_and_user_id(video_class_id, package_id, video_id, user_id)
    end
  end

  def increment!
    self.counter += 1
    save!
  end

  def view_limit
    return @view_limit if @view_limit

    @view_limit = video_class_video.present? && video_class_video.view_limit? ? (video_class_video.view_limit * count_by_approved_invoices) : -1
  end

  def view_limit_complete
    self.view_limit + self.additional_views
  end

  def reach_view_limit?
    self.view_limit_complete <= self.counter
  end

  def remain_views
    remain_count = self.view_limit_complete - self.counter
    remain_count < 0 ? 0 : remain_count
  end

  private

  def count_by_approved_invoices
    self.package_id ? count_by_approved_invoices_for_package : count_by_approved_invoices_for_video_class
  end

  def count_by_approved_invoices_for_package
    UserVideoViewCounter.count :conditions => ["faturas.status=? AND video_class_id=? AND package_id=? AND fatura_items.produto_id=? AND user_video_view_counters.user_id=? AND user_video_view_counters.video_id=?", Fatura::STATUS_APPROVED.to_s, self.video_class_id, self.package_id, self.package_id, self.user_id, self.video_id], :joins => "INNER JOIN faturas ON faturas.user_id = user_video_view_counters.user_id INNER JOIN fatura_items ON fatura_items.fatura_id = faturas.id"
  end

  def count_by_approved_invoices_for_video_class
    UserVideoViewCounter.count :conditions => ["faturas.status=? AND video_class_id=? AND package_id IS NULL AND fatura_items.produto_id=? AND user_video_view_counters.user_id=? AND user_video_view_counters.video_id=?", Fatura::STATUS_APPROVED.to_s, self.video_class_id, self.video_class_id, self.user_id, self.video_id], :joins => "INNER JOIN faturas ON faturas.user_id = user_video_view_counters.user_id INNER JOIN fatura_items ON fatura_items.fatura_id = faturas.id"
  end

  def video_class_video
    @video_class_video ||= self.video_class.videos.first(:conditions => {:video_upload_id => self.video_id})
  end

  def video_class_should_belongs_to_package
    return unless self.package_id
    video_class = self.package.video_aulas.first(self.video_class_id)
    errors.add :video_class_id, "Invalid video class" unless video_class
  end

  def video_should_belongs_to_video_class
    errors.add :video_id, "Invalid video" unless video_class_video
  end
end
