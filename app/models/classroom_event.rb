class ClassroomEvent < Produto
  include Sanitizable

  attr_accessor :start_datetime_string

  acts_as_audited

  has_many :professors, :class_name => "ProdutosProfessor", :dependent => :destroy, :foreign_key => 'produto_id'
  belongs_to :video_upload_demonstracao, :class_name => "VideoUpload", :foreign_key => "video_upload_demonstracao_id"

  sanitize_attributes_for_association :professors, :association_key => :professor_id

  validates_presence_of :video_upload_demonstracao_id
  validates_presence_of :categoria_curso_id
  validate :validates_start_datetime
  validates_attachment_presence :imagem

  named_scope :search,  lambda { |filtros|
    conditions = Produto.generate_conditions(filtros)

    conditions[:query].push("ativo=?")
    conditions[:parameters].push(true)
    conditions[:query].push("disponivel_venda=?")
    conditions[:parameters].push(true)
    conditions[:query].push("start_datetime>=?")
    conditions[:parameters].push(DateTime.now.utc)

    if conditions[:query].length > 0
      { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
    end
  }

  def start_datetime_string
    start_datetime.to_s(:padrao_data_hora_datetimepicker) unless start_datetime.blank?
  end

  def start_datetime_string=(start_datetime_str)
    format = "#{Time::DATE_FORMATS[:padrao_data_hora_datetimepicker]} %z"
    self.start_datetime = DateTime.strptime("#{start_datetime_str} -0300", format)
  rescue ArgumentError
    @start_datetime_invalid = true
  end

  def to_param
    Slug.create([id, nome])
  end

  def after_invoice_approved(invoice)
    return if invoice.nil? || invoice.user.nil?
    begin
      classroom_tickets = ClassroomTicket.user_tickets(invoice.user).for_classroom_event(self)
      UserMailer.deliver_classroom_tickets(invoice.user, classroom_tickets)
    rescue Exception => e
      logger.error "Catching exception on ClassroomEvent#after_invoice_approved. Reason: #{e.message} (#{DateTime.now.to_s})"
    end
  end

  def make_fatura_items(quantity, fatura, cupom)
    if !cupom.nil? && Cupom.valido?(cupom.codigo,[self.id])
      item = FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
    else
      item = FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
    end

    create_tickets(fatura, quantity)
    return item
  end

  def can_buy_multiple?
    true
  end

  private

  def validates_start_datetime
    errors.add(:start_datetime_string, "é inválida") if @start_datetime_invalid
  end

  def create_tickets(fatura, quantity)
    return if quantity.nil?

    quantity.times do
      ClassroomTicket.create :user_id => fatura.user.id, :classroom_event_id => self.id, :fatura_id => fatura.id
    end
  end
end
