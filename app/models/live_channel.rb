class LiveChannel < ActiveRecord::Base
  acts_as_audited
  has_many :supporting_materials, :class_name => "LiveChannelSupportingMaterial", :order => "label ASC"

  accepts_nested_attributes_for :supporting_materials, :allow_destroy => true

  def self.singleton_instance
    LiveChannel.first || LiveChannel.create
  end
end
