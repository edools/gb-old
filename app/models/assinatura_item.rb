class AssinaturaItem < ActiveRecord::Base
  belongs_to :assinatura
  belongs_to :produto

  validates_presence_of :produto_id
  validates_uniqueness_of :produto_id, :scope => :assinatura_id

  ordered_by 'produtos.nome ASC'
end
