class Banner < ActiveRecord::Base

  # tipos de banners
  FLASH = 1
  IMAGEM = 2

  # posicoes de banners
  #TOPO = 1
  #RODAPE = 2
  FLOATER = 3
  DESTAQUE = 4

  NOVA_JANELA = true
  MESMA_JANELA = false

  has_many :banner_histories, :dependent => :destroy

  ordered_by 'created_at DESC'

  has_attachment :arquivo

  validates_presence_of :tipo
  validates_inclusion_of :tipo, :in => [FLASH, IMAGEM]
  validates_presence_of :posicao
  validates_inclusion_of :posicao, :in => [DESTAQUE, FLOATER] #[TOPO, RODAPE, FLOATER, DESTAQUE]
  validates_attachment_presence :arquivo
  validates_format_of :url, :with => /^(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?$/, :unless => Proc.new { |p| p.url.blank? }
  before_validation :format_url

  validate :validate_restricao

  named_scope :exibiveis, lambda { { :conditions => self.get_exibiveis_conditions } }

  def validate_restricao
    if limite_cliques.blank? && limite_data.blank? && limite_visualizacoes.blank?
      ['limite_cliques', 'limite_visualizacoes', 'limite_data'].each do |field|
        errors.add(field, 'ou outra forma de gerenciamento deve ser selecionada')
      end
    end
  end

  def self.get_exibiveis_conditions
    query = values = []

    # visivel
    values += [true]

    # limite de cliques e visualizacoes
    query  += ["CASE WHEN limite_cliques IS NULL THEN ? ELSE limite_cliques > total_cliques END"]
    values += [true]
    query  += ["CASE WHEN limite_visualizacoes IS NULL THEN ? ELSE limite_visualizacoes > total_visualizacoes END"]
    values += [true]

    # data expiracao
    query  += ["CASE WHEN limite_data IS NULL THEN ? ELSE limite_data > ? END"]
    values += [true]
    values += [DateTime.now]

    ["visivel = ? AND (" + query.join(" AND ") + ")"] + values
  end

  def self.tipos
    {"Flash" => FLASH, "Imagem" => IMAGEM}
  end

  def self.posicoes
    #{"Topo (468x60)" => TOPO, "Rodapé (900x80)" => RODAPE, "Floater (250x250)" => FLOATER, "Destaque (300x250)" => DESTAQUE}
    {"Destaque (300x350)" => DESTAQUE, "Flutuante (250x250)" => FLOATER }
  end

  def self.targets
    {"Em uma nova janela" => true, "Na mesma janela" => false}
  end

  def self.dimensoes
    {
#      TOPO => [468, 60],
#      RODAPE => [900, 80],
      FLOATER => [250, 250],
      DESTAQUE => [300, 350]
    }
  end

  def tipo_label
    Banner.tipos.invert[tipo]
  end

  def posicao_label
    Banner.posicoes.invert[posicao]
  end
  
  def format_url    
    if !url.blank? && !url.start_with?('http://') && !url.start_with?('https://')
      self.url = "http://" + self.url
    end    
  end

end
