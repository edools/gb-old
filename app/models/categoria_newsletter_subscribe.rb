class CategoriaNewsletterSubscribe < ActiveRecord::Base

  has_many :newsletter_subscribes

  ordered_by 'nome ASC'

  named_scope :exibiveis, lambda { { :conditions => { :exibivel => true } } }

end