class UserObserver < ActiveRecord::Observer

  def after_save(user)
    UserMailer.deliver_reset_password_notification(user) if user.recently_forgot_password?
  end

end
