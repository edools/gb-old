class UserSimulatedTest < ActiveRecord::Base
	belongs_to :user, :class_name => "User", :foreign_key => "user_id"
	belongs_to :simulated_test, :class_name => "SimulatedTest", :foreign_key => "simulated_test_id"
	has_many :user_simulated_test_answers, :class_name => "UserSimulatedTestAnswer", :foreign_key => 'user_simulated_test_id', :dependent => :destroy
	accepts_nested_attributes_for :user_simulated_test_answers
	validates_associated :user_simulated_test_answers

	validates_presence_of :user_id
	validates_presence_of :simulated_test_id

	def correct_questions_count
		user_simulated_test_answers.select(&:is_answer_correct?).size
	end

	def correct_questions_utilization
		correct_questions = correct_questions_count
		utilization = correct_questions / user_simulated_test_answers.size.to_f
		percentage = utilization * 100
	end
end
