class Admin::ManageViewsSearch
  attr_accessor :user_filter, :product_id_filter, :page, :valid
  protected :valid=, :user_filter=, :product_id_filter=, :page=
  alias_method :valid?, :valid

  def initialize(filters, page)
    self.valid = filters.present?
    return unless valid?
    self.user_filter = filters[:user_filter]
    self.product_id_filter = filters[:product_id_filter].to_i
    self.page = page
  end

  def search_users
    @users ||= User.ordered("name ASC").com_algum_produto_ativo.scoped(conditions_filter).paginate(:page => page)
  end

  private

  def conditions_filter
    statements = []
    values = []
    if user_filter.present?
      statements << "(unaccent(login) ILIKE unaccent(?) OR unaccent(name) ILIKE unaccent(?) OR unaccent(email) ILIKE unaccent(?) OR cpf=?)"
      values << user_filter << user_filter << user_filter << user_filter
    end
    if !product_id_filter.zero?
      statements << "fatura_items.produto_id=?"
      values << product_id_filter
    end
    { :conditions => [statements.join(" AND ")].concat(values) }
  end
end