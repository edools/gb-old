class Horario < ActiveRecord::Base

  belongs_to :curso

  validates_presence_of  :inicio
  validate               :validate_date

  ordered_by "inicio, fim"

  def validate_date
    errors.add(:fim, "deve ser maior que a data de início") if !fim.blank? && inicio >= fim
  end
  
end
