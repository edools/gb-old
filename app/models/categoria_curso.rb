class CategoriaCurso < ActiveRecord::Base
  ordered_by 'nome ASC'
  has_many :produtos

  has_ancestry :orphan_strategy => :rootify

  validates_presence_of :nome
  validates_uniqueness_of :nome

  named_scope :available_ancestors, lambda { |id|
    descendants = if not id.blank?
                    CategoriaCurso.find(id).descendant_ids.concat([id])
                  end
    {:conditions => ["id NOT IN (?)", descendants] } if not descendants.blank?
  }

  def self.available_ancestors_array_ordered_by_name(categoria_id = nil, categorias = nil)
    categorias ||= CategoriaCurso.available_ancestors(categoria_id).arrange(:order => 'nome')

    arranged_categorias = []
    categorias.each do |categoria, children|
      arranged_categorias << categoria
      arranged_categorias += available_ancestors_array_ordered_by_name(nil, children) unless children.empty?
    end
    arranged_categorias
  end

  def nome_for_selects
    "#{"&nbsp;&nbsp;&nbsp;" * depth}#{nome}".html_safe
  end

end
