class Cidade < ActiveRecord::Base
  belongs_to :estado
  ordered_by 'nome ASC'
end
