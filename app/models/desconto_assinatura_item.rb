class DescontoAssinaturaItem < ActiveRecord::Base
  belongs_to :assinatura, :foreign_key => "produto_id", :class_name => "Assinatura"
  belongs_to :desconto

  validates_presence_of :produto_id

  ordered_by 'produtos.nome ASC'
end
