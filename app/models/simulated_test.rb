class SimulatedTest < Produto
	include Sanitizable
	include Validatable

	acts_as_audited

	has_many :professors, :class_name => "ProdutosProfessor", :dependent => :destroy, :foreign_key => 'produto_id'
	has_many :questions, :class_name => "SimulatedTestQuestion", :foreign_key => 'simulated_test_id', :dependent => :destroy, :order => Settings.simulated_test_questions_order
	has_many :simulated_test_products, :dependent => :destroy, :class_name => "SimulatedTestProduct", :include => :product
	has_many :products, :through => :simulated_test_products, :order => "simulated_test_products.created_at DESC"

	sanitize_attributes_for_direct_children :questions, :association_key => :id
	sanitize_attributes_for_association :professors, :association_key => :professor_id
	sanitize_attributes_for_association :simulated_test_products, :association_key => :product_id

	validates_associated :questions
	validates_associated :simulated_test_products

	validates_presence_of :categoria_curso_id
	validates_presence_of :tempo_expiracao
	validates_numericality_of :tempo_expiracao, :only_integer => true, :greater_than => 0, :less_than_or_equal_to => 999
	validates_attachment_presence :imagem

	named_scope :search,  lambda { | filtros |
	 conditions = Produto.generate_conditions(filtros)

	 #conditions[:query].push("type IN (?)")
	 #conditions[:parameters].push([SimulatedTest.name])
	 conditions[:query].push("ativo=?")
	 conditions[:parameters].push(true)
	 conditions[:query].push("disponivel_venda=?")
	 conditions[:parameters].push(true)

	 if conditions[:query].length > 0
		 { :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) }
	 end
	}

	# busca todos os simulados ativos, que nao expiraram
	named_scope :search_my_simulated_tests,  lambda { |filtros, current_user|
		 statements = {}
		 conditions_query = []
		 conditions_params = []
		 if !current_user.is_admin?
			 statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
										 :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list}
			 conditions_query = ['faturas.status=?', 'faturas.user_id=?', '? <= fatura_items.expira_at']
			 conditions_params = [Fatura::STATUS_APPROVED.to_s, current_user.id, Date.today.to_s]
		 end

		 conditions_filtros = generate_conditions(filtros)
		 #conditions_filtros[:query].push("type IN (?)")
		 #conditions_filtros[:parameters].push([Arquivo.name])

		 conditions_filtros[:query].push("ativo=?")
		 conditions_filtros[:parameters].push(true)
		 if conditions_filtros[:query].length > 0
			 conditions_query.concat(conditions_filtros[:query])
			 conditions_params.concat(conditions_filtros[:parameters])
		 end

		 statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
		 return statements
 	}

	def self.find_all_by_product(product)
		return [] if product.blank?

		product_ids_filter = [product.id]
		if product.is_a?(Pacote)
			package_courses_ids = product.video_aulas.map(&:id)
			product_ids_filter.concat(package_courses_ids) if package_courses_ids.present?
		end

		self.find(:all, :conditions => ["std.product_id IN (?) AND produtos.ativo=?", product_ids_filter, true], :joins => "INNER JOIN simulated_test_products std ON std.simulated_test_id=produtos.id", :order => "produtos.nome ASC")
	end

	def to_param
		Slug.create([id, nome])
	end

	# veja na classe Produto
	def make_fatura_items(quantity, fatura, cupom)
		if !cupom.nil? && Cupom.valido?(cupom.codigo, [self.id])
			return FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
		end
		FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
	end

	def downloadable?(user)
		return true if self.has_bought_simulated_test?(user)
		return has_bought_any_associated_product?(user)
	end


	def has_bought_simulated_test?(user)
		return false if user.nil?
		return true if user.is_admin?
		return true if user.trial_period?
		return true if Assinatura.alguma_ativa_autoriza_produto?(self, user)

		SimulatedTest.count(:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
												:conditions => ['faturas.status=? AND faturas.user_id=? AND produtos.id=? AND fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at', Fatura::STATUS_APPROVED.to_s, user, self, Date.today.to_s]) > 0
	end

	def has_bought_any_associated_product?(user)
		return false if self.products.blank?
		return self.products.any? { |product|
			product_downloadable = product.downloadable?(user)
			return true if product_downloadable
			return false unless product.is_a?(VideoAula)
			product.packages.any? { |package| package.downloadable?(user) }
		}
	end

	def downloable_associated_product(user)
		return nil if self.products.blank?
		associated_product = nil
		self.products.each do |product|
			product_downloadable = product.downloadable?(user)
			if product_downloadable
				associated_product = product
				break
			elsif product.is_a?(VideoAula)
				associated_product = product.packages.detect { |package| package.downloadable?(user) }
				break if associated_product.present?
			else
				next
			end
		end
		associated_product
	end

	def self.count_simulated_tests_for_products(products)
		return 0 if products.blank?

		products_ids = []

		products.each do |product|
			products_ids << product.id
			if product.is_a?(Pacote)
				package_courses_ids = product.video_aulas.map(&:id)
				products_ids.concat(package_courses_ids)
			end
		end

		self.count(:conditions => ["std.product_id IN (?) AND produtos.ativo=?", products_ids, true], :joins => "INNER JOIN simulated_test_products std ON std.simulated_test_id=produtos.id")
	end

	def expira_at(user = nil, format = true)
		if self.has_bought_simulated_test?(user)
			return super(user, format)
		else
			product = self.downloable_associated_product(user)
			return nil if product.blank?
			return product.expira_at(user, format)
		end
	end

	# veja em Produto::get_expira_at
	def get_expira_at(date, fatura_item)
		date + tempo_expiracao.days
	end

	def self.answer_options
		["a", "b", "c", "d", "e"]
	end

	def self.multiple_choice_options
		["certo", "errado"]
	end
end
