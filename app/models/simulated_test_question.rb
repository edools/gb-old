class SimulatedTestQuestion < ActiveRecord::Base
  acts_as_audited

  MULTIPLA_ESCOLHA = "multipla_escolha"
  CERTO_ERRADO = "certo_errado"

  belongs_to :simulated_test, :class_name => "SimulatedTest", :foreign_key => "simulated_test_id"
  belongs_to :video_upload, :class_name => "VideoUpload", :foreign_key => "video_upload_id"

  has_many :user_simulated_test_answers, :class_name => "UserSimulatedTestAnswer", :foreign_key => 'simulated_test_question_id', :dependent => :destroy

  validates_inclusion_of :question_type, :in => [MULTIPLA_ESCOLHA, CERTO_ERRADO]

  def multiple_choice?
    self.question_type == MULTIPLA_ESCOLHA
  end

  def right_wrong
    self.question_type == CERTO_ERRADO
  end
end
