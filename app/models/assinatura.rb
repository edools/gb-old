class Assinatura < Produto
  include Sanitizable

  has_many :items_ignorados, :dependent => :destroy, :class_name => "AssinaturaItemsIgnorado", :include => :produto
  has_many :items, :dependent => :destroy, :class_name => "AssinaturaItem", :include => :produto

  has_many :produtos_ignorados, :through => :items_ignorados
  has_many :produtos, :through => :items

  sanitize_attributes_for_association :items, :association_key => :produto_id
  sanitize_attributes_for_association :items_ignorados, :association_key => :produto_id

  values_for :periodicidade, :has => [:mensal, :trimestral, :semestral, :anual], :add => [:named_scopes, :predicate_methods, :constants]

  def self.processar_itens_em_vencimento
    data_aviso_vencimento = 10.days.from_now.to_date.to_s
    items_vencendo = FaturaItem.search_aprovados_tipo_assinatura_por_data_expiracao data_aviso_vencimento
    items_vencendo.each do |item|
      begin
        UserMailer.deliver_item_vencendo(item)
        UserMailer.deliver_aviso_item_vencendo(item)
      rescue Exception => e
        logger.error "Catching exception on Assinatura::processar_itens_em_vencimento method. Reason: #{e.message} (#{DateTime.now.to_s})"
      end
    end
  end
  # retorna todas as assinaturas ativas de um user
  named_scope :ativas_do_user, lambda { |current_user|
    statements = {}
    conditions_query = []
    conditions_params = []
    if !current_user.is_admin?
      statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
        :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list }
      conditions_query = ['faturas.status=?', 'faturas.user_id=?', '(fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at)']
      conditions_params = [Fatura::STATUS_APPROVED.to_s,current_user.id, Date.today.to_s]
    end

    conditions_filtros = generate_conditions(nil)
    conditions_filtros[:query].push("produtos.ativo=?")
    conditions_filtros[:parameters].push(true)
    if conditions_filtros[:query].length > 0
      conditions_query.concat(conditions_filtros[:query])
      conditions_params.concat(conditions_filtros[:parameters])
    end

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  # retorna a assinatura ativa do user
  named_scope :ativa_do_user, lambda { |current_user, assinatura_id|
    statements = {}
    conditions_query = []
    conditions_params = []
    if !current_user.is_admin?
      statements = {:joins => 'INNER JOIN fatura_items ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
        :select => 'DISTINCT(produtos.*), MAX(fatura_items.expira_at) AS fatura_items_expira_at', :group => Produto.col_list}
      conditions_query = ['faturas.status=?', 'faturas.user_id=?', '(fatura_items.expira_at IS NOT NULL AND ? <= fatura_items.expira_at)']
      conditions_params = [Fatura::STATUS_APPROVED.to_s,current_user.id, Date.today.to_s]
    end


    conditions_filtros = generate_conditions(nil)
    conditions_filtros[:query].push("produtos.ativo=?")
    conditions_filtros[:parameters].push(true)
    conditions_filtros[:query].push("produtos.id=?")
    conditions_filtros[:parameters].push(assinatura_id)
    if conditions_filtros[:query].length > 0
      conditions_query.concat(conditions_filtros[:query])
      conditions_params.concat(conditions_filtros[:parameters])
    end

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  # retorna o total de assinaturas ativas
  def self.total_ativas(user)
    Assinatura.ativas_do_user(user).count(:distinct => true, :select => 'produtos.id')
  end

  # checa se pra todas as assinaturas ativas do usuario todas elas nao ignoram o
  # o produto. Se todas assinaturas ativas ignoram o produto significa que o
  # user nao pode assitir os videos do curso
  # se user nao possui nenhum assinatura ativa o metodo retorna false
  def self.alguma_ativa_autoriza_produto?(produto,user)
    assinaturas_ativas = Assinatura.ativas_do_user(user)
    return false if assinaturas_ativas.count == 0

    pelo_menos_uma_autoriza = false
    assinaturas_ativas.each do |assinatura|
      # se exists? retorna false significa que o produto nao esta na lista de ignorados
      #if !assinatura.produtos_ignorados.exists?(:id => produto.id)
      #  pelo_menos_uma_autoriza = true
      #  break
      #end
      if assinatura.produtos.exists?(:id => produto.id)
        pelo_menos_uma_autoriza = true
        break
      end
    end

    return pelo_menos_uma_autoriza
  end

  # retorna a data de expiracao do produto dado o user
  # a data de expiracao e' a data da primeira assinatura que autoriza a exibicao do curso
  # or null caso contrario
  def self.data_expiracao_do_produto(produto,user)
    assinaturas_ativas = Assinatura.ativas_do_user(user)
    return false if assinaturas_ativas.count == 0

    a = nil
    assinaturas_ativas.each do |assinatura|
      #if !assinatura.produtos_ignorados.exists?(:id => produto.id) and (a.blank? or Date.parse(a.expira_at) < Date.parse(assinatura.expira_at))
      #  a = assinatura
      #end
      if assinatura.produtos.exists?(:id => produto.id) && (a.blank? || Date.parse(a.expira_at) < Date.parse(assinatura.expira_at))
        a = assinatura
      end
    end

    return a.blank? ? nil : a.expira_at(nil,false)
  end

  def periodicidade_legible
    Assinatura.periodicidade_label(self.periodicidade)
  end

  def self.periodicidade_label(value)
    case value
    when Assinatura::PERIODICIDADE_MENSAL
      'Mensal'
    when Assinatura::PERIODICIDADE_TRIMESTRAL
      'Trimestral'
    when Assinatura::PERIODICIDADE_SEMESTRAL
      'Semestral'
    when Assinatura::PERIODICIDADE_ANUAL
      'Anual'
    end
  end

  def make_fatura_items(quantity, fatura, cupom)
    if !cupom.nil? && Cupom.valido?(cupom.codigo,[self.id])
      return FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura, :cupom_codigo => cupom.codigo, :cupom_desconto => cupom.desconto)
    end
    FaturaItem.new(:quantity => quantity, :produto => self, :price => self.investimento_com_desconto_ativo.to_f, :fatura => fatura)
  end

  # veja em Produto::get_expira_at
  def get_expira_at(date,fatura_item)
    expiration_date = date + self.periodicidade_time
    conditions = ["fatura_items.produto_id = ? AND faturas.user_id = ? AND fatura_items.expira_at < ?",fatura_item.produto_id,fatura_item.fatura.user_id, date]
    if not fatura_item.new_record?
      conditions[0] += " AND fatura_items.id != ?"
      conditions << fatura_item.id
    end

    not_expired_date = FaturaItem.maximum("expira_at",
                                          :conditions => conditions,
                                          :joins => :fatura)
    if not_expired_date
      expiration_date += (date - not_expired_date).days
    end
    return expiration_date
  end

  # veja em Produto::enviar_mensagem_expiracao_atualizou
  def enviar_mensagem_expiracao_atualizou(user, expira_at)
    begin
      UserMailer.deliver_confirmacao_assinatura(user,self,expira_at)
    rescue Exception => e
      logger.error "Catching exception on Assinatura::enviar_mensagem_expiracao_atualizou method. Reason: #{e.message} (#{DateTime.now.to_s})"
    end
  end

  def to_param
    Slug.create([id, nome])
  end

  protected

  def periodicidade_time
    case self.periodicidade
    when Assinatura::PERIODICIDADE_MENSAL
      1.month
    when Assinatura::PERIODICIDADE_TRIMESTRAL
      3.months
    when Assinatura::PERIODICIDADE_SEMESTRAL
      6.months
    when Assinatura::PERIODICIDADE_ANUAL
      1.year
    end
  end
end
