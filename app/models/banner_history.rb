class BannerHistory < ActiveRecord::Base

  # tipos de history
  EXIBICAO = true
  CLIQUE = false

  belongs_to :banner

  ordered_by 'created_at ASC'

  validates_inclusion_of :tipo, :in => [EXIBICAO, CLIQUE]

  named_scope :search,  lambda { | filtros |
    conditions = {:query => [], :parameters => []}

    filtros.each do |filtro, valor|
      unless valor.blank?
        case filtro
        when "data_inicio"
          conditions[:query].push("created_at >= ?")
          conditions[:parameters].push(Date.strptime(valor, '%d/%m/%Y').to_datetime)
        when "data_fim"
          conditions[:query].push("created_at <= ?")
          conditions[:parameters].push(Date.strptime(valor, '%d/%m/%Y').to_datetime)
        when "tipo"
          if valor == "cliques" || valor == "visualizacoes"
            conditions[:query].push("tipo = ?")
            conditions[:parameters].push(valor == "visualizacoes")
          end
        end
      end
    end

    { :order => "created_at ASC", :conditions => [conditions[:query].join(" AND ")].concat(conditions[:parameters]) } if conditions[:query].length > 0
  }

  def self.tipos
    {"Exibição" => EXIBICAO, "Clique" => CLIQUE}
  end

  def self.per_page
    50
  end

end
