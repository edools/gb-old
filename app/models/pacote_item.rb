class PacoteItem < ActiveRecord::Base
  acts_as_audited

  belongs_to :video_aula, :class_name => "VideoAula", :foreign_key => "video_aula_id"
  belongs_to :pacote, :class_name => "Pacote", :foreign_key => "pacote_id"

  validates_presence_of :video_aula_id
  validates_uniqueness_of :video_aula_id, :scope => :pacote_id
end
