module Validatable
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def validates_presence_of_many association
      define_method "validates_presence_of_many_#{association}" do
        errors.add(association, :empty_has_many) if self.send(association).reject {|item| item.marked_for_destruction? }.empty?
      end
      validate :"validates_presence_of_many_#{association}"
    end
  end
end