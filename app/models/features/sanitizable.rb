module Sanitizable
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def sanitize_attributes_for_association(association, *args)
      options = args.extract_options!
      options.assert_valid_keys(:association_key)
      association_key = options[:association_key].to_s

      accepts_nested_attributes_for association, :reject_if => proc { |attributes| attributes[association_key].blank? }

      define_method "#{association}_attributes_with_sanitize=" do |association_attributes|
        association_attributes = Sanitizer.new(self, association, options).process(association_attributes)
        self.send "#{association}_attributes_without_sanitize=", association_attributes unless association_attributes.blank?
      end
      alias_method_chain "#{association}_attributes=", :sanitize

      define_method "assign_attributes_with_blank_#{association}" do |*args|
        attributes = args.first
        self.send("#{association}_attributes=", []) if attributes.blank? || attributes[:"#{association}_attributes"].blank?
        send "assign_attributes_without_blank_#{association}", attributes
      end
      alias_method_chain :assign_attributes, "blank_#{association}"
    end

    def sanitize_attributes_for_direct_children(association, *args)
      options = args.extract_options!

      accepts_nested_attributes_for association

      define_method "#{association}_attributes_with_sanitize=" do |association_attributes|
        association_attributes = DirectChildrenSanitizer.new(self, association, options).process(association_attributes)
        self.send "#{association}_attributes_without_sanitize=", association_attributes unless association_attributes.blank?
      end
      alias_method_chain "#{association}_attributes=", :sanitize

      define_method "assign_attributes_with_blank_#{association}" do |*args|
        attributes = args.first
        self.send("#{association}_attributes=", []) if attributes.blank? || attributes[:"#{association}_attributes"].blank?
        send "assign_attributes_without_blank_#{association}", attributes
      end
      alias_method_chain :assign_attributes, "blank_#{association}"
    end
  end

  private

  class DirectChildrenSanitizer
    attr_accessor :model_instance, :association_name

    def initialize(model_instance, association_name, options = {})
      self.model_instance = model_instance
      self.association_name = association_name.to_s
    end

    def process(association_attributes)
      collected_ids = collect_ids(association_attributes)
      mark_for_destruction_when_not_in_list(collected_ids)

      return collect_attributes(association_attributes)
    end

    private

    def collect_ids(association_attributes)
      association_attributes.map { |association_attribute| collect_id(association_attribute) }
    end

    def collect_id(association_attribute = {})
      association_attribute.with_indifferent_access[:id].to_s
    end

    def mark_for_destruction_when_not_in_list(all_ids = [])
      association.each do |association_element|
        id = association_element.id.to_s
        association_element.mark_for_destruction unless all_ids.include?(id)
      end
    end

    def collect_attributes(association_attributes = {})
      collected_attributes = []
      association_attributes.each do |association_attribute|
        collected_attributes << build_attributes_for_change(association_attribute)
      end
      return collected_attributes
    end

    def update_position?
      @update_position ||= model_class.reflect_on_association(association_name).klass.column_names.include?("position")
    end

    def build_attributes_for_change(association_attribute)
      opts = {}
      opts[:position] = update_position! if update_position?
      association_attribute.merge(opts)
    end

    def update_position!
      @position ||= 0
      @position += 1
      @position.pred
    end

    def association
      model_instance.send(association_name)
    end

    def model_class
      model_instance.class
    end
  end

  class Sanitizer
    attr_accessor :model_instance, :association_name, :association_key

    def initialize(model_instance, association_name, options = {})
      self.model_instance = model_instance
      self.association_name = association_name.to_s
      self.association_key = options[:association_key]
    end

    def process(association_attributes)
      association_attributes = remove_duplicates(association_attributes)
      collected_ids = collect_ids(association_attributes)
      existing_ids = collect_existing_ids
      ids_change_types = build_ids_changes(collected_ids, existing_ids)

      mark_for_destruction_when_not_in_list(collected_ids)

      return collect_attributes(ids_change_types, association_attributes)
    end

    private

    def remove_duplicates(association_attributes)
      unique_association_ids = []
      association_attributes.select do |association_attribute|
        association_id = collect_id(association_attribute)
        unless unique_association_ids.include? association_id
          unique_association_ids << association_id
          true
        end
      end
    end

    def collect_ids(association_attributes)
      association_attributes.map { |association_attribute| collect_id(association_attribute) }
    end

    def collect_id(association_attribute = {})
      association_attribute.with_indifferent_access[association_key].to_s
    end

    def collect_existing_ids
      association.collect(&:"#{association_key}").map(&:to_s)
    end

    def mark_for_destruction_when_not_in_list(all_ids = [])
      association.each do |association_element|
        id = association_element.send(association_key).to_s
        association_element.mark_for_destruction unless all_ids.include?(id)
      end
    end

    def collect_attributes(ids_change_types, association_attributes = {})
      collected_attributes = []
      association_attributes.each do |association_attribute|
        id = collect_id(association_attribute)
        change_type = ids_change_types[id]
        collected_attributes << build_attributes_for_change(change_type, id, association_attribute)
      end
      return collected_attributes
    end

    def update_position?
      @update_position ||= model_class.reflect_on_association(association_name).klass.column_names.include?("position")
    end

    def build_ids_changes(all_ids, existing_ids)
      ids_change_types = {}
      all_ids.each do |id|
        change_type = existing_ids.include?(id) ? :update : :new
        ids_change_types[id] = change_type
      end
      ids_change_types
    end

    def build_attributes_for_change(change_type, association_id, association_attribute)
      opts = {}
      opts[:id] = find_association_element_by_association_id(association_id).id if change_type.eql?(:update)
      opts[:position] = update_position! if update_position?
      association_attribute.merge(opts)
    end

    def find_association_element_by_association_id(id)
      association.find(:first, :conditions => ["#{association_key}=?",id])
    end

    def update_position!
      @position ||= 0
      @position += 1
      @position.pred
    end

    def association
      model_instance.send(association_name)
    end

    def model_class
      model_instance.class
    end
  end
end
