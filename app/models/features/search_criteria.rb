class SearchCriteria
  attr_accessor :field, :options

  def initialize(field, options = {})
    self.field = field
    self.options = options
  end

  def to_condition(value)
    formula = "#{field} #{options[:operator]} ?"
    refined_value = send("refine_for_#{options[:operator]}", value)
    [formula, refined_value]
  end

  private

  def refine_for_ilike(value)
    case options[:match_style]
    when :contains
      "%#{value}%"
    end
  end
end