module Searchable
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def search(filters = {})
      conditions = parse(filters)
      conditions.blank? ? empty_scope : scoped(:conditions => conditions)
    end

    protected

    def refine_search(field, options = {})
      criterias[field] ||= []
      criterias[field] << SearchCriteria.new("#{self.table_name}.#{field}", options)
    end

    private

    CONNECTOR = "and"

    def criterias
      @@criterias ||= {}.with_indifferent_access
    end

    def parse(filters)
      return if filters.blank?
      formulas, args = [], []
      filters.each do |filter, value|
        next if invalid_filter? filter, value
        criterias[filter].each do |criteria|
          formula, arg = criteria.to_condition value
          formulas << formula; args << arg
        end
      end
      concat(formulas, args) unless formulas.empty?
    end

    def invalid_filter?(filter, value)
      value.blank? || criterias[filter].blank? || !criterias.has_key?(filter)
    end

    def empty_scope
      scoped({})
    end

    def concat(formulas, args)
      [formulas.join(" #{CONNECTOR} ")].concat(args)
    end
  end
end