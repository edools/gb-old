# Enabled themes:
# - gustavo_brigido
# - plantao_enem
# - marta_garcia
class SiteTheme
  class << self
    def name
      ENV['SITE_THEME'] || "gustavo_brigido"
    end
  end
end