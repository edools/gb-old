class UserMailer < ActionMailer::Base
  def contact(message)
    recipients Settings.email.contact
    from message[:email]
    content_type "text/html" 
    subject "[#{Settings.site} - Contato] #{message[:subject]}"
    body :contact => message
  end

  def support(message)
    recipients Settings.email.support
    from message[:email]
    content_type "text/html" 
    subject "[#{Settings.site} - Suporte] #{message[:subject]}"
    body :support => message
  end

  def cadastro(user)
    recipients user.email
    from Settings.email.site
    content_type "text/html" 
    subject "#{user.name}, seja bem-vindo ao site #{Settings.site}"
    body :user => user
  end

  def aviso_cadastro(user)
    recipients Settings.email.site
    from Settings.email.alert
    content_type "text/html" 
    subject "Site #{Settings.site} - Novo cliente cadastrado"
    body :user => user
  end

  def pedido(order, user)
    recipients user.email
    from Settings.email.sales
    content_type "text/html" 
    subject "#{Settings.site} - Pedido efetuado com sucesso"
    body :user => user, :order => order
  end

  def aviso_pedido(order,fatura,user)
    recipients Settings.email.sale
    from Settings.email.alert
    content_type "text/html"
    subject "#{Settings.site} - Novo pedido efetuado"
    body :user => user, :order => order, :fatura => fatura
  end

  def confirmacao(order,current_user)
    recipients order.user.email
    from Settings.email.sales
    content_type "text/html" 
    subject "#{Settings.site} - Pagamento confirmado e aula liberada"
    body :user => order.user, :order => order
  end

  def aviso_mudanca_status(fatura)
    recipients Settings.email.sales
    from Settings.email.alert
    content_type "text/html" 
    subject "Site #{Settings.site} - Alteração no status de fatura"
    body :fatura => fatura, :user => fatura.user
  end

  def forgot_password(message)
    recipients message[:email]
    from Settings.email.site
    content_type "text/html" 
    subject "#{Settings.site} - Recuperação de senha"
    body :contact => message
  end

  def aviso_forgot_password(user)
    recipients Settings.email.site
    from Settings.email.alert
    content_type "text/html" 
    subject "Site #{Settings.site} - Tentativa de recuperação de senha"
    body :user => user
  end

  def new_password(message)
    recipients message[:email]
    from Settings.email.site
    content_type "text/html"
    subject "#{Settings.site} - Nova senha"
    body :contact => message
  end

  def aviso_new_password(user)
    recipients Settings.email.site
    from Settings.email.alert
    content_type "text/html" 
    subject "Site #{Settings.site} - Recuperação de senha"
    body :user => user
  end

  def confirmacao_assinatura(user, assinatura, expira_at)
    recipients user.email
    from Settings.email.sales
    content_type "text/html" 
    subject "#{Settings.site} - Confirmação de assinatura"
    body :user => user, :assinatura => assinatura, :expira_at => expira_at
  end

  def item_vencendo(fatura_item)
    recipients fatura_item.fatura.user.email
    from Settings.email.sales
    content_type "text/html" 
    subject "#{Settings.site} - Sua assinatura expira em 10 dias"
    body :fatura_item => fatura_item
  end

  def aviso_item_vencendo(fatura_item)
    recipients Settings.email.sales
    from Settings.email.alert
    content_type "text/html" 
    subject "Site #{Settings.site} - Uma assinatura vai expirar em 10 dias"
    body :produto => fatura_item.produto, :fatura_item => fatura_item, :fatura => fatura_item.fatura, :user => fatura_item.fatura.user
  end

  def classroom_tickets(user, classroom_tickets)
    classroom_event = classroom_tickets.first.classroom_event
    recipients user.email
    from Settings.email.sales
    content_type "text/html"
    subject "#{Settings.site} - Seus ingressos foram liberados"
    body :user => user, :classroom_event => classroom_event, :classroom_tickets => classroom_tickets
  end

  protected
  def setup_email(user)
    @recipients  = "#{user.email}"
    @from        = "ADMINEMAIL"
    @subject     = "[YOURSITE] "
    @sent_on     = Time.now
    @body[:user] = user
    default_url_options[:host] = "localhost:3000"
  end
end
