class FaturaItem < ActiveRecord::Base

  acts_as_audited

  belongs_to :fatura
  belongs_to :produto
  belongs_to :grupo, :foreign_key => 'grupo_id', :class_name => 'FaturaItem', :dependent => :destroy
  has_many :items_grupo, :foreign_key => 'grupo_id', :class_name => 'FaturaItem', :dependent => :destroy

  ordered_by 'created_at DESC'

  usar_como_dinheiro :price

  def total_price
    self.quantity * self.price
  end

  def price_com_desconto
    preco_ajustado = self.quantity * self.price
    if !self.cupom_desconto.blank?
      preco_ajustado -= (preco_ajustado * self.cupom_desconto / 100)
    end
    preco_ajustado
  end

  # define a data que o item da fatura expira para o usuario a partir da data passada
  def update_expira_at(date)
    unless self.produto.blank?
      self.expira_at = self.produto.get_expira_at(date,self)
    end
  end

  # envia uma mensagem ao cliente informando que a data de expiracao foi alterada
  def enviar_mensagem_expiracao_atualizou
    unless self.produto.blank? or self.fatura.blank? or self.fatura.user.blank?
     self.produto.enviar_mensagem_expiracao_atualizou(self.fatura.user,self.expira_at)
    end
  end

  # indica se o item da fatura expirou
  def expirou?
    Date.today > self.expira_at
  end

  # busca todos os produtos ativos, que nao expiraram
  named_scope :search_meus_itens, lambda { | current_user |
    statements = {}
    conditions_query = []
    conditions_params = []
    if !current_user.is_admin?
      statements = {:joins => 'INNER JOIN produtos ON fatura_items.produto_id = produtos.id INNER JOIN faturas ON fatura_items.fatura_id=faturas.id',
        :select => 'fatura_items.*'}
      conditions_query = ['faturas.status=?', 'faturas.user_id=?']
      conditions_params = [Fatura::STATUS_APPROVED.to_s,current_user.id]
    end

    conditions_query << "produtos.type IN (?)"
    conditions_params << [VideoAula.name,Pacote.name, Assinatura.name]

    conditions_query << "produtos.ativo=?"
    conditions_params << true

    statements[:conditions] = [conditions_query.join(" AND ")].concat(conditions_params)
    return statements
  }

  named_scope :search_aprovados_tipo_assinatura_por_data_expiracao, lambda { |data_expiracao|
    { :include => [{:fatura => :user}, :produto],
      :conditions => { :faturas => {:status => Fatura::STATUS_APPROVED.to_s },
                       :expira_at => data_expiracao.to_s,
                       :produtos => {:type => Assinatura.name}  } }
  }
end
