class Courses::CoursePresenter
  include ActionController::UrlWriter
  include ActionView::Helpers::AssetTagHelper

  attr_accessor :course

  DEFAULT_THUMBNAIL = "marta_garcia/works/thumbs/image-01.jpg"

  def initialize(course)
    self.course = course
  end

  def name
    @name ||= course.nome
  end

  def thumbnail
    url = course.imagem.file? ? course.imagem.url(:thumbnail) : DEFAULT_THUMBNAIL
    image_path(url)
  end

  def url
    polymorphic_path(course)
  end

  def offer?
    promotional_price = course.investimento_com_desconto_ativo
    price = course.investimento
    price != promotional_price
  end

  def price
    course.investimento.reais_formatado.to_s
  end

  def promotional_price
    course.investimento_com_desconto_ativo.reais_formatado.to_s
  end
end