class Session::LoginCell < ::Cell::Base
  def initialize(controller, args={})
    @user = args[:user]
    super controller, args
  end

  def show
    view_name = @user.blank? ? :request_access : :user_area
    render :view => view_name
  end
end