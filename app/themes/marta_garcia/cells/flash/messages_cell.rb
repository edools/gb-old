class Flash::MessagesCell < ::Cell::Base
  private

  MESSAGE_STYLES = {:warning => "", :error => "alert-error", :message => "alert-success", :notice => "alert-info"}
  MESSAGE_TYPES = [:error, :message, :notice]

  public

  def initialize(controller, args={})
    @flash = args[:flash] || {}
    super controller, args
  end

  def show
    type = read_type
    @style = MESSAGE_STYLES[type]
    @message = @flash[type]
    render :nothing => hide_message?
  end

  private

  def hide_message?
    @flash.blank? || @flash.values_at(*MESSAGE_TYPES).all?(&:blank?)
  end

  def read_type
    return if @flash.blank?
    MESSAGE_TYPES.each do |key|
      return key unless @flash[key].blank?
    end
  end
end