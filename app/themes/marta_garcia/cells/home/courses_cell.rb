class Home::CoursesCell < ::Cell::Base
  def show
    @courses = Curso.all(:order => "created_at DESC", :limit => 4)
    render
  end
end