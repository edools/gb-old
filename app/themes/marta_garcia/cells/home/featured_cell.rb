class Home::FeaturedCell < ::Cell::Base
  def show
    @tickers = Ticker.ordered.find(:all)
    render
  end
end
