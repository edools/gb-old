class Testimonials::TestimonialsCell < ::Cell::Base
  helper ApplicationHelper

  def show_home
    @testimonials = Depoimento.videos.all(:order => "created_at DESC")
    render
  end

  def show_about
    @video_testimonials = Depoimento.videos.all(:order => "created_at DESC")
    @text_testimonials = Depoimento.texts.all(:order => "created_at DESC")
    render
  end
end