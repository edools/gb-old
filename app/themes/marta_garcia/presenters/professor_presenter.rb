class Themes::MartaGarcia::Presenters::ProfessorPresenter
  include ActionController::UrlWriter
  include ActionView::Helpers::AssetTagHelper
  extend Forwardable

  attr_reader :professor

  def_delegators :professor, :nome, :email

  DEFAULT_THUMBNAIL = "marta_garcia/dummies/testimonial-author1.png"

  def initialize(professor)
    @professor = professor
  end

  def thumbnail
    url = professor.imagem.file? ? professor.imagem.url(:big_thumbnail) : DEFAULT_THUMBNAIL
    image_path url
  end

  def thumbnail_name
    "Foto de #{nome}"
  end

  def url
    professor_path(professor)
  end
end