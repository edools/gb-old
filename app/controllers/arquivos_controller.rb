require 'open-uri'
class ArquivosController < ApplicationController
  #before_filter :login_required, :only => [:meus_cursos, :assistir]
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    @arquivos = Arquivo.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @arquivo = Arquivo.ativos.disponivel_venda.find(params[:id])
    @categorias = CategoriaCurso.ordered.all
  end

  # adiciona uma aula na cesta
  def add_to_cart
    arquivo = Arquivo.ativos.disponivel_venda.find(params[:id])
    add_to_current_tiny_basket arquivo.id
    redirect_to :controller => 'carrinho', :action => 'index'
  end

  # lista todas as video aulas compradas
  def meus_arquivos
    params.delete(:page) if params[:page] && params[:page].blank?
    @arquivos = Arquivo.search_meus_arquivos(params[:filtros],current_user).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def acessar
    @arquivo = Arquivo.ativos.find(params[:id])

    if !@arquivo.downloadable?(current_user)
      render_optional_error_file 404
      return
    end
    @arquivo.expira_at(current_user)
  end

  def download_demonstracao
    arquivo = Arquivo.ativos.find(params[:id])
    redirect_to arquivo.material_demonstracao.arquivo.url
  end

  def download
    render_optional_error_file(404) and return if (anexo = ProdutoAnexo.find_by_id(params[:id])).nil?
    render_optional_error_file(401) and return if !anexo.produto.downloadable?(current_user)

    attachment =
 UserWatermarkAttachment.find_last_by_base_product_attachment_id_and_user_id(anexo.id, current_user.id)

    respond_to do |format|
      format.json do
        if attachment.nil?
          job_id = AttachmentWatermarkJob.enqueue(anexo.id, current_user.id)
          render :json => {:response => {:job_id => job_id, :attachment_id => anexo.id}}
        else
          render :json => {:response => {:download_url => attachment.watermarked_attachment.url }}
        end
      end
    end
  end

  def verify_watermark_job
    attachment = UserWatermarkAttachment.find_by_base_product_attachment_id_and_job_id_and_user_id(params[:attachment_id], params[:job_id], current_user.id)

    respond_to do |format|
      format.json do
        if attachment.nil?
          render :json => {:response => {:status => :processing}}
        else
          render :json => {:response => {:status => :done, :download_url => attachment.watermarked_attachment.url}}
        end
      end
    end
  end
end
