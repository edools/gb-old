class VideoAulasController < ApplicationController
  before_filter :login_required, :only => [:meus_cursos, :assistir]
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC',
                      'Nome (Z-A)' => 'nome DESC',
                      'Data de Criação (mais novos)' => 'created_at DESC',
                      'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'

  def index
    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @cursos = []
    if not params[:filtros].blank?
      @cursos = Produto.search(params[:filtros]).ordered('nome ASC').paginate(:page => params[:page])
      products_count = Produto.search(params[:filtros]).count
      if @cursos.blank?
        flash.now[:notice] = "Sua busca não retornou nenhum resultado." if @cursos.blank?
      else
        flash.now[:notice] = "Sua busca retornou #{products_count} resultado(s)."
      end
    end
  end

  def sort
    featured_products_view = render_cell("produtos_destaque", :show, :only_assistiveis => true, :show_sorting => true, :sort => params[:sort])
    respond_to do |format|
     format.json { render :json => {:data => featured_products_view.html_safe }}
    end
  end

  def sort_plantao_enem
    featured_products_view = render_cell("produtos_destaque", :show_plantao_enem, :only_assistiveis => true, :show_sorting => true, :sort => params[:sort], :sort_action => "sort_plantao_enem")
    respond_to do |format|
     format.json { render :json => {:data => featured_products_view.html_safe }}
    end
  end

  def paginate
    categoria = CategoriaCurso.find(params[:categoria_id])
    products_view = render_cell("produtos_destaque", :show_produtos, :categoria => categoria, :sort => params[:sort], :only_assistiveis => true)
    respond_to do |format|
     format.json { render :json => {:data => products_view.html_safe }}
    end
  end

  def paginate_plantao_enem
    categoria = CategoriaCurso.find(params[:categoria_id])
    products_view = render_cell("produtos_destaque", :show_produtos_plantao_enem, :categoria => categoria, :sort => params[:sort], :only_assistiveis => true, :paginate_action => "paginate_plantao_enem")
    respond_to do |format|
     format.json { render :json => {:data => products_view.html_safe }}
    end
  end

  def show
    @curso = Produto.assistiveis.ativos.disponivel_venda.find(params[:id])
    @categorias = CategoriaCurso.ordered.all
    render :template => 'video_aulas/show_pacote' if @curso.kind_of? Pacote
  end

  # adiciona uma aula na cesta
  def add_to_cart
    produto = Produto.assistiveis.ativos.disponivel_venda.find(params[:id])
    add_to_current_tiny_basket produto.id
    redirect_to :controller => 'carrinho', :action => 'index'
  end

  # lista todas as video aulas compradas
  def meus_cursos
    params.delete(:page) if params[:page] && params[:page].blank?

    @total_assinaturas = Assinatura.total_ativas(current_user)

    if params[:visualizacao].blank?
      @cursos = Produto.search_meus_cursos(params[:filtros],current_user).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    elsif @total_assinaturas > 0
      @cursos = Produto.search_meus_cursos_da_assinatura(params[:filtros], current_user, params[:visualizacao].to_i).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    else
      @cursos = []
    end

    @total_video_aulas = 0
    @total_pacotes = 0
    Produto.search_meus_cursos(nil,current_user).each do |produto|
      @total_video_aulas +=1 if produto.kind_of? VideoAula
      @total_pacotes +=1 if produto.kind_of? Pacote
    end

    @number_of_available_simulated_tests  = SimulatedTest.count_simulated_tests_for_products(@cursos)

    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
    @visualizacoes = Assinatura.ativas_do_user(current_user).collect { |a| ["Assinatura #{a.nome}", a.id]}.insert(0,['Cursos Avulsos',nil]) if @total_assinaturas > 0
  end

  def assistir
    @curso = Produto.assistiveis.ativos.find(params[:id])

    if !@curso.downloadable?(current_user)
      render_optional_error_file 404
      return
    end
    @curso.expira_at(current_user)

    render :template => 'video_aulas/assistir_pacote' if @curso.kind_of? Pacote
  end

  def view
    video = VideoUpload.find(params[:video_id])
    return if video.nil?
    return if !video.downloadable?(current_user,params[:curso_id])
    render :update do |page|
      page.replace_html 'view_video', :partial => 'view', :locals => {:video => video, :curso_id => params[:curso_id], :pacote_video_aula_id => params[:pacote_video_aula_id]}
    end
  end

  def view_cursos_lateral
    pacote = Pacote.find(params[:pacote_id])

    return if !pacote.downloadable?(current_user)
    curso = pacote.video_aulas.find params[:curso_id]
    render :update do |page|
      page.replace_html 'lista_cursos', :partial => "view_curso_lateral", :locals => {:curso => curso, :pacote_id => params[:pacote_id] }
      page.replace_html 'label_lista_material_apoio', pluralize(curso.produto_anexos.count, "Material de Apoio", "Materiais de Apoio") + " - #{curso.nome}"
      page.replace_html 'simulated_area', render_cell("available_product_simulated_tests_for_user", :show, :product => curso)
      if curso.produto_anexos.empty?
        page.replace_html 'lista_material_apoio', '<p>Nenhum material disponível para este curso.</p>'
      else
        page.replace_html 'lista_material_apoio', :partial => 'produto_anexo', :collection => curso.produto_anexos, :locals => {:pacote_id => params[:pacote_id], :curso_id => curso.id }
      end

    end
  end

  def rss
    render_rss_feed(Produto.assistiveis.ordered.all(:limit => 10, :conditions => {:ativo => true}),
      { :feed => {:title => (@title || "Cursos On-line") + " - #{Settings.site}",
          :url => url_for(:controller => 'video_aulas', :action => 'show'),
          :item_url_prefix => '/'},
        :item => {:title => :nome,
          :description => :descricao,
          :pub_date => :created_at,
          :permalink => :id } })
  end

  def download_material_apoio
    render_optional_error_file(404) and return if (anexo = ProdutoAnexo.find_by_id(params[:id])).nil?
    if params[:pacote_id]
      render_optional_error_file(404) and return if (pacote = Pacote.find_by_id(params[:pacote_id])).nil?
      render_optional_error_file(404) and return unless PacoteItem.exists? :pacote_id => params[:pacote_id], :video_aula_id => params[:curso_id]
      render_optional_error_file(404) and return unless ProdutoAnexo.exists? :id => params[:id], :produto_id => params[:curso_id]

      render_optional_error_file(401) and return if !pacote.downloadable?(current_user)
    else
      render_optional_error_file(401) and return if !anexo.produto.downloadable?(current_user)
    end

    redirect_to anexo.arquivo.url
  end
end
