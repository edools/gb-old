class PedidoController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:confirmacao]
  skip_before_filter :setup_total_basket_items, :only => [:checkout]
  before_filter :login_required, :except => [:confirmacao]

  # mostra todos os pedidos feitos para o usuario logado
  def index
    if is_cliente?
      @faturas = Fatura.find_all_by_user_id current_user.id
    else
      render_optional_error_file 401
    end
  end

  # mostra o resumo do pedido com os produtos que estao na cesta
  def show
    if !params[:id]
      if current_user && is_cliente?
        @basket = current_full_basket

        redirect_to :controller => 'carrinho', :action => 'index' unless @basket && @basket[:items].count > 0
        return
      else
        # se existe usuario entao ele nao e' cliente (is_cliente? => false), caso contrario nao foi feito login
        if !current_user
          store_location
          redirect_to new_session_path
          return
        end
      end

    else
      if current_user
        @fatura = Fatura.find_by_id_and_user_id params[:id], current_user.id
        if !@fatura.nil?
          render :template => 'pedido/show_pedido_existente'
          return
        end
      end
    end
    render_optional_error_file 404
  end

  # converte a cesta em uma fatura para o sistema e uma ordem para o pagseguro
  def checkout
    # se nao tem id significa que o cliente esta fazendo checkout pela primeira vez.
    # se tem id significa que o cliente esta tentando pagar novamente uma fatura existente
    if !params[:id]
      if is_cliente?

        basket = current_full_basket
        cupom = Cupom.find_by_codigo(basket[:current_discount])
        cupom ||= Cupom.new # cria um cupom que nao vai aplicar nenhum desconto
        if basket && basket[:items].count > 0

          # abre a transacao para salvar a fatura e os items da fatura
          Fatura.transaction do
            # coloca a fatura como pendente ate receber alguma resposta do pagseguro
            fatura = Fatura.new(:status => Fatura::STATUS_PENDING, :user => current_user)

            if fatura.save
              @order = PagSeguro::Order.new(fatura.id)
              basket[:items].each do |item_id, info|
                desconto = cupom.desconto_total(info[:item],1)
                @order.add :id => item_id, :price => info[:item].total_com_desconto(1,desconto).to_f, :description => info[:item].nome, :quantity => info[:qty]
                fatura.fatura_items<<info[:item].make_fatura_items(info[:qty],fatura,cupom)
              end

              begin
                UserMailer.deliver_pedido(@order, current_user)
                UserMailer.deliver_aviso_pedido(@order,fatura,current_user)
              rescue Exception => e
                @sending_error = true
              end

              # destroy a cesta depois de montar e salvar o pedido
              destroy_tiny_basket
              # checa se o cliente tem uma fatura gratis
              if fatura.free?
                fatura.status = Fatura::STATUS_APPROVED
                fatura.save
                redirect_to :action => 'confirmacao', :type => :free
              end
            else
              render_optional_error_file 500
              return
            end

          end

        else # a cesta esta vazia
          redirect_to :controller => 'carrinho', :action => 'index'
          return
        end
      else
        # se existe usuario entao ele nao e' cliente (is_cliente? => false), caso contrario nao foi feito login
        if current_user
          render_optional_error_file 401
          return
        else
          redirect_to :controller => 'sessions', :action => 'new'
          return
        end
      end
    else
      if current_user
        fatura = Fatura.find_by_id_and_user_id params[:id], current_user.id
        if !fatura.nil? && fatura.pending?
          @order = PagSeguro::Order.new(fatura.id)
          fatura.fatura_items.each do |fatura_item|
            @order.add :id => fatura_item.produto.id, :price => fatura_item.price_com_desconto.to_f, :description => fatura_item.produto.nome, :quantity => fatura_item.quantity          
          end
          setup_total_basket_items
          return
        end
      end
      render_optional_error_file 404
      return
    end
    setup_total_basket_items

  end

  # mostra a pagina de confirmacao depois que o usuario voltou do processo do pagseguro ou
  # action por onde o pagseguro envia o status de uma fatura
  def confirmacao
    # se for post o pag seguro esta mandando o novo status de uma fatura
    if request.post?
      pagseguro_notification do |notification|

        fatura = Fatura.find_by_id notification.order_id
        if !fatura.nil?
          # TODO: e' possivel melhorar essa checagem
          if fatura.pending?
            fatura.status = notification.status

            fatura.processado_at = notification.processed_at

            unless fatura.save then
              logger.info "Catching exception on confirmacao method: Não foi possível atualizar fatura " + fatura.id.to_s + "(" + DateTime.now.to_s + ")"
            end

            begin
              UserMailer.deliver_aviso_mudanca_status(fatura)
            rescue Exception => e
              @sending_error = true
            end
            # envia email para o usuario dizendo que o pagamento foi confirmado
            begin
              if fatura.approved?
                UserMailer.deliver_confirmacao(fatura, current_user)
              end
            rescue Exception => e
              @sending_error = true
            end
          end

        else
          logger.info "Catching exception on confirmacao method: O pagseguro enviou uma atualização para uma fatura inexistente. FATURA ID = " + notification.order_id.to_s + "(" + DateTime.now.to_s + ")"
        end

      end
      render :nothing => true
    end
  end

end
