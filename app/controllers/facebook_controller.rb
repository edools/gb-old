class FacebookController < ApplicationController
  layout false

  def index
    @live_channel = LiveChannel.singleton_instance
  end
end
