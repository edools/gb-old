class ProfessorsController < ApplicationController
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC' }
  @@mapa_ordenacao.default = 'nome ASC'
  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    
    @professores = Professor.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    
    @ordenacao = ['Nome (A-Z)', 'Nome (Z-A)']
  end
  
  def show
    @professor = Professor.find(params[:id])
  end

end
