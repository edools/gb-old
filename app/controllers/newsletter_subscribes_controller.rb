class NewsletterSubscribesController < ApplicationController
  def create
    @subscribe = NewsletterSubscribe.new(params[:newsletter_subscribe])
    @subscribe.nome = @subscribe.email.split("@").first
    @subscribe.categoria = CategoriaNewsletterSubscribe.first

    respond_to do |format|
      if @subscribe.save
        flash[:notice] = 'Cadastro efetuado com sucesso'
        flash.discard(:notice)
        format.js
      else
        if @subscribe.errors && @subscribe.errors[:email] && @subscribe.errors.length == 1
          flash[:error] = 'E-mail já cadastrado ou inválido.'
        else
          flash[:error] = 'Preencha os campos corretamente.'
        end
        flash.discard(:error)
        format.js
      end
    end
  end

  # atualiza o status de receber ou nao newsletter
  def newsletter
#    if params[:id] && params[:token] && NewsletterSubscribe.find(params[:id])
#      @subscribe = NewsletterSubscribe.find(params[:id])
#      if !@subscribe.blank? && Newsletter.token_by_subscribe(@subscribe) == params[:token]
#        @mensagem = (@subscribe.newsletter) ? "Você foi descadastrado(a) com sucesso da newsletter." : "Você foi cadastrado(a) com sucesso na newsletter."
#        @subscribe.newsletter = !@subscribe.newsletter
#        @subscribe.save
#      else
#        redirect_to home_url
#      end
#    else
#      redirect_to home_url
#    end
  end
end
