class NoticiasController < ApplicationController
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Data (mais novos)' => 'created_at DESC', 'Data (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  def index
    params.delete(:page) if params[:page] && params[:page].blank?

    if params[:tag]
      options =  Noticia.find_options_for_find_tagged_with(params[:tag]).merge :page => params[:page]
      @noticias = Noticia.search(params[:filtros]).ordered.paginate(options)
    else
      @noticias = Noticia.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    end

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @ordenacao = ['Data (mais novos)', 'Data (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @noticia = Noticia.find(params[:id])
  end

  def rss
    render_rss_feed(Noticia.ordered.all(:limit => 10),
      { :feed => {:title => (@title || "Notícias") + " - #{Settings.site}",
          :url => url_for(:controller => 'noticias', :action => 'show'),
          :item_url_prefix => '/'},
        :item => {:title => :nome,
          :description => :descricao,
          :pub_date => :created_at,
          :permalink => :id } })
  end

end
