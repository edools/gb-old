class CursosController < ApplicationController

  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at ASC'
  
  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    @cursos = Curso.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @curso = Curso.find(params[:id], :conditions => {:ativo => true})
    @categorias = CategoriaCurso.ordered.all
  end

  def rss
    render_rss_feed(Curso.ordered.all(:limit => 10, :conditions => {:ativo => true}),
      { :feed => {:title => (@title || "Cursos") + " - #{Settings.site}",
          :url => url_for(:controller => 'cursos', :action => 'show'),
          :item_url_prefix => '/'},
        :item => {:title => :nome,
          :description => :descricao,
          :pub_date => :created_at,
          :permalink => :id } })
  end

end
