class PasswordsController < ApplicationController

  before_filter :login_required, :only => [:update]

  def new
    if request.post?
      email = params[:user][:email]
      @user = User.find_by_email email
      if @user
        @user.create_reset_password_token!
        begin
          @message = {:name => @user.name, :email => email, :message => @user.reset_password_token}
          UserMailer.deliver_forgot_password(@message)
          UserMailer.deliver_aviso_forgot_password(@user)
          flash[:notice] = "Informações enviadas para o e-mail #{email}"
        rescue Exception => e
          logger.error "Catching exception on PasswordsController::new method. Reason: #{e.message} (#{DateTime.now.to_s})"
          flash[:error] = 'Ocorreu um erro no envio do e-mail de recuperação de senha'
        end
        redirect_back_or_default(login_url)
      else
        error_message = email.blank? ? "Preencha o campo com um e-email válido" : "#{email} não está cadastrado"
        flash.now[:error] = error_message
      end
    end
  end

  def create
    @reset_password_token = params[:reset_password_token]

    if @reset_password_token.blank?
      flash[:error] = "O código para recuperar senha está incorreto ou não existe. Utilize a URL enviada por e-mail e tente novamente."
      redirect_back_or_default(login_url)
      flash.discard
    else
      @user = User.find_by_reset_password_token @reset_password_token
      if @user && @user.valid_reset_password_token?
        # generate random password
        new_password = ActiveSupport::SecureRandom.base64(6)
        @user.validacao_simples = true;
        if @user.reset_password!( new_password, new_password )
          login(@user)
          begin
            @message = {:login => @user.login, :name => @user.name, :email => @user.email, :message => new_password}
            UserMailer.deliver_new_password(@message)
            UserMailer.deliver_aviso_new_password(@user)
            flash[:notice] = "Sua senha foi resetada com sucesso. Um e-mail foi enviado para você com a nova senha."
          rescue
            flash[:error] = 'Ocorreu um erro no envio do e-mail com sua nova senha. Altere agora sua senha ou resete-a novamente.'
          end
          flash.discard
        else
          flash[:error] = "Ocorreu um erro ao gerar a nova senha. Tente novamente."
          flash.discard
        end
      else
        flash[:error] = "O código para recuperar senha está incorreto ou expirou. Utilize a URL enviada por e-mail e tente novamente."
        redirect_to login_url
        flash.discard
      end
    end
  end

  def update
    @user = current_user
    if User.authenticate(@user.login, params.delete(:old_password) )
      # FIXME: adicionar validacao ao inves do hack de colocar um password fake de tamanho 1
      #@user.errors.add_on_blank(:password)
      params[:user][:password] = params[:user][:password_confirmation] = rand(9) if params[:user][:password].blank?
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
      @user.validacao_simples = true;
      if @user.save
        flash[:notice] = "Senha alterada com sucesso!"
        redirect_back_or_default(alterar_senha_path)
      else
        flash.now[:error] = "A nova senha é diferente da confirmação. Por favor, redigite-as!"
      end
    else
      flash.now[:error] = "Sua senha atual está incorreta!"
    end if request.post?
    @user.password = @user.password_confirmation = nil
  end

end
