class CarrinhoController < ApplicationController

  def index
    if current_user and !is_cliente?
      flash.now[:error] = "O carrinho está inativo pois você não é um cliente."
    end
    @basket = current_full_basket
  end

  def delete
    remove_product_current_tiny_basket params[:id].to_i
    redirect_to :action => 'index'
  end

  # carrinho/atualizar_cupom
  def atualizar_cupom
    if(params[:commit] == 'Remover cupom')
      if delete_current_discount_code_from_current_tiny_basket
        flash.now[:notice] = 'Cupom removido com sucesso.'
      end
      @basket = current_full_basket
      render :action => 'index' and return
    end
    cupom = params[:cupom_codigo]

    cupom.strip! if !cupom.blank?

    if cupom.blank?
      flash.now[:error] = 'Você não digitou o código do cupom.'
    elsif has_discount_to_current_tiny_basket(cupom)
      save_discount_code_to_current_tiny_basket(cupom)
      flash.now[:notice] = 'Cupom utilizado com sucesso.'
    else
      flash.now[:error] = 'Código do cupom inválido.'
    end
    @basket = current_full_basket
    render :action => 'index'
  end

  def update_quantity
    product = Produto.find(params[:id].to_i)

    if product.can_buy_multiple?

      quantity_increment = 0
      quantity_increment = 1 if params[:increment].eql? "plus"
      quantity_increment = -1 if params[:increment].eql? "minus"

      add_to_current_tiny_basket(product.id, quantity_increment, false)
    end

    redirect_to :controller => 'carrinho', :action => 'index'
  end
end