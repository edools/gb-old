class TicketsController < ApplicationController
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    @tickets = Ticket.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @ticket = Ticket.ativos.disponivel_venda.available.find(params[:id])
    @categorias = CategoriaCurso.ordered.all
  end

  # adiciona um ticket na cesta
  def add_to_cart
    ticket = Ticket.ativos.disponivel_venda.available.find(params[:id])
    add_to_current_tiny_basket ticket.id
    redirect_to :controller => 'carrinho', :action => 'index'
  end

  def meus_tickets
    params.delete(:page) if params[:page] && params[:page].blank?
    @tickets = Ticket.search_meus_tickets(params[:filtros],current_user).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def assistir
    @ticket = Ticket.ativos.available.find(params[:id])

    if !@ticket.downloadable?(current_user)
      render_optional_error_file 404
      return
    end
  end

  def download_material_apoio
    render_optional_error_file(404) and return if (anexo = ProdutoAnexo.find_by_id(params[:id])).nil?
    render_optional_error_file(401) and return if !anexo.produto.downloadable?(current_user)

    redirect_to anexo.arquivo.url
  end

  def view
    video = VideoUpload.find(params[:video_id])
    return if video.nil?
    return if !video.downloadable?(current_user,params[:curso_id])
    render :update do |page|
      page.replace_html 'view_video', :partial => 'view', :locals => {:video => video, :curso_id => params[:curso_id]}
    end
  end
end
