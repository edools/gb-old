class ParceirosController < ApplicationController

  def index
    @parceiros = Parceiro.ordered.all
    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
  end

  def show
    @parceiro = Parceiro.find(params[:id])
    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
  end

end
