class ArtigosController < ApplicationController
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Data (mais novos)' => 'created_at DESC', 'Data (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    @artigos = Artigo.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @professores = Professor.ordered.all
    @ordenacao = ['Data (mais novos)', 'Data (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @artigo = Artigo.find(params[:id])
    @professores = Professor.ordered.all
  end

  def rss
    render_rss_feed(Artigo.ordered.all(:limit => 10),
      { :feed => {:title => (@title || "Artigos") + " - #{Settings.site}",
          :url => url_for(:controller => 'artigos', :action => 'show'),
          :item_url_prefix => '/'},
        :item => {:title => :nome,
          :description => :descricao,
          :pub_date => :created_at,
          :permalink => :id } })
  end

end
