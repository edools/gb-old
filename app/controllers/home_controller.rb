class HomeController < ApplicationController
  def home
  end

  def index
    render :layout => false
  end

  def paginate
    categoria = CategoriaCurso.find(params[:categoria_id])
    products_view = render_cell("produtos_destaque", :show_produtos, :categoria => categoria, :paginate_controller => "home")
    respond_to do |format|
     format.json { render :json => {:data => products_view.html_safe }}
    end
  end

  def paginate_plantao_enem
    categoria = CategoriaCurso.find(params[:categoria_id])
    products_view = render_cell("produtos_destaque", :show_produtos_plantao_enem, :categoria => categoria, :paginate_controller => "home", :paginate_action => "paginate_plantao_enem")
    respond_to do |format|
     format.json { render :json => {:data => products_view.html_safe }}
    end
  end

  def sobre
    @sobre = SitePageContent.find_or_create_by_name(SitePageContent::NAME_SOBRE.to_s)
  end

  def faq
    @sobre = SitePageContent.find_or_create_by_name(SitePageContent::NAME_SOBRE.to_s)
  end

  def termos
    @termos = SitePageContent.find_or_create_by_name(SitePageContent::NAME_TERMOS.to_s)
  end

  def browsers
    if cookies[:checked_browser].blank?
      cookies[:checked_browser] = {
        :value => '1',
        :expires => Time.now + 2.week
      }
      render :layout => false
    else
      redirect_to frame_url
    end
  end
end
