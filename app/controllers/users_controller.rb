class UsersController < ApplicationController
  # Be sure to include AuthenticationSystem in Application Controller instead
  include AuthenticatedSystem

  def new
    @user = User.new(params[:user])
  end

  def edit
    @user = current_user
  end

  def create
    #logout_keeping_session!
    @user = User.new(params[:user])
    @user.roles = []
    @user.roles = [Role.find_by_name(Role.cliente)]

    success = @user && @user.save
    if success && @user.errors.empty?
      # Protects against session fixation attacks, causes request forgery
      # protection if visitor resubmits an earlier form using back
      # button. Uncomment if you understand the tradeoffs.
      # reset session
      # self.current_user = @user # !! now logged in
      flash[:notice] = "Usuário #{@user.login} cadastrado com sucesso."

      # adiciona na newsletter, se tiver selecionado que deseja
      if params[:newsletter] && params[:newsletter] == "on"
        NewsletterSubscribe.create(:nome => @user.name, :email => @user.email, :categoria => CategoriaNewsletterSubscribe.find_by_removivel(false))
      end

      begin
        UserMailer.deliver_cadastro(@user)
        UserMailer.deliver_aviso_cadastro(@user)
      rescue Exception => e
        @sending_error = true
      end

      respond_to do |format|
        format.html { render :action => "success" }
        format.js
        format.xml  { head :ok }
      end
    else
      flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
      render :action => 'new'
      flash.discard(:error)
    end
  end

  def update
    # remove alguns valores, caso tenham sido injetados
    [:roles, :login, :cpf, :password, :password_confirmation].each do |p|
      params[:user].delete p
    end

    @user = current_user
    email_antigo = @user.email

    respond_to do |format|
      if @user.update_attributes(params[:user])
        @newsletter = NewsletterSubscribe.find_by_email(email_antigo)
        if params[:newsletter] && params[:newsletter] == "on"
          # atualiza os dados para newsletter
          if @newsletter
            @newsletter.nome = @user.name
            @newsletter.email = @user.email
            @newsletter.save
          else
            # cria o registro ja que nao existe
            NewsletterSubscribe.create(:nome => @user.name, :email => @user.email, :categoria => CategoriaNewsletterSubscribe.find_by_removivel(false))
          end
        else
          # remove da tabela subscribe
          NewsletterSubscribe.destroy(@newsletter) if @newsletter
        end

        flash[:notice] = "Seus dados foram editados com sucesso!"
        format.html { redirect_to( edit_user_path(@user) ) }
        format.js
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :edit }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  def success
  end

  # historico de cursos on-line, pacotes e assinaturas do cliente
  def historico
    @fatura_items = FaturaItem.search_meus_itens(current_user).ordered('produtos.type ASC, fatura_items.expira_at ASC')
  end

  def update_cidade
    @estado = Estado.find(params[:estado_id])
    render :layout => false
  end
end
