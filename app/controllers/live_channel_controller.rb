class LiveChannelController < ApplicationController
  def download_supporting_material
    render_optional_error_file(404) and return if (supporting_material = LiveChannelSupportingMaterial.find_by_id(params[:id])).nil?
    redirect_to supporting_material.attachment.url
  end
end
