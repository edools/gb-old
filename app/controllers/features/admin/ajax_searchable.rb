module AjaxSearchable
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def create_ajax_search(action, *args)
      define_method action do
        AjaxSearchProcessor.new(self, *args).process
      end
    end
  end

  private

  class AjaxSearchProcessor
    attr_accessor :controller, :model, :scope

    def initialize(controller, *args)
      options = args.extract_options!
      self.controller = controller
      self.model = create_model(options[:model])
      self.scope = options[:scope]
    end

    def process
      field, value = search_criteria
      options = find_options(field, value)
      items = model_with_scope.all options

      self.controller.send :render, "admin/features/ajax_search", :locals => {:field => field, :items => items}
    end

    private

    def create_model(default_model)
      model_name = default_model ? default_model.to_s : self.controller.controller_name.singularize
      model_name.classify.constantize
    end

    def search_criteria
      criteria = self.controller.params.select {|key, value| key =~ /searchable_field_/}.first
      field = criteria.first.gsub("searchable_field_","")
      value = criteria.last
      [field, value]
    end

    def model_with_scope
      self.scope ? self.scope.call(self.model) : self.model
    end

    def find_options(field, value)
      options = {}

      model_has_table_name = self.model.present? && self.model.respond_to?(:table_name)
      if model_has_table_name
        table_name =  self.model.table_name
        field = "#{table_name}.#{field}"
      end

      options[:conditions] = ["unaccent(#{field}) ILIKE unaccent(?)", "#{value}%"] unless value.blank?
      options[:order] = ["#{field} ASC"]
      return options
    end
  end
end
