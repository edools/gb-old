class ClassroomEventsController < ApplicationController
  before_filter :login_required, :only => [:my_tickets]
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    @classroom_events = ClassroomEvent.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @classroom_event = ClassroomEvent.ativos.disponivel_venda.not_started.find(params[:id])
    @categorias = CategoriaCurso.ordered.all
  end

  # adiciona uma aula na cesta
  def add_to_cart
    classroom_event = ClassroomEvent.ativos.disponivel_venda.not_started.find(params[:id])
    quantity = params[:tickets_quantity].to_i
    quantity = 1 unless quantity >= 1
    add_to_current_tiny_basket classroom_event.id, quantity, false
    redirect_to :controller => 'carrinho', :action => 'index'
  end

  def my_tickets
    @classroom_tickets = ClassroomTicket.user_tickets(current_user)
  end
end