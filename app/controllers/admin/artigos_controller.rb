class Admin::ArtigosController < Admin::AdminController
  # GET /artigos
  # GET /artigos.xml
  def index
    @artigos = (params[:ordenar].blank?) ? Artigo.paginate(:page => params[:page], :joins => :professor) : Artigo.ordered(params[:ordenar]).paginate(:page => params[:page], :joins => :professor)
    @ordenacao = {'Nome' => 'nome', 'Cadastrado em' => 'created_at', 'Professor' => 'professors.nome'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @artigos }
    end
  end

  # GET /artigos/new
  # GET /artigos/new.xml
  def new
    @artigo = Artigo.new
    @professores = Professor.ordered.all

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @artigo }
    end
  end

  # GET /artigos/1/edit
  def edit
    @artigo = Artigo.find(params[:id])
    @professores = Professor.ordered.all
  end

  # POST /artigos
  # POST /artigos.xml
  def create
    @artigo = Artigo.new(params[:artigo])

    respond_to do |format|
      if @artigo.save
        flash[:notice] = 'Artigo adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_artigo_url : admin_artigos_url }
        format.xml  { render :xml => @artigo, :status => :created, :location => @artigo }
      else
        @professores = Professor.ordered.all
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @artigo.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /artigos/1
  # PUT /artigos/1.xml
  def update
    @artigo = Artigo.find(params[:id])

    respond_to do |format|
      if @artigo.update_attributes(params[:artigo])
        flash[:notice] = 'Artigo editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_artigo_url : admin_artigos_url }
        format.xml  { head :ok }
      else
        @professores = Professor.ordered.all
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @artigo.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /artigos/1
  # DELETE /artigos/1.xml
  def destroy
    begin
      Artigo.destroy(params[:id])
      flash[:notice] = "Artigo removido com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_artigos_url) }
      format.xml  { head :ok }
    end
  end

  def download_attachment
    attachment = ArtigoAnexo.find params[:id]
    redirect_to attachment.arquivo.url
  end

  def update_attachment
    @attachment = ArtigoAnexo.find params[:id]
    @attachment.legenda = params[:value]
    @attachment.save
    render :layout => false
  end
end
