class Admin::UsersController < Admin::AdminController

  def index
    @users = (params[:ordenar].blank?) ? User.search(params[:filtro]).paginate(:page => params[:page]) : User.search(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Nome' => 'name', 'Login' => 'login', 'Criado em' => 'created_at'}

    @total_administradores = User.count :joins => :roles, :conditions => { :roles => { :name => Role.administrador } }
    @total_clientes = User.count :joins => :roles, :conditions => { :roles => { :name => Role.cliente } }
    @total_ativos = User.com_algum_produto_ativo.count(:distinct => :true, :select => 'users.id')
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @artigos }
    end
  end

  def new
    @user = User.new
  end

  def create
    #logout_keeping_session!
    @user = User.new(params[:user])
    @roles = params[:user][:roles] || []
    @user.roles = @roles.map { |key, value| Role.find(key) }

    success = @user && @user.save
    if success && @user.errors.empty?
      # Protects against session fixation attacks, causes request forgery
      # protection if visitor resubmits an earlier form using back
      # button. Uncomment if you understand the tradeoffs.
      # reset session
      # self.current_user = @user # !! now logged in

      # adiciona na newsletter, se tiver selecionado que deseja
      if params[:newsletter] && params[:newsletter] == "on"
        NewsletterSubscribe.create(:nome => @user.name, :email => @user.email, :categoria => CategoriaNewsletterSubscribe.find_by_removivel(false))
      end

      flash[:notice] = "Usuário #{@user.login} criado com sucesso."
      respond_to do |format|
        format.html { redirect_to( admin_users_path ) }
        format.js
        format.xml  { head :ok }
      end
    else
      flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
      render :action => 'new'
      flash.discard(:error)
    end
  end

  # render new.rhtml
  def edit
    @id = params[:id]
    @user = User.find @id
  end

  # PUT /users/1
  # PUT /users/1.xml
  def update
    @id = params[:id]
    @user = User.find(@id)
    @roles = params[:user][:roles] || []
    @user.roles = @roles.map { |key, value| Role.find(key) }

    # prevent form injection
    [:password, :password_confirmation].each do |p|
      params[:user].delete p
    end

    email_antigo = @user.email

    respond_to do |format|
      if @user.update_attributes(params[:user])
        @newsletter = NewsletterSubscribe.find_by_email(email_antigo)
        if params[:newsletter] && params[:newsletter] == "on"
          # atualiza os dados para newsletter
          if @newsletter
            @newsletter.nome = @user.name
            @newsletter.email = @user.email
            @newsletter.save
          else
            # cria o registro ja que nao existe
            NewsletterSubscribe.create(:nome => @user.name, :email => @user.email, :categoria => CategoriaNewsletterSubscribe.find_by_removivel(false))
          end
        else
          # remove da tabela subscribe
          NewsletterSubscribe.destroy(@newsletter) if @newsletter
        end

        flash[:notice] = "Usuário '#{@user.login}' editado com sucesso!"
        format.html { redirect_to( admin_users_path ) }
        format.js
        format.xml  { head :ok }
      else
        format.html {
          flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
          render :edit
          flash.discard(:error)
        }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @id = params[:id]
    if @id == current_user.id
      flash[:error] = "Você não pode remover seu próprio usu&aacute;rio"
    else
      begin
        User.destroy(@id)
        flash[:notice] = "Usuário removido com sucesso!"
      rescue
        flash[:error] = "N&atilde;o foi poss&iacute;vel remover esse usu&aacute;rio! Verifique se ele &eacute; respons&aacute;vel por um blog e substitua-o antes de remov&ecirc;-lo."
      end
    end

    respond_to do |format|
      format.html { redirect_to( admin_users_path ) }
      format.js
      format.xml  { head :ok }
    end
  end

  def exportar_emails
    emails = User.all(:select => 'DISTINCT(email)', :order => "email ASC").collect(&:email).join("\n")
    send_data emails,
                :type => 'text/plain',
                :filename => "emails.txt"
  end

  def update_cidade
    @estado = Estado.find(params[:estado_id])
    render :layout => false
  end

end
