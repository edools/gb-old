class Admin::ClassroomEventsController < Admin::AdminController
  # GET /classroom-events
  # GET /classroom-events.xml
  def index
    @classroom_events = (params[:ordenar].blank?) ? ClassroomEvent.ordered.search_backend(params[:filtro]).paginate(:page => params[:page], :include => [:categoria]) : ClassroomEvent.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page], :include => [:categoria])
    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Investimento Ascendente' => 'investimento ASC', 'Investimento Descendente' => 'investimento DESC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}
  end

  # GET /classroom-events/new
  # GET /classroom-events/new.xml
  def new
    @classroom_event = ClassroomEvent.new
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => {:video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao}}
  end

  def edit
    @classroom_event = ClassroomEvent.find(params[:id])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => {:video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao}}
  end

  # POST /classroom-events
  # POST /classroom-events.xml
  def create
    @classroom_event = ClassroomEvent.new(params[:classroom_event])

    respond_to do |format|
      if @classroom_event.save
        flash[:notice] = 'Evento adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_classroom_event_url : admin_classroom_events_url }
        format.xml { render :xml => @classroom_event, :status => :created, :location => @classroom_event }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => {:video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao}}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "new" }
        format.xml { render :xml => @classroom_event.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /classroom-events/1
  # PUT /classroom-events/1.xml
  def update
    @classroom_event = ClassroomEvent.find(params[:id])

    respond_to do |format|
      if @classroom_event.update_attributes(params[:classroom_event])
        flash[:notice] = 'Evento editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_classroom_event_url : admin_classroom_events_url }
        format.xml { head :ok }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => {:video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao}}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml { render :xml => @classroom_event.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /classroom-events/1
  # DELETE /classroom-events/1.xml
  def destroy
    begin
      ClassroomEvent.destroy(params[:id])
      flash[:notice] = "Evento removido com sucesso"
    rescue Exception => e
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_classroom_events_url) }
      format.xml { head :ok }
    end
  end

  def tickets
    code = params[:filter][:ticket] if params[:filter].present?
    @classroom_tickets = ClassroomTicket.code(code).paginate(:page => params[:page])
  end

  def write_off
    ticket = ClassroomTicket.active.find(params[:id])
    ticket.write_off!

    flash[:notice] = "Ingresso utilizado com sucesso"

    respond_to do |format|
      format.html { redirect_to(tickets_admin_classroom_event_url(ticket.classroom_event)) }
      format.xml { head :ok }
    end
  end
end