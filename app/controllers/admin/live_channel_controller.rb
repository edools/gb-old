class Admin::LiveChannelController < Admin::AdminController
  # GET /live-channel/edit
  def edit
    @live_channel = LiveChannel.singleton_instance
  end

  # PUT /live-channel
  # PUT /live-channel.xml
  def update
    @live_channel = LiveChannel.first

    respond_to do |format|
      if @live_channel.update_attributes(params[:live_channel])
        flash[:notice] = 'Canal ao Vivo atualizado com sucesso'
        format.html { redirect_to :action => "edit" }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @live_channel.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  def download_attachment
    attachment = LiveChannelSupportingMaterial.find params[:id]
    redirect_to attachment.attachment.url
  end

  def update_attachment
    @attachment = LiveChannelSupportingMaterial.find params[:id]
    @attachment.label = params[:value]
    @attachment.save
    render :layout => false
  end
end
