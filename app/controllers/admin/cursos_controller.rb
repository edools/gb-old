class Admin::CursosController < Admin::AdminController
  # GET /cursos
  # GET /cursos.xml
  def index
    @cursos = (params[:ordenar].blank?) ?
      Curso.ordered.paginate(:page => params[:page], :include => [:categoria]) :
      Curso.ordered(params[:ordenar]).paginate(:page => params[:page], :include => [:categoria])
    @ordenacao = {'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC', 'Categoria (A-Z)' => 'categoria_cursos.nome ASC', 'Categoria (Z-A)' => 'categoria_cursos.nome DESC', 'Investimento Ascendente' => 'investimento ASC', 'Investimento Descendente' => 'investimento DESC', 'Ativo' => 'ativo', 'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @cursos }
    end
  end

  # GET /cursos/new
  # GET /cursos/new.xml
  def new
    @curso = Curso.new
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @demonstration_video_uploads = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @curso }
    end
  end

  # GET /cursos/1/edit
  def edit
    @curso = Curso.find(params[:id])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @demonstration_video_uploads = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}
  end

  # POST /cursos
  # POST /cursos.xml
  def create
    @curso = Curso.new(params[:curso])

    respond_to do |format|
      if @curso.save
        flash[:notice] = 'Curso adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_curso_url : admin_cursos_url }
        format.xml  { render :xml => @curso, :status => :created, :location => @curso }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @demonstration_video_uploads = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "new" }
        format.xml  { render :xml => @curso.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /cursos/1
  # PUT /cursos/1.xml
  def update
    @curso = Curso.find(params[:id])

    respond_to do |format|
      if @curso.update_attributes(params[:curso])
        flash[:notice] = 'Curso editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_curso_url : admin_cursos_url }
        format.xml  { head :ok }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @demonstration_video_uploads = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml  { render :xml => @curso.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /cursos/1
  # DELETE /cursos/1.xml
  def destroy
    begin
      Curso.destroy(params[:id])
      flash[:notice] = "Curso removido com sucesso"
    rescue Exception => ex
      logger.error("Erro ao deletar o curso presencial #{params[:id]}. Motivo: #{ex.message}")
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_cursos_url) }
      format.xml  { head :ok }
    end
  end

  def download_attachment
    attachment = CursoAnexo.find params[:id]
    redirect_to attachment.arquivo.url
  end

  def update_attachment
    @attachment = CursoAnexo.find params[:id]
    @attachment.legenda = params[:value]
    @attachment.save
    render :layout => false
  end
end
