class Admin::NewsletterSubscribesController < Admin::AdminController

  # GET /subscribe
  # GET /subscribe.xml
  def index
    @subscribe = (params[:ordenar].blank?) ? NewsletterSubscribe.ordered.paginate(:page => params[:page]) : NewsletterSubscribe.ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Nome' => 'nome', 'E-mail' => 'email', 'Cadastrada em' => 'created_at'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @subscribe }
    end
  end

  # GET /subscribe/new
  # GET /subscribe/new.xml
  def new
    @subscribe = NewsletterSubscribe.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @subscribe }
    end
  end

  # GET /subscribe/1/edit
  def edit
    @subscribe = NewsletterSubscribe.find(params[:id])
  end

  # POST /subscribe
  # POST /subscribe.xml
  def create
    @subscribe = NewsletterSubscribe.new(params[:subscribe])

    respond_to do |format|
      if @subscribe.save
        flash[:notice] = 'Registro adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_subscribe_url : admin_subscribe_url }
        format.xml  { render :xml => @subscribe, :status => :created, :location => @subscribe }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @subscribe.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /subscribe/1
  # PUT /subscribe/1.xml
  def update
    @subscribe = NewsletterSubscribe.find(params[:id])

    respond_to do |format|
      if @subscribe.update_attributes(params[:subscribe])
        flash[:notice] = 'Registro editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_subscribe_url : admin_subscribe_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @subscribe.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /subscribe/1
  # DELETE /subscribe/1.xml
  def destroy
    begin
      NewsletterSubscribe.destroy(params[:id])
      flash[:notice] = "Registro removido com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_subscribe_url) }
      format.xml  { head :ok }
    end
  end

end
