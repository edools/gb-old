class Admin::BannersController < Admin::AdminController
  # GET /banners
  # GET /banners.xml
  def index
    @banners = (params[:ordenar].blank?) ? Banner.paginate(:page => params[:page]) : Banner.ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Tipo' => 'tipo', 'Cadastrado em' => 'created_at'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @banners }
    end
  end

  # GET /banners/1/report
  def report
    @banner = Banner.find(params[:id])
    if params[:filtro]
      @inicio = Date.strptime(params[:filtro][:data_inicio], '%d/%m/%Y').to_datetime if params[:filtro][:data_inicio]
      @fim = Date.strptime(params[:filtro][:data_fim], '%d/%m/%Y').to_datetime if params[:filtro][:data_fim]
      @histories = (params[:ordenar].blank?) ? BannerHistory.search(params[:filtro]).ordered.paginate(:page => params[:page]) : BannerHistory.search(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page])
    else
      @inicio = DateTime.now - 15.days
      @fim = DateTime.now + 1.day
      @histories = @banner.banner_histories
    end
  end

  # GET /banners/new
  # GET /banners/new.xml
  def new
    @banner = Banner.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @banner }
    end
  end

  # GET /banners/1/edit
  def edit
    @banner = Banner.find(params[:id])
  end

  # POST /banners
  # POST /banners.xml
  def create
    @banner = Banner.new(params[:banner])

    respond_to do |format|
      if @banner.save
        flash[:notice] = 'Banner adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_banner_url : admin_banners_url }
        format.xml  { render :xml => @banner, :status => :created, :location => @banner }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @banner.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /banners/1
  # PUT /banners/1.xml
  def update
    @banner = Banner.find(params[:id])

    respond_to do |format|
      if @banner.update_attributes(params[:banner])
        flash[:notice] = 'Banner editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_banner_url : admin_banners_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @banner.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /banners/1
  # DELETE /banners/1.xml
  def destroy
    begin
      Banner.destroy(params[:id])
      flash[:notice] = "Banner removido com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_banners_url) }
      format.xml  { head :ok }
    end
  end

end
