class Admin::TickersController < Admin::AdminController

  # cache_sweeper :home_sweeper, :only => [:create, :update, :destroy]

  # GET /tickers
  # GET /tickers.xml
  def index
    @tickers = (params[:ordenar].blank?) ? Ticker.ordered.search_backend(params[:filtro]).paginate(:page => params[:page]) : Ticker.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Categoria' => 'category', 'Descrição' => 'descricao', 'Cadastrado em' => 'created_at DESC'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @tickers }
    end
  end

  # GET /tickers/new
  # GET /tickers/new.xml
  def new
    @ticker = Ticker.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ticker }
    end
  end

  # GET /tickers/1/edit
  def edit
    @ticker = Ticker.find(params[:id])
  end

  # POST /tickers
  # POST /tickers.xml
  def create
    @ticker = Ticker.new(params[:ticker])

    respond_to do |format|
      if @ticker.save
        flash[:notice] = 'Ticker adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_ticker_url : admin_tickers_url }
        format.xml  { render :xml => @ticker, :status => :created, :location => @ticker }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @ticker.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /tickers/1
  # PUT /tickers/1.xml
  def update
    @ticker = Ticker.find(params[:id])

    respond_to do |format|
      if @ticker.update_attributes(params[:ticker])
        flash[:notice] = 'Ticker editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_ticker_url : admin_tickers_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ticker.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /tickers/1
  # DELETE /tickers/1.xml
  def destroy
    begin
      Ticker.destroy(params[:id])
      flash[:notice] = "Ticker removido com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_tickers_url) }
      format.xml  { head :ok }
    end
  end

end
