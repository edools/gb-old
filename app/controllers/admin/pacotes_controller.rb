class Admin::PacotesController < Admin::AdminController
  # GET /pacotes
  # GET /pacotes.xml
  def index
    @pacotes = (params[:ordenar].blank?) ? Pacote.ordered.search_backend(params[:filtro]).paginate(:page => params[:page], :include => [:categoria]) : Pacote.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page], :include => [:categoria])
    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Investimento Ascendente' => 'investimento ASC', 'Investimento Descendente' => 'investimento DESC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}
    @pacotes_com_quantidade_nao_expirada = Pacote.com_quantidade_nao_expirada.ordered("produtos.nome ASC")
    @total = Pacote.count
    @total_nao_expirados = @pacotes_com_quantidade_nao_expirada.inject(0) {|sum, pacote| sum + pacote.quantidade_nao_expirada.to_i }
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pacotes }
    end
  end

  # GET /pacotes/new
  # GET /pacotes/new.xml
  def new
    @pacote = Pacote.new
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @pacote }
    end
  end

  # GET /pacotes/1/edit
  def edit
    @pacote = Pacote.find(params[:id])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}
  end

  # POST /pacotes
  # POST /pacotes.xml
  def create
    @pacote = Pacote.new(params[:pacote])

    respond_to do |format|
      if @pacote.save
        flash[:notice] = 'Pacote adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_pacote_url : admin_pacotes_url }
        format.xml  { render :xml => @pacote, :status => :created, :location => @pacote }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "new" }
        format.xml  { render :xml => @pacote.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /pacotes/1
  # PUT /pacotes/1.xml
  def update
    @pacote = Pacote.find(params[:id])

    respond_to do |format|
      if @pacote.update_attributes(params[:pacote])
        flash[:notice] = 'Pacote editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_pacote_url : admin_pacotes_url }
        format.xml  { head :ok }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml  { render :xml => @pacote.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /pacotes/1
  # DELETE /pacotes/1.xml
  def destroy
    begin
      Pacote.destroy(params[:id])
      flash[:notice] = "Pacote removido com sucesso"
    rescue
      flash[:error] = "Não foi possível excluir esse Pacote."
    end

    respond_to do |format|
      format.html { redirect_to(admin_pacotes_url) }
      format.xml  { head :ok }
    end
  end

end
