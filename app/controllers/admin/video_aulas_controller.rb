class Admin::VideoAulasController < Admin::AdminController
  include ::AjaxSearchable
  create_ajax_search :ajax_search

  # GET /video_aulas
  # GET /video_aulas.xml
  def index
    @video_aulas = (params[:ordenar].blank?) ? VideoAula.ordered.search_backend(params[:filtro]).paginate(:page => params[:page], :include => [:categoria, :packages]) : VideoAula.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page], :include => [:categoria, :packages])
    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Investimento Ascendente' => 'investimento ASC', 'Investimento Descendente' => 'investimento DESC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}
    @video_aulas_com_quantidade_nao_expirada = VideoAula.com_quantidade_nao_expirada.ordered("produtos.nome ASC")
    @total = VideoAula.count
    @total_nao_expirados = @video_aulas_com_quantidade_nao_expirada.inject(0) {|sum, video_aula| sum + video_aula.quantidade_nao_expirada.to_i }
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @video_aulas }
    end
  end

  # GET /video_aulas/new
  # GET /video_aulas/new.xml
  def new
    @video_aula = VideoAula.new
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @video_aula }
    end
  end

  def edit
    @video_aula = VideoAula.find(params[:id])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}
  end

  # POST /video_aulas
  # POST /video_aulas.xml
  def create
    @video_aula = VideoAula.new(params[:video_aula])

    respond_to do |format|
      if @video_aula.save
        flash[:notice] = 'Curso on-line adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_video_aula_url : admin_video_aulas_url }
        format.xml  { render :xml => @video_aula, :status => :created, :location => @video_aula }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "new" }
        format.xml  { render :xml => @video_aula.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /video_aulas/1
  # PUT /video_aulas/1.xml
  def update
    @video_aula = VideoAula.find(params[:id])

    respond_to do |format|
      if @video_aula.update_attributes(params[:video_aula])
        flash[:notice] = 'Curso on-line editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_video_aula_url : admin_video_aulas_url }
        format.xml  { head :ok }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @video_upload_demonstracaos = VideoUpload.ordered.all :joins => [:tipo], :conditions => { :video_upload_tipos => {:identificador => VideoUploadCategoria.demonstracao }}

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml  { render :xml => @video_aula.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /video_aulas/1
  # DELETE /video_aulas/1.xml
  def destroy
    begin
      VideoAula.destroy(params[:id])
      flash[:notice] = "Curso on-line removido com sucesso"
    rescue Exception => e
      flash[:error] = "Não foi possível excluir essa Vídeo Aula."
    end

    respond_to do |format|
      format.html { redirect_to(admin_video_aulas_url) }
      format.xml  { head :ok }
    end
  end
end
