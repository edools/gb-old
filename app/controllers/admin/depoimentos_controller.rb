class Admin::DepoimentosController < Admin::AdminController

  # cache_sweeper :home_sweeper, :only => [:create, :update, :destroy]

  # GET /depoimentos
  # GET /depoimentos.xml
  def index
    @depoimentos = (params[:ordenar].blank?) ? Depoimento.ordered.paginate(:page => params[:page]) : Depoimento.ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Nome' => 'nome', 'Cadastrado em' => 'created_at DESC'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @depoimentos }
    end
  end

  # GET /depoimentos/new
  # GET /depoimentos/new.xml
  def new
    @depoimento = Depoimento.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @depoimento }
    end
  end

  # GET /depoimentos/1/edit
  def edit
    @depoimento = Depoimento.find(params[:id])
  end

  # POST /depoimentos
  # POST /depoimentos.xml
  def create
    @depoimento = Depoimento.new(params[:depoimento])

    respond_to do |format|
      if @depoimento.save
        flash[:notice] = 'Depoimento adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_depoimento_url : admin_depoimentos_url }
        format.xml  { render :xml => @depoimento, :status => :created, :location => @depoimento }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @depoimento.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /depoimentos/1
  # PUT /depoimentos/1.xml
  def update
    @depoimento = Depoimento.find(params[:id])

    respond_to do |format|
      if @depoimento.update_attributes(params[:depoimento])
        flash[:notice] = 'Depoimento editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_depoimento_url : admin_depoimentos_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @depoimento.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /depoimentos/1
  # DELETE /depoimentos/1.xml
  def destroy
    begin
      Depoimento.destroy(params[:id])
      flash[:notice] = "Depoimento removido com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_depoimentos_url) }
      format.xml  { head :ok }
    end
  end

end
