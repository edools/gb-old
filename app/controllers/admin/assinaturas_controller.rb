class Admin::AssinaturasController < Admin::AdminController

  # GET /assinaturas
  # GET /assinaturas.xml
  def index
    @assinaturas = (params[:ordenar].blank?) ? Assinatura.ordered.search_backend(params[:filtro]).paginate(:page => params[:page]) : Assinatura.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}
    @assinaturas_com_quantidade_nao_expirada = Assinatura.com_quantidade_nao_expirada.ordered("produtos.nome ASC")
    @total = Assinatura.count
    @total_nao_expirados = @assinaturas_com_quantidade_nao_expirada.inject(0) {|sum, assinatura| sum + assinatura.quantidade_nao_expirada.to_i }
  end

  # GET /assinaturas/new
  # GET /assinaturas/new.xml
  def new
    @assinatura = Assinatura.new
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @assinatura }
    end
  end

  # GET /assinaturas/1/edit
  def edit
    @assinatura = Assinatura.find(params[:id])
  end

  # POST /assinaturas
  # POST /assinaturas.xml
  def create
    @assinatura = Assinatura.new(params[:assinatura])

    respond_to do |format|
      if @assinatura.save
        flash[:notice] = 'Assinatura adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_assinatura_url : admin_assinaturas_url }
        format.xml  { render :xml => @assinatura, :status => :created, :location => @assinatura }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "new" }
        format.xml  { render :xml => @assinaturas.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /assinaturas/1
  # PUT /assinaturas/1.xml
  def update
    @assinatura = Assinatura.find(params[:id])

    respond_to do |format|
      if @assinatura.update_attributes(params[:assinatura])
        flash[:notice] = 'Assinatura editada com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_assinatura_url : admin_assinaturas_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @assinaturas.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /assinaturas/1
  # DELETE /assinaturas/1.xml
  def destroy
    begin
      Assinatura.destroy(params[:id])
      flash[:notice] = "Assinatura removida com sucesso"
    rescue Exception => ex
      logger.error("Erro ao deletar a assinatura #{params[:id]}. Motivo: #{ex.message}")
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_assinaturas_url) }
      format.xml  { head :ok }
    end
  end

end
