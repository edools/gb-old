class Admin::VideoUploadsController < Admin::AdminController
  include ::AjaxSearchable
  create_ajax_search :ajax_search
  create_ajax_search :ajax_search_simulados, :model => :video_upload, :scope => lambda { |model| model.simulados }

  # GET /video_uploads
  # GET /video_uploads.xml
  def index
    sort_field = !params[:ordenar].blank? ? params[:ordenar] : VideoUpload.default_ordering
    @video_uploads = VideoUpload.search_backend(params[:filtro]).ordered(sort_field).paginate(:page => params[:page], :include => [:tipo, :categoria])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @tipos = VideoUploadTipo.ordered.all
    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @video_uploads }
    end
  end

  # GET /video_uploads/new
  # GET /video_uploads/new.xml
  def new
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @tipos = VideoUploadTipo.ordered.all
    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /video_uploads
  # POST /video_uploads.xml
  def create
    video_upload = VideoUpload.new params[:video_upload]
    video_upload.attach_file params[:Filedata]
    video_upload.creator = current_user
    video_upload.save!
    render :json => true
  rescue => ex
    logger.error("Erro ao fazer upload de video. Motivo: #{ex.message}, Params: #{params}")
    raise ex
  end

  # GET /video_uploads/1/edit
  def edit
    @video_upload = VideoUpload.find(params[:id])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
    @tipos = VideoUploadTipo.ordered.all
  end

  # PUT /video_uploads/1
  # PUT /video_uploads/1.xml
  def update
    @video_upload = VideoUpload.find(params[:id])

    respond_to do |format|
      if @video_upload.update_attributes(params[:video_upload])
        flash[:notice] = 'Vídeo editado com sucesso'
        format.html { redirect_to admin_video_uploads_url }
        format.xml  { head :ok }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
        @tipos = VideoUploadTipo.ordered.all

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml  { render :xml => @video_upload.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /video_uploads/1
  # DELETE /video_uploads/1.xml
  def destroy
    begin
      VideoUpload.destroy(params[:id])
      flash[:notice] = "Vídeo removido com sucesso"
    rescue Exception => e
      flash[:error] = "Existe algum curso on-line usando esse vídeo. Remova-o antes de remover o vídeo."
    end

    respond_to do |format|
      format.html { redirect_to(admin_video_uploads_url) }
      format.xml  { head :ok }
    end
  end

  # GET video_uploads/download/:id
  def download
    video_upload = VideoUpload.find_by_id(params[:id])
    if video_upload.nil?
      render_optional_error_file(404)
    elsif !video_upload.streaming_exists?
      render_optional_error_file(500)
    else
      respond_to do |format|
        format.json do
          render :json => {:response => render_to_string(:partial => "shared/watch.html",
                                               :locals => {:url => video_upload.streaming.url}).html_safe
                          }
        end
      end
    end
  end
end
