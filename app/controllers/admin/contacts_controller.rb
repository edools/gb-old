class Admin::ContactsController < Admin::AdminController

  def index
    @support = Contact.new
  end

  def create
    @support = Contact.new params[:contact]
    @support.name = current_user.name
    @support.email = current_user.email

    respond_to do |format|
      if @support.save
        begin
          UserMailer.deliver_support(@support)
          flash[:notice] = 'Mensagem enviada com sucesso.'
        
          format.html { redirect_to root_url }
          format.js
        rescue
          @sending_error = true
        end
      else
        flash[:error] = 'Houve um erro no envio da mensagem. Tente novamente.'
        format.html { render :action => "index" }
        format.js
      end
    end
  end
end