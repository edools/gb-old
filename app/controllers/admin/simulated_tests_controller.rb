class Admin::SimulatedTestsController < Admin::AdminController
	# GET /simulados
	# GET /simulados.xml
	def index
		@simulated_tests = (params[:ordenar].blank?) ? SimulatedTest.ordered.search_backend(params[:filtro]).paginate(:page => params[:page], :include => [:categoria]) : SimulatedTest.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page], :include => [:categoria])
		@ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Investimento Ascendente' => 'investimento ASC', 'Investimento Descendente' => 'investimento DESC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}
		@simulated_test_com_quantidade_nao_expirada = SimulatedTest.com_quantidade_nao_expirada.ordered("produtos.nome ASC")
		@total = SimulatedTest.count
		@total_nao_expirados = @simulated_test_com_quantidade_nao_expirada.inject(0) {|sum, simulated_test| sum + simulated_test.quantidade_nao_expirada.to_i }
		respond_to do |format|
			format.html # index.html.erb
			format.xml  { render :xml => @simulated_tests }
		end
	end

	# GET /simulados/new
	# GET /simulados/new.xml
	def new
		@simulated_test = SimulatedTest.new
		@categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

		respond_to do |format|
			format.html # new.html.erb
			format.xml  { render :xml => @simulated_test }
		end
	end

	# GET /simulados/1/edit
	def edit
		@simulated_test = SimulatedTest.find(params[:id])
		@categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

		if UserSimulatedTest.find_all_by_simulated_test_id(@simulated_test.id).count > 0
			flash.now[:error] = "Simulado já possui respostas. A remoção de uma pergunta ocasionará em remoção das respectivas respostas."
		end
	end

	# POST /simulados
	# POST /simulados.xml
	def create
		@simulated_test = SimulatedTest.new(params[:simulated_test])
		respond_to do |format|
			if @simulated_test.save
				flash[:notice] = 'Simulado adicionado com sucesso'
				format.html { redirect_to params.key?(:save_and_new) ? new_admin_simulated_test_url : admin_simulated_tests_url }
				format.xml  { render :xml => @simulated_test, :status => :created, :location => @simulated_test }
			else
				@categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

				flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

				format.html { render :action => "new" }
				format.xml  { render :xml => @simulated_test.errors, :status => :unprocessable_entity }
				flash.discard(:error)
			end
		end
	end

	# PUT /simulados/1
	# PUT /simulados/1.xml
	def update
		@simulated_test = SimulatedTest.find(params[:id])

		respond_to do |format|
			if @simulated_test.update_attributes(params[:simulated_test])
				flash[:notice] = 'Simulado editado com sucesso'
				format.html { redirect_to params.key?(:save_and_new) ? new_admin_simulated_test_url : admin_simulated_tests_url }
				format.xml  { head :ok }
			else
				@categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

				flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

				format.html { render :action => "edit" }
				format.xml  { render :xml => @simulated_test.errors, :status => :unprocessable_entity }
				flash.discard(:error)
			end
		end
	end

	# DELETE /simulados/1
	# DELETE /simulados/1.xml
	def destroy
		begin
			SimulatedTest.destroy(params[:id])
			flash[:notice] = "Simulado removido com sucesso"
		rescue
			flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
		end

		respond_to do |format|
			format.html { redirect_to(admin_simulated_tests_url) }
			format.xml  { head :ok }
		end
  end

  def activity_report
    @simulated_test = SimulatedTest.find params[:id]
  end
end
