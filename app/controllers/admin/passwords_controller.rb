class Admin::PasswordsController < Admin::AdminController

  def update
    @user = current_user
    if User.authenticate(@user.login, params.delete(:old_password) )
      # FIXME: adicionar validacao ao inves do hack de colocar um password fake de tamanho 1
      #@user.errors.add_on_blank(:password)
      params[:user][:password] = params[:user][:password_confirmation] = rand(9) if params[:user][:password].blank?
      @user.password = params[:user][:password]
      @user.password_confirmation = params[:user][:password_confirmation]
      if @user.save
        flash[:notice] = "Senha alterada com sucesso!"
        redirect_back_or_default(admin_dashboard_path)
      else
        flash.now[:error] = "A nova senha é diferente da confirmação. Por favor, redigite-as!"
      end
    else
      flash.now[:error] = "Sua senha atual está incorreta!"
    end if request.post?
    @user.password = @user.password_confirmation = nil
  end

end
