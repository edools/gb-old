class Admin::FaturasController < Admin::AdminController
  # GET /faturas
  # GET /faturas.xml
  def index
    @faturas = (params[:ordenar].blank?) ? Fatura.search("DISTINCT(faturas.*)", params[:filtro]).ordered.paginate(:page => params[:page]) : Fatura.search("DISTINCT(faturas.*)",params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page])
    @total = Fatura.count
    @total_aprovadas = Fatura.count :conditions => {:status => Fatura::STATUS_APPROVED.to_s }
    @total_canceladas = Fatura.count :conditions => {:status => Fatura::STATUS_CANCELED.to_s }
    @total_pendentes = Fatura.count :conditions => {:status => Fatura::STATUS_PENDING.to_s }
    @total_reembolsadas = Fatura.count :conditions => {:status => Fatura::STATUS_REFUNDED.to_s }
    @total_verificando = Fatura.count :conditions => {:status => Fatura::STATUS_VERIFYING.to_s }

    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}
    @cursos = Produto.find(:all, :order => "nome ASC")
    session[:export_faturas] = params

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @faturas }
    end

  end

  # GET /faturas/1/edit
  def edit
    @fatura = Fatura.find(params[:id])
  end

  # PUT /categoria_cursos/1
  # PUT /categoria_cursos/1.xml
  def update
    @fatura = Fatura.find(params[:id])

    respond_to do |format|
      if @fatura.update_attributes(params[:fatura])
        flash[:notice] = 'Fatura editada com sucesso'
        format.html { redirect_to admin_faturas_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @faturas.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # GET /faturas/resumo
  # GET /faturas/resumo.xml
  def resumo
    conditions = {:query => [], :params => []}
    if !params[:filtro].blank? and !params[:filtro][:curso].blank?
     conditions[:query] << 'produto_id=?'
     conditions[:params] << params[:filtro][:curso]
    end

   produtos_conditions = {}
    if !params[:filtro].blank? and !params[:filtro][:tipo].blank?
     conditions[:query] << 'produtos.type=?'
     conditions[:params] << params[:filtro][:tipo]
     produtos_conditions = { :type => params[:filtro][:tipo] }
    end

    periodo_inicial = ((!params[:filtro].blank? && !params[:filtro][:periodo_inicial].blank?) ? DateTime.strptime(params[:filtro][:periodo_inicial], '%d/%m/%Y') : nil)
    periodo_final = ((!params[:filtro].blank? && !params[:filtro][:periodo_final].blank?) ? DateTime.strptime(params[:filtro][:periodo_final], '%d/%m/%Y') : nil)

    if !periodo_inicial.blank? && !periodo_final.blank?
      conditions[:query] << 'faturas.created_at >= ?'
      conditions[:params] << ((periodo_inicial <= periodo_final) ? periodo_inicial.to_datetime : periodo_final.to_datetime)
    elsif !periodo_inicial.blank?
      conditions[:query] << 'faturas.created_at >= ?'
      conditions[:params] << periodo_inicial.to_datetime
    end

    if !periodo_inicial.blank? && !periodo_final.blank?
      conditions[:query] << 'faturas.created_at <= ?'
      if periodo_final > periodo_inicial
        periodo_final_adiantado = (periodo_final.to_time + 23.hours + 59.minutes + 59.seconds).to_datetime
      else
        periodo_final_adiantado = (periodo_inicial.to_time + 23.hours + 59.minutes + 59.seconds).to_datetime
      end
      conditions[:params] << periodo_final_adiantado.to_datetime
    elsif !periodo_final.blank?
      conditions[:query] << 'faturas.created_at <= ?'
      conditions[:params] << periodo_final.to_datetime
    end

    conditions[:query] << 'faturas.status=?'
    conditions[:params] << Fatura::STATUS_APPROVED.to_s

    conditions = [conditions[:query].join(" AND ")].concat(conditions[:params])

    @resumo = FaturaItem.sum(:quantity,
                  :group => :produto,
                  :conditions => conditions,
                  :order => 'sum_quantity DESC',
                  :joins => 'INNER JOIN faturas ON faturas.id=fatura_items.fatura_id INNER JOIN produtos ON fatura_items.produto_id=produtos.id')
    @total = FaturaItem.sum(:quantity, :conditions => conditions, :joins => 'INNER JOIN faturas ON faturas.id=fatura_items.fatura_id INNER JOIN produtos ON fatura_items.produto_id=produtos.id')
    @cursos = Produto.find(:all, :order => "nome ASC", :conditions => produtos_conditions)
    @tipos_produtos = {'Assinatura' => Assinatura.name, 'Pacote' => Pacote.name, 'Vídeo Aula' => VideoAula.name, 'Ebook' => Arquivo.name, 'Aula Ao Vivo' => Ticket.name, 'Evento' => ClassroomEvent.name }.sort
  end

  # DELETE /faturas/1
  # DELETE /faturas/1.xml
  def destroy
    begin
      Fatura.destroy(params[:id])
      flash[:notice] = "Fatura removida com sucesso"
    rescue Exception => e
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_faturas_url) }
      format.xml  { head :ok }
    end
  end

  def summary_change_product_type
    options = { :order => "nome ASC" }
    options[:conditions] = { :type => params[:type] } unless params[:type].blank?
    products = Produto.all(options)
    partial = render_to_string(:partial => 'tipo_produto', :locals => {:cursos => products })

    respond_to do |format|
      format.json {
        render :json => { :partial => partial }
      }
    end
  end

  def exportar_emails
    if (session[:export_faturas].nil? || session[:export_faturas].empty?)
      emails = Fatura.search("DISTINCT(users.email), users.first_name, users.last_name", nil).collect{|fatura| fatura.name + ", " + fatura.email}.join("\n")
    else
      parameters = session[:export_faturas]
      faturas = Fatura.search("DISTINCT(users.email), users.first_name, users.last_name", parameters[:filtro])
      emails = faturas.collect{|fatura| fatura.first_name + (fatura.last_name.blank? ? "" : " #{fatura.last_name}") + ", " + fatura.email}.join("\n")
    end

    send_data emails,
      :type => 'text/plain',
      :filename => "emails_faturas.txt"
  end
end
