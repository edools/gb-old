class Admin::DescontosController < Admin::AdminController
  @@mapa_ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  # GET /descontos
  # GET /descontos.xml
  def index
    @descontos = Desconto.ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    @ordenacao = @@mapa_ordenacao.keys

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @descontos }
    end
  end

  # GET /descontos/new
  # GET /descontos/new.xml
  def new
    @desconto = Desconto.new
    @assinaturas = Assinatura.ordered.all
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @desconto }
    end
  end

  # POST /descontos
  # POST /descontos.xml
  def create
    @desconto = Desconto.new(params[:desconto])

    respond_to do |format|
      if @desconto.save
        flash[:notice] = 'Desconto adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_desconto_url : admin_descontos_url }
        format.xml  { render :xml => @desconto, :status => :created, :location => @desconto }
      else
        @assinaturas = Assinatura.ordered.all
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @desconto.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # GET /descontos/1/edit
  def edit
    @desconto = Desconto.find(params[:id])
    @assinaturas = Assinatura.ordered.all
  end

  # PUT /descontos/1
  # PUT /descontos/1.xml
  def update
    @desconto = Desconto.find(params[:id])

    respond_to do |format|
      if @desconto.update_attributes(params[:desconto])
        flash[:notice] = 'Desconto editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_desconto_url : admin_descontos_url }
        format.xml  { head :ok }
      else
        @assinaturas = Assinatura.ordered.all
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @descontos.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /descontos/1
  # DELETE /descontos/1.xml
  def destroy
    begin
      Desconto.destroy(params[:id])
      flash[:notice] = "Desconto removido com sucesso"
    rescue Exception => ex
      logger.error("Erro ao deletar o desconto #{params[:id]}. Motivo: #{ex.message}")
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_descontos_url) }
      format.xml  { head :ok }
    end
  end

end
