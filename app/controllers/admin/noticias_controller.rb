class Admin::NoticiasController < Admin::AdminController
  # GET /noticias
  # GET /noticias.xml
  def index
    @noticias = (params[:ordenar].blank?) ? Noticia.ordered.paginate(:page => params[:page]) : Noticia.ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Nome' => 'nome', 'Criada em' => 'created_at'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @noticias }
    end
  end

  # GET /noticias/new
  # GET /noticias/new.xml
  def new
    @noticia = Noticia.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @noticia }
    end
  end

  # GET /noticias/1/edit
  def edit
    @noticia = Noticia.find(params[:id])
  end

  # POST /noticias
  # POST /noticias.xml
  def create
    @noticia = Noticia.new(params[:noticia])

    respond_to do |format|
      if @noticia.save
        flash[:notice] = 'Notícia criada com sucesso'
        format.html { redirect_to :action => (params.key?(:save_and_new)) ? "new" : "index" }
        format.xml  { render :xml => @noticia, :status => :created, :location => @noticia }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @noticia.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /noticias/1
  # PUT /noticias/1.xml
  def update
    @noticia = Noticia.find(params[:id])

    respond_to do |format|
      if @noticia.update_attributes(params[:noticia])
        flash[:notice] = 'Not&iacute;cia atualizada com sucesso'
        format.html { redirect_to :action => (params.key?(:save_and_new)) ? "new" : "index" }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @noticia.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /noticias/1
  # DELETE /noticias/1.xml
  def destroy
    begin
      Noticia.destroy(params[:id])
      flash[:notice] = "Not&iacute;cia removida com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_noticias_url) }
      format.xml  { head :ok }
    end
  end

end

