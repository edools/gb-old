# Filters added to this controller apply to all admin controllers in the application.
# Likewise, all the methods added will be available for all admin controllers.

class Admin::AdminController < ApplicationController
  # define the base layout
  layout 'backend'
  
  before_filter :login_required
  skip_before_filter :setup_total_basket_items
  require_role [Role.administrador]

end
