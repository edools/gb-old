class Admin::DashboardsController < Admin::AdminController

  def show
    @noticias = {:total => Noticia.count(:all), :semana => Noticia.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @artigos = {:total => Artigo.count(:all), :semana => Artigo.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @cursos = {:total => Curso.count(:all), :semana => Curso.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @professores = {:total => Professor.count(:all), :semana => Professor.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @video_aulas = {:total => VideoAula.count(:all), :semana => VideoAula.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @pacotes = {:total => Pacote.count(:all), :semana => Pacote.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @assinaturas = {:total => Assinatura.count(:all), :semana => Assinatura.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @arquivos = {:total => Arquivo.count(:all), :semana => Arquivo.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @faturas = {:total => Fatura.count(:all), :semana => Fatura.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}
    @users = {:total => User.count(:all), :semana => User.count(:conditions => ['created_at > ?', Time.now.beginning_of_week()])}

    @support = Contact.new
  end

end