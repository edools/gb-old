class Admin::TinymceAssetsController < Admin::AdminController
  # skip_before_filter :login_required
  # skip_before_filter :verify_authenticity_token

  def create
    # geometry = Paperclip::Geometry.from_file params[:file]
    imagem = Imagem.create :imagem => params[:file], :user_id => current_user.id

    respond_to do |format|
      format.json {
        render :json => { :image => { :url => imagem.imagem.url(:original) } }, :layout => false, :content_type => "text/html"
      }
    end
  end
end
