class Admin::ManageViewsController < Admin::AdminController
  def index
    @search = Admin::ManageViewsSearch.new(params[:admin_manage_views_search], params[:page])
    @presenter = Admin::ManageViewsIndexPresenter.new
  end

  def edit
    @user = User.find(params[:id])
    @view_counts = UserVideoViewCounter.find_all_not_expired.scoped(:conditions => {:user_id => @user.id})
  end

  def view_counts
    @user = User.find(params[:id])
    all_valid = true
    @view_counts = params[:manage_views].collect do |view_count_attrs|
        view_count = UserVideoViewCounter.find_all_not_expired.find(view_count_attrs[:id])
        all_valid &&= view_count.update_attributes(view_count_attrs)
        view_count
      end

    respond_to do |format|
      if all_valid
        flash[:notice] = 'Limites atualizados com sucesso'
        format.html { redirect_to admin_manage_views_url }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        flash.discard(:error)
      end
    end
  end
end
