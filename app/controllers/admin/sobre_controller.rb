class Admin::SobreController < Admin::AdminController
  # GET /sobre/edit
  def edit
    @sobre = SitePageContent.find_or_create_by_name(SitePageContent::NAME_SOBRE.to_s)
  end

  # PUT /sobre
  # PUT /sobre.xml
  def update
    @sobre = SitePageContent.find_by_name(SitePageContent::NAME_SOBRE.to_s)

    respond_to do |format|
      if @sobre.update_attributes(params[:site_page_content])
        flash[:notice] = 'Sobre o site atualizado com sucesso'
        format.html { redirect_to :action => "edit" }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @sobre.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end
end
