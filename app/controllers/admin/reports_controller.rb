# -*- encoding : utf-8 -*-
class Admin::ReportsController < Admin::AdminController
  def financial
    conditions = {:query => [], :params => []}

    conditions[:query] << '(faturas.status= ?)'
    conditions[:params] << Fatura::STATUS_APPROVED.to_s

    if !params[:filtro].blank?
      #filtro de curso
      if !params[:filtro][:curso].blank?
        conditions[:query].push("fatura_items.produto_id = ?")
        conditions[:params] << params[:filtro][:curso]
      end

      #filtro de período
      periodo_inicial = (!params[:filtro][:periodo_inicial].blank?) ? DateTime.strptime(params[:filtro][:periodo_inicial], '%d/%m/%Y') : nil
      periodo_final = (!params[:filtro][:periodo_final].blank?) ? DateTime.strptime(params[:filtro][:periodo_final], '%d/%m/%Y') : nil

      if !periodo_inicial.blank? && !periodo_final.blank?
        conditions[:query] << 'faturas.created_at >= ?'
        conditions[:params] << ((periodo_inicial <= periodo_final) ? periodo_inicial.to_datetime : periodo_final.to_datetime)
      elsif !periodo_inicial.blank?
        conditions[:query] << 'faturas.created_at >= ?'
        conditions[:params] << periodo_inicial.to_datetime
      end

      if !periodo_inicial.blank? && !periodo_final.blank?
        conditions[:query] << 'faturas.created_at <= ?'
        if periodo_final > periodo_inicial
          periodo_final_adiantado = (periodo_final.to_time + 23.hours + 59.minutes + 59.seconds).to_datetime
        else
          periodo_final_adiantado = (periodo_inicial.to_time + 23.hours + 59.minutes + 59.seconds).to_datetime
        end
        conditions[:params] << periodo_final_adiantado.to_datetime
      elsif !periodo_final.blank?
        conditions[:query] << 'faturas.created_at <= ?'
        conditions[:params] << periodo_final.to_datetime
      end
    end

    current_date = DateTime.now
    first_date = Date.new(current_date.year, current_date.month, 1)
    last_date = Date.new(current_date.year, current_date.month, 28)
    if (current_date.month != 12)
      last_date = Date.new(current_date.year, current_date.month+1, 1) - 1.day
    else
      last_date = Date.new(current_date.year, current_date.month, 31)
    end

    # se nenhum período foi passado como filtro, seleciona apenas o mês atual
    if ((periodo_inicial.nil? || periodo_inicial.blank?) &&
          (periodo_final.nil? || periodo_final.blank?))
      conditions[:query] << 'faturas.created_at >= ?'
      conditions[:params] << DateTime.new(current_date.year,current_date.month,1,0,0,0)
      conditions[:query] << 'faturas.created_at <= ?'

      if (current_date.month == 12)
        conditions[:params] << DateTime.new(current_date.year,current_date.month,31,23,59,59)
      else
        conditions[:params] << DateTime.new(current_date.year,current_date.month+1,1,23,59,59) - 1.day
      end
    else
      first_date = periodo_inicial.nil? ? first_date : periodo_inicial
      last_date = periodo_final.nil? ? last_date : periodo_final
    end

    @period_title = ""
    if (!first_date.nil?)
      @period_title << first_date.strftime('%d/%m/%Y')
    end

    if (!first_date.nil? && !last_date.nil?)
      @period_title << " - "
    end

    if (!last_date.nil?)
      @period_title << last_date.strftime('%d/%m/%Y')
    end

    # clonando as conditions uma para cada caso: VideoAulas, Pacotes e Assinaturas
    conditions_video_aulas =  Marshal.load(Marshal.dump(conditions))
    conditions_pacotes = Marshal.load(Marshal.dump(conditions))
    conditions_assinaturas = Marshal.load(Marshal.dump(conditions))
    conditions_simulados = Marshal.load(Marshal.dump(conditions))

    #=== Video Aulas ===
    conditions_video_aulas[:query] << "(produtos.type = 'VideoAula' OR produtos.type = 'VideoAulaAoVivo')"

    video_aulas_joins = 'INNER JOIN faturas ON faturas.id = fatura_items.fatura_id
                         INNER JOIN produtos ON produtos.id = fatura_items.produto_id'

    # filtro de professor foi passado?
    if !params[:filtro].blank? && !params[:filtro][:professor].blank?
      conditions_video_aulas[:query] << 'produtos_professors.professor_id = ?'
      conditions_video_aulas[:params] << params[:filtro][:professor]

      video_aulas_joins = 'INNER JOIN faturas ON faturas.id = fatura_items.fatura_id
                           INNER JOIN produtos ON produtos.id = fatura_items.produto_id
                           LEFT OUTER JOIN produtos_professors as produtos_professors ON fatura_items.produto_id = produtos_professors.produto_id'
    end

    conditions_video_aulas = [conditions_video_aulas[:query].join(" AND ")].concat(conditions_video_aulas[:params])

    @fatura_items_video_aulas = FaturaItem.find(:all,
      :select => 'produtos.id as produto_id, produtos.nome as nome,
                 SUM(fatura_items.quantity) as quantidade,
                 SUM(COALESCE(fatura_items.price - (fatura_items.price * (fatura_items.cupom_desconto / 100.0)), fatura_items.price)) as total,
                 produtos.investimento as investimento',
      :conditions => conditions_video_aulas,
      :joins => video_aulas_joins,
      :group => "produtos.id, produtos.nome, produtos.investimento",
      :order => "total DESC")

    #=== Simulados ===
    conditions_simulados[:query] << "produtos.type = 'SimulatedTest'"
    conditions_simulados = [conditions_simulados[:query].join(" AND ")].concat(conditions_simulados[:params])

    @fatura_items_simulados = FaturaItem.find(:all,
      :select => 'produtos.id as produto_id, produtos.nome as nome,
                  SUM(fatura_items.quantity) as quantidade,
                  SUM(COALESCE(fatura_items.price - (fatura_items.price * (fatura_items.cupom_desconto / 100.0)), fatura_items.price)) as total,
                  produtos.investimento as investimento',
      :conditions => conditions_simulados,
      :joins => 'INNER JOIN faturas ON faturas.id = fatura_items.fatura_id
                 INNER JOIN produtos ON produtos.id = fatura_items.produto_id',
      :group => "produtos.id, produtos.nome, produtos.investimento",
      :order => "total DESC")

    ids_produtos_professor = []
    if !params[:filtro].blank? && !params[:filtro][:professor].blank?
      #construindo array com os produtos que o professor filtrado possui participacao
      produtos_professor = ProdutosProfessor.find(:all,
        :select => "*",
        :conditions => "professor_id = '#{params[:filtro][:professor]}'")

      produtos_professor.each { |stp|
        ids_produtos_professor << stp.produto_id
      }

      indices_pacotes_remover = []
      @fatura_items_simulados.each_with_index { |fip,index|
        ids_produtos_incluidos = []
        fip.produto.products.each { |produto_incluido|
          ids_produtos_incluidos << produto_incluido.id
        }

        produtos_simulado_result = []
        produtos_simulado_result = ids_produtos_incluidos - ids_produtos_professor
        if (produtos_simulado_result.length == ids_produtos_incluidos.length)
          indices_pacotes_remover << index
        end
      }

      delta = 0
      indices_pacotes_remover.each { |ip|
        @fatura_items_simulados.delete_at(ip - delta)
        delta += 1
      }
    end

    #=== Pacotes ===
    conditions_pacotes[:query] << "produtos.type = 'Pacote'"
    conditions_pacotes = [conditions_pacotes[:query].join(" AND ")].concat(conditions_pacotes[:params])

    @fatura_items_pacotes = FaturaItem.find(:all,
      :select => 'produtos.id as produto_id, produtos.nome as nome,
                  SUM(fatura_items.quantity) as quantidade,
                  SUM(COALESCE(fatura_items.price - (fatura_items.price * (fatura_items.cupom_desconto / 100.0)), fatura_items.price)) as total,
                  produtos.investimento as investimento',
      :conditions => conditions_pacotes,
      :joins => 'INNER JOIN faturas ON faturas.id = fatura_items.fatura_id
                 INNER JOIN produtos ON produtos.id = fatura_items.produto_id',
      :group => "produtos.id, produtos.nome, produtos.investimento",
      :order => "total DESC")

    ids_produtos_professor = []
    if !params[:filtro].blank? && !params[:filtro][:professor].blank?
      #construindo array com os produtos que o professor filtrado possui participacao
      produtos_professor = ProdutosProfessor.find(:all,
        :select => "*",
        :conditions => "professor_id = '#{params[:filtro][:professor]}'")

      produtos_professor.each { |vap|
        ids_produtos_professor << vap.produto_id
      }

      indices_pacotes_remover = []
      @fatura_items_pacotes.each_with_index { |fip,index|
        ids_produtos_incluidos = []
        fip.produto.video_aulas.each { |produto_incluido|
          ids_produtos_incluidos << produto_incluido.id
        }

        produtos_pacote_result = []
        produtos_pacote_result = ids_produtos_incluidos - ids_produtos_professor
        if (produtos_pacote_result.length == ids_produtos_incluidos.length)
          indices_pacotes_remover << index
        end
      }

      delta = 0
      indices_pacotes_remover.each { |ip|
        @fatura_items_pacotes.delete_at(ip - delta)
        delta += 1
      }
    end

    #=== Assinaturas ===
    conditions_assinaturas[:query] << "produtos.type = 'Assinatura'"

    conditions_assinaturas = [conditions_assinaturas[:query].join(" AND ")].concat(conditions_assinaturas[:params])

    @fatura_items_assinaturas = FaturaItem.find(:all,
      :select => 'produtos.id as produto_id, produtos.nome as nome,
                  SUM(fatura_items.quantity) as quantidade,
                  SUM(COALESCE(fatura_items.price - (fatura_items.price * (fatura_items.cupom_desconto / 100.0)), fatura_items.price)) as total,
                  produtos.investimento as investimento',
      :conditions => conditions_assinaturas,
      :joins => [:fatura, :produto],
      :group => "produtos.id, produtos.nome, produtos.investimento",
      :order => "total DESC")

    if !params[:filtro].blank? && !params[:filtro][:professor].blank?
      #construindo array com os ids dos produtos ignorados pelas assinaturas
      indices_assinaturas_remover = []
      @fatura_items_assinaturas.each_with_index { |fia,index|
        # constroi um array com os ids de cada video aula ignorada
        ids_produtos_ignorados = []
        fia.produto.produtos.each { |produto_ignorado|
          if produto_ignorado.kind_of?(VideoAula) || produto_ignorado.kind_of?(VideoAulaAoVivo)
            ids_produtos_ignorados << produto_ignorado.id
          elsif produto_ignorado.kind_of?(Pacote)
            produto_ignorado.video_aulas.each { |pva|
              ids_produtos_ignorados << pva.id
            }
          end
        }

        produtos_result = []
        produtos_result = ids_produtos_professor - ids_produtos_ignorados
        if (produtos_result.empty?)
          indices_assinaturas_remover << index
        end
      }

      delta = 0
      indices_assinaturas_remover.each { |ia|
        @fatura_items_assinaturas.delete_at(ia - delta)
        delta += 1
      }
    end

    # calculando total geral
    @total = 0
    @qtd_total = 0
    @fatura_items_video_aulas.each { |fva| @total += fva.total.to_f; @qtd_total += fva.quantidade.to_i; }
    @fatura_items_pacotes.each { |fp| @total += fp.total.to_f; @qtd_total += fp.quantidade.to_i; }
    @fatura_items_assinaturas.each { |fa| @total += fa.total.to_f; @qtd_total += fa.quantidade.to_i; }

    # titulos para os diferentes layouts
    @nome_professor = "Todos"
    @nome_produto = "Todos"
#    @period_title = "Desde o início"
    if (!params[:filtro].blank?)
      if !params[:filtro][:professor].blank?
        @nome_professor = Professor.find(params[:filtro][:professor]).nome
      end

      if !params[:filtro][:curso].blank?
        @nome_produto = Produto.find(params[:filtro][:curso]).nome
      end
    end

    @print = false
    # layout de impressao
    if (!params[:print].blank? && params[:print])
      @print = true
      render :layout => 'backend_impressao'
    elsif (!params[:export].blank? && params[:export]) #layout de exportacao para csv

      faturas_professores = "Professor,#{@nome_professor}\n"
      faturas_professores += "Produto,#{@nome_produto}\n"
      faturas_professores += "Período,#{@period_title}\n"
      faturas_professores += "\nVídeo Aulas\n"

      faturas_professores += "Produto,Desconto,Qtde,Preço,Total\n"
      faturas_professores += @fatura_items_video_aulas.collect{|fiva|
        fiva.nome.gsub(","," ") + "," +
          exibir_desconto(fiva.quantidade,fiva.investimento,fiva.total) + "," +
          fiva.quantidade + "," +
          fiva.investimento + "," +
          fiva.total
      }.join("\n")

      faturas_professores += "\n\nPacotes\n"
      faturas_professores += "Produto,Desconto,Qtde,Preço,Total\n"
      faturas_professores += @fatura_items_pacotes.collect{|fipa|
        fipa.nome.gsub(","," ") + "," +
          exibir_desconto(fipa.quantidade,fipa.investimento,fipa.total) + "," +
          fipa.quantidade + "," +
          fipa.investimento + "," +
          fipa.total
      }.join("\n")

      faturas_professores += "\n\nAssinaturas\n"
      faturas_professores += "Produto,Desconto,Qtde,Preço,Total\n"
      faturas_professores += @fatura_items_assinaturas.collect{|fias|
        fias.nome.gsub(","," ") + "," +
          exibir_desconto(fias.quantidade,fias.investimento,fias.total) + "," +
          fias.quantidade + "," +
          fias.investimento + "," +
          fias.total
      }.join("\n")

      send_data faturas_professores,
        :type => 'text/plain',
        :filename => "relatorio_professores.csv"
    else # layout normal
      @professors = Professor.find(:all, :order => "nome ASC")
      @cursos = Produto.find(:all, :order => "nome ASC")

      respond_to do |format|
        format.html
      end
    end
  end

  private

  def exibir_desconto(quantidade, preco_produto, preco_final)
    desconto = 0.0

    if (preco_produto.to_f != 0)
      desconto = ((quantidade.to_f * preco_produto.to_f - preco_final.to_f) / (quantidade.to_f * preco_produto.to_f)) * 100
    end

    desconto.round(2).to_s
  end
end
