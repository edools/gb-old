class Admin::ProductsController < Admin::AdminController
  include ::AjaxSearchable
  create_ajax_search :ajax_search_watchable, :model => :produto, :scope => lambda { |model| model.assistiveis }
  create_ajax_search :ajax_search_without_signatures, :model => :produto, :scope => lambda { |model| model.nao_incluir_assinaturas }

  def download_attachment
    attachment = ProdutoAnexo.find params[:id]
    redirect_to attachment.arquivo.url
  end

  def update_attachment
    @attachment = ProdutoAnexo.find params[:id]
    @attachment.legenda = params[:value]
    @attachment.save
    render :layout => false
  end
end
