
class Admin::TicketsController < Admin::AdminController
  # GET /tickets
  # GET /tickets.xml
  def index
    @tickets = (params[:ordenar].blank?) ? Ticket.ordered.search_backend(params[:filtro]).paginate(:page => params[:page], :include => [:categoria]) : Ticket.search_backend(params[:filtro]).ordered(params[:ordenar]).paginate(:page => params[:page], :include => [:categoria])
    @ordenacao = {'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Investimento Ascendente' => 'investimento ASC', 'Investimento Descendente' => 'investimento DESC', 'Nome (A-Z)' => 'nome ASC', 'Nome (Z-A)' => 'nome DESC'}
    @total_por_ticket = Ticket.total_por_produto.ordered("produtos.nome ASC")
    @total = Ticket.count
    @total_comprados = @total_por_ticket.inject(0) {|sum, ticket| sum + ticket.total_comprado.to_i }
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @tickets }
    end
  end

  # GET /tickets/new
  # GET /tickets/new.xml
  def new
    @ticket = Ticket.new
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ticket }
    end
  end

  def edit
    @ticket = Ticket.find(params[:id])
    @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name
  end

  # POST /tickets
  # POST /tickets.xml
  def create
    @ticket = Ticket.new(params[:ticket])

    respond_to do |format|
      if @ticket.save
        flash[:notice] = 'Aula ao vivo adicionada com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_ticket_url : admin_tickets_url }
        format.xml  { render :xml => @ticket, :status => :created, :location => @ticket }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "new" }
        format.xml  { render :xml => @ticket.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /video_aulas/1
  # PUT /video_aulas/1.xml
  def update
    @ticket = Ticket.find(params[:id])

    respond_to do |format|
      if @ticket.update_attributes(params[:ticket])
        flash[:notice] = 'Aula ao vivo editada com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_ticket_url : admin_tickets_url }
        format.xml  { head :ok }
      else
        @categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml  { render :xml => @ticket.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.xml
  def destroy
    begin
      Ticket.destroy(params[:id])
      flash[:notice] = "Aula ao vivo removida com sucesso"
    rescue Exception => e
      flash[:error] = "Não foi possível excluir essa Vídeo Aula Ao Vivo."
    end

    respond_to do |format|
      format.html { redirect_to(admin_tickets_url) }
      format.xml  { head :ok }
    end
  end
end
