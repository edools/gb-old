class Admin::CategoriaCursosController < Admin::AdminController
  include Admin::CategoriaCursosAjax

  # GET /categoria_cursos
  def index
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /categoria_cursos/new
  # GET /categoria_cursos/new.xml
  def new
    @categoria_curso = CategoriaCurso.new
    @parent_categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @categoria_curso }
    end
  end

  # GET /categoria_cursos/1/edit
  def edit
    @categoria_curso = CategoriaCurso.find(params[:id])
    @parent_categorias = CategoriaCurso.available_ancestors_array_ordered_by_name(@categoria_curso.id)
  end

  # POST /categoria_cursos
  # POST /categoria_cursos.xml
  def create
    @categoria_curso = CategoriaCurso.new(params[:categoria_curso])

    respond_to do |format|
      if @categoria_curso.save
        flash[:notice] = 'Categoria adicionada com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_categoria_curso_url : admin_categoria_cursos_url }
        format.xml  { render :xml => @categoria_curso, :status => :created, :location => @categoria_curso }
      else
        @parent_categorias = CategoriaCurso.available_ancestors_array_ordered_by_name

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @categoria_curso.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /categoria_cursos/1
  # PUT /categoria_cursos/1.xml
  def update
    @categoria_curso = CategoriaCurso.find(params[:id])

    respond_to do |format|
      if @categoria_curso.update_attributes(params[:categoria_curso])
        flash[:notice] = 'Categoria editada com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_categoria_curso_url : admin_categoria_cursos_url }
        format.xml  { head :ok }
      else
        @parent_categorias = CategoriaCurso.available_ancestors_array_ordered_by_name(@categoria_curso.id)

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @categoria_curso.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /categoria_cursos/1
  # DELETE /categoria_cursos/1.xml
  def destroy
    respond_to do |format|
      begin
        @categoria_curso = CategoriaCurso.find(params[:id])
        @categoria_curso.destroy
        flash[:notice] = "Categoria removida com sucesso"
        format.html { redirect_to(admin_categoria_cursos_url) }
        format.xml  { head :ok }
      rescue
        @parent_categorias = CategoriaCurso.available_ancestors_array_ordered_by_name(@categoria_curso.id)
        flash[:error] = "Existe algum curso usando essa categoria. Remova-o antes de remover a categoria."
        format.html { render :action => "edit" }
        format.xml  { head :ok }
        flash.discard(:error)
      end
    end
  end

  # GET /categoria_cursos/destaques
  # POST /categoria_cursos/destaques
  def destaques
    if request.post?
      DestaqueProduto.process_list(params[:destaques])
      flash[:notice] = "Destaques salvos com sucesso"
    end

    @produtos = Produto.nao_incluir_assinaturas
    @destaques = Produto.destaques.ordered("destaque_produtos.position ASC")

  end

end
