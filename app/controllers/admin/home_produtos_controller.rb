class Admin::HomeProdutosController < Admin::AdminController
  def index
    @produtos_in_home = Produto.all_in_home
    @produtos_not_in_home = Produto.all_not_in_home
  end

  def save
  end
end
