class Admin::ParceirosController < Admin::AdminController
  # GET /parceiros
  # GET /parceiros.xml
  def index
    @parceiros = (params[:ordenar].blank?) ? Parceiro.paginate(:page => params[:page]) : Parceiro.ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Nome' => 'nome', 'Cadastrado em' => 'created_at'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @parceiros }
    end
  end

  # GET /parceiros/new
  # GET /parceiros/new.xml
  def new
    @parceiro = Parceiro.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @parceiro }
    end
  end

  # GET /parceiros/1/edit
  def edit
    @parceiro = Parceiro.find(params[:id])
  end

  # POST /parceiros
  # POST /parceiros.xml
  def create
    @parceiro = Parceiro.new(params[:parceiro])

    respond_to do |format|
      if @parceiro.save
        flash[:notice] = 'Parceiro adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_parceiro_url : admin_parceiros_url }
        format.xml  { render :xml => @parceiro, :status => :created, :location => @parceiro }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @parceiro.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /parceiros/1
  # PUT /parceiros/1.xml
  def update
    @parceiro = Parceiro.find(params[:id])

    respond_to do |format|
      if @parceiro.update_attributes(params[:parceiro])
        flash[:notice] = 'Parceiro editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_parceiro_url : admin_parceiros_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @parceiro.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /parceiros/1
  # DELETE /parceiros/1.xml
  def destroy
    begin
      Parceiro.destroy(params[:id])
      flash[:notice] = "Parceiro removido com sucesso"
    rescue
      flash[:error] = "Ocorreu um erro ao tentar remover o registro. Tente novamente."
    end

    respond_to do |format|
      format.html { redirect_to(admin_parceiros_url) }
      format.xml  { head :ok }
    end
  end

end
