class Admin::ProfessorsController < Admin::AdminController
  include ::AjaxSearchable
  create_ajax_search :ajax_search

  # GET /professors
  # GET /professors.xml
  def index
    @professors = (params[:ordenar].blank?) ? Professor.paginate(:page => params[:page]) : Professor.ordered(params[:ordenar]).paginate(:page => params[:page])
    @ordenacao = {'Nome' => 'nome', 'Cadastrado em' => 'created_at'}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @professors }
    end
  end

  # GET /professors/new
  # GET /professors/new.xml
  def new
    @professor = Professor.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @professor }
    end
  end

  # GET /professors/1/edit
  def edit
    @professor = Professor.find(params[:id])
  end

  # POST /professors
  # POST /professors.xml
  def create
    @professor = Professor.new(params[:professor])

    respond_to do |format|
      if @professor.save
        flash[:notice] = 'Professor adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_professor_url : admin_professors_url }
        format.xml  { render :xml => @professor, :status => :created, :location => @professor }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @professor.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # PUT /professors/1
  # PUT /professors/1.xml
  def update
    @professor = Professor.find(params[:id])

    respond_to do |format|
      if @professor.update_attributes(params[:professor])
        flash[:notice] = 'Professor editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_professor_url : admin_professors_url }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @professor.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /professors/1
  # DELETE /professors/1.xml
  def destroy
    begin
      Professor.destroy(params[:id])
      flash[:notice] = "Professor removido com sucesso"
    rescue
      flash[:error] = "Existe algum curso com esse professor. Remova a referência antes de remover o professor."
    end

    respond_to do |format|
      format.html { redirect_to(admin_professors_url) }
      format.xml  { head :ok }
    end
  end

end
