class Admin::CupomsController < Admin::AdminController
  before_filter :update_params, :only => [:create, :update]
  # GET /cupons
  # GET /cupons.xml
  @@mapa_ordenacao = {'Código (A-Z)' => 'codigo ASC', 'Código (Z-A)' => 'codigo DESC', 'Data de Criação (mais novos)' => 'created_at DESC', 'Data de Criação (mais antigos)' => 'created_at ASC', 'Válido Até (mais novos)' => 'validade DESC', 'Válido Até (mais antigos)' => 'validade ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'
  def index
    @cupoms = Cupom.ordered(@@mapa_ordenacao[params[:ordenar]]).search_backend(params[:filters]).paginate(:page => params[:page])
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Código (A-Z)', 'Código (Z-A)', 'Válido Até (mais novos)', 'Válido Até (mais antigos)']

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @cupoms }
    end
  end

  # GET /cupons/new
  # GET /cupons/new.xml
  def new
    @cupom = Cupom.new
    @produtos = Produto.ordered('nome ASC').find_all_by_type([VideoAula.to_s,Assinatura.to_s,Arquivo.to_s,Pacote.to_s,Ticket.to_s])

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cupom }
    end
  end

  # POST /cupons
  # POST /cupons.xml
  def create
    @cupom = Cupom.new(params[:cupom])

    respond_to do |format|
      if @cupom.save
        flash[:notice] = 'Cupom adicionado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_cupom_url : admin_cupoms_url }
        format.xml  { render :xml => @cupom, :status => :created, :location => @cupom }
      else
        @produtos = Produto.ordered('nome ASC').find_all_by_type([VideoAula.to_s,Assinatura.to_s,Arquivo.to_s,Pacote.to_s,Ticket.to_s])

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @cupom.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # GET /cupons/1/edit
  def edit
    @cupom = Cupom.find(params[:id])
    @produtos = Produto.ordered('nome ASC').find_all_by_type([VideoAula.to_s,Assinatura.to_s,Arquivo.to_s,Pacote.to_s,Ticket.to_s])
  end

  # PUT /cupons/1
  # PUT /cupons/1.xml
  def update
    @cupom = Cupom.find(params[:id])

    respond_to do |format|
      if @cupom.update_attributes(params[:cupom])
        flash[:notice] = 'Cupom editado com sucesso'
        format.html { redirect_to params.key?(:save_and_new) ? new_admin_cupom_url : admin_cupoms_url }
        format.xml  { head :ok }
      else
        @produtos = Produto.ordered('nome ASC').find_all_by_type([VideoAula.to_s,Assinatura.to_s,Arquivo.to_s,Pacote.to_s,Ticket.to_s])

        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'

        format.html { render :action => "edit" }
        format.xml  { render :xml => @cupom.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end

  # DELETE /cupons/1
  # DELETE /cupons/1.xml
  def destroy
    begin
      Cupom.destroy(params[:id])
      flash[:notice] = "Cupom removida com sucesso"
    rescue
      flash[:error] = "O cupom já foi utilizado e não pode ser removido."
    end

    respond_to do |format|
      format.html { redirect_to(admin_cupoms_url) }
      format.xml  { head :ok }
    end
  end

  # GET /cupons/gerar_codigo
  def generate_code
    respond_to do |format|
     format.json { render :json => {:code => CodeGenerator.generate_code }}
    end
  end

  private

  def update_params
    if params[:cupom] && !params[:cupom][:validade].blank?
      params[:cupom][:validade] = DateTime.strptime(params[:cupom][:validade], '%d/%m/%Y').to_date
    end
  end
end