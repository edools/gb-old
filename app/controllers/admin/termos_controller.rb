class Admin::TermosController < Admin::AdminController
  # GET /termos/edit
  def edit
    @termos = SitePageContent.find_or_create_by_name(SitePageContent::NAME_TERMOS.to_s)
  end

  # PUT /termos
  # PUT /termos.xml
  def update
    @termos = SitePageContent.find_by_name(SitePageContent::NAME_TERMOS.to_s)

    respond_to do |format|
      if @termos.update_attributes(params[:site_page_content])
        flash[:notice] = 'Termos atualizado com sucesso'
        format.html { redirect_to :action => "edit" }
        format.xml  { head :ok }
      else
        flash[:error] = 'Existe(m) erro(s) no formulário. Verifique os campos em destaque.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @termos.errors, :status => :unprocessable_entity }
        flash.discard(:error)
      end
    end
  end
end
