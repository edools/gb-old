class UserSimulatedTestsController < ApplicationController
	include UserSimulatedTestsHelper
  before_filter :login_required

  def index
		@simulated_test = SimulatedTest.find_or_create_by_id(1)
		if !permit_simulated_test?
			redirect_to home_url
			return
		end
		@user_simulated_test = UserSimulatedTest.find_or_initialize_by_user_id(current_user.id)
	end

	def create
		@user_simulated_test = UserSimulatedTest.new(params[:user_simulated_test])
		@user_simulated_test.user = current_user
		respond_to do |format|
			if @user_simulated_test.save
				flash[:notice] = 'Suas respostas foram enviadas com sucesso. Confira o gabarito das questões abaixo.'
				format.html { redirect_to user_simulated_tests_url }
			else
				@simulated_test = SimulatedTest.find_or_create_by_id(1)
				flash[:error] = 'Você ainda não respondeu todas as questões.'
				format.html { render :action => "index" }
				flash.discard(:error)
			end
		end
	end
end
