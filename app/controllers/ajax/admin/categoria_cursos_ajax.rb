module CategoriaCursosAjax
  def index_ajax
    @categoria_cursos = CategoriaCurso.ordered.roots
    respond_to do |format|
      format.json
    end
  end
end
