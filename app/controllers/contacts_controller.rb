class ContactsController < ApplicationController

  def index
    @contact = Contact.new params[:contact]
  end

  def create
    @contact = Contact.new params[:contact]

    respond_to do |format|
      if @contact.save
        begin
          UserMailer.deliver_contact(@contact)
          flash[:notice] = 'Mensagem enviada com sucesso.'
        rescue
          flash[:error] = 'Houve um problema no envio da sua mensagem.'
          @sending_error = true
        end
        format.html { redirect_to root_url }
        format.js
        format.xml  { render :xml => @contact, :status => :created, :location => @contact }
      else
        if @contact.errors.blank?
          flash[:error] = 'Houve um problema no envio da sua mensagem. Tente novamente.'
        else
          flash[:error] = 'Preencha todos os campos corretamente.'
        end
        format.html { render :action => "index" }
        format.js
        format.xml  { render :xml => @contact.errors, :status => :unprocessable_entity }
      end
    end
  end

end
