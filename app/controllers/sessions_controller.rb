# This controller handles the login/logout function of the site.
class SessionsController < ApplicationController
  auto_session_timeout_actions
  before_filter :login_required, :except => [:new, :create, :login_frontend, :active, :timeout]

  def new
    if current_user
      redirect_to home_path
    end
  end

  def create
    logout_keeping_session!
    user = User.authenticate(params[:login], params[:password])
    if user
      # Protects against session fixation attacks, causes request forgery
      # protection if user resubmits an earlier form using back
      # button. Uncomment if you understand the tradeoffs.
      # reset_session
      login user
      new_cookie_flag = (params[:remember_me] == "1")
      handle_remember_cookie! new_cookie_flag

      # se o usuario for administrador redireciona para o painel administrativo
      if user.has_role?(Role.administrador)
        redirect_back_or_default(admin_dashboard_path)
			elsif verify_existing_course(user)
				redirect_back_or_default(assistir_video_aula_path(@course))
			else
        redirect_back_or_default(home_path)
        flash[:notice] = "Acesso Autorizado! Utilize os links acima da Área do Aluno para ver seus cursos."
      end
    else
      note_failed_signin
      @login       = params[:login]
      @remember_me = params[:remember_me]
      render :action => 'new'
    end
  end

  def destroy
    # dependendo da role vai para a home ou para a pagina de login
    path = home_path
    if current_user && current_user.has_role?(Role.administrador)
        path = login_path
    end

    logout_keeping_session!
    flash[:notice] = "Você saiu do sistema."
    redirect_back_or_default(path)
  end

  def active
    render_session_status
  end
  
  def timeout
    flash[:notice] = "Sua sessão expirou. Tente efetuar o login novamente!"
    redirect_to "/login"
  end

protected
  # Track failed login attempts
  def note_failed_signin
    if params[:login].blank?
      flash[:error] = "O login não pode ser vazio"
    else
      flash[:error] = "Não foi possível entrar como '#{params[:login]}'"
    end
    logger.warn "[ERROR] Failed login for '#{params[:login]}' from #{request.remote_ip} at #{Time.now.utc}"
    flash.discard(:error)
	end

	def verify_existing_course(user)
		return false unless Settings.user.redirect_to_first_course
		@course = Produto.assistiveis.ativos.first
		@course.present? && @course.downloadable?(user)
	end
end
