class AssinaturasController < ApplicationController
  def index
    @assinaturas = Assinatura.ativos.disponivel_venda.ordered("investimento ASC").all(:include => :produtos)
    @nomes_cursos = Produto.assistiveis.ativos.disponivel_venda.ordered("nome ASC").collect(&:nome)
  end

  # adiciona uma assinatura na cesta
  def add_to_cart
    assinatura = Assinatura.ativos.disponivel_venda.find(params[:id])
    add_to_current_tiny_basket assinatura.id    
    redirect_to :controller => 'carrinho', :action => 'index'
  end

end
