include BannersHelper

class BannersController < ApplicationController

  def redirect
    # params[:hash] contem o id do banner embaralhado entre alguns dados aleatorios
    banner_id = decode_banner_hash(params[:hash]) if params[:hash]

    # obtem o banner
    banner = Banner.find(banner_id) if banner_id

    if banner
      # salva o log do clique
      banner.increment! :total_cliques
      BannerHistory.create(:banner => banner, :ip => request.remote_ip, :tipo => BannerHistory::CLIQUE)
      # redireciona para a url do banner
      redirect_to banner.url
    else
      render :inline => "URL ou Hash invalidos"
    end
  end

end