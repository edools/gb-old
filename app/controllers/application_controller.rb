# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
	helper UserSimulatedTestsHelper
  # Be sure to include AuthenticationSystem in Application Controller instead
  include AuthenticatedSystem
  # You can move this into a different controller, if you wish.  This module gives you the require_role helpers, and others.
  include RoleRequirementSystem
  # Automatic sesison timeout via javascript
  include AutoSessionTimeout

  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # define the base layout
  layout 'frontend'

   # Auto session timeout to avoid login sharing
  auto_session_timeout 6.hour

  # Scrub sensitive parameters from your log
  filter_parameter_logging :password

  before_filter :setup_total_basket_items

  # set the current_user variable
  def login(user)
    self.current_user = user
    self.current_user.update_login_information!
  end

  # mostra pagina de acesso negado
  def render_optional_error_file(status)
    logger.info "Catching exception on render_optional_error_file method: " + status.to_s + " (" + DateTime.now.to_s + ")"
    case status.to_s
    when "401", "unauthorized"
      page = "/401.html"
      code = 401
    when "500", "bad_request", "internal_server_error"
      page = "/500.html"
      code = 500
    when "404", "not_found"
      page = "/404.html"
      code = 404
    when "422"
      page = "/422.html"
      code = 422
    else
      page = "/500.html"
      code = 500
    end
    respond_to do |format|
      format.html { redirect_to page }
      format.json { render :status => code, :json => {}.to_json }
    end
  end

  # TODO: descrever esse metodo
  def clear_index_cache
    expire_action :action => :index
  end

  # verifica se o current_user é um administrador
  def is_admin?
    !current_user.nil? && current_user.is_admin?
  end

  def is_cliente?
    !current_user.nil? && current_user.is_cliente?
  end

  # retorna a cesta simplificada (onde estao somente ids de itens e quantidades) corrente
  def current_tiny_basket
    if current_user
        # somente usuario do tipo cliente possui cesta
        if is_cliente?

          # inicializa o array de cestas dos clientes que estao logados
          unless session[:clientes_baskets]
            session[:clientes_baskets] = {}
          end

          # inicializa a cesta do usuario logado
          # o identificador da cesta e' o id do usuario
          unless (session[:clientes_baskets].has_key? current_user.id) then
            session[:clientes_baskets][current_user.id] = {}
          end

          unless session[:clientes_baskets][current_user.id] then
            session[:clientes_baskets][current_user.id] = {}
          end

          unless session[:clientes_baskets][current_user.id][:products] then
            session[:clientes_baskets][current_user.id][:products] = {}
          end

          # se existe cesta de convidado entao faz o merge pra cesta do usuario logado e apaga a cesta de convidado
          if session[:guest_basket]

            # adiciona os items da cesta de convidado na cesta do usuario logado
            add_to_tiny_basket session[:clientes_baskets][current_user.id], session[:guest_basket][:products], true
            if !session[:guest_basket][:current_discount].blank?
              session[:clientes_baskets][current_user.id][:current_discount] = session[:guest_basket][:current_discount]
            end
            # apaga a cesta de convidado
            session[:guest_basket] = nil
          end

          current_discount_code = session[:clientes_baskets][current_user.id][:current_discount]
          if !current_discount_code.blank? and !Cupom.valido?(current_discount_code)
            flash.now[:error] = 'O código do cupom não é mais válido'
            session[:clientes_baskets][current_user.id].delete(:current_discount)
          end

          return session[:clientes_baskets][current_user.id]
        end
    else

      # inicializa cesta de convidado caso nao exista
      unless session[:guest_basket] then
        session[:guest_basket] = {}
      end

      unless session[:guest_basket][:products] then
        session[:guest_basket][:products] = {}
      end

      current_discount_code = session[:guest_basket][:current_discount]
      if !current_discount_code.blank? and !Cupom.valido?(current_discount_code)
        flash.now[:error] = 'O código do cupom não é mais válido'
        session[:guest_basket].delete(:current_discount)
      end
      return session[:guest_basket]

    end
  end

  def current_full_basket
    to_full_basket current_tiny_basket
  end

  # retorna a versao completa da cesta simplificada
  def to_full_basket(tiny_basket)
    if tiny_basket
      subtotal = 0 # total sem desconto
      total = 0 # total com desconto
      total_discount = 0
      cupom = Cupom.find_by_codigo(tiny_basket[:current_discount])
      cupom ||= Cupom.new # cria um cupom que nao vai aplicar nenhum desconto
      basket = {:items => {}, :total=> 0, :subtotal => 0, :current_discount => cupom.codigo, :total_discount => 0, :show_quantity => false}
      tiny_basket[:products].each do |item_id,qty|
        # evita que se o admin removeu uma video aula e esta ainda esteja no carrinho, o sistema exiba uma mesagem de erro pro usuario impedindo do cliente
        # visualizar o carrinho
        begin
          item = Produto.ativos.disponivel_venda.find(item_id)
          basket[:show_quantity] = basket[:show_quantity] || item.can_buy_multiple?
          subtotal += item.total(qty)
          current_discount = cupom.desconto_total(item,qty)
          total_discount += current_discount
          total += item.total_com_desconto(qty,current_discount)
          basket[:items][item_id] = {:item => item, :qty => qty}
        rescue
          tiny_basket[:products].delete item_id
        end
      end
      basket[:subtotal] = subtotal # total sem desconto
      basket[:total_discount] = total_discount
      basket[:total] = total # total com desconto
      return basket
    end
  end

  # deleta a cesta
  def destroy_tiny_basket
    if current_user
        # somente usuario do tipo cliente possui cesta
        if is_cliente? && session[:clientes_baskets] && session[:clientes_baskets].has_key?(current_user.id)
            session[:clientes_baskets][current_user.id] = nil
        end
    else
      if session[:guest_basket]
        session[:guest_basket] = nil
      end
    end
  end

  protected

  # adiciona um item na cesta corrente
  def add_to_current_tiny_basket(item_id, quantity = 1, override_quantity = true)
    add_to_tiny_basket current_tiny_basket, {item_id.to_s => quantity}, override_quantity
  end

  # adiciona items a cesta simplificada
  def add_to_tiny_basket(tiny_basket, items, override_qty = false)
    if items && tiny_basket && tiny_basket[:products]
      items.each do |item_id,qty|

        # inicializa a quantidade na cesta caso nao exista
        if !tiny_basket[:products].has_key?(item_id.to_i)
          tiny_basket[:products][item_id.to_i] = 0
        end

        tiny_basket[:products][item_id.to_i] = override_qty ? qty.to_i : tiny_basket[:products][item_id.to_i] + qty.to_i
        tiny_basket[:products][item_id.to_i] = 1 if tiny_basket[:products][item_id.to_i] <= 0
      end
    end
  end

  def current_tiny_basket_product_quantity(item_id)
    current_tiny_basket[:products][item_id.to_i]
  end

  # remove um produto da cesta
  def remove_product_current_tiny_basket(product_id)
    current_basket = current_tiny_basket

    if current_basket
      current_basket[:products].delete product_id
      if Cupom.desconto_vale_na_lista?(current_basket[:current_discount], [product_id])
        flash[:notice] = "O cupom #{current_basket[:current_discount]} tambem foi removido."
        delete_current_discount_code_from_current_tiny_basket
      end

    end
  end

  # checa se a cesta tem algum desconto pro cupom especificado
  def has_discount_to_current_tiny_basket(cupom)
    Cupom.desconto_vale_na_lista?(cupom, current_tiny_basket[:products].keys)
  end

  def save_discount_code_to_current_tiny_basket(cupom)
    current_tiny_basket[:current_discount] = cupom
  end

  def delete_current_discount_code_from_current_tiny_basket
    !current_tiny_basket.delete(:current_discount).blank? # retorna true se existe codigo de desconto
  end

  private

  # retorna o numero de itens na cesta
  def setup_total_basket_items
    @total = 0
    basket = current_tiny_basket
    if basket && basket[:products]
      basket[:products].each do |item_id,qty|
        @total += qty
      end
    end
    @total
  end
end
