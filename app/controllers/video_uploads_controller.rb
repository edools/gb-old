class VideoUploadsController < ApplicationController
  # GET video_uploads/download/:id
  def download
    video_upload = VideoUpload.find_by_id(params[:id])
    if video_upload.nil?
      render_optional_error_file(404)
    elsif !video_upload.downloadable?(current_user, params[:curso_id])
      render_optional_error_file(401)
    elsif !video_upload.streaming_exists?
      render_optional_error_file(500)
    else
      respond_to do |format|
        format.json do
          render :json => {:response => render_to_string(video_partial(video_upload)).html_safe}
        end
      end
    end
  end

  # GET video_uploads/download_file/:id
  def download_file
    product = Produto.find_by_id(params[:curso_id])
    video_upload = VideoUpload.find_by_id(params[:id])
    if product.nil? || video_upload.nil?
      render_optional_error_file(404)
    elsif !product.enable_download? || !video_upload.downloadable?(current_user, params[:curso_id])
      render_optional_error_file(401)
    elsif !video_upload.streaming_exists?
      render_optional_error_file(500)
    else
      redirect_to video_upload.streaming.url
    end
  end

  private

  def count_view(video)
    return unless Settings.user.manage_views
    return if video.tipo.is_demonstracao? || current_user.is_admin? || current_user.trial_period?

    counter = get_counter(video)
    counter.increment!
  end

  def extract_course_data(video)
    product = Produto.find(params[:curso_id])

    return {:video_class_id => product.id} unless product.is_a? Pacote

    video_class = product.video_aulas.find(params[:pacote_video_aula_id])
    {:video_class_id => video_class.id, :package_id => product.id}
  end

  def views_number_status(video)
    return [:ok, 0] unless Settings.user.manage_views
    return [:ok, 0] if video.tipo.is_demonstracao? || current_user.is_admin?

    counter = get_counter(video)
    status = counter.reach_view_limit? ? :block : :ok
    return [status, counter.view_limit_complete]
  end

  def get_counter(video)
    course_data = extract_course_data(video)
    UserVideoViewCounter.find_or_create_counter(course_data[:video_class_id], course_data[:package_id], video.id, current_user.id)
  end

  def video_partial(video)
    status, limit = views_number_status(video)
    if status == :block
      {:partial => "shared/watch_forbidden.html", :locals => {:limit => limit}}
    else
      count_view(video)
      {:partial => "shared/watch.html",
       :locals => {:url => video.streaming.url}}
    end
  end
end