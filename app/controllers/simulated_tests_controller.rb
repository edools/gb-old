class SimulatedTestsController < ApplicationController
  before_filter :login_required, :only => [:my_simulated_tests]
  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC',
                      'Nome (Z-A)' => 'nome DESC',
                      'Data de Criação (mais novos)' => 'created_at DESC',
                      'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'

  def index
    params.delete(:page) if params[:page] && params[:page].blank?
    @simulated_tests = SimulatedTest.search(params[:filtros]).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])

    @depoimento = Depoimento.find(:first, :order => "RANDOM()")
    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def show
    @simulated_test = SimulatedTest.ativos.disponivel_venda.find(params[:id])
    @categorias = CategoriaCurso.ordered.all
  end

  # adiciona um simulado na cesta
  def add_to_cart
    simulated_test = SimulatedTest.assistiveis.ativos.disponivel_venda.find(params[:id])
    add_to_current_tiny_basket simulated_test.id
    redirect_to :controller => 'carrinho', :action => 'index'
  end

  # lista todas os simulados compradas
  def my_simulated_tests
    params.delete(:page) if params[:page] && params[:page].blank?
    @simulated_tests = SimulatedTest.search_my_simulated_tests(params[:filtros], current_user).ordered(@@mapa_ordenacao[params[:ordenar]]).paginate(:page => params[:page])
    @user_simulated_tests = find_answered_simulated_tests(@simulated_tests)

    @categorias = CategoriaCurso.ordered.all
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def acessar
    @simulated_test = SimulatedTest.ativos.find(params[:id])

    if !@simulated_test.downloadable?(current_user)
      render_optional_error_file 404
      return
    end

    @simulated_test.expira_at(current_user)
    @user_simulated_test = UserSimulatedTest.find_or_initialize_by_user_id_and_simulated_test_id(current_user.id, @simulated_test.id)
    if @user_simulated_test.new_record?
      @simulated_test.questions.each do |simulated_test_question|
        @user_simulated_test.user_simulated_test_answers <<
          UserSimulatedTestAnswer.new(:simulated_test_question_id => simulated_test_question.id)
      end
    end
  end

  def refazer
    simulated_test = SimulatedTest.ativos.find(params[:id])

    if !simulated_test.downloadable?(current_user)
      render_optional_error_file 404
      return
    end

    user_simulated_test = UserSimulatedTest.find_by_simulated_test_id_and_user_id(simulated_test.id, current_user.id)
    user_simulated_test.destroy if user_simulated_test
    redirect_to :action => :acessar, :id => simulated_test.id
  end

  def responder
    @simulated_test = SimulatedTest.ativos.find(params[:id])

    if !@simulated_test.downloadable?(current_user)
      render_optional_error_file 404
      return
    end

    @user_simulated_test = UserSimulatedTest.new(params[:user_simulated_test])
    @user_simulated_test.user = current_user
    @user_simulated_test.simulated_test = @simulated_test

    respond_to do |format|
      if @user_simulated_test.save
        flash[:notice] = 'Suas respostas foram enviadas com sucesso. Confira o gabarito das questões abaixo.'
        format.html { redirect_to acessar_simulated_test_path(@simulated_test.id) }
      else
        flash[:error] = 'Você ainda não respondeu todas as questões obrigatórias.'
        format.html { render :action => "acessar" }
        flash.discard(:error)
      end
    end
  end

  def rss
    render_rss_feed(SimulatedTest.ordered.all(:limit => 10, :conditions => {:ativo => true}),
      { :feed => {:title => (@title || "Simulados") + " - #{Settings.site}",
          :url => url_for(:controller => 'simulated_tests', :action => 'show'),
          :item_url_prefix => '/'},
        :item => {:title => :nome,
          :description => :descricao,
          :pub_date => :created_at,
          :permalink => :id } })
  end

  private

  def find_answered_simulated_tests(simulated_tests)
    answered_simulated_tests_map = {}
    ids = simulated_tests.map(&:id)

    UserSimulatedTest.find_all_by_user_id_and_simulated_test_id(current_user.id, ids).each do |user_simulated_test|
      answered_simulated_tests_map[user_simulated_test.simulated_test.id] = user_simulated_test
    end
    answered_simulated_tests_map
  end
end
