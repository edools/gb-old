module UserSimulatedTestsHelper
	def answer_options
		["a", "b", "c", "d", "e"]
	end

	def user_answer?(user_simulated_test, question_number, option)
		user_simulated_test.send(:"answer_#{question_number}").to_s == option.to_s
	end

	def user_correct_questions_number(simulated_test, user_simulated_test)
		correct_count = 0
		1.upto(80) do |i|
			next unless simulated_test.send("answer_option_#{i}").to_s == user_simulated_test.send("answer_#{i}").to_s
			correct_count += 1
		end
		correct_count
	end

	def user_correct_questions_utilization(simulated_test, user_simulated_test)
		correct_questions = user_correct_questions_number(simulated_test, user_simulated_test)
		utilization = correct_questions / 80.0
		percentage = utilization * 100
		"#{number_with_precision(percentage, :precision => 2)}%"
	end

	def permit_simulated_test?
		course = Produto.find_by_id(538)
		simulated_test = SimulatedTest.find_or_create_by_id(1)
		course.present? && course.downloadable?(current_user) && simulated_test.enabled?
	end
end
