module HomeHelper
  def current_live_channel
    @live_channel ||= LiveChannel.singleton_instance
  end

  def carousel_image_size
    Settings.ticker_size
  end
end
