module Admin::DescontosHelper
  def desconto_cursos_inclusos(desconto)
    count = desconto.aplicar_todos_items ? Produto.nao_incluir_assinaturas.count : desconto.desconto_items.count
    return "Válido em #{pluralize(count, 'curso', 'cursos')}"
  end

  def desconto_assinaturas_inclusas(desconto)
    count = desconto.aplicar_todas_assinaturas ? Assinatura.count : desconto.assinaturas.count
    return "Válido em #{pluralize(count, 'assinatura', 'assinaturas')}"
  end
end
