module Admin::SimulatedTestsHelper
  def show_right_answers_count(simulated_test_question)
    UserSimulatedTestAnswer.count(:conditions => ["simulated_test_question_id=? AND answer=?", simulated_test_question.id, simulated_test_question.answer_option])
  end

  def show_wrong_answers_count(simulated_test_question)
    UserSimulatedTestAnswer.count(:conditions => ["simulated_test_question_id=? AND answer <> ?", simulated_test_question.id, simulated_test_question.answer_option])
  end

  def number_of_users_who_finalized_simulated_test(simulated_test)
    UserSimulatedTest.find_all_by_simulated_test_id(simulated_test.id).count
  end

  def number_of_sold_simulated_test(simulated_test)
    SimulatedTest.count_sold(simulated_test)
  end
end
