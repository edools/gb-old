module Admin::FaturasHelper
  def invoice_items_count(invoice)
    invoice_items = invoice.fatura_items || []
    invoice_items.sum('quantity')
  end
end
