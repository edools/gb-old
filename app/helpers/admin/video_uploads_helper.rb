module Admin::VideoUploadsHelper
    
  def options_with_blank(items, *options)
    return items if items.blank?
    options_from_collection_for_select([items.first.class.new].concat(items),*options)
  end
end
