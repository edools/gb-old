require 'base64'
module FacebookHelper
  def user_liked_page(request)
    return false if request.nil? || request['signed_request'].blank?
    signed_request = request['signed_request']
    encoded_sig, payload = signed_request.split('.', 2)
    encoded_data = payload.tr('-_', '+/')
    encoded_data += '=' while !(encoded_data.size % 4).zero?
    data = JSON.parse(Base64.decode64(encoded_data))
    data && data["page"] && data["page"]["liked"]
  end
end
