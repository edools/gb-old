module VideoAulasHelper
  def html_preco_video_aula(curso)
    preco_desconto = curso.investimento_com_desconto_ativo
    preco_normal = curso.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "<p>De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span><p>"
    else
      "<p><span class='preco'>#{preco_normal.reais_formatado}</span></p>"
    end
  end

  def html_preco_pacote(pacote)
    preco_desconto = pacote.investimento_com_desconto_ativo
    preco_normal = pacote.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span>"
    else
      "<span class='preco'>#{preco_normal.reais_formatado}</span>"
    end
  end

  def html_marta_preco_video_aula(curso)
    preco_desconto = curso.investimento_com_desconto_ativo
    preco_normal = curso.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "<p>De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <span class='preco_por'>#{preco_desconto.reais_formatado}</span><p>"
    else
      "<p><span class='preco'>#{preco_normal.reais_formatado}</span></p>"
    end
  end

  def html_marta_preco_pacote(pacote)
    preco_desconto = pacote.investimento_com_desconto_ativo
    preco_normal = pacote.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span>"
    else
      "<span class='preco'>#{preco_normal.reais_formatado}</span>"
    end
  end
end
