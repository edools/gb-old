require 'digest/sha1'
include ActionView::Helpers::DebugHelper

module BannersHelper

  def encode_banner_hash(banner_id)
    sha1 = Digest::SHA1.hexdigest('6b628068f76f1db6453925457ca5ecfc' + Time.now.utc.iso8601.gsub(/\W/, ''))
    sha1[0..9] + banner_id.to_s + sha1[10..-1]
  end

  def decode_banner_hash(hash)
    (hash.size < 41) ? -1 : hash[10..(10 + (hash.size - 40 - 1))]
  end

  def show_banner(banner, options = {})
    unless banner.blank?
      log = (options.key?(:log)) ? options[:log] : true
      if log
        banner.increment! :total_visualizacoes
        BannerHistory.create(:banner => banner, :ip => request.remote_ip, :tipo => BannerHistory::EXIBICAO)
      end

      # Banner.dimensoes traz primeiro a altura depois a largura em casa posicao
      dimensoes = Banner.dimensoes[banner.posicao].join("x")

      if banner.tipo == Banner::FLASH
        swf_tag banner.arquivo.url, :size => dimensoes
      elsif banner.tipo == Banner::IMAGEM
        if banner.url.blank?
          image_tag banner.arquivo.url, :size => dimensoes
        else
          link_to image_tag(banner.arquivo.url, :size => dimensoes), url_for_banner_redirect(banner), :target => (banner.target ? "_blank" : "_parent")
        end
      end
    end
  end

  def show_banner_by_position(posicao, options = {})
    if posicao && posicao > 0
      show_banner(Banner.exibiveis.find_by_posicao(posicao, :order => "RANDOM()"))
    end
  end

  def has_banner_to_show(posicao)
    return false unless posicao && posicao > 0
    return Banner.exibiveis.exists? ['posicao=?',posicao]
  end

  def url_for_banner_redirect(banner)
    base_url = redirect_url
    base_url + "?hash=" + encode_banner_hash(banner.id).to_s
  end

end
