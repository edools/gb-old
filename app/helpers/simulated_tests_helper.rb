module SimulatedTestsHelper
  def html_preco_simulated_test(simulated_test)
    preco_desconto = simulated_test.investimento_com_desconto_ativo
    preco_normal = simulated_test.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "<p>De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span><p>"
    else
      "<p><span class='preco'>#{preco_normal.reais_formatado}</span></p>"
    end
  end
end
