module AssinaturasHelper
  def cursos_disponiveis(assinatura)
    assinatura.produtos.disponivel_venda.collect { |produto| produto.nome}
  end

  def cursos_nao_disponiveis(nomes_cursos, assinatura)
    disponiveis = assinatura.produtos.disponivel_venda.collect { |produto| produto.nome}
    nomes_cursos - disponiveis
  end
  
end
