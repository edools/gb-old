module TicketsHelper
  def html_preco_ticket(ticket)
    preco_desconto = ticket.investimento_com_desconto_ativo
    preco_normal = ticket.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "<p>De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span><p>"
    else
      "<p><span class='preco'>#{preco_normal.reais_formatado}</span></p>"
    end
  end
end
