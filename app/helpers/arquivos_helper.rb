module ArquivosHelper
  def html_preco_arquivo(arquivo)
    preco_desconto = arquivo.investimento_com_desconto_ativo
    preco_normal = arquivo.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "<p>De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span><p>"
    else
      "<p><span class='preco'>#{preco_normal.reais_formatado}</span></p>"
    end
  end
end
