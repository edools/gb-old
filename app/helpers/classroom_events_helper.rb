module ClassroomEventsHelper
  def html_preco_classroom_event(classroom_event)
    preco_desconto = classroom_event.investimento_com_desconto_ativo
    preco_normal = classroom_event.investimento

    # significa que tem desconto
    if(preco_desconto != preco_normal)
      "<p>De <span class='preco_de'>#{preco_normal.reais_formatado}</span> por <br /> <span class='preco_por'>#{preco_desconto.reais_formatado}</span><p>"
    else
      "<p><span class='preco'>#{preco_normal.reais_formatado}</span></p>"
    end
  end
end