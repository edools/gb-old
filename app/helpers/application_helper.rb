include BannersHelper

# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  include TwitterPresenter
  include FacebookPresenter
  include AutoSessionTimeoutHelper

  def session_key
    if defined?(Rails.application)
      Rails.application.config.session_options[:key]
    else
      ActionController::Base.session_options[:key]
    end
  end

  def title(page_title)
    content_for(:title) { page_title }
  end

  # Define dinamicamente a tag <meta name="description"> do views/layout/application.html.erb
  def description(page_description)
    @description = "<meta name=\"description\" content=\"#{page_description}\" />\n"
  end

  # exibe um loading ao submeter um remote_form
  def remote_form_for_with_loading(object, *args, &block)
    options = args.extract_options!

    options[:after]    ||= "toggleLoading('p.botao input');"

    remote_form_for(object, options, &block)
  end

  # abre uma url usando a biblioteca tinybox
  def tinybox(object, url, width, height, time = "")
    link_to_function object, "TINY.box.show('#{url}', 1, #{width}, #{height}, 1#{((time.blank?) ? "" : ", " + time)})"
  end

  # verifies if a paperclip image or file is missing
  def missing?(object)
    begin
      !object.file?
    rescue
      true
    end
  end

  # torna a url mais limpa
  def clean_url(url)
    url.gsub(/(http|https):\/\//, '').gsub('www.', '').gsub(/\/$/, '')
  end

  # retornar a lista de meta tags para ser incluida no cabeçalho dos layouts
  def include_meta_tags(page_description)
    value  = "<meta http-equiv=\"content-language\" content=\"pt\" />\n"
    value += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
    value += "<meta name=\"google-site-verification\" content=\"9MZ6iUyvhlZ-9-3SUjxnF9WZIe8M8g0XXdtFV-sXwXE\" />\n"
    value += "<meta name=\"author\" content=\"#{t 'meta_tags.author'}\" />\n"
    value += "<meta name=\"reply-to\" content=\"contato@promine.com.br\" />\n"
    value += page_description ? page_description : description(Settings.site)
    value += "<meta name=\"keywords\" content=\"#{Settings.site}, #{Settings.meta_tags.keywords}\" />\n"
  end

  def icon_by_type(arquivo, method_name = :arquivo_content_type)
    type = case arquivo.instance.send(method_name)
    when /image/
      "image"
    when /pdf/
      "pdf"
    when /word/
      "doc"
    when /powerpoint/
      "ppt"
    else
      "file"
    end

    image_path("icons/anexos/" + type + ".png")
  end

  def show_player(options = {})
    options = {
      :video => '',
      :style => 'display:block;width:300px;height:300px',
      :splash => ''
    }.merge(options)

    options[:player] = {:video => options.delete(:video)}

    render :partial => 'shared/player', :locals => { :options => options }
  end

  def show_multiplayer(options = {})
    options = {
      :video => '',
      :style => 'display:block;width:300px;height:300px',
      :splash => ''
    }.merge(options)

    options[:player] = {:video => options.delete(:video)}

    render :partial => 'shared/multiplayer', :locals => { :options => options }
  end

  def options_for_player(url, options = {})
    auto_play = options[:auto_play].nil? ? true : options[:auto_play]
    public_video = options[:public_video].nil? ? false : options[:public_video]
    options = {:clip => {:autoPlay => auto_play, :key => Settings.flowplayer.key}}
    options[:key] = Settings.flowplayer.key

    if Rails.env.production?
      options[:clip][:url] = url.gsub('?','%3F').gsub('=','%3D').gsub('&','%26')
      if Settings.cloudfront.enable_rtmp && !public_video
        options[:clip][:provider] = 'rtmp'
        options[:clip][:autoBuffering] = true
        options[:clip][:bufferLength] = 10
        options[:clip][:netConnectionUrl] = "rtmp://#{Settings.cloudfront.streaming_distribution}/cfx/st"
        options[:plugins] = {:rtmp => {:url => '/swf/flowplayer.rtmp-3.2.12.swf'}}
      end
    else
      options[:clip][:url] = url
    end
    return options
  end

  def option_groups_from_produtos_for_select(produtos, selected_produto = nil)
    selected_value = selected_produto.id if selected_produto
    grouped_produtos = produtos.group_by { |produto| I18n.t("activerecord.models.#{produto.class.name.underscore}").capitalize }
    grouped_produtos.sort.map do |type, group|
      produtos_for_options = group.map { |produto| options_for_select({produto.nome => produto.id}, selected_value) }.join("")
      "<optgroup label='#{type}'>#{produtos_for_options}</optgroup>"
    end.join("")
  end

  def exibir_desconto(quantidade, preco_produto, preco_final)
    desconto = 0.0
    if (preco_produto.to_f != 0)
      desconto = ((quantidade.to_f * preco_produto.to_f - preco_final.to_f) / (quantidade.to_f * preco_produto.to_f)) * 100
    end
    "#{desconto.round(2)}%"
  end
end
