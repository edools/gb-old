class AttachmentWatermarkJob < Struct.new(:pdf_attachment_id, :user_id, :job_id)
  class << self
    def enqueue(pdf_attachment_id, user_id)
      existing_job = verify_existing_job(pdf_attachment_id, user_id)

      if existing_job.nil?
        job_id = generate_job_id
        delayed_job = Delayed::Job.enqueue AttachmentWatermarkJob.new(pdf_attachment_id, user_id, job_id)
        delayed_job.update_attributes({:polling_job_id => job_id, :custom_id => custom_id(pdf_attachment_id, user_id) })
        return job_id
      else
        existing_job.polling_job_id
      end
    end

    private

    def generate_job_id
      SecureRandom.hex(40)
    end

    def verify_existing_job(pdf_attachment_id, user_id)
      Delayed::Job.find_last_by_custom_id(custom_id(pdf_attachment_id, user_id))
    end

    def custom_id(pdf_attachment_id, user_id)
      "awj-#{pdf_attachment_id}-#{user_id}"
    end
  end

  def perform
    attachment = ProdutoAnexo.find(self.pdf_attachment_id)
    user = User.find(self.user_id)

    pdf = open(attachment_url(attachment))

    Dir.mktmpdir do |work_dir|
      watermarked_file_name = File.join(work_dir, attachment.arquivo.original_filename)
      watermarked_pdf = generate_watermarked_pdf(watermarked_file_name, pdf, user)
      persist_pdf(watermarked_pdf, user)
    end
  end

  private

  def persist_pdf(watermarked_pdf, user)
    UserWatermarkAttachment.create! :job_id => self.job_id, :user_id => user.id, :base_product_attachment_id => self.pdf_attachment_id, :watermarked_attachment => watermarked_pdf
  end

  def generate_watermarked_pdf(watermarked_file_name, pdf_file, user)
    watermarked_pdf_file = File.open(watermarked_file_name, "w+")
    stamp_file = generate_stamp(user)
    command pdf_file.path, stamp_file.path, watermarked_pdf_file.path
    watermarked_pdf_file
  end

  def command(input, stamp, output)
    `pdftk #{input} stamp #{stamp} output #{output}`
  end

  def generate_stamp(user)
    stamp_file = Tempfile.new "stamp.pdf"
    user_stamp = generate_watermark_info(user)
    Prawn::Document.generate(stamp_file.path) do
      create_stamp("user-info") do
        transparent(0.5) do
    	  fill_color "999999"
    	  text_box user_stamp,
    	    :size   => 30,
    	    :width  => bounds.width,
    	    :height => bounds.height,
    	    :align  => :center,
    	    :valign => :center,
    	    :at     => [0, bounds.height],
    	    :rotate => 45,
    	    :rotate_around => :center
        end
      end
      stamp("user-info")
    end
    stamp_file
  end

  def generate_watermark_info(user)
    text = []
    text << user.name if user.name
    text << user.cpf if user.cpf
    text.join("\n")
  end

  DEVELOPMENT_HOST = "http://localhost:3000"

  def attachment_url(attachment)
    url = attachment.arquivo.url
    return url if Rails.env.production?
    "#{DEVELOPMENT_HOST}#{url}"
  end
end