class Admin::ManageViewsIndexPresenter
  def products_filter
    @products_filter ||= Produto.ordered.find(:all, :conditions => ["type IN (?)", [VideoAula.to_s, Pacote.to_s]])
  end
end