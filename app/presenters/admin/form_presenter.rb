class Admin::FormPresenter
  class << self
    def select_field(form, name, options = {})
      SelectField.render(form, name, options)
    end

    def datepicker(form, name, options = {})
      DatepickerField.new(form, name, options).render
    end

    def datepicker_tag(name, value, options = {})
      options = {:value => value}.merge(options)
      DatepickerField.new(nil, name, options).render_tag
    end

    def datetimepicker(form, name, options = {})
      DatetimepickerField.new(form, name, options).render
    end

    def datetimepicker_tag(name, value, options = {})
      options = {:value => value}.merge(options)
      DatetimepickerField.new(nil, name, options).render_tag
    end

    def existing_attachment(form, name, view, options = {})
      ExistingAttachment.new(form, name, view, options).render
    end

    def new_attachment(form, name, view, options)
      NewAttachment.new(form, name, view, options).render
    end

    def title_with_validation(form, title, name, view)
      TitleWithValidation.new(form, title, name, view).render
    end
  end

  private

  class TitleWithValidation
    attr_accessor :form, :title, :input_name, :view

    def initialize(form, title, input_name, view)
      self.form = form
      self.title = title
      self.input_name = input_name
      self.view = view
    end

    def render
      if error_message
        instance_tag = InstanceTagWrapper.new(record, input_name)
        FieldErrorPresenter.new.field_error_for_default(title_tag, instance_tag)
      else
        title_tag
      end
    end

    private

    def record
      form.object
    end

    def error_message
      if record.respond_to?(:errors) && record.errors.respond_to?(:on)
        record.errors.on input_name
      end
    end

    def title_tag
      view.content_tag :h3, title
    end

    class InstanceTagWrapper < Struct.new(:object, :method_name)
    end
  end

  class NewAttachment
    attr_accessor :form, :record_name, :view, :options

    PARTIAL = 'admin/shared/new_attachment'
    PARTIAL_ITEM = 'admin/shared/new_attachment_item'
    DEFAULT_ATTACHMENT_CLASS = ProdutoAnexo
    DEFAULT_ATTACHMENT_NAME = :arquivo
    DEFAULT_LABEL = "Arquivo"

    def initialize(form, record_name, view, options = {})
      self.form = form
      self.record_name = record_name
      self.view = view
      self.options = options
    end

    def render
      view.render :partial => PARTIAL, :locals => {:presenter => self}
    end

    def partial_item
      PARTIAL_ITEM
    end

    def new_attachment_instance
      options[:attachment_class].nil? ? DEFAULT_ATTACHMENT_CLASS.new : options[:attachment_class].new
    end

    def attachment_name
      options[:attachment_name].nil? ? DEFAULT_ATTACHMENT_NAME : options[:attachment_name]
    end

    def title
      options[:title]
    end

    def has_title?
      !title.blank?
    end

    def label
      options[:label] || DEFAULT_LABEL
    end

    def add_more?
      return true if options[:add_more].nil?
      return options[:add_more]
    end
  end

  class ExistingAttachment
    attr_accessor :form, :record_name, :view, :options

    PARTIAL = 'admin/shared/existing_thumbnail_attachment'
    PARTIAL_GRID = 'admin/shared/existing_grid_attachment'

    def initialize(form, record_name, view, options = {})
      self.form = form
      self.record_name = record_name
      self.view = view
      self.options = options
      @item_index = 0
    end

    def render
      view.render :partial => partial, :locals => {:presenter => self} if render?
    end

    def title
      options[:title]
    end

    def readonly?
      options[:readonly] || false
    end

    def grid?
      options[:layout].eql?("grid")
    end

    def thumbnail?
      !grid?
    end

    def render_item(record_form = nil)
      @item_index = @item_index.next
      presenter_item = ExistingAttachmentItem.new self, record_form, @item_index
      presenter_item.render
    end

    private

    def partial
      grid? ? PARTIAL_GRID : PARTIAL
    end

    def record
      form.object
    end

    def render?
      !attachment_record.blank?
    end

    def attachment_record
      record.send(record_name)
    end
  end

  class ExistingAttachmentItem
    extend Forwardable
    attr_accessor :parent, :record_form
    def_delegators :parent, :view, :options, :readonly?, :grid?, :thumbnail?

    PARTIAL_ITEM = 'admin/shared/existing_thumbnail_attachment_item'
    PARTIAL_GRID_ITEM = 'admin/shared/existing_grid_attachment_item'
    DEFAULT_ATTACHMENT_NAME = :arquivo
    DEFAULT_LABEL_NAME = :legenda
    def initialize(parent, record_form, index)
      self.parent = parent
      self.record_form = record_form
      @index = index
    end

    def render
      remove_id if readonly?
      view.render :partial => partial, :locals => {:presenter_item => self, :index => @index} if render?
    end

    def link_to_download
      view.link_to download_image,
                   download_url,
                   :target => "_blank"
    end

    def label
      method_name = options[:label_method].nil? ? DEFAULT_LABEL_NAME : options[:label_method]
      record.send(method_name)
    end

    def checkbox_to_destroy
      options = {}
      options[:class] = 'remover-image-galeria' if thumbnail?
      record_form.check_box '_destroy', options
    end

    def edit_url
      view.send(options[:edit_url], :id => record)
    end

    def record
      record_form.object
    end

    private

    def partial
      options[:layout].eql?("grid") ? PARTIAL_GRID_ITEM : PARTIAL_ITEM
    end

    def attachment_name
      options[:attachment_name].nil? ? DEFAULT_ATTACHMENT_NAME : options[:attachment_name]
    end

    def remove_id
      record_form.instance_variable_set :@emitted_hidden_id, true
    end

    def download_url
      view.send(options[:download_url], :id => record)
    end

    def attachment_object
      record.send(attachment_name).instance
    end

    def attachment_file_name
      attachment_object.send("#{attachment_name}_file_name")
    end

    def attachment_content_type
      attachment_object.send("#{attachment_name}_content_type")
    end

    def render?
      !record.new_record?
    end

    def download_image
      path = view.image_path("icons/anexos/#{download_type}.png")
      view.image_tag(path,
          :alt => attachment_file_name,
          :title => attachment_file_name)
    end

    def download_type
      case attachment_content_type
      when /image/
        "image"
      when /pdf/
        "pdf"
      when /word/
        "doc"
      when /powerpoint/
        "ppt"
      else
        "file"
      end
    end
  end

  class DatepickerField
    include ActionController::UrlWriter
    include ActionView::Helpers::AssetTagHelper
    include ActionView::Helpers::TagHelper
    include ActionView::Helpers::FormTagHelper

    DEFAULT_ICON = "backend/calendar.gif"

    attr_accessor :form, :input_name, :options

    def initialize(form, input_name, options = {})
      self.form = form
      self.input_name = input_name
      self.options = {
        :class => field_class,
        :readonly => true
      }.merge(options)
    end

    def render
      output = form.text_field input_name, options
      output << image_tag(image_path(DEFAULT_ICON), :class => calendar_class)
      output
    end

    def render_tag
      value = options.delete :value
      output = text_field_tag(input_name, value, options)
      output << image_tag(image_path(DEFAULT_ICON), :class => calendar_class)
      output
    end

    private

    def calendar_class
      "datepicker-calendar"
    end

    def field_class
      "datepicker-field"
    end
  end

  class DatetimepickerField < DatepickerField
    def calendar_class
      "datetimepicker-calendar"
    end

    def field_class
      "datetimepicker-field"
    end
  end

  class SelectField
    class << self
      def render(form, name, options = {})
        options = {
          :remote => false,
          :allow_clear => false
        }.merge(options)

        options[:remote] ?
          render_hidden_field(form, name, options) :
          render_collection_select(form, name, options)
      end

      def render_hidden_field(form, name, options)
        field_options = {
          :class => "select2 ajax",
          :"data-searchable-url" => options[:url],
          :"data-searchable-field" => options[:field],
          :"data-allow-clear" => options[:allow_clear]
        }
        field_options[:"data-selected-item"] = to_json(options[:item], options[:field]) unless options[:item].nil?
        form.hidden_field name, field_options
      end

      def render_collection_select(form, name, options)
        select_options = {}
        select_options[:include_blank] = options[:include_blank]
        field_options = {
          :class => "select2",
          :"data-allow-clear" => options[:allow_clear]
        }
        form.collection_select name, options[:collection], :id, options[:field], select_options, field_options
      end

      def to_json(item, field)
        {:id => item.id, :text => item.send(field)}.to_json
      end
    end
  end
end
