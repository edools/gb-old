class Admin::ManageViewsCountPresenter
  attr_accessor :view_count

  def initialize(view_count)
    self.view_count = view_count
  end

  def description
    if view_count.package
      [view_count.video.nome, "#{view_count.video_class.nome} (Pacote: #{view_count.package.nome})"].join " - "
    else
      [view_count.video.nome, view_count.video_class.nome].join " - "
    end
  end

  def last_view_at
    view_count.updated_at.to_s(:padrao_data)
  end

  def views
    "#{view_count.counter}/#{view_limit}"
  end

  private

  def view_limit
    view_count.view_limit.eql?(-1) ? "indisponível" : view_count.view_limit
  end
end