class Admin::DiscountPresenter
  class << self
    def selected_signatures_ids(discount)
      discount.desconto_assinatura_items.map { |signature_item| signature_item.produto_id }
    end
  end
end