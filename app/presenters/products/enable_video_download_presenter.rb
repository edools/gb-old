class Products::EnableVideoDownloadPresenter
  class << self
    def enable?(video, user, product_id)
      product = Produto.find(product_id)
      product.enable_download? && video.downloadable?(user, product.id)
    end
  end
end