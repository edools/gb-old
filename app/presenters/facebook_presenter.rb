module FacebookPresenter
  FACEBOOK_LIKE_URL = "https://www.facebook.com/plugins/like.php"

  def facebook_url
    Settings.social.facebook
  end

  # like current page
  def facebook_button
    url = CGI::escape(request.url)
    content_tag :iframe, nil, :src => "#{FACEBOOK_LIKE_URL}?href=#{url}&layout=button_count&show_faces=true&width=450&action=like&font=arial&colorscheme=light&height=20", :scrolling => 'no', :frameborder => '0', :allowtransparency => true, :id => :facebook_like
  end
end