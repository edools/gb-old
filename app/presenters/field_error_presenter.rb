class FieldErrorPresenter
  include ActionController::UrlWriter
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::AssetTagHelper

  class << self
    def field_error_proc
      Proc.new do |html_tag, instance_tag|
        method = admin_namespace?(instance_tag) ? "default" : SiteTheme.name
        skip_tag?(html_tag) ? html_tag : FieldErrorPresenter.new.send("field_error_for_#{method}", html_tag, instance_tag)
      end
    end

    private

    def skip_tag?(html_tag)
      doc = Hpricot(html_tag)
      !(html_tag =~ /<(input|textarea|select)/) ||
			!doc.search('input[@type="hidden"]').empty? ||
			!doc.search('input[@type="radio"]').empty?
    end

    def admin_namespace?(instance_tag)
      instance_tag.instance_variable_get("@template_object").request.path_info =~ /^\/admin/
    end
  end

  def field_error_for_default(html_tag, instance_tag)
    error_content = content_tag :span, [error_message(instance_tag), '<span class="hint"></span>'].join, :class => "fieldErrorMessage"
    field_content = [html_tag, error_content].join
    content_tag :span, field_content, :class => "fieldWithErrors"
  end

  private

  def field_error_for_gustavo_brigido(html_tag, instance_tag)
    field_error_for_default html_tag, instance_tag
  end

  def field_error_for_plantao_enem(html_tag, instance_tag)
    field_error_for_default html_tag, instance_tag
  end

  def field_error_for_marta_garcia(html_tag, instance_tag)
    [html_tag, content_tag(:div, error_message(instance_tag), :class => "validation")].join("\n")
  end

  def error_message(instance_tag)
    (instance_tag.object.errors[instance_tag.method_name] =~ /^\^/) ?
         instance_tag.object.errors[instance_tag.method_name][1..-1] :
         instance_tag.object.class.human_attribute_name(instance_tag.method_name).capitalize + " " + (instance_tag.object.errors[instance_tag.method_name].is_a?(Array) ? instance_tag.object.errors[instance_tag.method_name].first : instance_tag.object.errors[instance_tag.method_name])
  end
end
