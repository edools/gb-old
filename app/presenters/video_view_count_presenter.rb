class VideoViewCountPresenter
  extend ActionView::Helpers::TextHelper
  class << self
    def remaining_user_views(user, video, video_class, package_id = nil)
      return "período grátis" if user.trial_period?
      return "ilimitado para administradores" if user.is_admin?

      counter = UserVideoViewCounter.find_or_create_counter(video_class.id, package_id, video.id, user.id)
      remaining_views = counter.view_limit_complete - counter.counter
      "#{remaining_views} de #{pluralize(counter.view_limit_complete, 'visualização disponível', 'visualizações disponíveis')}"
    end
  end
end