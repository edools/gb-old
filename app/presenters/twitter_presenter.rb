module TwitterPresenter
  TWITTER_URL = "https://twitter.com/"

  def twitter_url
    "#{TWITTER_URL}#{Settings.social.twitter}"
  end

  def twitter_button(options = {})
    options = {
      :via => Settings.social.twitter,
      :lang => "pt-BR"
    }.merge(options)

    <<-HTML
    <a href="https://twitter.com/share" class="twitter-share-button" data-via="#{options[:via]}" data-lang="#{options[:lang]}">Tweetar</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    HTML
  end
end