class HomeCell < ::Cell::Base
  cache :show_tickers, :expires_in => 10.minutes
  cache :show_news, :expires_in => 10.minutes
  cache :show_partners, :expires_in => 10.minutes
  cache :show_products, :expires_in => 10.minutes

  def show_tickers
    @categorias = [Ticker::ONLINE, Ticker::PRESENCIAL]
    render
  end

  def show_news
    @news = Noticia.ordered.find(:all, :limit => 3)
    render
  end

  def show_partners
    @partners = Parceiro.find(:all, :order => "RANDOM()", :limit => 3)
    render
  end

  def show_products
    render
  end
end
