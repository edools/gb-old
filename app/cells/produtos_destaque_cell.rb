class ProdutosDestaqueCell < ::Cell::Base
  cache :show, :expires_in => 10.minutes
  cache :show_plantao_enem, :expires_in => 10.minutes

  @@mapa_ordenacao = {'Nome (A-Z)' => 'nome ASC',
                      'Nome (Z-A)' => 'nome DESC',
                      'Data de Criação (mais novos)' => 'created_at DESC',
                      'Data de Criação (mais antigos)' => 'created_at ASC'}
  @@mapa_ordenacao.default = 'created_at DESC'

  def initialize(controller, args={})
    @categoria = args[:categoria]
    @only_assistiveis = args[:only_assistiveis] || false
    @show_sorting = args[:show_sorting] || false
    @featured = args[:featured] || []
    @sort = args[:sort] || nil
    @paginate_controller = args[:paginate_controller] || "video_aulas"
    @paginate_action = args[:paginate_action] || "paginate"
    super controller, args
  end

  def show
    common_show
    render
  end

  def show_plantao_enem
    common_show
    render
  end

  def show_produtos
    common_show_produtos
    render
  end

  def show_produtos_plantao_enem
    common_show_produtos
    render
  end

  def show_featured_products
    render
  end

  def show_featured_products_plantao_enem
    render
  end

  private
  def common_show
    @destaques = @only_assistiveis ?
      Produto.destaques.not_started.search({}).ordered(@@mapa_ordenacao[@sort]) :
      Produto.destaques.ativos.not_started.disponivel_venda.ordered("destaque_produtos.position ASC")
    @categorias = CategoriaCurso.ordered.roots
    @ordenacao = ['Data de Criação (mais novos)', 'Data de Criação (mais antigos)', 'Nome (A-Z)', 'Nome (Z-A)']
  end

  def common_show_produtos
    params.delete(:page) if params[:page] && params[:page].blank?
    page = params[:page] if @categoria.id.to_s.eql? params[:categoria_id]
    categoria_tree_ids = @categoria.subtree_ids
    @produtos = @only_assistiveis ?
      Produto.not_started.search({}).ordered(@@mapa_ordenacao[@sort]).scoped(:conditions => ["produtos.categoria_curso_id IN (?)", categoria_tree_ids]).paginate(:page => page) :
      Produto.ativos.not_started.disponivel_venda.ordered("created_at DESC").scoped(:conditions => ["produtos.categoria_curso_id IN (?)",categoria_tree_ids]).paginate(:page => page)
  end
end
