class AvailableProductSimulatedTestsForUserCell < ::Cell::Base
  cache :show_tickers, :expires_in => 10.minutes
  cache :show_news, :expires_in => 10.minutes
  cache :show_partners, :expires_in => 10.minutes
  cache :show_products, :expires_in => 10.minutes

  def initialize(controller, args={})
    product = args[:product]
    @simulated_tests = SimulatedTest.find_all_by_product(product)
    super controller, args
  end

  def show
    render
  end
end
