class AnalyticsCell < ::Cell::Base
  def show
    @account = Settings.analytics.account
    @domain = Settings.analytics.domain

    Rails.env.production? ? render : render(:nothing => true)
  end
end