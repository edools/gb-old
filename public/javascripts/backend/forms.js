(function($) {
  $.backend = $.backend || {};

  $.backend.forms = function() {
    var nestedForm = function() {
      var deleteRowHandler = function(event) {
        event.preventDefault();
        $(this).parents('.nested-form-row').remove();
      };

      var addRowHandler = function(event) {
        event.preventDefault();
        var el = $(this);
        var container = el.closest(".nested-form").find('.nested-form-rows');
        var template = $(el.data("template"));

        $.backend.select2(template.find(".select2"));
        template.find(".nested-form-delete-row").on("click", deleteRowHandler);
        template.appendTo(container);
        template.find(".select2").select2('open');
        template.find(".tinymce").each(function() {
          var d = new Date().getTime();
          var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
          });
          $(this).attr("id", uuid);
          jQuery.backend.tinymce.attachToElement(this);
        });

        /* Handler to differentiate between multiple choice or right/wrong questions
           for simulated tests */
        try {
          $(".st_question_type").on("change", function() {
            if($(this).val() === "multipla_escolha") {
              $(this).closest(".conteudo-questao").find(".st_answer_option").empty()
                .append('<option value=""></option>')
                .append('<option value="a">a</option>')
                .append('<option value="b">b</option>')
                .append('<option value="c">c</option>')
                .append('<option value="d">d</option>')
                .append('<option value="e">e</option>');
            } else {
              $(this).closest(".conteudo-questao").find(".st_answer_option").empty()
                .append('<option value=""></option>')
                .append('<option value="certo">certo</option>')
                .append('<option value="errado">errado</option>');
            }
          });
        } catch (ex){

        }
      };

      $(".nested-form-delete-row").on("click", deleteRowHandler);
      $(".nested-form-add-row").on("click", addRowHandler);
    };

    var existingThumbnailAttachment = function() {
      var selector = ".inplace-label-editor";
      var tooltip = 'Clique para editar...';
      var indicator = 'Atualizando...';
      var cancelLabel = 'Cancelar';
      var confirmLabel = 'Ok';
      var method = "PUT";
      var token = $('input[name=authenticity_token]').val();
      var setup = function() {
        var el = $(this);
        var url = el.data('in-place-edit-url');

        el.editable(url, {
          indicator : indicator,
          tooltip   : tooltip,
          cancel    : cancelLabel,
          submit    : confirmLabel,
          method    : method,
          submitdata: { authenticity_token: token }
        });
      };

      $(selector).each(setup);
    };

    var newAttachment = function() {
      var selector = ".form-new-attachment-add-row";
      var jsIndexRegex = /js-deferred-index/g;
      var countItems = 4;
      var addRowsHandler = function() {
        event.preventDefault();
        var el = $(this);
        cloneTemplate(el);
      };
      var cloneTemplate = function(addRowEl) {
        var container = addRowEl.siblings(".form-new-attachment-items");

        for(var row = 0; row < countItems; row++) {
          var template = addRowEl.data("template");
          var formattedTemplate = formatTemplate(template, container);
          $(formattedTemplate).appendTo(container);
        }
      };
      var formatTemplate = function(template, container) {
        var objectId = (-1 * container.children().length) -1;
        return template.replace(jsIndexRegex, objectId);
      };

      $(selector).on("click", addRowsHandler);
    };

    var nestedFormDragDrop = function() {
      $(".nested-form .nested-form-rows").sortable({
        items: '> .nested-form-row',
        handle: '.drag-drop-table-icon'
      });
    };

    var setup = function() {
      nestedForm();
      newAttachment();
      existingThumbnailAttachment();
      nestedFormDragDrop();
    };

    return {
      setup: setup
    };
  }();
})(jQuery);
