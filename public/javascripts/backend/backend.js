(function($) {
  $.backend = $.backend || {};
  $.backend.select2 = function(selector) {
    if (selector === undefined) {
      selector = ".select2";
    }
    var width = "60%";

    var setupForAjax = function() {
      var select = $(this);
      var url = select.data("searchable-url");
      var field = select.data("searchable-field");


      var options = {
        width: width,
        minimumInputLength: 1,
        ajax: ajaxSettings(url, field),
        initSelection: initSelectionCallback
      };

      var allowClear = select.data("allow-clear");
      if(allowClear === true) {
        options.allowClear = true;
        options.placeholder = "SELECIONE";
      }

      select.select2(options);
    };

    var ajaxSettings = function(url, field) {
      return {
        url: url,
        dataType: 'json',
        data: function(term, page) {
          var data = {};
          data["searchable_field_" + field] = term;
          return data;
        },
        results: function (data, page) {
          return { results: data };
        }
      };
    };

    var initSelectionCallback = function(element, callback) {
      var selectedItem = $(element).data("selected-item");
      if (selectedItem) {
        callback(selectedItem);
      }
    };

    var setup = function() {
      var select = $(this);
      if (select.hasClass("ajax")) {
        setupForAjax.call(select);
      } else {
        var allowClear = select.data("allow-clear");
        select.select2({ width: width, allowClear: allowClear });
      }
    };

    $(selector).each(setup);
  };

  var calendarI18n = {
    previousMonth : 'Mês Anterior',
    nextMonth     : 'Próximo Mês',
    months        : ['Janeiro','Fevereiro','Março',
                     'Abril','Maio','Junho','Julho',
                     'Agosto','Setembro','Outubro',
                     'Novembro','Dezembro'],
    weekdays      : ['Domingo','Segunda','Terça',
                     'Quarta','Quinta','Sexta','Sábado'],
    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
  };

  $.backend.datepicker = function() {
    var selectorField = ".datepicker-field";
    var selectorCalendar = ".datepicker-calendar";
    var dateFormat = "DD/MM/YYYY";

    var setup = function() {
      var field = $(this);
      field.pikaday({format: dateFormat, i18n: calendarI18n});
      var calendar = field.siblings(selectorCalendar);
      calendar.on("click", function() {
        field.pikaday('show');
      });
    };

    $(selectorField).each(setup);
  };

  $.backend.datetimepicker = function() {
    var selectorField = ".datetimepicker-field";
    var selectorCalendar = ".datetimepicker-calendar";
    var dateFormat = "DD/MM/YYYY HH:mm";

    var setup = function() {
      var field = $(this);
      field.pikaday({format: dateFormat, i18n: calendarI18n, showTime: true, use24hour: true});
      var calendar = field.siblings(selectorCalendar);
      calendar.on("click", function() {
        field.pikaday('show');
      });
    };

    $(selectorField).each(setup);
  };
})(jQuery);

jQuery(document).ready(function() {
  jQuery.backend.select2();
  jQuery.backend.datepicker();
  jQuery.backend.datetimepicker();
});
