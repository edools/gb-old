(function ($) {
    $.backend = $.backend || {};
    $.backend.tinymce = function () {
        var attachToElement = function (element) {
            $(element).tinymce({
                script_url: "/javascripts/tinymce/tinymce.min.js",
                theme_url: "/javascripts/tinymce/themes/modern/theme.min.js",
                content_css: "/stylesheets/tinymce/skins/lightgray/content.min.css",
                skin_url: "/stylesheets/tinymce/skins/lightgray",
                language: "pt_BR",
                plugins: "uploadimage anchor image link preview print searchreplace table wordcount advlist textcolor",
                toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link uploadimage | forecolor backcolor"
            });
        };

        var detachFromElement = function (element) {
            $(element).tinymce().remove();
        };

        var setup = function () {
            attachToElement("textarea.tinymce");
        };

        return {
            setup: setup,
            attachToElement: attachToElement,
            detachFromElement: detachFromElement
        };
    }();
})(jQuery);

jQuery(document).ready(function () {
    jQuery.backend.tinymce.setup();
});
