(function($) {
  $.backend = $.backend || {};
  $.backend.invoiceSummary = function() {
    var bindEvents = function() {
      $(".filter-product-type").on("change", function() {
        var el = $(this);
        var type = el.find(":selected").val();

        $.ajax({
          dataType: "json",
          url: "/admin/faturas/summary_change_product_type",
          data: { type: type }
        }).done(function(data) {
          $(".products-filter-container").html(data.partial);
        });
      });
    };

    var setup = function() {
      bindEvents();
    };

    return {
      setup: setup
    };
  }();
})(jQuery);