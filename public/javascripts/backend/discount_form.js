(function($) {
  $.backend = $.backend || {};
  $.backend.discountForm = function() {
    var updateItems = function() {
      var checked = $(".discount-checkbox-all-items").is(':checked');
      if(checked) {
        $(".discount-add-item, .discount-items").hide();
        $(".discount-items").empty();
      } else {
        $(".discount-add-item, .discount-items").show();
      }
    };

    var updateSignatures = function() {
      var checked = $(".discount-checkbox-all-signatures").is(":checked");

      if(checked) {
        $(".discount-signatures").hide();
        $(".discount-signatures input").attr("checked",false);
      } else {
        $(".discount-signatures").show();
      }
    };

    var bindEvents = function() {
      $(".discount-checkbox-all-items").on("click", updateItems);
      $(".discount-checkbox-all-signatures").on("click", updateSignatures);
    };

    var setup = function() {
      jQuery.backend.forms.setup();
      bindEvents();
      updateItems();
      updateSignatures();
    };

    return {
      setup: setup
    };
  }();
})(jQuery);