(function($) {
  $.backend = $.backend || {};
  $.backend.couponForm = function() {
    var updateCheckboxRules = function() {
      $('.coupon-checkbox-all').attr('checked', allRulesChecked());
      $('#cupom_produto_id').attr('disabled', someRuleIsChecked());
    };

    var updateCheckboxRulesForAll = function() {
      var checkbox = $(this);
      var checked = checkbox.is(':checked');
      $('.coupon-checkbox-item').attr('checked', checked);
      $('#cupom_produto_id').attr('disabled', checked);
    };

    var generateCodeCallback = function(event) {
      event.preventDefault();
      var el = $(this);
      var url = el.attr('href');
      $.ajax({
        dataType: "json",
        url: url
      }).done(function(data) {
        $(".coupon-code-field").val(data.code);
      });
    };

    var bindEvents = function() {
      $('.coupon-checkbox-item').on('click', updateCheckboxRules);
      $('.coupon-checkbox-all').on('click', updateCheckboxRulesForAll);
      $('.coupon-generate-code').on('click', generateCodeCallback);
    };

    var allRulesChecked = function() {
      return $('.coupon-checkbox-item:checked').length === $('.coupon-checkbox-item').length;
    };

    var someRuleIsChecked = function() {
      return $('.coupon-checkbox-item').is(':checked');
    };

    var setup = function() {
      bindEvents();
      updateCheckboxRules();
    };

    return {
      setup: setup
    };
  }();
})(jQuery);