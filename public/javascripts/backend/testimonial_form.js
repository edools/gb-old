(function($) {
  $.backend = $.backend || {};
  $.backend.testimonialForm = function() {
    var updateType = function() {
      show_attachment_video = $(".testimonial-type:checked").val() === "video";

      if(show_attachment_video) {
        $(".video-attachment").show();
      } else {
        $(".video-attachment").hide();
      }
    };

    var bindEvents = function() {
      $(".testimonial-type").on("change", updateType);
    };

    var setup = function() {
      bindEvents();
      updateType();
    };

    return {
      setup: setup
    };
  }();
})(jQuery);