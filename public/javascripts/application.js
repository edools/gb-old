function toggleLoading(element){
  if(typeof element == 'undefined') {
      element = '#ajax-loading';
  }
  loading = jQuery(element);
  if(loading) {
      if(loading.hasClass('enviando')) {
        jQuery(element).removeClass('enviando');
        jQuery(element).removeAttr('disabled');
      }
      else {
        jQuery(element).addClass('enviando');
        jQuery(element).attr('disabled', 'disabled');
      }
  }
}

jQuery(document).ready(function() {
    jQuery.facebox.settings.closeImage = '/images/facebox/closelabel.png';
    jQuery.facebox.settings.loadingImage = '/images/facebox/loading.gif';
    jQuery('a[rel*=facebox]').facebox();

  if(!Modernizr.input.placeholder) {
    jQuery('[placeholder]').focus(function() {
      var input = jQuery(this);
      if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
      }
    }).blur(function() {
      var input = jQuery(this);
      if (input.val() === '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
      }
    }).blur();
    jQuery('[placeholder]').parents('form').submit(function() {
      jQuery(this).find('[placeholder]').each(function() {
            var input = jQuery(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
      });
    });
  }

  jQuery('.ticker_next').hide();
  jQuery('.ticker_previous').hide();
  jQuery('.ticker').hover(function() {
		jQuery('.ticker_next, .ticker_previous').fadeIn('fast');
	}, function() {
		jQuery('.ticker_next, .ticker_previous').fadeOut('fast');
	});
  if(jQuery('.ticker_content') && jQuery('.ticker_content').length > 0) {
    jQuery('.ticker_content').cycle({
      fx:         'fade',
      speedIn:    '5000',
      speedOut:   '3000',
      timeout:    '9500',
      next:       '.ticker_next',
      prev:       '.ticker_previous'
    });
  }
});
