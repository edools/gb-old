(function ($) {
    $.productsWatch = $.productsWatch || {};

    var currentVideoEl = function (videoId) {
        return $(".lista_aulas_curso #li-video-" + videoId);
    };

    var videoIdData = function (el) {
        return $(el).data("video-id");
    }

    var previousButtonHandler = function () {
        var videoId = videoIdData(this);
        var previousVideoEl = currentVideoEl(videoId).prev();
        previousVideoEl.find("a").trigger("click");
    };

    var nextButtonHandler = function () {
        var videoId = videoIdData(this);
        var nextVideoEl = currentVideoEl(videoId).next();
        nextVideoEl.find("a").trigger("click");
    };

    var toggleElement = function (el, hide) {
        if (hide) {
            $(el).hide();
        } else {
            $(el).show();
        }
    }

    $.productsWatch.enableVideoControls = function () {
        $("body").on("click", ".previous-video-button", previousButtonHandler);
        $("body").on("click", ".next-video-button", nextButtonHandler);
    };

    $.productsWatch.toggleVideoControls = function () {
        $(".previous-video-button").livequery(function () {
            var videoId = videoIdData(this);
            var previousVideoEl = currentVideoEl(videoId).prev();
            toggleElement(this, previousVideoEl.length === 0);
        });

        $(".next-video-button").livequery(function () {
            var videoId = videoIdData(this);
            var nextVideoEl = currentVideoEl(videoId).next();
            toggleElement(this, nextVideoEl.length === 0);
        });
    };

    $.productsWatch.init = function () {
        $.productsWatch.enableVideoControls();
        $.productsWatch.toggleVideoControls();
    };

    $(document).ready(function () {
        $(".lista_aulas_curso a").click(function () {
            $(document).scrollTop($(".exibi_cursos").offset().top);
        });
        $.productsWatch.init();
    });
})(jQuery);