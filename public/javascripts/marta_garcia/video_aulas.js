(function($) {
  $.videoAulas = $.videoAulas || {};

  $.videoAulas.changeSortValueHander = function() {
    var sortValue = $(this).val();
    $.videoAulas.sortFeatured(sortValue);
  };

  $.videoAulas.enableTabs = function(defaultTab) {
    $("#categorias_tabs").easytabs({
      tabs: "div > div > div > div",
      tabActiveClass: "c1a_b",
      defaultTab: defaultTab,
      updateHash: false
    });
  };

  $.videoAulas.enableTabScroll = function() {
    $(".c1_abas").scrollable();
    var activeTab = $('.c1a_b');
    var scrollApi = $(".c1_abas").data('scrollable');
    scrollApi.seekTo(activeTab.index());
  };

  $.videoAulas.sortFeatured = function(sortValue) {
    $("#loader").show();
    $.ajax({
      url: $.videoAulas.sortProductsUrl,
      data: {sort: sortValue},
      success: function(response) {
        var selectedTab = $("#categorias_tabs .abas_scrollable_items .c1a_b a").attr("href");
        $("#categorias_tabs").replaceWith(response.data);
        $.videoAulas.enableTabs("div#tab_" + selectedTab.substr(1));
        $.videoAulas.enableTabScroll();
      },
      dataType: "json"
    });
  };

  $.videoAulas.enableAjaxPagination = function() {
    $("#categorias_tabs .pagination a").live("click", function(){
      $.ajax({
        url: this.href,
        success: function(response) {
          var selectedTab = $("#categorias_tabs .abas_scrollable_items .c1a_b a").attr("href");
          $(selectedTab).empty();
          $(selectedTab).append(response.data);
        },
        dataType: "json"
      });
      return false;
    });
  };

  $.videoAulas.init = function(sortProductsUrl) {
    $.videoAulas.sortProductsUrl = sortProductsUrl;
    $.videoAulas.enableTabs("div:first-child");
    $.videoAulas.enableAjaxPagination();
    $.videoAulas.enableTabScroll();
    $("#ordenar").live("change",$.videoAulas.changeSortValueHander);
  };
})(jQuery);
