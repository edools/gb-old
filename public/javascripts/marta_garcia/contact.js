(function($) {
  $.contact = $.contact || {};
  $.contact.afterRequest = function() {
    submit = $("#contactform button.btn");
    submit.attr('disabled', 'disabled');
    submit.text('Enviando...');
  };
})(jQuery);