(function ($) {
    $.classroom_event = function () {
        var setup = function () {
            $(document).bind('reveal.facebox', function() {
                $(".tickets-quantity").focus();
            });
        };

        return {
            setup: setup
        };
    }();
})(jQuery);
