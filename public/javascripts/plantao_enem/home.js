(function($) {
  $.home = $.home || {};
  $.home.enableTabs = function(defaultTab) {
    $("#categorias_tabs").easytabs({
      tabs: "div.c1_abas .abas_scrollable_items > div",
      defaultTab: defaultTab,
      tabActiveClass: "c1a_b",
      updateHash: false
    });
  };

  $.home.enableAjaxPagination = function() {
    $("#categorias_tabs .pagination a").live("click", function(){
      $.ajax({
        url: this.href,
        success: function(response) {
          var selectedTab = $("#categorias_tabs .abas_scrollable_items .c1a_b a").attr("href");
          $(selectedTab).empty();
          $(selectedTab).append(response.data);
        },
        dataType: "json"
      });
      return false;
    });
  };

  $.home.enableTabScroll = function() {
    $(".c1_abas").scrollable();
    var activeTab = $('.c1a_b');
    var scrollApi = $(".c1_abas").data('scrollable');
    scrollApi.seekTo(activeTab.index());
  };

  $.home.enableCarousel = function() {
    if ($('#carousel').length > 0) {
      $('#carousel').cycle({
        fx:         'fade',
        timeout:    '5000',
        pager:  '#carousel_navigation',
        pagerAnchorBuilder: function(idx, slide) {
          return '#carousel_navigation li:eq(' + idx + ') a';
        }
      });
    }
  };

  $.home.init = function() {
    $.home.enableTabs("div:first-child");
    $.home.enableAjaxPagination();
    $.home.enableTabScroll();
    $.home.enableCarousel();
  };
})(jQuery);

