(function($) {
  $.fileProductAttachment = $.fileProductAttachment || {};
  $.fileProductAttachment = function() {

    var checkJobStatus = function(jobId, attachmentId) {
      setTimeout(function() {
        params = {attachment_id: attachmentId, job_id: jobId};
        $.getJSON('/cursos-em-pdf/verify_watermark_job', params, function(data) {
          if(data.response.status === "processing") {
              checkJobStatus(jobId, attachmentId);
          } else {
            $.isLoading("hide");
            window.location.href = data.response.download_url;
          }
        });
      }, 2000);
    };

    var attachmentClickHandler = function() {
      var attachmentURL = $(this).attr("href");
      $.isLoading({ text: "Aguarde um momento..." });

      $.getJSON(attachmentURL, function(data) {
        if(data.response.download_url) {
          window.location.href = data.response.download_url;
        } else {
          checkJobStatus(data.response.job_id, data.response.attachment_id);
        }
      });

      return false;
    };

    var setup = function() {
      $(".product-file-attachment").on("click", attachmentClickHandler);
    };

    return {
      setup: setup
    };
  }();
})(jQuery);
