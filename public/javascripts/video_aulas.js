(function($) {
  $.videoAulas = $.videoAulas || {};

  $.videoAulas.changeSortValueHander = function() {
    var sortValue = $(this).val();
    $.videoAulas.sortFeatured(sortValue);
  };

  $.videoAulas.enableTabs = function(defaultTab) {
    $("#categorias_tabs").easytabs({
      tabActiveClass: "ativo",
      defaultTab: defaultTab,
      updateHash: false
    });
  };

  $.videoAulas.sortFeatured = function(sortValue) {
    $("#loader").show();
    $.ajax({
      url: $.videoAulas.sortProductsUrl,
      data: {sort: sortValue},
      success: function(response) {
        var selectedTab = $("#categorias_tabs ul li .ativo").attr("href");
        $("#categorias_tabs").replaceWith(response.data);
        $.videoAulas.enableTabs("li#tab_" + selectedTab.substr(1));
      },
      dataType: "json"
    });
  };

  $.videoAulas.enableAjaxPagination = function() {
    $("#categorias_tabs .pagination a").live("click", function(){
      $("#loader").show();
      $.ajax({
        url: this.href,
        success: function(response) {
          var selectedTab = $("#categorias_tabs ul li .ativo").attr("href");
          $(selectedTab).empty();
          $(selectedTab).append(response.data);
        },
        dataType: "json",
        complete: function() {
          $("#loader").hide();
        }
      });
      return false;
    });
  };

  $.videoAulas.init = function(sortProductsUrl) {
    $.videoAulas.sortProductsUrl = sortProductsUrl;
    $.videoAulas.enableTabs("li:first-child");
    $.videoAulas.enableAjaxPagination();
    $("#ordenar").live("change",$.videoAulas.changeSortValueHander);
  };
})(jQuery);
