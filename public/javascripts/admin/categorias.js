$(document).ready(function() {
  $("#categorias").dynatree({
    debugLevel: 0,
    fx: { height: "toggle", duration: 200 },
    initAjax: {
      url: $.adminSite.categorias.urls.index,
      data: $.extend({
      }, $.adminSite.categorias.ajaxParams)
    },
    onPostInit: function() {
      $("#categorias").dynatree("getRoot").visit(function(node) {
        node.expand(true);
      });
    },
    onClick: function(node, event) {
      if(node.getEventTargetType(event) == "title") {
        window.open(node.data.url, "_self");
        return false;
      }
    },
    strings: {
      loading: "Carregando...",
      loadError: "Erro ao carregar!"
    }
  });
});
