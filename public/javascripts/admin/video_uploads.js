$(document).ready(function() {
  var uploadSettings = function() {
    var filesOnQueue = 0;
    var lastUploadOnQueue = false;

    function onFileUploadComplete(allFilesUploadedHandler) {
      onFileRemoved();
      if(filesOnQueue === 0) {
        lastUploadOnQueue = true;
        alert("Upload concluído");
        allFilesUploadedHandler();
      }
    }

    function onFileQueued() {
      filesOnQueue++;
    }

    function onBeforeCreateQueueItem() {
      if(lastUploadOnQueue) {
        $("#queue .queue_item").remove();
        lastUploadOnQueue = false;
      }
    }

    function onFileRemoved() {
      if(filesOnQueue > 0) {
        filesOnQueue--;
      }
    }

    return {
      fileUploadComplete: onFileUploadComplete,
      fileQueued: onFileQueued,
      fileRemoved: onFileRemoved,
      beforeCreateQueueItem: onBeforeCreateQueueItem
    };

  }();

  function fileQueued(file) {
    uploadSettings.beforeCreateQueueItem();
    createQueueItem(file);
    uploadSettings.fileQueued();
    toggleQueue();
    toggleUpdateQueueControls();
    toggleUploadButton();
  }

  function createQueueItem(file) {
    var queueItem = $(".queue_item_template").clone().removeClass("queue_item_template");
    queueItem.attr("id",file.id);
    queueItem.find(".queue_item_name").val(file.name);
    //queueItem.find(".queue_item_hint").text(file.name);
    queueItem.find(".queue_item_percentage").progressbar();
    queueItem.find(".queue_item_progress_value").text("pendente");
    $("#queue").append(queueItem);
  }

  function uploadStart(file) {
    showQueueItemStartingStatus(file);
    appendQueueItemParams(file);
    return true;
  }

  function uploadProgress(file, bytesLoaded, bytesTotal) {
    showQueueItemUploadingStatus(file, bytesLoaded, bytesTotal);
  }

  function uploadSuccess(file, serverData) {
    showQueueItemSuccessStatus(file);
    return true;
  }

  function uploadError(file, errorCode, message) {
    if (errorCode === SWFUpload.UPLOAD_ERROR.FILE_CANCELLED) {
      return;
    }
    showQueueItemFailureStatus(file);
  }

  function uploadComplete(file) {
    uploadSettings.fileUploadComplete(allFilesUploadComplete);
  }

  function allFilesUploadComplete() {
    toggleQueue();
    toggleUploadButton();
    toggleAddVideosControl(true);
  }

  function appendQueueItemParams(file) {
    var queueItem = $("#queue #" + file.id);

    swfu.addFileParam(file.id,"video_upload[nome]", queueItem.find(".queue_item_name").val());
    swfu.addFileParam(file.id,"video_upload[categoria_curso_id]", queueItem.find(".queue_item_categorias").val());
    swfu.addFileParam(file.id,"video_upload[video_upload_tipo_id]", queueItem.find(".queue_item_tipos").val());
  }

  function showQueueItemStartingStatus(file) {
    var queueItem = $("#queue #" + file.id);
    queueItem.find(".queue_item_progress_value").text("iniciando...");
  }

  function showQueueItemUploadingStatus(file, bytesLoaded, bytesTotal) {
    var percentage = Math.ceil((bytesLoaded / bytesTotal) * 100);
    var queueItem = $("#queue #" + file.id);
    queueItem.find(".queue_item_percentage").progressbar("option", "value", percentage);
    var percentageLabel = percentage.toString() + " %";
    queueItem.find(".queue_item_progress_value").text(percentageLabel);
  }

  function showQueueItemSuccessStatus(file) {
    var queueItem = $("#queue #" + file.id);
    queueItem.find(".queue_item_progress_value").text("concluído");
  }

  function showQueueItemFailureStatus(file) {
    var queueItem = $("#queue #" + file.id);
    queueItem.find(".queue_item_progress_value").text("falhou");
  }

  function toggleQueue() {
    var show = $("#queue").children().length > 0;
    $("#queue").toggle(show);
  }

  function toggleUploadButton(enable) {
    if(enable === undefined) {
      enable = $("#queue").children().length > 0;
    }
    $("#enviar_videos").attr("disabled", !enable);
  }

  function toggleUpdateQueueControls(enable) {
    if(enable === undefined) {
      enable = $("#queue").children().length > 0;
    }
    $("#toggle_queue_items").attr("disabled", !enable);
    $("#categorias").attr("disabled", !enable);
    $("#tipos").attr("disabled", !enable);
    $("#update_queue_items").attr("disabled", !enable);

    resetUpdateQueueControls(!enable);
  }

  function resetUpdateQueueControls(reset) {
    if(reset) {
      $("#toggle_queue_items").attr("checked", false);
      $("#categorias").val("");
      $("#tipos").val("");
    }
  }

  function prepareQueueToUpload() {
    $("#queue .queue_item_checkbox").attr("checked", false);
    $("#queue .queue_item_checkbox").attr("disabled", true);
    $("#queue .queue_item_name").attr("disabled", true);
    $("#queue .queue_item_delete").attr("disabled", true);
    $("#queue .queue_item_categorias").attr("disabled", true);
    $("#queue .queue_item_tipos").attr("disabled", true);
    $("#queue .queue_item_delete").attr("disabled", true);
  }

  function toggleAddVideosControl(enable) {
    swfu.setButtonDisabled(!enable);
    $("#add_videos").attr("disabled", !enable);
    if(enable) {
      swfu.setButtonCursor(SWFUpload.CURSOR.ARROW);
    } else {
      swfu.setButtonCursor(SWFUpload.CURSOR.HAND);
    }
  }

  var swfSettings = {
    flash_url : "/swf/swfupload.swf",
    flash9_url : "/swf/swfupload_fp9.swf",
    upload_url: $.adminSite.videoUploads.uploadUrl,
    post_params: $.adminSite.videoUploads.swfPostParams,
    file_types : "*.flv",
    file_upload_limit : 0,
    file_queue_limit : 0,
    button_placeholder_id : "swf_placeholder",
    button_width: 61,
    button_height: 22,
    button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
    button_cursor: SWFUpload.CURSOR.HAND,
    file_queued_handler : fileQueued,
    upload_start_handler : uploadStart,
    upload_progress_handler : uploadProgress,
    upload_error_handler : uploadError,
    upload_success_handler : uploadSuccess,
    upload_complete_handler : uploadComplete,
    debug: false
  };

  var swfu = new SWFUpload(swfSettings);

  $("#queue").delegate(".queue_item_delete", "click", function() {
    var queueItem = $(this).closest(".queue_item").remove();
    swfu.cancelUpload(queueItem.attr("id"));
    uploadSettings.fileRemoved();
    toggleQueue();
    toggleUpdateQueueControls();
    toggleUploadButton();
  });

  $("#enviar_videos").click(function() {
    prepareQueueToUpload();
    toggleUploadButton(false);
    toggleAddVideosControl(false);
    toggleUpdateQueueControls(false);
    resetUpdateQueueControls(true);
    swfu.startUpload();
  });

  $("#update_queue_items").click(function() {
    var selectedCategoria = $("#categorias").val();
    var selectedQueueItems = $("#queue .queue_item_checkbox:checked");
    if(!$.isEmptyObject(selectedCategoria)) {
      selectedQueueItems.siblings(".queue_item_categorias").val(selectedCategoria);
    }

    var selectedTipo = $("#tipos").val();
    if(!$.isEmptyObject(selectedTipo)) {
      selectedQueueItems.siblings(".queue_item_tipos").val(selectedTipo);
    }
  });

  $("#toggle_queue_items").click(function() {
    var toggleValue = $("#toggle_queue_items").attr("checked");
    var queueItemsCheckboxes = $("#queue .queue_item_checkbox");
    queueItemsCheckboxes.attr("checked",toggleValue);
  });

  $("#queue").delegate(".queue_item_checkbox", "change", function() {
    hasAllCheckedItems = $("#queue .queue_item_checkbox:not(:checked)").length === 0;
    $("#toggle_queue_items").attr("checked",hasAllCheckedItems);
  });
});