(function($) {
  $.player = $.player || {};
  $.player = function() {
    var container = "#player-container";
    var splash = "#player-container img";

    var setup = function() {
      if($(splash).length) {
        splashSetup();
      }
      else {
        loadVideo();
      }
    };

    var splashSetup = function() {
      $(splash).on("click", function(){
        unbindSplash();
        loadVideo();
      });
    };

    var unbindSplash = function() {
      $(splash).unbind("click");
    };

    var loadVideo = function() {
      var options = $(container).data("player");
      $.getJSON(options.video, function(data) {
        $(container).html(data.response);
      }).error(function(data) {
        //window.location = "/" + data.status + ".html";
      });
    };

    var multiplayerLoadVideo = function(multiplayerContainer) {
      var options = $(multiplayerContainer).data("player");
      $.getJSON(options.video, function(data) {
        $(multiplayerContainer).html(data.response);
      }).error(function(data) {
        //window.location = "/" + data.status + ".html";
      });
    };

    var multiplayerSplashSetup = function(multiplayerContainer, multiplayerSplash) {
      $(multiplayerSplash).on("click", function(){
        unbindMultiplayerSplash(multiplayerSplash);
        multiplayerLoadVideo(multiplayerContainer);
      });
    };

    var unbindMultiplayerSplash = function(multiplayerSplash) {
      $(multiplayerSplash).unbind("click");
    };

    var multiplayerSetup = function() {
      $(".multiplayer-player").each(function() {
        var multiplayerSplash = $(this).find("img");
        if($(multiplayerSplash).length) {
          multiplayerSplashSetup(this, multiplayerSplash);
        }
        else {
          multiplayerLoadVideo(this);
        }
      });
    };

    return {
      setup: setup,
      multiplayerSetup: multiplayerSetup
    };
  }();
})(jQuery);
