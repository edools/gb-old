(function($) {
  $.home = $.home || {};

  $.home.enableTabs = function(defaultTab) {
    $("#categorias_tabs").easytabs({
      tabActiveClass: "ativo",
      defaultTab: defaultTab,
      updateHash: false
    });

    $(".lancamentos").easytabs({
      tabActiveClass: "ativo",
      defaultTab: defaultTab,
      updateHash: false
    });
  };

  $.home.enableAjaxPagination = function() {
    $("#categorias_tabs .pagination a").live("click", function(){
      $.ajax({
        url: this.href,
        success: function(response) {
          var selectedTab = $("#categorias_tabs ul li .ativo").attr("href");
          $(selectedTab).empty();
          $(selectedTab).append(response.data);
        },
        dataType: "json"
      });
      return false;
    });
  };

  $.home.init = function() {
    $.home.enableTabs("li:first-child");
    $.home.enableAjaxPagination();
  };
})(jQuery);
