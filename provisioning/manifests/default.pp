Exec {
  path    => ['/usr/bin','/usr/sbin', '/bin']
}

package { 'git':
  ensure => present
}

package { 'libxml2':
  ensure => latest
}

package { 'libxml2-dev':
  ensure => latest
}

package { 'libxslt1-dev':
  ensure => latest
}

package { 'curl':
  ensure => latest
}

package { 'bash':
  ensure => latest
}

package { 'gnupg':
  ensure => latest
}

package { 'imagemagick':
  ensure => latest
}

class { 'postgresql::server': }

class { 'postgresql::lib::devel': require => Class['postgresql::server'] }

class { 'postgresql::server::contrib':}

postgresql::server::role { 'gustavo_brigido':
  password_hash => postgresql_password('gustavo_brigido', 'gustavo_brigido'),
  superuser => true,
  createdb => true,
  createrole => true
}

postgresql::server::role { 'plantao_enem':
  password_hash => postgresql_password('plantao_enem', 'plantao_enem'),
  superuser => true,
  createdb => true,
  createrole => true
}

postgresql::server::role { 'marta_garcia':
  password_hash => postgresql_password('marta_garcia', 'marta_garcia'),
  superuser => true,
  createdb => true,
  createrole => true
}

postgresql::server::database { 'gustavo_brigido_development': }
postgresql::server::database { 'gustavo_brigido_test': }
postgresql::server::database { 'plantao_enem_development': }
postgresql::server::database { 'plantao_enem_test': }
postgresql::server::database { 'marta_garcia_development': }
postgresql::server::database { 'marta_garcia_test': }

postgresql::server::database_grant { 'gustavo_brigido_development':
  privilege => 'ALL',
  db        => 'gustavo_brigido_development',
  role      => 'gustavo_brigido',
  require => Postgresql::Server::Database['gustavo_brigido_development']
}

postgresql::server::database_grant { 'gustavo_brigido_test':
  privilege => 'ALL',
  db        => 'gustavo_brigido_test',
  role      => 'gustavo_brigido',
  require => Postgresql::Server::Database['gustavo_brigido_test']
}

postgresql::server::database_grant { 'plantao_enem_development':
  privilege => 'ALL',
  db        => 'plantao_enem_development',
  role      => 'plantao_enem',
  require => Postgresql::Server::Database['plantao_enem_development']
}

postgresql::server::database_grant { 'plantao_enem_test':
  privilege => 'ALL',
  db        => 'plantao_enem_test',
  role      => 'plantao_enem',
  require => Postgresql::Server::Database['plantao_enem_test']
}

postgresql::server::database_grant { 'marta_garcia_development':
  privilege => 'ALL',
  db        => 'marta_garcia_development',
  role      => 'marta_garcia',
  require => Postgresql::Server::Database['marta_garcia_development']
}

postgresql::server::database_grant { 'marta_garcia_test':
  privilege => 'ALL',
  db        => 'marta_garcia_test',
  role      => 'marta_garcia',
  require => Postgresql::Server::Database['marta_garcia_test']
}

$rvm_install_command = "su -c 'gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3 && curl -L https://get.rvm.io | bash -s stable' - vagrant"
exec { $rvm_install_command:
  creates => '/home/vagrant/.rvm/bin/rvm',
  require => [Package['curl'], Package['bash'], Package['gnupg']]
}

$rvm_ruby_install_command = "su -c 'rvm install ruby-1.8.7' - vagrant"
exec { $rvm_ruby_install_command:
  creates => '/home/vagrant/.rvm/rubies/ruby-1.8.7-head/bin/ruby',
  timeout => 36000, # takes too long... lets give it some time
  require => Exec[$rvm_install_command]
}

$downgrade_rubygems = "su -c 'rvm use 1.8.7-head; gem update --system 1.6.2' - vagrant"
exec { $downgrade_rubygems:
  unless => "su -c 'rvm use 1.8.7-head; gem -v | grep 1.6.2' - vagrant",
  require => Exec[$rvm_ruby_install_command]
}

file { '/home/vagrant/.gemrc':
  content => 'gem: --no-rdoc --no-ri',
  owner => 'vagrant',
  group => 'vagrant'
}

$create_gemset = "su -c 'rvm gemset create gustavo-brigido' - vagrant"
exec { $create_gemset:	
  unless => "su -c 'rvm use 1.8.7-head; rvm gemset list | grep gustavo-brigido'",
  require => Exec[$downgrade_rubygems]
}

$bundle_install = "su -c 'cd /vagrant; rvm use 1.8.7@gustavo-brigido; bundle install' - vagrant"
exec { $bundle_install:
  require => [Exec[$create_gemset], Package['libxml2'], Package['libxml2-dev'], Class['postgresql::lib::devel']]
}

exec { 'gustavo-brigido-db-migrate-dev':
  command => "su -c 'cd /vagrant; rvm use 1.8.7@gustavo-brigido; rake db:migrate' - vagrant",
  require => [Exec[$bundle_install], Postgresql::Server::Database_grant["gustavo_brigido_development"], Class['postgresql::server::contrib']]
}

exec { 'plantao-enem-db-migrate-dev':
  command => "su -c 'cd /vagrant; rvm use 1.8.7@gustavo-brigido; SITE_THEME=plantao_enem rake db:migrate' - vagrant",
  require => [Exec[$bundle_install], Postgresql::Server::Database_grant["plantao_enem_development"], Class['postgresql::server::contrib']]
}

exec { 'marta-garcia-db-migrate-dev':
  command => "su -c 'cd /vagrant; rvm use 1.8.7@gustavo-brigido; SITE_THEME=marta_garcia rake db:migrate' - vagrant",
  environment => ['SITE_THEME=marta_garcia','RAILS_ENV=development'],
  require => [Exec[$bundle_install], Postgresql::Server::Database_grant["marta_garcia_development"], Class['postgresql::server::contrib']]
}

class { locales:
  default_value  => 'en_US.UTF-8',
  available      => ['en_US.UTF-8']
}